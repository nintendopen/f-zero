		GLB		Rival_speed_data
		GLB		Zako_speed_data
		GLB		Rival_handle_data
		GLB		Zako_handle_12
		GLB		Zako_handle_34
		GLB		Rival_pase_data
		GLB		Crash_pase_data
		GLB		Set_distance_data
;=================================================================================
;		ORG		2F000H		;DATA_4 format
		ORG		2FB20H		;DATA_6 format
;=================================================================================
Rival_MAX_data	;	TABL	(NS) (US)
;*** LEVEL 1 ***
;		;Level1	 00
		BYTE		06EH,06EH
;		;Level1	 01
		BYTE		072H,072H
;		;Level1	 10
		BYTE		076H,076H
;		;Level1	 11
		BYTE		07AH,07AH
;*** LEVEL 2 ***
;		;Level2	 00
		BYTE		078H,07AH
;		;Level2	 01
		BYTE		078H,07CH
;		;Level2	 10
		BYTE		07CH,07EH
;		;Level2	 11
		BYTE		080H,082H
;*** LEVEL 3 ***
;		;Level3	 00
		BYTE		084H,084H
;		;Level3	 01
		BYTE		084H,086H
;		;Level3	 10
		BYTE		088H,08AH
;		;Level3	 11
		BYTE		08CH,08EH
;--------------------------------------------------------------------------
Rival_speed_data ;		(0) (1) (2)
		BYTE		54H,64H,6EH	  ;LV.1
		BYTE		56H,68H,74H	  ;LV.2
		BYTE		58H,6EH,7EH	  ;LV.3
Zako_speed_data	 ;
;Level_1			(0) (1) (2) (3)
		BYTE		54H,64H,6EH,6EH	  ;ZAKO (CAR_1)
		BYTE		50H,60H,68H,68H	  ;	(CAR_2)
		BYTE		50H,5CH,60H,64H	  ;	(CAR_3)
		BYTE		50H,58H,5CH,60H	  ;	(CAR_4)
;Level_2
		BYTE		56H,68H,76H,76H	  ;ZAKO (CAR_1)
		BYTE		56H,68H,72H,72H	  ;	(CAR_2)
		BYTE		52H,5EH,62H,66H	  ;	(CAR_3)
		BYTE		52H,5AH,5EH,62H	  ;	(CAR_4)
;Level_3
		BYTE		58H,6EH,82H,82H	  ;ZAKO (CAR_1)
		BYTE		58H,6EH,7EH,7EH	  ;	(CAR_2)
		BYTE		54H,60H,64H,68H	  ;	(CAR_3)
		BYTE		54H,5CH,60H,64H	  ;	(CAR_4)
;--------------------------------------------------------------------------
Rival_handle_data ;		(0) (1) (2) (3)
;LV_1
		BYTE		07H,0DH,15H,1DH	  ;LV.1
;LV_2
		BYTE		08H,0EH,16H,1EH	  ;LV.2
;LV_3
		BYTE		0AH,11H,18H,21H	  ;LV.3
Zako_handle_12	  ;		(0) (1) (2) (3)
;LV_1
		BYTE		07H,0DH,15H,1DH	  ;CAR1,2
;LV_2
		BYTE		08H,0EH,16H,1EH	  ;CAR1,2
;LV_3
		BYTE		0AH,12H,18H,1FH	  ;CAR1,2
Zako_handle_34	  ;		(0) (1) (2) (3)
;LV_1
		BYTE		07H,0BH,15H,1CH	  ;CAR3,4
;LV_2
		BYTE		07H,0DH,16H,1DH	  ;CAR3,4
;LV_3
		BYTE		08H,0EH,16H,1EH	  ;CAR3,4
;-----------------------------------------------------------------------------
Rival_pase_data
;********** WORLD_1 (KNIGHT) **********
;**** Level_1 (BIGINER) ****			 0:SLOW PACE
;Course_1		       (54321)=(LAP)	 1:FAST PACE
		BYTE		00000001B
;Course_2		       (54321)
		BYTE		00000101B
;Course_3		       (54321)	 ; W=1,L=1
		BYTE		00000001B
;Course_4		       (54321)
		BYTE		00000101B
;Course_5		       (54321)
		BYTE		00000101B
;**** Level_2 (STANDERD) ****			 0:SLOW PACE
;Course_1		       (54321)=(LAP)	 1:FAST PACE
		BYTE		00000101B
;Course_2		       (54321)
		BYTE		00000101B
;Course_3		       (54321)
		BYTE		00000101B	  ; W=1,L=2
;Course_4		       (54321)
		BYTE		00000011B
;Course_5		       (54321)
		BYTE		00001011B
;**** Level_3 (EXPERT) ****			 0:SLOW PACE
;Course_1		       (54321)=(LAP)	 1:FAST PACE
		BYTE		00000101B
;Course_2		       (54321)
		BYTE		00000101B
;Course_3		       (54321)	 ; W=1,L=3
		BYTE		00000101B
;Course_4		       (54321)
		BYTE		00001011B
;Course_5		       (54321)
		BYTE		00001011B
;********** WORLD_2 (QUEEN) ***********
;**** Level_1 (BIGINER) ****			 0:SLOW PACE
;Course_1		       (54321)=(LAP)	 1:FAST PACE
		BYTE		00000011B
;Course_2		       (54321)
		BYTE		00000011B
;Course_3		       (54321)	 ; W=2,L=1
		BYTE		00000011B
;Course_4		       (54321)
		BYTE		00000011B
;Course_5		       (54321)
		BYTE		00000011B
;**** Level_2 (STANDERD) ****			 0:SLOW PACE
;Course_1		       (54321)=(LAP)	 1:FAST PACE
		BYTE		00001011B
;Course_2		       (54321)
		BYTE		00000011B
;Course_3		       (54321)	 ; W=2,L=2
		BYTE		00000011B
;Course_4		       (54321)
		BYTE		00000011B
;Course_5		       (54321)
		BYTE		00001011B
;**** Level_3 (EXPERT) ****			 0:SLOW PACE
;Course_1		       (54321)=(LAP)	 1:FAST PACE
		BYTE		00001011B
;Course_2		       (54321)
		BYTE		00000011B
;Course_3		       (54321)	 ; W=2,L=3
		BYTE		00000011B
;Course_4		       (54321)
		BYTE		00001011B
;Course_5		       (54321)
		BYTE		00000111B
;********** WORLD_3 (KING) ***********
;**** Level_1 (BIGINER) ****			 0:SLOW PACE
;Course_1		       (54321)=(LAP)	 1:FAST PACE
		BYTE		00001011B
;Course_2		       (54321)
		BYTE		00001011B
;Course_3		       (54321)	 ; W=2,L=1
		BYTE		00001011B
;Course_4		       (54321)
		BYTE		00001011B
;Course_5		       (54321)
		BYTE		00001011B
;**** Level_2 (STANDERD) ****			 0:SLOW PACE
;Course_1		       (54321)=(LAP)	 1:FAST PACE
		BYTE		00001011B
;Course_2		       (54321)
		BYTE		00001011B
;Course_3		       (54321)	 ; W=3,L=2
		BYTE		00001011B
;Course_4		       (54321)
		BYTE		00001011B
;Course_5		       (54321)
		BYTE		00000111B
;**** Level_3 (EXPERT) ****			 0:SLOW PACE
;Course_1		       (54321)=(LAP)	 1:FAST PACE
		BYTE		00001011B
;Course_2		       (54321)
		BYTE		00001011B
;Course_3		       (54321)	 ; W=3,L=3
		BYTE		00001011B
;Course_4		       (54321)
		BYTE		00000111B
;Course_5		       (54321)
		BYTE		00000111B
;--------------------------------------------------------------------------
Set_distance_data
;level 1
		WORD		0200H,0400H,1900H,6400H
;level 2
		WORD		0100H,0200H,1000H,6400H
;level 3
		WORD		0100H,0200H,1000H,6400H
;--------------------------------------------------------------------------
Crash_pase_data
;******** WORLD_1 ********
;Level 1       (COURSE)		(1) (2) (3) (4) (5)
		BYTE		06H,06H,07H,05H,06H
;Level 2
		BYTE		04H,04H,05H,03H,04H
;Level 3
		BYTE		02H,02H,03H,01H,02H
;******** WORLD_2 ********
;Level 1       (COURSE)		(1) (2) (3) (4) (5)
		BYTE		05H,06H,05H,06H,06H
;Level 2
		BYTE		03H,05H,03H,05H,04H
;Level 3
		BYTE		02H,03H,02H,03H,02H
;******** WORLD_3 ********
;Level 1       (COURSE)		(1) (2) (3) (4) (5)
		BYTE		05H,04H,06H,04H,05H
;Level 2
		BYTE		03H,02H,04H,02H,03H
;Level 3
		BYTE		02H,02H,02H,00H,01H
