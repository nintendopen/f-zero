;************************************************************************
;*	 PLAYER		   -- player's car control module --		*
;************************************************************************
;
		INCLUDE	  RP5A22:
		INCLUDE	  RP5C77:
		INCLUDE	  VARIABLE
		INCLUDE	  BUFFER
		INCLUDE	  ALPHA
;
;=============== Cross reference =======================================
;
		EXT	  Clear_OAM,Print_OBJmsg,Mycar_spin_init,Enemy_spin_init
		EXT	  Reset_location,Camera_move,Display_roll,Display_retry
		EXT	  Clear_carcolor,Reset_roadcolor,Set_laser_color,Reset_carcolor
		EXT	  Pull_beam_start,Pull_beam_end,Pull_beam_move,Display_UFO
		EXT	  Handle_data,Dash_handle,Slide_data,Grip_limit,Repair_speed
		EXT	  Accele_datptr,Handle_datptr,Enemy_bomb_ctrl,Frash_rank_wind
		EXT	  Crash_damage,Graze_damage,Display_shield,Set_small_char
		EXT	  Title_demo_end,Select_sound,Calc_lap_time,Set_best_time
		EXT	  Copy_time_rank,Meter_off
;
;=============== Cross Definition ======================================
;
		GLB	  Player_initial,Player_control
		GLB	  Player_display
;
;=============== Define local variable =================================
;
work0		EQU	  0000H		      ; 2 byte	:Work
work1		EQU	  0002H		      ; 2 byte	:Work
BGstatus	EQU	  0004H		      ; 1 byte	:Player BG status
check_flag	EQU	  0005H		      ; 1 byte	:Enemy check flag
slide_angle	EQU	  0006H		      ; 2 byte	:slide angle
drift_angle	EQU	  0008H		      ; 1 byte	:drift angle
;-----------------------------------------------------------------------
car_crash	EQU	  0010H		      ; 1 byte	:Car crash flag temporaly
hit_car_pointer EQU	  0011H		      ; 2 byte	:Car hit pointer
hit_status	EQU	  0013H		      ; 1 byte	:Car hit status
hit_distance	EQU	  0014H		      ; 1 byte	:Car hit distance H
hit_spin_sign	EQU	  0015H		      ; 2 byte	:Car hit spin sign
angle_offset	EQU	  0017H		      ; 2 byte	:Car drift angle offset
old_control	EQU	  0019H		      ; 1 byte	:Old car control status
;
;=======================================================================
;
		PROG
		EXTEND
;
;///////////////////////////////////////////////////////////////////////
;/////////////// Player initialize routines ////////////////////////////
;************************************************************************
;*		 Initialize player work					*
;************************************************************************
;
		MEM8
		IDX8
Player_initial
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  #05H
		STA	  <car_wing	      ; Set Car wing pattern number
		STZ	  <wing_count
		LDA	  #03H
		STA	  <car_canopy	      ; Set Car canopy pattern number
		STZ	  !burner_count	      ; Clear after burner counter
		STZ	  !burner_inc_flag    ; Clear after burner increment flag
		STZ	  <wing_spark_cnt     ; Clear canopy display counter
		STZ	  <drift_status
		STZ	  !anime_car_index    ; Clear animation car pointer
		STZ	  <repair_counter     ; Clear shield repair counter
		STZ	  <repair_UFOwait     ; Clear shield repair UFO wait counter
		STZ	  !short_cut_flag
		STZ	  !turbo_ok_count
		STZ	  !non_turbo_cnt
		LDA	  #07H
		STA	  <shield_status
		LDA	  #16
		STA	  !clear_rank
		STZ	  !rank_warnning
		LDA	  <mycar_number
		ASL	  A
		STA	  <mycar_pointer
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; memory 16 bit mode
		LDA	  #0080H
		STA	  !car_position_h+0   ; Set player position h
		LDA	  #00BCH
		STA	  !car_position_v+0   ; Set player position v
;-----------------------------------------------------------------------
		STZ	  <handle_position    ; Clear handle position
		LDA	  #0100H
		STA	  <handle_velocity    ; Set handle velocity
		LDA	  #0800H
		STA	  <mycar_damage	      ; Set car damage
;
;=============== Set my car performance ================================
Player_init200
		MEM8
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		LDA	  <mycar_number
		TAX
		ASL	  A
		TAY
;-----------------------------------------------------------------------
		LDA	  !Accele_datptr,X
		STA	  !accele_perform
;
		LDA	  !Handle_datptr,X
		STA	  <handle_perform
;-----------------------------------------------------------------------
		PLP			      ; Restore mode
		RTS
;
;///////////////////////////////////////////////////////////////////////
;/////////////// Player control routines ///////////////////////////////
;************************************************************************
;*		 Player control entry					*
;************************************************************************
;
		MEM8
		IDX8
Player_control
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		JSR	  Shield_repair	      ; Shield repair
		LDA	  <exception_flag     ; exception ?
		BNE	  Player_exception    ; yes.
		LDA	  !short_cut_flag     ; Short cut ?
		BNE	  Player_ctrl200      ; yes.
;
;=============== player control ========================================
Player_ctrl100
		JSR	  Player_check	      ; Check enemy hit
		JSR	  Handle_control      ; Car handle control
		JSR	  Check_accele	      ; Check accele
		JSR	  Check_handle	      ; Set car max speed
		JSR	  Spin_start
		JSR	  Start_exception     ; Roket start exception
		JSR	  Check_reverse	      ; Check reverse run
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== player stop ===========================================
Player_ctrl200
		LDA	  #00010000B
		STA	  !car_control
		PLP
		RTS
;
;************************************************************************
;*		 Player control exception entry				*
;************************************************************************
;
		MEM8
		IDX8
Player_exception
		LDA	  <exception_flag
		BMI	  Player_except70
		ASL	  A
		BMI	  Player_except60
		ASL	  A
		BMI	  Player_except50
		ASL	  A
		BMI	  Player_except40
		ASL	  A
		BMI	  Player_except30
;----------------------------------------------------- Reserve ---------
Player_except20 PLP
		RTS
;----------------------------------------------------- Camera pan ------
Player_except30 JSR	  Camera_pan
		PLP
		RTS
;----------------------------------------------------- Game ending -----
Player_except40 JSR	  Game_ending
		PLP
		RTS
;----------------------------------------------------- Course clear ----
Player_except50 JSR	  Course_clear
		PLP
		RTS
;----------------------------------------------------- Player explode --
Player_except60 JSR	  Player_explode
		PLP
		RTS
;----------------------------------------------------- Game lost -------
Player_except70 JSR	  Game_lost
		PLP
		RTS
;
;************************************************************************
;*		 Player explode						*
;************************************************************************
;
		MEM8
		IDX8
Player_explode
		LDA	  #00100000B
		STA	  !car_control	      ; Set car control status
		STZ	  <turbo_status	      ; Clear turbo status
		LDA	  #11000000B
		STA	  !OAM_sub+0BH	      ; Clear wing spark
;
;=============== Check sequence ========================================
Explode100
		LDA	  <explosion_count
		CMP	  #48		      ; explode animation end ?
		BCS	  Explode400	      ; yes.
;-----------------------------------------------------------------------
Explode110	INC	  A		      ; Increment counter
		STA	  <explosion_count    ; Set explosion counter
;-----------------------------------------------------------------------
		CMP	  #7		      ; counter == 7 ?
		BEQ	  Explode200	      ; yes. ( camera set )
		CMP	  #9		      ; counter == 9 ?
		BEQ	  Explode150	      ; yes. ( set sound )
		CMP	  #19		      ; counter == 19 ?
		BEQ	  Explode300	      ; yes. ( white )
		CMP	  #21		      ; counter == 21 ?
		BEQ	  Explode310	      ; yes. ( yellow )
		CMP	  #23		      ; copunter == 23 ?
		BEQ	  Explode300	      ; yes. ( white )
		CMP	  #47		      ; counter == 47 ?
		BNE	  Explode200	      ; no.
;-----------------------------------------------------------------------
Explode120	LDA	  #255
		STA	  <initial_timer      ; Set timer
		STZ	  <screen_contrast
		LDA	  #00010000B
		STA	  <window_main	      ; left side blanking
		LDA	  #00000100B
		TSB	  <message_flag	      ; Display "YOU LOST"
;-----------------------------------------------------------------------
;;;;		DEC	  <retry_counter
;;;;		JSR	  Display_retry
		LDX	  #00H
		JSR	  Reset_carcolor
		BRA	  Explode200
;-----------------------------------------------------------------------
Explode150	LDX	  #10000000B
		STZ	  <sound_status_0     ; Set SOUND
		STX	  <sound_status_2     ; Set SOUND
		STX	  <sound_control      ; Set sound clear mode
;
;=============== Set camera ============================================
Explode200
		LDA	  #10000000B
		TRB	  !car_flag+0	      ; my car work disable
		LDA	  !car_angle+0
		STA	  !car_scrl_angle+0   ; Set scroll angle low
		LDA	  !car_angle+1
		STA	  !car_scrl_angle+1   ; Set scroll angle high
		LDA	  #02H
		STA	  !car_speed+1	      ; Set scroll speed high
		STZ	  !car_speed+0	      ; Set scroll speed low
		JSR	  Camera_move	      ; Camera move
		RTS
;
;=============== BG color flash ========================================
Explode300
		LDA	  #11111111B
		STA	  !flash_color	      ; Flash color = white
		LDA	  #11100000B
		STA	  !spot_color_B	      ; Clear my car shadow
		INC	  <colwte_flag
		INC	  <colwte_flag	      ; Flash frame = 2
		STZ	  <objwte_flag	      ; OBJ DMA normal mode
		RTS
;-----------------------------------------------------------------------
Explode310	LDA	  #00011111B
		STA	  !flash_color	      ; Flash color = yellow
		INC	  <colwte_flag
		INC	  <colwte_flag	      ; Flash frame = 2
		RTS
;
;=============== Game over =============================================
Explode400
		LDA	  <initial_timer      ; timer == 0 ?
		BEQ	  Explode500	      ; yes. ( game end )
		CMP	  #80		      ; timer >= 80 ?
		BCC	  Explode490	      ; no.
		CMP	  #128		      ; timer >= 128 ?
		BCC	  Explode450	      ; no. ( camera turn )
;-----------------------------------------------------------------------
		SEC
		LDA	  #255
		SBC	  <initial_timer
		LSR	  A
		LSR	  A
		LSR	  A
		STA	  <screen_contrast    ; Set screen contrast
		JSR	  Camera_move	      ; Camera move
		BRA	  Explode490
;-----------------------------------------------------------------------
Explode450	LDA	  !car_angle+1
		CLC
		ADC	  #2
		CMP	  #192
		BCC	  Explode460
		SBC	  #192
Explode460	STA	  !car_angle+1	      ; Set Camera angle
Explode490	RTS
;
;=============== Game end ==============================================
Explode500
		PHK
		JSR	  Copy_time_rank
		LDA	  #0FFH
		STA	  !over_rank
		LDA	  #002H
		STA	  !over_status
		LDA	  #06H
		STA	  <game_process	      ; Set exit check mode
		STZ	  <game_status
		RTS
;
;************************************************************************
;*		 Game lost						*
;************************************************************************
;
		MEM8
		IDX8
Game_lost
		LDA	  #10000000B
		STA	  <sound_control      ; Set explode sound
		LDA	  #00100000B
		STA	  !car_control	      ; Set car control status
;
;=============== Check sequence ========================================
Game_lost100
		LDA	  <initial_timer
		BEQ	  Game_lost130	      ; if ( timer == 0 )
		CMP	  #128
		BCC	  Game_lost120
;-----------------------------------------------------------------------
Game_lost110	STZ	  <sound_status_0     ; Set SOUND
		RTS
;---------------------------------------------------- Screen fade out --
Game_lost120	LSR	  A
		LSR	  A
		LSR	  A
		STA	  <screen_contrast
		LDA	  #03H
		STA	  <sound_status_0     ; Set LOST music
		RTS
;---------------------------------------------------- Screen blanking --
Game_lost130	LDA	  #10000000B
		STA	  <screen_contrast    ; Screen blanking
		JSR	  Meter_off	      ; ( screen display )
		LDA	  #10000000B
		STA	  <screen_contrast    ; Screen blanking
		LDX	  #00H
		JSR	  Reset_carcolor
		PHK
		JSR	  Copy_time_rank
;----------------------------------------------------- Screen fade in --
;;;;		DEC	  <retry_counter
		LDA	  #07H
		STA	  <game_process	      ; Set YOU LOST mode
		STZ	  <game_status
		RTS
;
;************************************************************************
;*		 Course clear						*
;************************************************************************
;
		MEM8
		IDX8
Course_clear
		LDA	  <demo_flag	      ; title demo ?
		BNE	  Course_clear10      ; yes.
;-----------------------------------------------------------------------
		STZ	  <sound_status_0     ; Clear music
		LDA	  <initial_timer
		CMP	  #177
		BCS	  Course_clear10
		LDA	  #00001000B
		STA	  <sound_status_0
;-----------------------------------------------------------------------
Course_clear10	STZ	  <sound_status_1
		LDA	  #11000000B
		STA	  !OAM_sub+0BH	      ; Clear wing spark
;-----------------------------------------------------------------------
		LDY	  !anime_car_index    ; IY = anime car pointer
		LDA	  <play_mode	      ; free run mode ?
		BNE	  Course_clear100     ; yes.
;-----------------------------------------------------------------------
		LDA	  !course_clr_flag
		DEC	  A		      ; camera demo ?
		BEQ	  Course_clear200     ; yes.
;
;=============== Free run mode and rank 2,3 ============================
Course_clear100
		LDA	  <initial_timer      ; timer == 0 ?
		BEQ	  Course_clear150     ; yes.
;-----------------------------------------------------------------------
Course_clear110 CMP	  #170		      ; timer == 170 ?
		BNE	  Course_clear120     ; no.
		LDA	  #00000010B
		TRB	  <exception_flag     ; Reset camera chase flag
Course_clear120 LDA	  !course_clr_flag    ; free mode ?
		BNE	  Course_clear130     ; no.
		LDA	  <demo_flag	      ; demo mode ?
		BNE	  Course_clear140     ; yes.
;-----------------------------------------------------------------------
		LDA	  !goal_in_rank
		ASL	  A
		TSB	  <message_flag	      ; Display message
		RTS
;-----------------------------------------------------------------------
Course_clear130 JSR	  Frash_rank_wind
Course_clear140 RTS
;--------------------------------------------------- End goal in demo --
Course_clear150 LDA	  !course_clr_flag    ; free mode ?
		BNE	  Course_clear160     ; no.
		LDA	  <demo_flag	      ; demo mode ?
		BNE	  Course_clear170     ; yes.
		LDA	  #04H
		STA	  <end_status	      ; Set course clear
		RTS
;-----------------------------------------------------------------------
Course_clear160 LDA	  #90
		STA	  <initial_timer      ; Set timer
		LDA	  #00001000B
		STA	  <exception_flag     ; Set camera pan mode
		RTS
;-----------------------------------------------------------------------
Course_clear170 PHP
		JMP	  Title_demo_end
;
;=============== Camera angle control ( battle mode ) ==================
Course_clear200
		MEM8
		JSR	  Frash_rank_wind
		LDA	  <initial_timer      ; timer == 0 ?
		BEQ	  Course_clear400     ; yes.
;-----------------------------------------------------------------------
		LDA	  !car_angle+1	      ; camera_angle == 0 deg  ?
		BEQ	  Course_clear220     ; yes.
		CMP	  #96		      ; camera angle >= 180deg ?
		BCS	  Course_clear210     ; yes.
		DEC	  !car_angle+1	      ; decrement angle
		BEQ	  Course_clear220     ; if ( angle == 0 )
		BRA	  Course_clear300
;-----------------------------------------------------------------------
Course_clear210 INC	  !car_angle+1	      ; increment angle
		LDA	  !car_angle+1
		CMP	  #192		      ; agle == 360deg ?
		BNE	  Course_clear300     ; no.
		STZ	  !car_angle+1
;-----------------------------------------------------------------------
Course_clear220 LDA	  #00000001B
		BIT	  <exception_flag     ; back scroll exception ?
		BNE	  Course_clear300     ; yes.
		TSB	  <exception_flag     ; Set back scroll exception
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !car_locatex_h,Y
		LSR	  A
		CLC
		ADC	  !car_locatex_h,Y
		STA	  <back_zelo_angle    ; Set back screen zolo angle
;
;=============== Location contorol =====================================
Course_clear300
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
		LDA	  !car_locatex_h,Y
		STA	  !car_locatex_h+0    ; Set camera location x
;-----------------------------------------------------------------------
		LDA	  !013FEH	      ; compare start line locate y
		ADC	  #60H
		CMP	  !car_locatey_h+0
		BCC	  Course_clear310
		LDA	  !car_locatey_h+0
		ADC	  #0004H
		STA	  !car_locatey_h+0    ; Set camera location y
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;-----------------------------------------------------------------------
		MEM8
Course_clear310 SEP	  #00100000B
		LDA	  <game_scene
		CMP	  #04H		      ; last scene ?
		BNE	  Course_clear320     ; no.
;-----------------------------------------------------------------------
Course_clear315 LDA	  !car_angle+1	      ; camera_angle == 0 deg  ?
		BNE	  Course_clear320     ; no.
		LDA	  #00010001B
		STA	  <exception_flag     ; Set game ending mode
		LDA	  #10H
		STA	  <ending_timer+1
		STZ	  <ending_timer+0     ; Set ending timer
		LDA	  #11111111B
		STA	  !OAM_sub+0BH	      ; Clear turbo meter
Course_clear320 RTS
;
;=============== Camera pan initial ====================================
Course_clear400
		LDA	  #180
		STA	  <initial_timer      ; Set timer
		LDA	  #00001001B
		STA	  <exception_flag     ; Set camera pan mode
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !car_locatex_h,Y
		LSR	  A
		CLC
		ADC	  !car_locatex_h,Y
		SEC
		SBC	  <back_zelo_angle
		STA	  <back_offs_angle    ; Set back screen offset angle
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Camera pan						*
;************************************************************************
;
		MEM8
		IDX8
Camera_pan
		JSR	  Frash_rank_wind
		LDY	  !anime_car_index
		LDA	  <initial_timer      ; Timer == 0 ?
		BNE	  Camera_pan100	      ; no.
;-----------------------------------------------------------------------
		LDA	  #02H
		STA	  <end_status	      ; Set course clear
		RTS
;
;=============== Camera pan main =======================================
Camera_pan100
		LDA	  !course_clr_flag
		DEC	  A		      ; camera demo ?
		BNE	  Camera_pan190	      ; no.
;-----------------------------------------------------------------------
		LDA	  !car_flag,Y
		AND	  #00001000B	      ; anime car out of screen ?
		BEQ	  Camera_pan190	      ; yes.
;-----------------------------------------------------------------------
		LDA	  !car_position_h+1,Y
		BMI	  Camera_pan150	      ; if ( car_position_h <	0 )
		BNE	  Camera_pan110	      ; if ( car_position_h > 255 )
;-----------------------------------------------------------------------
		LDA	  !car_position_h+0,Y
		CMP	  #068H		      ; car_position_h < 60H ?
		BCC	  Camera_pan150	      ; yes.
		CMP	  #098H		      ; car_position_h < A0H ?
		BCC	  Camera_pan190	      ; yes.
;-----------------------------------------------------------------------
Camera_pan110	INC	  !car_angle+1
		LDA	  !car_angle+1
		CMP	  #192
		BNE	  Camera_pan190
		STZ	  !car_angle+1
		BRA	  Camera_pan190
;-----------------------------------------------------------------------
Camera_pan150	DEC	  !car_angle+1
		LDA	  !car_angle+1
		CMP	  #255
		BNE	  Camera_pan190
		LDA	  #191
		STA	  !car_angle+1
Camera_pan190	RTS
;
;************************************************************************
;*		 Game ending						*
;************************************************************************
;
		MEM8
		IDX8
Game_ending
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDY	  <initial_timer      ; initial_timer == 0 ?
		BEQ	  Game_ending100      ; yes.
		DEY			      ; initial_timer == 1 ?
		BEQ	  Game_ending10	      ; yes.
;-----------------------------------------------------------------------
		JSR	  Frash_rank_wind     ; Frashing rank number
		BRA	  Game_ending100
;-----------------------------------------------------------------------
Game_ending10	LDY	  #11100000B
		STY	  !spot_color_B	      ; Clear rank number
;
;=============== Camera location control entry =========================
Game_ending100
		LDY	  !anime_car_index
		LDA	  !car_locatex_h,Y
		STA	  !car_locatex_h+0    ; Set camera location x
;-----------------------------------------------------------------------
		LDA	  !car_position_v,Y
		CMP	  #00A0H	      ; position_v >= A0H ?
		BCS	  Game_ending120      ; yes.
		CMP	  #0060H	      ; position_v < 60H ?
		BCS	  Game_ending200      ; no.
;------------------------------------------------------- Pos < 60H -----
Game_ending110	LDA	  !car_speedy_h,Y
		BPL	  Game_ending200
		BRA	  Game_ending150
;------------------------------------------------------- Pos >= A0H ----
Game_ending120	LDA	  !car_speedy_h,Y
		BMI	  Game_ending200
;-----------------------------------------------------------------------
Game_ending150	CLC
		LDA	  !car_locatey_l+0
		ADC	  !car_speedy_l,Y
		STA	  !car_locatey_l+0
		LDA	  !car_locatey_h+0
		ADC	  !car_speedy_h,Y
		AND	  #0FFFH
		STA	  !car_locatey_h+0
;
;=============== Send small character to VRAM ==========================
Game_ending200
		LDY	  <ending_timer+1
		CPY	  #0EH
		BCC	  Game_ending250
		BNE	  Game_ending900
;-----------------------------------------------------------------------
Game_ending210	LDY	  <ending_timer+0
		BNE	  Game_ending220
		STZ	  !roll_counter
		STZ	  !roll_pointer
		STZ	  !roll_timer
Game_ending220	CPY	  #0E0H
		BNE	  Game_ending230
		LDA	  #07H
		STA	  <sound_status_0     ; Start ENDING music
Game_ending230	CPY	  #2DH
		BCS	  Game_ending900
		TYA
		JSR	  Set_small_char
		BRA	  Game_ending900
;-----------------------------------------------------------------------
Game_ending250	JSR	  Display_roll
;
;=======================================================================
Game_ending900
		DEC	  <ending_timer
		LDA	  <ending_timer	      ; timer == 0 ?
		BEQ	  Game_ending940      ; yes.
		CMP	  #003CH	      ; timer < 3CH ?
		BCC	  Game_ending930      ; yes.
		MEM8
;-----------------------------------------------------------------------
Game_ending910	SEP	  #00100000B	      ; Memory 8 bit mode
Game_ending920	RTS
;-----------------------------------------------------------------------
Game_ending930	SEP	  #00100000B	      ; Memory 8 bit mode
		LSR	  A
		LSR	  A
		STA	  <screen_contrast    ; Set screen contrast
		LDA	  <ending_timer	      ; timer == 0 ?
		CMP	  #32
		BCS	  Game_ending935      ; yes.
		LDA	  #10000000B
		TSB	  <sound_status_0     ; Set fade out flag
Game_ending935	RTS
;-----------------------------------------------------------------------
Game_ending940	SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #10000000B
		STA	  <screen_contrast    ; Screen blanking
		STA	  <sound_status_0
		STZ	  <sound_status_1
		STZ	  <sound_status_2
		STZ	  <sound_status_3
;-----------------------------------------------------------------------
		STZ	  !shield_meter+1     ; Shield meter window off
		STZ	  <meter_switch	      ; Clear meter switch
		STZ	  <objwte_flag	      ; OAM buffer normal
		STZ	  <part_flag
		STZ	  <pers_flag
		STZ	  <window_flag
		STZ	  <colwte_flag
;-----------------------------------------------------------------------
		PHK
		JSR	  Calc_lap_time	      ; Calculate lap time
		PHK
		JSR	  Set_best_time	      ; Set best time
;-----------------------------------------------------------------------
		INC	  <game_mode
		LDA	  #06H
		STA	  <game_process
		STZ	  <game_status
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ My car control routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Roket start exception					*
;************************************************************************
;
		MEM8
		IDX8
Start_exception
		LDA	  #00000010B
		BIT	  !control_status     ; start exception ?
		BEQ	  Start_except190     ; no.
;
;=============== Roket start exception =================================
Start_except100
		LDA	  !car_speed+1
		CMP	  #02H		      ; my car speed < 2 dot ?
		BCC	  Start_except120     ; yes.
;-----------------------------------------------------------------------
Start_except110 LDA	  !car_control
		AND	  #00000011B	      ; Reset accele control
		ORA	  #00011000B	      ; Set break flag
		STA	  !car_control
		RTS
;-----------------------------------------------------------------------
Start_except120 LDA	  #00000010B
		TRB	  !control_status     ; Reset start exception flag
Start_except190 RTS
;
;************************************************************************
;*		 Check reverse run					*
;************************************************************************
;
		MEM8
		IDX8
Check_reverse
		LDA	  !car_flag+1
		LSR	  A		      ; mycar reverse ?
		BCC	  Check_reverse20     ; no.
;-----------------------------------------------------------------------
Check_reverse10 LDA	  #10000000B
		TSB	  <message_flag	      ; Display "REVERSE"
		RTS
;-----------------------------------------------------------------------
Check_reverse20 LDA	  #10000000B
		TRB	  <message_flag	      ; Clear "REVERSE"
		RTS
;
;************************************************************************
;*		 Shield repair						*
;************************************************************************
;
		MEM8
		IDX8
Shield_repair
		LDA	  <exception_flag     ; exception ?
		BNE	  Repair500	      ; yes.
		LDA	  !short_cut_flag     ; Short cut ?
		BNE	  Shield_repair2      ; yes.
		LDA	  <road_status_2
		AND	  #00010000B	      ; Pit ?
		BEQ	  Repair500	      ; no.
;-----------------------------------------------------------------------
Shield_repair2	LDA	  <repair_counter     ; repair mode already ?
		BEQ	  Repair200	      ; no.
		LDY	  #10H
		STY	  <repair_UFOwait
		CMP	  #22H		      ; check counter
		BCC	  Repair210	      ; if ( UFO move	)
		BEQ	  Repair220	      ; if ( start beam )
;
;=============== Shield repair =========================================
Repair100
		JSR	  Pull_beam_move      ; beam move
		LDA	  !short_cut_flag
		BEQ	  Repair105
		JMP	  Short_cut
Repair105	LDA	  #01000000B
		TSB	  <sound_status_2     ; Set beam sound flag
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
		LDA	  <mycar_damage
		BPL	  Repair110	      ; if ( damage >= 0 )
		LDA	  #0000H	      ; damage = 0
		BRA	  Repair120
;-----------------------------------------------------------------------
Repair110	LDY	  <mycar_pointer
		ADC	  !Repair_speed,Y     ; Shield repair
		CMP	  #0800H
		BCC	  Repair120
		LDA	  #0800H
Repair120	STA	  <mycar_damage	      ; Set shield
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		JSR	  Display_shield      ; Display shield meter
		RTS
;
;=============== Set repair mode =======================================
Repair200
		LDA	  #00000001B
		TSB	  <message_flag+0     ; Message OAM request
		INC	  <repair_counter     ; Set repair mode
		RTS
;------------------------------------------------------- UFO move ------
Repair210	JSR	  Display_UFO	      ; Display UFO
		LDA	  #0F0H
		STA	  !OAM_main+0D1H      ; Clear P.1,P.2,P.3
		STA	  !OAM_main+0D5H
		STA	  !OAM_main+0D9H      ; Clear CHECK
		STA	  !OAM_main+0DDH
		INC	  <repair_counter
		LDA	  !short_cut_flag     ; Short cut ?
		BEQ	  Repair215	      ; no.
		LDA	  <repair_counter
		CMP	  #22H
		BEQ	  Repair215
		LDA	  #21H		      ; check counter
		STA	  <repair_counter
Repair215	RTS
;------------------------------------------------------- Start beam ----
Repair220	JSR	  Pull_beam_start     ; Start pull beam
		INC	  <repair_counter
		RTS
;
;=============== End of pit ============================================
Repair500
		LDA	  <repair_counter     ; repair mode ?
		BEQ	  Repair550	      ; no.
		CMP	  #23H		      ; shield repair now ?
		BEQ	  Repair540	      ; yes.
		LDA	  <repair_UFOwait     ; UFO wait ?
		BNE	  Repair530	      ; yes.
;-----------------------------------------------------------------------
Repair510	DEC	  <repair_counter
		BEQ	  Repair520	      ; if ( end of repair )
		JSR	  Display_UFO	      ; Display UFO
		RTS
;-----------------------------------------------------------------------
Repair520	LDA	  #00000001B
		TRB	  <message_flag	      ; message OAM clear
		STZ	  !OAM_sub+000H
		STZ	  !OAM_sub+001H
		STZ	  !OAM_sub+002H	      ; Clear OAM sub
		RTS
;-----------------------------------------------------------------------
Repair530	DEC	  <repair_UFOwait
		RTS
;-----------------------------------------------------------------------
Repair540	JSR	  Pull_beam_end	      ; Stop pull beam
		DEC	  <repair_counter
		LDA	  <mycar_damage+1
		CMP	  #02H		      ; My car damage < 200H ?
		BCC	  Repair550	      ; yes.
		LDA	  #01000000B
		TRB	  <message_flag	      ; Clear "POWER DOWN"
Repair550	RTS
;
;************************************************************************
;*		 Short cut process					*
;************************************************************************
;
		MEM8
		IDX8
Short_cut
		LDA	  #01000000B
		TSB	  <sound_status_1     ; Set beam sound flag
		LDA	  !car_speed+0
		ORA	  !car_speed+1	      ; car speed == 0 ?
		BEQ	  Short_cut100	      ; yes.
		JMP	  Short_cut410	      ; car hight up
;
;=============== Turn car angle ========================================
Short_cut100
		LDY	  #00H
		SEC
		LDA	  !car_angle+1
		SBC	  !short_cut_angle+1
		BEQ	  Short_cut130
		BCS	  Short_cut110
		ADC	  #192
;-----------------------------------------------------------------------
Short_cut110	CMP	  #96
		BCC	  Short_cut120
		INC	  !car_angle+1
		LDA	  !car_angle+1
		CMP	  #192
		BNE	  Short_cut200
		STZ	  !car_angle+1
		BRA	  Short_cut200
;-----------------------------------------------------------------------
Short_cut120	DEC	  !car_angle+1
		LDA	  !car_angle+1
		CMP	  #255
		BNE	  Short_cut200
		LDA	  #191
		STA	  !car_angle+1
		BRA	  Short_cut200
;-----------------------------------------------------------------------
Short_cut130	INY
;
;=============== Move position x =======================================
Short_cut200
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		SEC
		LDA	  !car_locatex_h
		SBC	  !short_cut_posx
		BEQ	  Short_cut290
		BCC	  Short_cut250
;-----------------------------------------------------------------------
Short_cut210	CMP	  #1000H
		BCS	  Short_cut220
		LDA	  !car_locatex_h
		SEC
		SBC	  #0006H
		CMP	  !short_cut_posx
		BCS	  Short_cut280
		LDA	  !short_cut_posx
		BRA	  Short_cut280
;-----------------------------------------------------------------------
Short_cut220	LDA	  !car_locatex_h
		CLC
		ADC	  #0006H
		BRA	  Short_cut280
;-----------------------------------------------------------------------
Short_cut250	CMP	  #0F000H
		BCC	  Short_cut260
		LDA	  !car_locatex_h
		ADC	  #0006H
		CMP	  !short_cut_posx
		BCC	  Short_cut280
		LDA	  !short_cut_posx
		BRA	  Short_cut280
;-----------------------------------------------------------------------
Short_cut260	LDA	  !car_locatex_h
		SEC
		SBC	  #0006H
;-----------------------------------------------------------------------
Short_cut280	STA	  !car_locatex_h
		BRA	  Short_cut300
Short_cut290	INY
;
;=============== Move position y =======================================
Short_cut300
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		SEC
		LDA	  !car_locatey_h
		SBC	  !short_cut_posy
		BEQ	  Short_cut390
		BCC	  Short_cut350
;-----------------------------------------------------------------------
Short_cut310	CMP	  #0800H
		BCS	  Short_cut320
		LDA	  !car_locatey_h
		SEC
		SBC	  #0006H
		CMP	  !short_cut_posy
		BCS	  Short_cut380
		LDA	  !short_cut_posy
		BRA	  Short_cut380
;-----------------------------------------------------------------------
Short_cut320	LDA	  !car_locatey_h
		CLC
		ADC	  #0006H
		BRA	  Short_cut380
;-----------------------------------------------------------------------
Short_cut350	CMP	  #0F800H
		BCC	  Short_cut360
		LDA	  !car_locatey_h
		ADC	  #0006H
		CMP	  !short_cut_posy
		BCC	  Short_cut380
		LDA	  !short_cut_posy
		BRA	  Short_cut380
;-----------------------------------------------------------------------
Short_cut360	LDA	  !car_locatey_h
		SEC
		SBC	  #0006H
;-----------------------------------------------------------------------
Short_cut380	STA	  !car_locatey_h
		BRA	  Short_cut400
Short_cut390	INY
;
;=============== Hight control =========================================
Short_cut400
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		CPY	  #03H		      ; move end ?
		BEQ	  Short_cut430	      ; yes.
;-----------------------------------------------------------------------
Short_cut410	LDA	  !car_hight+1
		CMP	  #20H
		BCS	  Short_cut420
		INC	  !car_hight+1	      ; hight up
Short_cut420	RTS
;-----------------------------------------------------------------------
Short_cut430	LDA	  !car_hight+1
		CMP	  #05H
		BCC	  Short_cut440
		DEC	  !car_hight+1	      ; hight down
		RTS
;-----------------------------------------------------------------------
Short_cut440	STZ	  !short_cut_flag     ; end of short cut
		RTS
;
;************************************************************************
;*		 Check accele						*
;************************************************************************
;
		MEM8
		IDX8
Check_accele
		LDA	  !car_control
		STA	  <old_control
		STZ	  !car_control	      ; Clear car control
;-----------------------------------------------------------------------
		JSR	  Check_accele500     ; Check after burner
		JSR	  Check_accele800     ; Check jump
		LDA	  <stick_status+0
		ORA	  <stick_status+1
		ASL	  A		      ; break on ?
		BMI	  Check_accele100     ; yes.
		LDA	  <stick_status+1     ; accele on ?
		BMI	  Check_accele200     ; yes.
;-----------------------------------------------------------------------
;;		LDA	  #00001000B
;;		BIT	  >control_status+0   ; after burner on ?
;;		BEQ	  Check_accele010     ; no.
;;		LDA	  <stick_status+0     ; accele on ?
;;		BMI	  Check_accele200     ; yes.
;-----------------------------------------------------------------------
Check_accele010 STZ	  <turbo_status	      ; Clear turbo status
		RTS
;
;=============== Set brake =============================================
Check_accele100
		LDA	  #00100000B
		TSB	  !car_control	      ; Set brake
		STZ	  <turbo_status	      ; Clear turbo status
		RTS
;
;=============== Set accele ============================================
Check_accele200
		LDA	  #10000000B
		TSB	  !car_control	      ; Set accele
		LDA	  <turbo_status	      ; turbo trigger ?
		BEQ	  Check_accele400     ; yes.
		RTS
;
;=============== Turbo trigger process =================================
Check_accele400
		LDA	  #80H
		STA	  <turbo_status	      ; Set turbo status
;-----------------------------------------------------------------------
		LDA	  !control_status     ; Jumpping ?
		BMI	  Check_accele410     ; yes.
;-----------------------------------------------------------------------
		LDA	  <mycar_number
		ASL	  A
		TAY
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !Grip_limit,Y
		CMP	  !car_speed	      ; car_speed > grip limit ?
		BCC	  Check_accele410     ; yes.
		LDY	  #00000001B
		STY	  <drift_status
;-----------------------------------------------------------------------
Check_accele410 SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Check after burner ====================================
Check_accele500
		MEM8
		LDA	  #00001000B
		BIT	  !control_status+0   ; after burner on ?
		BNE	  Check_accele600     ; yes.
;-----------------------------------------------------------------------
		JSR	  Check_accele700     ; burnner increment
		LDA	  <stick_trigger+0    ; push A ?
		BPL	  Check_accele630     ; no.
;-----------------------------------------------------------------------
		LDA	  !burner_count	      ; after burner ok ?
		BEQ	  Check_accele650     ; no.
;-----------------------------------------------------------------------
		LDA	  #00001000B
		TSB	  !control_status+0   ; Set after buffer flag
		STZ	  !burner_timer
		DEC	  !burner_count
		LDX	  #00110011B
		BRA	  Check_accele610
;
;=============== After burner control ==================================
Check_accele600
		DEC	  !burner_timer
		BNE	  Check_accele640
;-----------------------------------------------------------------------
		TRB	  !control_status+0
		LDX	  #00110101B
;-----------------------------------------------------------------------
Check_accele610 LDA	  !burner_count
Check_accele620 ASL	  A
		ASL	  A
		TAY
		TXA
		STA	  !OAM_main+0B3H,Y
Check_accele630 RTS
;-----------------------------------------------------------------------
Check_accele640 LDA	  #01000000B
		TSB	  !wind_priority
		RTS
;-----------------------------------------------------------------------
Check_accele650 LDA	  <break_counter
		BNE	  Check_accele660
		LDA	  #10H
		STA	  !non_turbo_cnt
Check_accele660 RTS
;
;=============== Check burner increment ================================
Check_accele700
		LDA	  !burner_inc_flag    ; burner increment ?
		BEQ	  Check_accele630     ; no. ( return )
;-----------------------------------------------------------------------
		DEC	  !burner_inc_flag    ; clear burner_inc_flag
		LDA	  !burner_count
		CMP	  #03H		      ; burner = 3 ?
		BEQ	  Check_accele630     ; yes. ( return )
;-----------------------------------------------------------------------
		INC	  !burner_count	      ; burner increment
		LDX	  #00110001B	      ; display nurner
		BRA	  Check_accele620
;
;=============== Check jump ============================================
Check_accele800
		LDA	  !control_status+0   ; my car jumpping ?
		BPL	  Check_accele810     ; no.
		LDA	  <stick_status+1
		AND	  #00001100B
		TSB	  !car_control+0
Check_accele810 RTS
;
;************************************************************************
;*		 Check handle						*
;************************************************************************
;
		MEM8
		IDX8
Check_handle
		LDY	  #00000001B	      ; car_control = right
		LDA	  <handle_position+1  ;
		BEQ	  Check_handle200     ; if handle == 0
		BPL	  Check_handle100     ; if handle == right
		EOR	  #0FFH
		INC	  A		      ; Acc = abs( handle )
		INY			      ; car_control = left
;
;=============== My car turn check =====================================
Check_handle100
		STA	  !Multiplicand	      ; Set handle position
;;;;		LDA	  #00100000B
;;;;		BIT	  >control_status     ; super dash ?
;;;;		PHP			      ; push Z flag
		LDA	  !car_speed+0
		ASL	  A
		LDA	  !car_speed+1
		ROL	  A
;;;;		PLP			      ; super dash ?
;;;;		BNE	  Check_handle120     ; yes.
		CMP	  #00010010B
		BCS	  Check_handle120
;-----------------------------------------------------------------------
Check_handle110 CLC
		ADC	  <handle_perform
		TAX
		LDA	  >Handle_data,X
		BRA	  Check_handle130
;-----------------------------------------------------------------------
Check_handle120 TAX
		LDA	  >Dash_handle,X
;-----------------------------------------------------------------------
Check_handle130 STA	  !Multiplier	      ; Set handle data ( by speed )
		NOP
		NOP
		NOP
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !Multiply
		LSR	  A
		STA	  !car_handle+0	      ; Set car handle velocity
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		TYA
		TSB	  !car_control+0      ; Set car_control flag
		RTS
;
;=============== My car slide check ====================================
Check_handle200
		STZ	  !car_handle+0
		STZ	  !car_handle+1	      ; Clear handle velocity
;-----------------------------------------------------------------------
		LDA	  !car_control
		AND	  #00010000B	      ; stop process ?
		BNE	  Check_handle290     ; yes.
;-----------------------------------------------------------------------
		LDA	  <stick_status+0
		AND	  #00110000B	      ; push L or R ?
		BEQ	  Check_handle290     ; no.
		CMP	  #00110000B	      ; push L and R ?
		BNE	  Check_handle300     ; no.
Check_handle290 RTS
;
;=============== Calculate slide angle =================================
Check_handle300
		SEC
		LDA	  !car_scrl_angle+1
		SBC	  !car_angle+1
		BCS	  Check_handle310
		ADC	  #192
Check_handle310 STA	  <drift_angle	      ; Set drift angle
;-----------------------------------------------------------------------
		LDA	  !car_speed+0
		ASL	  A
		LDA	  !car_speed+1
		ROL	  A
;;;;		ADC	  <handle_perform
		TAX
		LDA	  >Slide_data,X
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		AND	  #00FFH
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		STA	  <slide_angle	      ; Set slide angle
;-----------------------------------------------------------------------
		LDA	  <stick_status
		AND	  #0000000000010000B  ; push R ?
		BNE	  Check_handle500     ; yes.
;
;=============== Slide left ============================================
Check_handle400
		LDY	  <drift_angle
		CPY	  #96
		BCC	  Check_handle410     ; slide
;;;;		CPY	  #192-12
		CPY	  #192-6
		BCC	  Check_handle430     ; no slide
;-----------------------------------------------------------------------
Check_handle410 SEC
		LDA	  !car_scrl_angle
		SBC	  <slide_angle	      ; sub slide angle
		BCS	  Check_handle420
		ADC	  #192*256
Check_handle420 STA	  !car_scrl_angle
		LDY	  #00000010B
		STY	  <drift_status
;-----------------------------------------------------------------------
Check_handle430 LDY	  #00100000B
		STY	  <wing_count
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Slide right ===========================================
Check_handle500
		LDY	  <drift_angle
		CPY	  #96
		BCS	  Check_handle510     ; slide
;;;;		CPY	  #12
		CPY	  #6
		BCS	  Check_handle530     ; no slide
;-----------------------------------------------------------------------
Check_handle510 CLC
		LDA	  !car_scrl_angle
		ADC	  <slide_angle	      ; add slide angle
		CMP	  #192*256
		BCC	  Check_handle520
		SBC	  #192*256
Check_handle520 STA	  !car_scrl_angle
		LDY	  #00000010B
		STY	  <drift_status
;-----------------------------------------------------------------------
Check_handle530 LDY	  #00100001B
		STY	  <wing_count
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Car handle control					*
;************************************************************************
;
		MEM8
		IDX8
Handle_control
		LDA	  <stick_trigger+1
		AND	  #00000011B	      ; Turn start ?
		BEQ	  Handle_ctrl190      ; no.
;
;=============== Handle trigger process ================================
Handle_ctrl100
		LDA	  #01H
		STA	  <handle_velocity+1
		STZ	  <handle_velocity+0  ; Set initial handle velocity
;-----------------------------------------------------------------------
		LDA	  <car_wing	      ; my car slide ?
		BPL	  Handle_ctrl190      ; no.
		LSR	  A		      ; my car slide right ?
		BCS	  Handle_ctrl120      ; yes.
;------------------------------------------------------- slide left ----
Handle_ctrl110	LDA	  <stick_trigger+1
		AND	  #00000010B	      ; Turn left ?
		BEQ	  Handle_ctrl190      ; no.
		LDA	  <handle_position+1
		BEQ	  Handle_ctrl115
		CMP	  #0F3H
		BCC	  Handle_ctrl190
Handle_ctrl115	LDA	  #0F2H
		STA	  <handle_position+1
		STZ	  <handle_position+0
		JMP	  Handle_ctrl300
;------------------------------------------------------- slide right ---
Handle_ctrl120	LDA	  <stick_trigger+1
		AND	  #00000001B	      ; Turn right ?
		BEQ	  Handle_ctrl190      ; no.
		LDA	  <handle_position+1
		CMP	  #0EH
		BCS	  Handle_ctrl190
		LDA	  #0EH
		STA	  <handle_position+1
		STZ	  <handle_position+0
		JMP	  Handle_ctrl400
;-----------------------------------------------------------------------
Handle_ctrl190	LDA	  <stick_status+1
		LSR	  A		      ; push RIGHT ?
		BCS	  Handle_ctrl400      ; yes.
		LSR	  A		      ; push LEFT ?
		BCS	  Handle_ctrl300      ; yes.
;
;=============== Handle off ============================================
Handle_ctrl200
		MEM16
		REP	  #00100000B	      ; memory 16 bit mode
;-----------------------------------------------------------------------
		LDA	  #0100H
		STA	  <handle_velocity    ; Initial handle velocity
		LDA	  <handle_position
		BEQ	  Handle_ctrl500
		BMI	  Handle_ctrl220
;------------------------------------------------------- handle right --
Handle_ctrl210	SEC
		SBC	  #0100H
		STA	  <handle_position    ; Restore handle
		BPL	  Handle_ctrl500
		STZ	  <handle_position
		BRA	  Handle_ctrl500
;------------------------------------------------------- handle left ---
Handle_ctrl220	CLC
		ADC	  #0100H
		STA	  <handle_position    ; Restore handle
		BMI	  Handle_ctrl500
		STZ	  <handle_position
		BRA	  Handle_ctrl500
;
;=============== Handle left ===========================================
Handle_ctrl300
		MEM8
		LDY	  #0F2H		      ; IY = -0EH
		LDA	  <stick_status+0
		AND	  #00100000B	      ; push L ?
		BEQ	  Handle_ctrl310      ; no.
		LDY	  #0F0H		      ; IY = -10H
Handle_ctrl310	STY	  <work0+1
		STZ	  <work0+0	      ; Set max handle position
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  <handle_position
		BPL	  Handle_ctrl320      ; if ( handle_pos == right )
		CMP	  <work0
		BEQ	  Handle_ctrl500      ; if ( handle_pos == max_pos )
		BCC	  Handle_ctrl220      ; if ( handle_pos <  max_pos )
;-----------------------------------------------------------------------
Handle_ctrl320	SEC
		SBC	  <handle_velocity
		BPL	  Handle_ctrl450
		CMP	  <work0
		BCS	  Handle_ctrl450
		LDA	  <work0
		BRA	  Handle_ctrl450
;
;=============== Handle right ==========================================
Handle_ctrl400
		MEM8
		LDY	  #0EH		      ; IY = 0EH
		LDA	  <stick_status+0
		AND	  #00010000B	      ; push R ?
		BEQ	  Handle_ctrl410      ; no.
		LDY	  #10H		      ; IY = 10H
Handle_ctrl410	STY	  <work0+1
		STZ	  <work0+0	      ; Set max handle position
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  <handle_position
		BMI	  Handle_ctrl420      ; if ( handle_pos == left )
		CMP	  <work0
		BEQ	  Handle_ctrl500      ; if ( handle_pos == max_pos )
		BCS	  Handle_ctrl210      ; if ( handle_pos >  max_pos )
;-----------------------------------------------------------------------
Handle_ctrl420	CLC
		ADC	  <handle_velocity
		BMI	  Handle_ctrl450
		CMP	  <work0
		BCC	  Handle_ctrl450
		LDA	  <work0
;-----------------------------------------------------------------------
Handle_ctrl450	STA	  <handle_position
		LDA	  <handle_velocity
		CLC
		ADC	  #30		      ; #34
		STA	  <handle_velocity
;
;=============== End of handle routine =================================
Handle_ctrl500
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Spin start check					*
;************************************************************************
;
		MEM8
		IDX8
Spin_start
		LDA	  !control_status
		ASL	  A		      ; My car spin now ?
		BMI	  Spin_start390	      ; yes.
;-----------------------------------------------------------------------
		LDA	  !car_speed+1
		CMP	  #08H		      ; speed >= 8 ?
		BCC	  Spin_start390	      ; no.
		LDA	  !car_BG_flag
		AND	  #11100000B	      ; My car hit wall ?
		BNE	  Spin_start200	      ; yes.
;
;=============== Drift spin ============================================
Spin_start100
		LDA	  <road_status_2
		BPL	  Spin_start390
;-----------------------------------------------------------------------
		LDA	  !car_angle+1
		SEC
		SBC	  !car_scrl_angle+1
		BCS	  Spin_start110
		ADC	  #192
Spin_start110	CMP	  #96
		BCS	  Spin_start130
;-----------------------------------------------------------------------
Spin_start120	CMP	  #18
		BCS	  Spin_start220
		BRA	  Spin_start390
;-----------------------------------------------------------------------
Spin_start130	CMP	  #192-18
		BCC	  Spin_start210
		BRA	  Spin_start390
;
;=============== Crash spin ============================================
Spin_start200
		AND	  #11000000B	      ; My car hit wall ?
		BEQ	  Spin_start390	      ; yes.
;-----------------------------------------------------------------------
		CLC
		LDA	  !car_angle+1
		SBC	  <road_angle
		CMP	  #30H
		BCC	  Spin_start220	      ; Spin right
		CMP	  #60H
		BCC	  Spin_start210	      ; Spin left
		CMP	  #90H
		BCC	  Spin_start220	      ; Spin right
;-----------------------------------------------------------------------
Spin_start210	LDA	  #81H		      ; Spin left
		BRA	  Spin_start300
Spin_start220	LDA	  #01H		      ; Spin right
;
;=============== Spin start process ====================================
Spin_start300
		STA	  !car_spin_velo+1
		LDA	  #40H
		STA	  !car_spin_velo+0    ; spin_velocity = 140H
		LDA	  #01000000B
		TSB	  !control_status     ; Set spin flag
Spin_start390	RTS
;
;///////////////////////////////////////////////////////////////////////
;/////////////// Player display routines ///////////////////////////////
;************************************************************************
;*		 Player display entry					*
;************************************************************************
;
		MEM8
		IDX8
Player_display
		PHP
		SEP	  #00110000B	      ; memory,Index 8 bit mode
		LDA	  <exception_flag     ; Exception ?
		BNE	  Player_disp180      ; yes.
;
;=============== Check jump ============================================
Player_disp000
		LDA	  !control_status
		AND	  #10000001B
		CMP	  #10000000B	      ; normal jumpping ?
		BNE	  Player_disp090      ; no.
		LDA	  !car_control
		BIT	  #00000100B	      ; Jump UP ?
		BNE	  Player_disp010      ; yes.
		LDA	  <jump_count
		CMP	  #10H		      ; jump_count < 10H ?
		BCS	  Player_disp090      ; no.
;-----------------------------------------------------------------------
Player_disp010	LDA	  #80H
		STA	  <car_canopy	      ; Set jump pose
		LDA	  #05H
		STA	  <car_wing
		BRA	  Player_disp190
;-----------------------------------------------------------------------
Player_disp090	LDA	  #00100000B
		BIT	  <wing_count	      ; check wing display flag
		BMI	  Player_disp200      ; if ( on wall )
		BVS	  Player_disp300      ; if ( bomb    )
		BEQ	  Player_disp400      ; if ( normal display )
;
;=============== My car slide ==========================================
Player_disp100
		LDA	  <wing_count	      ; wing exception ?
		AND	  #00000001B
		ORA	  #10000000B
		STA	  <car_wing	      ; Set wing pattern
		STZ	  <wing_count	      ; Clear wing counter
;-----------------------------------------------------------------------
Player_disp180	STZ	  <wing_spark_cnt     ; Clear spark counter
Player_disp190	PLP
		RTS

;=============== My car on wall ========================================
Player_disp200
		LDA	  <wing_count
		AND	  #00001111B
		TAY
		LDA	  !On_wall_pattern,Y
		STA	  <car_wing	      ; Set wing pose number
		DEC	  <wing_count
		BMI	  Player_disp210
		STZ	  <wing_count
;-----------------------------------------------------------------------
Player_disp210	STZ	  <wing_spark_cnt
		PLP
		RTS
;
;=============== Bomb process ==========================================
Player_disp300
		LDA	  !car_unctrl_flag
		BNE	  Player_disp310
		STZ	  <wing_count
		PLP
		RTS
;-----------------------------------------------------------------------
Player_disp310	LDA	  <wing_count
		AND	  #00001111B
		TAY
		LDA	  !Bomb_pattern,Y
		STA	  <car_wing	      ; Set wing pose number
;-----------------------------------------------------------------------
		LDA	  #00000011B
		BIT	  <wing_count
		BEQ	  Player_disp320
		DEC	  <wing_count
		BRA	  Player_disp330
Player_disp320	TSB	  <wing_count
;-----------------------------------------------------------------------
Player_disp330	PLP
		RTS
;
;=============== Move my car wing ======================================
Player_disp400
		LDA	  <handle_position+1
		CLC
		ADC	  #16
		TAY
		LDA	  !Wing_pattern,Y
		STA	  <car_wing
;
;=============== Move my car canopy ====================================
Player_disp500
		LDA	  !Canopy_pattern,Y
		STA	  <car_canopy
;
;=============== Set wing spark counter ================================
Player_disp600
		LDA	  <car_wing
		BEQ	  Player_disp620
		CMP	  #10
		BEQ	  Player_disp610
;-----------------------------------------------------------------------
		STZ	  <wing_spark_cnt
		BRA	  Player_disp640
;-----------------------------------------------------------------------
Player_disp610	LDA	  <wing_spark_cnt
		BNE	  Player_disp640
		LDA	  #10000001B
		BRA	  Player_disp630
;-----------------------------------------------------------------------
Player_disp620	LDA	  <wing_spark_cnt
		BNE	  Player_disp640
		LDA	  #00000001B
;-----------------------------------------------------------------------
Player_disp630	STA	  <wing_spark_cnt
Player_disp640	PLP
		RTS
;
;=======================================================================
Wing_pattern	BYTE	  0,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5
		BYTE	  5,5,6,6,6,6,7,7,7,7,8,8,8,8,9,9,10
Canopy_pattern	BYTE	  0,0,0,0,0,0,1,1,1,1,2,2,2,2,3,3
		BYTE	  3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,6,6
;-----------------------------------------------------------------------
On_wall_pattern BYTE	  05H,09H,05H,01H,05H,09H,09H,05H,01H,01H
Bomb_pattern	BYTE	  00H,00H,80H,80H		     ; Left
		BYTE	  0AH,0AH,81H,81H		     ; Right

;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Check player hit enemy \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Player hit check entry					*
;************************************************************************
;
		MEM8
		IDX8
Player_check
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  !car_hight+1
		CMP	  #10H		      ; my car jumpping ?
		BCS	  Player_check190     ; yes.
;
;=============== Check main ============================================
Player_check100
		LDY	  #08H
Player_check110 LDA	  !car_flag,Y
		AND	  #10001110B
		CMP	  #10001000B	      ; enemy car active ?
		BNE	  Player_check120     ; no.
;-----------------------------------------------------------------------
		LDA	  !car_crash_flag,Y
		STA	  <car_crash	      ; Restore crash_flag
		PHY
		JSR	  Player_check200     ; Hit check !!
		PLY
		LDA	  <car_crash
		BRA	  Player_check130
;-----------------------------------------------------------------------
Player_check120 LDA	  #00H
Player_check130 STA	  !car_crash_flag,Y   ; Store crash_flag
		DEY
		DEY
		BNE	  Player_check110
;-----------------------------------------------------------------------
Player_check190 PLP
		RTS
;
;=============== Check position v ======================================
Player_check200
		LDA	  !car_hight+1,Y
		CMP	  #10H		      ; enemy car jumpping ?
		BCS	  Player_check220     ; yes. ( no check )
;-----------------------------------------------------------------------
		LDA	  !car_position_v+0
		SEC
		SBC	  !car_position_v,Y   ; My_car_v - Enemy_car_v >= 0 ?
		BCS	  Player_check210     ; yes. ( my_car hit )
;-----------------------------------------------------------------------
		CMP	  #0E8H		      ; distance_v >= -10H ?
		BCC	  Player_check220     ; no. ( return )
		LDA	  #00000100B
		STA	  <hit_status	      ; hit enemy front
		STY	  <hit_car_pointer+0  ; hit car = 'Enemy car'
		STZ	  <hit_car_pointer+1
		BRA	  Player_check300
;-----------------------------------------------------------------------
Player_check210 STZ	  <hit_status	      ; hit enemy back
		STZ	  <hit_car_pointer+0  ; hit car = 'My car'
		STY	  <hit_car_pointer+1
		CMP	  #19H		      ; distance_v < 11H ?
		BCC	  Player_check300     ; yes. ( hit )
;-----------------------------------------------------------------------
Player_check220 STZ	  <car_crash	      ; Clear crash_flag
		RTS
;
;=============== Check position h ======================================
Player_check300
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDA	  !car_position_h,Y
		SEC
		SBC	  !car_position_h+0   ; Enemy_car_h - My_car_h >= 0 ?
		BPL	  Player_check310     ; yes. ( my_car is left )
		EOR	  #0FFFFH
		INC	  A		      ; ( = NOT	  A )
		INC	  <hit_status	      ; hit left
		INC	  <hit_status	      ; ( = TSB	  #00000010B )
;-----------------------------------------------------------------------
Player_check310 CMP	  #0018H	      ; distance_x < 18H
		BCC	  Player_crash	      ; yes. ( crash )
		CMP	  #0028H	      ; distance_x < 28H
		BCC	  Player_graze	      ; yes. ( graze )
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		STZ	  <car_crash	      ; Clear crash_flag
		RTS
;
;************************************************************************
;*		 Player crash						*
;************************************************************************
;
		MEM8
		IDX8
Player_crash
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		LDA	  <car_crash
		CMP	  #02H		      ; crash already ?
		BEQ	  Player_crash290     ; yes. ( return )
		LDA	  <super_flag	      ; invincible ?
		BNE	  Player_crash200     ; yes.
;-----------------------------------------------------------------------
		LDA	  #02H
		STA	  <car_crash	      ; Set crash_flag
		LDA	  !car_flag,Y
		LSR	  A
		BCS	  Enemy_bomb
;
;=============== Set my car damage =====================================
Player_crash100
		PHY
		TYX
		JSR	  Crash_damage	      ; Set damage
		JSR	  Display_shield
;-----------------------------------------------------------------------
		LDA	  #09H
		STA	  <super_flag	      ; Set invincible
		STA	  !carcol_change+0    ; Set my car color change counter
;-----------------------------------------------------------------------
		LDA	  #11100000B
		BIT	  <wing_count
		BNE	  Player_crash110
		LDA	  #10000101B
		STA	  <wing_count
;-----------------------------------------------------------------------
Player_crash110 PLY
		LDA	  !carcol_change,Y    ; Enemy color change already ?
		BNE	  Player_crash200     ; yes.
		LDA	  #09H
		STA	  !carcol_change,Y    ; Set enemy color change counter
;
;=============== Car crash spin ========================================
Player_crash200
		JSR	  Car_crash_spin
		JSR	  Exchange_vector
Player_crash290 RTS
;
;************************************************************************
;*		 Enemy bomb						*
;************************************************************************
;
		MEM8
		IDX8
Enemy_bomb
		LDA	  <hit_status	      ; ( = TSB	  #00000010B )
		JSR	  Enemy_bomb_ctrl
		RTS
;
;************************************************************************
;*		 Player graze						*
;************************************************************************
;
		MEM8
		IDX8
Player_graze
		SEP	  #00100000B	      ; Memory 8 bit mode
		STA	  <hit_distance	      ; Set distance
		LDA	  <car_crash	      ; crash already ?
		BNE	  Player_crash290     ; yes.
		LDA	  <super_flag	      ; invincible ?
		BNE	  Player_graze100     ; yes.
;-----------------------------------------------------------------------
		LDA	  #01H
		STA	  <car_crash	      ; Set crash flag
		LDA	  !car_flag,Y
		LSR	  A
		BCS	  Enemy_bomb
;-----------------------------------------------------------------------
		PHY
		JSR	  Graze_damage	      ; Set damage
		JSR	  Display_shield
		LDX	  #08H
		STX	  <super_flag	      ; Set invincible
		PLY
;
;=============== Check angle difference ================================
Player_graze100
		LDA	  !car_scrl_angle+1,Y
		SEC
		SBC	  !car_scrl_angle+1
		BCS	  Player_graze110
		ADC	  #192
Player_graze110
		CMP	  #48
		BCC	  Player_graze200
		CMP	  #144
		BCS	  Player_graze200
		LDA	  #00001000B
		TSB	  <hit_status	      ; hit angle difference
;
;=============== Check speed ===========================================
Player_graze200
		LDA	  !car_speed+1
		CMP	  !car_speed+1,Y
		BCS	  Player_graze300
		LDA	  #00010000B
		TSB	  <hit_status
;
;=============== Set angle difference ==================================
Player_graze300
		LDA	  #2BH
		SEC
		SBC	  <hit_distance		; 18H - 27H
		STZ	  <angle_offset+0
		STA	  <angle_offset+1	; 13H - 04H
;
;=============== Index jump graze routines =============================
Player_graze400
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDX	  <hit_status
		LDA	  !Hitchk_table,X
		STA	  <work0
		JMP	  (!work0)
;-----------------------------------------------------------------------
Hitchk_table	WORD	  Player_hit_A_R      ; 0000
		WORD	  Player_hit_A_L      ; 0001
		WORD	  Player_hit_A_R      ; 0010
		WORD	  Player_hit_A_L      ; 0011
		WORD	  Player_hit_C_R      ; 0100
		WORD	  Player_hit_C_L      ; 0101
		WORD	  Player_hit_C_R      ; 0110
		WORD	  Player_hit_C_L      ; 0111
		WORD	  Player_hit_B_R      ; 1000
		WORD	  Player_hit_B_L      ; 1001
		WORD	  Player_hit_B_R      ; 1010
		WORD	  Player_hit_B_L      ; 1011
		WORD	  Player_hit_C_R      ; 1100
		WORD	  Player_hit_C_L      ; 1101
		WORD	  Player_hit_C_R      ; 1110
		WORD	  Player_hit_C_L      ; 1111
;
;=============== Hit A =================================================
Player_hit_A_R
		LDX	  #00H
		JSR	  Player_hit_L
		TYX
		JSR	  Player_hit_R
		LDA	  #0000H
		BRA	  Player_hit_spin
;-----------------------------------------------------------------------
Player_hit_A_L	LDX	  #00H
		JSR	  Player_hit_R
		TYX
		JSR	  Player_hit_L
		LDA	  #8000H
		BRA	  Player_hit_spin
;
;=============== Hit B =================================================
Player_hit_B_R
		LDX	  #00H
		JSR	  Player_hit_L
		TYX
		JSR	  Player_hit_R
		LDA	  #8000H
		BRA	  Player_hit_spin
;-----------------------------------------------------------------------
Player_hit_B_L	LDX	  #00H
		JSR	  Player_hit_R
		TYX
		JSR	  Player_hit_L
		LDA	  #0000H
		BRA	  Player_hit_spin
;
;=============== Hit C =================================================
Player_hit_C_R
		LDX	  #00H
		JSR	  Player_hit_L
		TYX
		JSR	  Player_hit_L
		LDA	  #0000H
		BRA	  Player_hit_spin
;-----------------------------------------------------------------------
Player_hit_C_L	LDX	  #00H
		JSR	  Player_hit_R
		TYX
		JSR	  Player_hit_R
		LDA	  #8000H
;
;=======================================================================
Player_hit_spin
		STA	  <hit_spin_sign
		LDA	  !car_speed+0
		CMP	  !car_speed,Y
		BCS	  Player_spin_2
		LDA	  !car_speed,Y
;-----------------------------------------------------------------------
Player_spin_2	LSR	  A
		LSR	  A
		CMP	  #0080H
		BCS	  Player_spin_3
		LDA	  #0080H
		BRA	  Player_spin_4
Player_spin_3	CMP	  #0240H
		BCC	  Player_spin_4
		LDA	  #0240H
;-----------------------------------------------------------------------
Player_spin_4	ORA	  <hit_spin_sign
		STA	  !car_spin_velo+0
		STA	  !car_spin_velo,Y
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  !control_status+0
		BIT	  #01000000B	      ; my car spin ?
		BEQ	  Player_spin_5	      ; no.
		LDA	  <hit_spin_sign+1
		INC	  A
		STA	  !car_spin_velo+1
		STZ	  !car_spin_velo+0
Player_spin_5	RTS
;
;=======================================================================
Player_hit_L
		MEM16
		LDA	  !car_scrl_angle,X
		SEC
		SBC	  <angle_offset
		BCS	  Player_hit_L2
		ADC	  #192*256
Player_hit_L2	STA	  !car_scrl_angle,X
		RTS
;
;=======================================================================
Player_hit_R
		LDA	  !car_scrl_angle,X
		CLC
		ADC	  <angle_offset
		CMP	  #192*256
		BCC	  Player_hit_R2
		SBC	  #192*256
Player_hit_R2	STA	  !car_scrl_angle,X
		RTS
;
;************************************************************************
;*		 Exchange car vector					*
;************************************************************************
;
		MEM8
		IDX8
Exchange_vector
		LDX	  <hit_car_pointer+0
		LDY	  <hit_car_pointer+1
		LDA	  #10
		STA	  !car_unctrl_flag,X
;
;=============== Check car angle =======================================
Exchange_vec100
		SEC
		LDA	  !car_angle+1,X
		SBC	  !car_angle+1,Y
		BCS	  Exchange_vec110
		ADC	  #192
Exchange_vec110 CMP	  #30H
		BCC	  Exchange_vec200
		CMP	  #90H
		BCS	  Exchange_vec200
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		JSR	  Exchange_vec500     ; Exchange speed vector
;-----------------------------------------------------------------------
		CLC
		LDA	  !car_speed,X
		ADC	  !car_speed,Y
		CMP	  #0200H
		BCS	  Exchange_vec310
;-----------------------------------------------------------------------
		JSR	  Exchange_vec700     ; Add speed
		BRA	  Exchange_vec310
;
;=============== Check speed difference ================================
Exchange_vec200
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		SEC
		LDA	  !car_speed,X
		SBC	  !car_speed,Y
		BMI	  Exchange_vec210     ; if ( speed_diff < 0 )
;-----------------------------------------------------------------------
		PHA
		JSR	  Exchange_vec500     ; Exchange vector
		PLA
		BRA	  Exchange_vec220
;-----------------------------------------------------------------------
Exchange_vec210 EOR	  #0FFFFH
		INC	  A		      ; Acc = speed difference
;-----------------------------------------------------------------------
Exchange_vec220 CMP	  #0100H
		BCS	  Exchange_vec300
		JSR	  Exchange_vec800     ; Add/Sub speed
;-----------------------------------------------------------------------
Exchange_vec300 JSR	  Exchange_vec900     ; For enemy set BUG !!
Exchange_vec310 SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Exchange speed vector =================================
Exchange_vec500
		MEM16
		LDA	  !car_speed,X
		PHA
		LDA	  !car_speed,Y
		STA	  !car_speed,X
		PLA
		STA	  !car_speed,Y	      ; exchange car speed
;
;=============== Exchange scroll angle =================================
Exchange_vec600
		LDA	  !car_scrl_angle,X
		PHA
		LDA	  !car_scrl_angle,Y
		STA	  !car_scrl_angle,X
		PLA
		STA	  !car_scrl_angle,Y   ; exchange scroll angle
		RTS
;
;=============== Add speed =============================================
Exchange_vec700
		LDA	  !car_speed,X
		CLC
		ADC	  #0080H
		STA	  !car_speed,X
;-----------------------------------------------------------------------
		LDA	  !car_speed,Y
		CLC
		ADC	  #0080H
		STA	  !car_speed,Y
		RTS
;
;=============== Add/Sub speed =========================================
Exchange_vec800
		LDA	  !car_speed,X
		SEC
		SBC	  #0080H
		BCS	  Exchange_vec810
		LDA	  #0000H
Exchange_vec810 STA	  !car_speed,X
;-----------------------------------------------------------------------
		LDA	  !car_speed,Y
		CLC
		ADC	  #0080H
		STA	  !car_speed,Y
		RTS
;
;=============== For enemy set BUG !!!! ================================
Exchange_vec900
		LDA	  !car_speed,Y
		CMP	  #0400H
		BCS	  Exchange_vec910
		LDA	  #0400H
		STA	  !car_speed,Y
Exchange_vec910 RTS
;
;************************************************************************
;*		 Car spin at crash					*
;************************************************************************
;
		MEM8
		IDX8
Car_crash_spin
		PHX
		LDX	  <mycar_pointer
		LDA	  !Enemy_spin_init+1,X
		STA	  !car_spin_velo+1,Y
		LDA	  !Enemy_spin_init+0,X
		STA	  !car_spin_velo+0,Y
		LDA	  !control_status,Y
		ORA	  #01000000B
		STA	  !control_status,Y
;-----------------------------------------------------------------------
		LDA	  !Mycar_spin_init+1,X
		STA	  !car_spin_velo+1
		LDA	  !Mycar_spin_init+0,X
		STA	  !car_spin_velo+0
		LDA	  #01000000B
		TSB	  !control_status+0
;-----------------------------------------------------------------------
		LDX	  <hit_status
		LDA	  !Hit_spin_status+0,X
		TSB	  !car_spin_velo+1
		LDA	  !Hit_spin_status+1,X
		ORA	  !car_spin_velo+1,Y
		STA	  !car_spin_velo+1,Y
;-----------------------------------------------------------------------
		PLX
		RTS
;
;=======================================================================
Hit_spin_status BYTE	  00H,00H,80H,80H,80H,80H,00H,00H
;
		END
