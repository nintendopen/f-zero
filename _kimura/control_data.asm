;************************************************************************
;*	 CTRL_DATA	   -- car control data --			*
;************************************************************************
;
		INCLUDE	  ALPHA
		INCLUDE	  ALPHA2
;
		GLB	  Handle_datptr,Handle_data,Dash_handle,Slide_data
		GLB	  Accele_datptr,Turbo_data,Brake_data
		GLB	  Slip_vector,Slip_vecspd1,Slip_vecspd2,Grip_vecspd
		GLB	  Maximum_speed,Slip_speed,Grip_limit
		GLB	  Friction_data,Reduce_data
		GLB	  Damage_speed,Damage_time,Mycar_spin_init,Enemy_spin_init
		GLB	  Damage_data_1,Damage_data_2,Damage_data_3,Damage_data_4,Damage_data_5
		GLB	  Repair_speed,Power_down_sens
		GLB	  Mycar1_jet_pos,Mycar2_jet_pos,Mycar3_jet_pos,Mycar4_jet_pos
		GLB	  Roll_data,Staff_roll_data
		GLB	  Demo_stick_wld1,Demo_stick_wld2,Demo_stick_wld3
;
;************************************************************************
;*		 Car accele data					*
;************************************************************************
;
Accele_datptr	BYTE	  Turbo_data_1-Turbo_data     ; My car 1
		BYTE	  Turbo_data_2-Turbo_data     ; My car 2
		BYTE	  Turbo_data_3-Turbo_data     ; My car 3
		BYTE	  Turbo_data_4-Turbo_data     ; My car 4

;Dash_data	 BYTE	   96,96,96,96,96,96,96,96,96,96,64,64,64,64,64,64,64,64,48
;		BYTE	  48,32,32,24,24,16,16,08,04,02,01,01,01
;
;************************************************************************
;*		 My car handle data					*
;************************************************************************
;
Handle_datptr	BYTE	  Handle_data_0-Handle_data	; My car 1
		BYTE	  Handle_data_1-Handle_data	; My car 2
		BYTE	  Handle_data_2-Handle_data	; My car 3
		BYTE	  Handle_data_3-Handle_data	; My car 4
;
;************************************************************************
;*		 Slip performance					*
;************************************************************************
;
;	  My car number	   1  2	 3  4
Slip_vector	BYTE	  30,30,30,30	      ; ( angle )
Grip_vecspd	BYTE	  10,06,12,04	      ; ( angle )
;-----------------------------------------------------------------------
Slip_vecspd1	BYTE	  25,23,20,35	      ; ( X/8 angle ( 32 = 1 angle ))
Slip_vecspd2	BYTE	  15,13,10,25	      ; ( X/8 angle ( 32 = 1 angle ))
;
;************************************************************************
;*		 Non turbo, Slit start, Grip limit speed		*
;************************************************************************
;
;	  My car number	     1	   2	 3     4
Maximum_speed	WORD	  08C0H,08D0H,0880H,0900H	; ( XX.XXH )
Slip_speed	WORD	  0580H,0580H,0580H,0580H	; ( XX.XXH )
Grip_limit	WORD	  0700H,0700H,0700H,0700H	; ( XX.XXH )
;
;************************************************************************
;*		 Friction and reduce performance			*
;************************************************************************
;
;	  My car number	   1  2	 3  4
Friction_data	BYTE	  04,06,04,02		; ( X/256 dot )
Reduce_data	BYTE	  14,10,18,04		; ( X/256 dot )
;
;************************************************************************
;*		 Damage and weight					*
;************************************************************************
;
;	  My car number	   Blue Green Yellow Red
Damage_data_1	BYTE	  00C0H,00A0H,00E0H,00B0H   ; damage at car crash
Damage_data_2	BYTE	  0018H,0018H,0030H,0018H   ; damage at car graze
Damage_data_3	BYTE	  0005H,0003H,0008H,0004H   ; damage at car on wall
Damage_data_4	BYTE	  0060H,0050H,00C0H,0058H   ; damage at car out of course
Damage_data_5	BYTE	  00F0H,00E0H,00FFH,00E8H   ; damage at bomb
;-----------------------------------------------------------------------
Repair_speed	WORD	  0004H,0002H,0008H,0003H   ; Power repair speed
Damage_speed	WORD	  0400H,0400H,0400H,0400H   ; damage speed ( on wall speed )
Power_down_sens WORD	  0008H,0008H,0008H,0010H   ; Power down sensitivity ( n/8 )
Mycar_spin_init WORD	  0068H,0018H,0088H,0040H   ; mycar spin velocity ( crash )
Enemy_spin_init WORD	  0080H,00E0H,0040H,00C0H   ; enemy spin velocity ( crash )
Damage_time	BYTE	  005,008,010,005	    ; ( frame )
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Bank #2 data \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 My car turbo jet display position			*
;************************************************************************
;
		ORG	  02C630H
;
;			  h1 v1 h2 v2 h3 v3 h4 v4
Mycar1_jet_pos	BYTE	  12,23,32,20,12,17,30,14
		BYTE	  11,22,31,20,11,16,29,14
		BYTE	  09,21,31,21,10,15,30,15
		BYTE	  29,22,09,20,29,16,10,14
		BYTE	  28,23,08,20,28,17,11,14
;-----------------------------------------------------------------------
Mycar2_jet_pos	BYTE	  18,14,15,26,29,24
		BYTE	  17,13,14,24,28,23
		BYTE	  17,11,13,23,27,23
		BYTE	  23,13,26,24,12,23
		BYTE	  22,14,25,26,11,24
		BYTE	  20,13,14,24,28,23
		BYTE	  20,13,26,24,12,23
;-----------------------------------------------------------------------
Mycar3_jet_pos	BYTE	  10,28,30,22,23,24,16,26
		BYTE	  09,27,30,24,23,25,16,26
		BYTE	  09,23,31,23,24,23,16,23
		BYTE	  31,27,10,24,17,25,24,26
		BYTE	  30,28,10,22,17,24,24,26
;-----------------------------------------------------------------------
Mycar4_jet_pos	BYTE	  15,25,27,23
		BYTE	  15,24,27,23
		BYTE	  14,23,26,23
		BYTE	  25,24,13,23
		BYTE	  25,25,13,23
;
;************************************************************************
;*		 Roll data						*
;************************************************************************
;
Roll_data	BYTE	  0F4H,0F3H,020H,0F5H,0F2H,00110011B,0F1H,7CH,30H,0F7H,0F8H
		BYTE	  0F1H,50H,40H,LL,AA,PP
		BYTE	  0F1H,90H,40H,RR,AA,NN,KK
		BYTE	  0F1H,50H,50H,0F2H,00110001B
		BYTE	  81H,6AH,82H,83H,6BH,84H,85H,0F0H,0F0H,0F0H,0C0H,0C1H
		BYTE	  0F1H,50H,60H,0F2H,00110111B
		BYTE	  87H,6AH,88H,89H,6BH,8AH,8BH,0F0H,0F0H,0F0H,0C2H,0C3H
		BYTE	  0F1H,50H,70H,0F2H,00110001B
		BYTE	  8DH,6AH,8EH,8FH,6BH,90H,91H,0F0H,0F0H,0F0H,0F0H,0C5H
		BYTE	  0F1H,50H,80H,0F2H,00110111B
		BYTE	  93H,6AH,94H,95H,6BH,96H,97H,0F0H,0F0H,0F0H,0F0H,0C7H
		BYTE	  0F1H,50H,90H,0F2H,00110001B
		BYTE	  99H,6AH,9AH,9BH,6BH,9CH,9DH,0F0H,0F0H,0F0H,0F0H,0C9H
		BYTE	  0F1H,50H,0A0H,TT,OO,TT,AA,LL
		BYTE	  0F1H,50H,0B0H,0F2H,00110101B
		BYTE	  9FH,6AH,0A0H,0A1H,6BH,0A2H,0A3H,0F0H,0F0H,0F0H,0F0H,0CBH
		BYTE	  0F3H,0E0H,0F3H,0A0H,0F3H,070H,0F6H
		BYTE	  0F4H,0F3H,060H,0F9H
;
;************************************************************************
;*		 Staff roll data					*
;************************************************************************
;
Staff_roll_data BYTE	  5CH,EE,XX,EE,CC,UU,TT,II,VV,EE,0,08H
		BYTE	  60H,PP,RR,OO,DD,UU,CC,EE,RR,0,18H
		BYTE	  40H,HH,II,RR,OO,SS,HH,II,SP,YY,AA,MM,AA,UU,CC,HH,II,0,0C8H
		BYTE	  60H,PP,RR,OO,DD,UU,CC,EE,RR,0,18H
		BYTE	  40H,SS,HH,II,GG,EE,RR,UU,SP,MM,II,YY,AA,MM,OO,TT,OO,0,0B8H
		BYTE	  6CH,SS,TT,AA,FF,FF,0,20H
		BYTE	  48H,II,SS,SS,HH,II,NN,SP,SS,HH,II,MM,II,ZZ,UU,0,18H
		BYTE	  4CH,YY,UU,KK,II,OO,SP,KK,AA,NN,EE,OO,KK,AA,0,18H
		BYTE	  50H,YY,UU,MM,II,KK,OO,SP,KK,AA,NN,KK,II,0,18H
		BYTE	  40H,YY,AA,SS,UU,NN,AA,RR,II,SP,NN,II,SS,HH,II,DD,AA,0,18H
		BYTE	  50H,NN,AA,OO,TT,OO,SP,II,SS,HH,II,DD,AA,0,18H
		BYTE	  4CH,MM,AA,SS,AA,TT,OO,SP,KK,II,MM,UU,RR,AA,0,18H
		BYTE	  3CH,SS,HH,II,GG,EE,KK,II,SP,YY,AA,MM,AA,SS,HH,II,RR,OO,0,18H
		BYTE	  48H,TT,AA,KK,AA,YY,AA,SP,II,MM,AA,MM,UU,RR,AA,0,18H
		BYTE	  44H,MM,AA,SS,AA,NN,AA,OO,SP,AA,RR,II,MM,OO,TT,OO,0,18H
		BYTE	  58H,NN,AA,OO,KK,II,SP,MM,OO,RR,II,0,0F0H
		BYTE	  48H,CC,OO,NN,GG,RR,AA,TT,UU,LL,AA,TT,II,OO,NN,SS,6CH,0,18H
		BYTE	  58H,WW,EE,SP,HH,OO,PP,EE,SP,YY,OO,UU,0,10H
		BYTE	  40H,TT,RR,YY,SP,6AH,MM,AA,SS,TT,EE,RR,6AH,LL,EE,VV,EE,LL,LN,0,20H
		BYTE	  50H,SS,EE,EE,SP,YY,OO,UU,SP,AA,GG,AA,II,NN,LN,0,60H
		BYTE	  0
;
;************************************************************************
;*		 Special demo stick data				*
;************************************************************************
;
Demo_stick_wld1 BYTE	  09FH,09FH,09FH,09FH,09FH,09FH,09FH,09FH
		BYTE	  08FH,0DFH,048H,09FH,098H,0B4H,087H,09FH
		BYTE	  09FH,09FH,09FH,09FH,09FH,09FH,09FH,0DFH
		BYTE	  08FH,0CFH,090H,0C8H,09FH,09FH,09FH,09FH
		BYTE	  084H,0DFH,0C8H,09FH,09FH,08FH,038H,0DFH
		BYTE	  09FH,087H,0BFH,030H,09FH,090H,0C8H,09FH
		BYTE	  018H,05FH,0C7H,050H,03FH,03FH,02FH,098H
		BYTE	  0CAH,09FH,09FH,09FH,09FH,09FH,09FH,008H
		BYTE	  0DAH,09FH,088H,05FH,054H,098H,037H,058H
		BYTE	  09FH,0C5H,09FH,09FH,09FH,09FH,09FH,09FH
		BYTE	  09FH,09FH,09FH,09FH,088H,0DFH,048H,09FH
		BYTE	  098H,0B4H,087H,09FH,09FH,09FH,09FH,000H
;-----------------------------------------------------------------------
Demo_stick_wld2 BYTE	  09FH,09FH,09FH,09FH,09FH,09FH,09FH,09FH
		BYTE	  09FH,09FH,083H,0B8H,09FH,09FH,090H,0ADH
		BYTE	  09FH,09FH,09FH,09FH,09FH,087H,0BFH,09FH
		BYTE	  098H,0DFH,09FH,03FH,0B8H,03FH,0B8H,018H
		BYTE	  05FH,0C1H,09FH,09FH,09FH,09FH,084H,01FH
		BYTE	  05FH,05FH,05FH,03FH,0B0H,038H,08FH,05FH
		BYTE	  05FH,05FH,09FH,08FH,0BFH,0BFH,0BFH,09FH
		BYTE	  09FH,0DFH,0C2H,09FH,09FH,0BFH,0BFH,0C8H
		BYTE	  0BFH,01FH,010H,0DFH,0C4H,09FH,09FH,098H
		BYTE	  0DFH,0C4H,09FH,09FH,09FH,08FH,0BFH,0A8H
		BYTE	  09FH,09FH,09FH,0BFH,0A6H,09FH,09FH,09FH
		BYTE	  09FH,09FH,09FH,09FH,09FH,09FH,09FH,000H
;-----------------------------------------------------------------------
Demo_stick_wld3 BYTE	  09FH,09FH,09FH,09FH,09FH,09FH,09FH,09FH
		BYTE	  09FH,09FH,09FH,09FH,09FH,09FH,09FH,09FH
		BYTE	  09FH,09FH,0C8H,0AEH,09FH,03FH,03FH,031H
		BYTE	  09FH,088H,0CFH,09FH,09FH,09FH,09FH,09FH
		BYTE	  086H,0D6H,01FH,018H,03FH,03FH,05FH,050H
		BYTE	  0DFH,0BFH,09FH,0D0H,09FH,08CH,0B8H,0DFH
		BYTE	  09FH,005H,0BCH,09FH,09FH,09FH,09FH,09FH
		BYTE	  09FH,098H,03FH,03FH,03FH,050H,090H,010H
		BYTE	  05FH,05FH,05FH,09FH,0BFH,09FH,099H,030H
		BYTE	  0A6H,030H,09FH,090H,0BFH,0B4H,090H,05FH
		BYTE	  0DFH,09FH,03FH,0AFH,09FH,010H,050H,01FH
		BYTE	  0DAH,09FH,0D8H,09FH,09FH,000H
;
;************************************************************************
;*		 My car handle data					*
;************************************************************************
;
;
Handle_data	EQU	  $
;		speed	  00 05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90
;---------------------------------------------------------------------------------
Handle_data_0	BYTE	  22,22,26,33,34,35,36,37,38,39,40,41,42,43,44,44,44,44,44
Handle_data_1	BYTE	  22,22,26,33,34,35,36,37,38,39,40,40,40,40,40,40,40,40,40
Handle_data_2	BYTE	  22,22,26,33,34,35,36,37,38,39,40,41,42,43,44,45,45,45,45
Handle_data_3	BYTE	  22,22,26,33,34,35,36,37,38,39,40,41,42,42,42,42,42,42,42
;							( X/8 angle ( 32 = 1 angle ))
;
;---------------------------------------------------------------------------------
Dash_handle	BYTE	  22,22,26,32,32,32,32,32,32,32,32,32,32,32,32,32
		BYTE	  32,30,28,26,24,22,20,18,16,14,12,10,08,06,04,02,00
;							( X/8 angle ( 32 = 1 angle ))
;
Slide_data	EQU	  $
;		speed	   00	05   10	  15   20   25	 30   35   40	45
;		speed	   50	55   60	  65   70   75	 80   85   90
;-----------------------------------------------------------------------
		BYTE	  011H,013H,014H,015H,016H,017H,018H,019H,01AH,01BH
		BYTE	  01CH,01DH,01EH,01FH,020H,021H,022H,022H,022H
		BYTE	  022H,022H,021H,021H,020H,020H,019H,019H,018H
		BYTE	  018H,017H,017H
;
;************************************************************************
;*		 Car accele data					*
;************************************************************************
;
Turbo_data	EQU	  $
;		speed	  00 05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90
;---------------------------------------------------------------------------------
Turbo_data_0	BYTE	  64,64,64,64,64,64,64,64,64,32,32,32,32,16,08,04,01,01,00
Turbo_data_1	BYTE	  64,64,64,64,64,64,64,64,64,32,32,32,32,16,08,04,01,01,00
Turbo_data_2	BYTE	  64,64,64,64,64,64,48,48,48,32,32,24,20,12,06,04,01,01,00
Turbo_data_3	BYTE	  96,96,96,96,96,96,96,80,80,80,72,64,48,32,16,08,01,01,00
Turbo_data_4	BYTE	  64,64,64,64,64,64,32,32,32,32,32,16,09,05,02,01,01,01,00
;								       ( X/256 dot )
;		speed	  00 05 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90
;---------------------------------------------------------------------------------
Brake_data	BYTE	  22,22,22,22,20,18,16,14,14,14,14,14,14,14,14,14,14,14,14
		BYTE	  15,16,17,18,19,20,21,22,23,24,25,26,27
;								       ( X/256 dot )
		END
