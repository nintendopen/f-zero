;************************************************************************
;*	 SET_BACK	   -- back screen set module --			*
;************************************************************************
;
		INCLUDE	  RP5A22:
		INCLUDE	  RP5C77:
		INCLUDE	  VARIABLE
		INCLUDE	  BUFFER
		INCLUDE	  ALPHA
;
;=============== Cross reference =======================================
;
		EXT	  Start_DMA,Write_VRAM,Expand_back,Special_roll
		EXT	  Clear_carcolor,Reset_carcolor,Laser_spark
;-----------------------------------------------------------------------
		EXT	  Print_OBJsrn,Print_OBJmsg
		EXT	  Print_OBJchr,Put_OBJchr
		EXT	  string_table,string_pointer
		EXT	  cursor_pos_h,cursor_pos_v
		EXT	  car_selecta
;
;=============== Cross definition ======================================
;
		GLB	  Init_backscrn,Init_forescrn
		GLB	  Move_backscrn
		GLB	  Meter_initial1,Meter_initial2,Set_mycar_style
		GLB	  Meter_control,Display_shield,Display_score,Disp_repair_cnt
		GLB	  Display_course1,Display_course2,Display_course3
		GLB	  Display_message,Display_retry,Disp_rank
		GLB	  Display_READY,Display_GO,Clear_GO,Set_rank_color
		GLB	  Set_rank_window,Frash_rank_wind,Set_small_char
		GLB	  Shadow_window_1,Shadow_window_2,Shadow_window_3
		GLB	  Back_screen
;
;=============== Define local variable =================================
;
work0		EQU	  0000H
work1		EQU	  0002H
back_angle	EQU	  0010H		      ; 2 byte :Back screen angle
prtmsg_pos_h	EQU	  0020H		      ; 2 byte :Message print position h
prtmsg_pos_v	EQU	  0022H		      ; 2 byte :Message print position v
;external	EQU	  0038H		      ; 8 byte :External work
;
;=======================================================================
;
		PROG
		EXTEND
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\  Back screen initialize routine \\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Initialize back screen					*
;************************************************************************
;
		MEM8
		IDX8
Init_backscrn
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Initial BG1,BG2 register ==============================
Init_baksrn100
		LDA	  #01111001B
		STA	  !Screen_bank+0      ; Set BG1 screen base address
		LDA	  #01110001B
		STA	  !Screen_bank+1      ; Set BG2 screen base address
		LDA	  #01100110B
		STA	  !Screen_segment+0   ; Set BG1,BG2 name base address
;
;=============== Set BG1,BG2 character =================================
Init_baksrn200
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDX	  #10000000B
		STX	  !Screen_step	      ; Set VRAM inc. mode
		LDA	  #6000H
		STA	  !Screen_address     ; Set VRAM address
		LDA	  #Backchar_DMA
		JSR	  Start_DMA	      ; Start DMA
;-----------------------------------------------------------------------
		LDA	  #7800H
		STA	  !Screen_address
		LDX	  #16
Init_baksrn210	STZ	  !Screen_write	      ; Set see through character
		DEX
		BNE	  Init_baksrn210
;
;=============== Set BG1 screen ========================================
Init_baksrn400
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  !course_number
		ASL	  A
		ASL	  A
		TAX			      ; IX = data pointer
;-----------------------------------------------------------------------
		IDX16
		REP	  #00010000B	      ; Index 16 bit mode
		LDY	  !Back_screen+0,X
		PHX
		PHK
		JSR	  Expand_back	      ; Expand back screen
;-----------------------------------------------------------------------
		LDX	  #7880H
		STX	  !Screen_address     ; Set VRAM address
		LDX	  #1801H
		STX	  !DMA_0+0	      ; Set DMA ctrl & B address
		LDX	  #3000H
		STX	  !DMA_0+2	      ; Set DMA A address
		LDA	  #7FH
		STA	  !DMA_0+4	      ; Set DMA A bank
		LDX	  #0700H
		STX	  !DMA_0+5	      ; Set DMA data length
		LDA	  #00000001B
		STA	  !DMA_burst	      ; Start DMA channel #0
;-----------------------------------------------------------------------
		LDX	  #7C80H
		STX	  !Screen_address     ; Set VRAM address
		LDX	  #31C0H
		STX	  !DMA_0+2	      ; Set DMA A address
		LDX	  #0540H
		STX	  !DMA_0+5	      ; Set DMA data length
		STA	  !DMA_burst	      ; Start DMA channel #0
;-----------------------------------------------------------------------
		LDX	  #3020H
		STX	  !DMA_0+2	      ; Set DMA A address
		LDX	  #01C0H
		STX	  !DMA_0+5	      ; Set DMA data length
		STA	  !DMA_burst	      ; Start DMA channel #0
;
;=============== Set BG2 screen ========================================
Init_baksrn500
		PLX
		LDY	  !Back_screen+2,X
		PHK
		JSR	  Expand_back	      ; Expand back screen
;-----------------------------------------------------------------------
		LDX	  #7160H
		STX	  !Screen_address     ; Set VRAM address
		LDX	  #3000H
		STX	  !DMA_0+2	      ; Set DMA A address
		LDA	  #7FH
		STA	  !DMA_0+4	      ; Set DMA A bank
		LDX	  #0540H
		STX	  !DMA_0+5	      ; Set DMA data length
		LDA	  #00000001B
		STA	  !DMA_burst	      ; Start DMA channel #0
;-----------------------------------------------------------------------
		LDX	  #7560H
		STX	  !Screen_address     ; Set VRAM address
		LDX	  #31C0H
		STX	  !DMA_0+2	      ; Set DMA A address
		LDX	  #0380H
		STX	  !DMA_0+5	      ; Set DMA data length
		STA	  !DMA_burst	      ; Start DMA channel #0
;-----------------------------------------------------------------------
		LDX	  #3000H
		STX	  !DMA_0+2	      ; Set DMA A address
		LDX	  #01C0H
		STX	  !DMA_0+5	      ; Set DMA data length
		STA	  !DMA_burst	      ; Start DMA channel #0
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== DMA parameter =========================================
;
Backchar_DMA	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  18H		      ; DMA B address low
		WORD	  0E000H	      ; DMA A address low,high
		BYTE	  0FH		      ; DMA A bank
		WORD	  2000H		      ; Number of data
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Fore screen initialize routines \\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Initialize fore screen entry				*
;************************************************************************
;
		MEM8
		IDX8
Init_forescrn
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		JSR	  Set_scroll_DMA      ; Initialize scroll DMA
		JSR	  Game_window	      ; Initialize game window
		JSR	  Gradation	      ; Initialize gradation
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Set scroll H_DMA					*
;************************************************************************
;
		MEM8
		IDX8
Set_scroll_DMA
		LDA	  #01H
		STA	  !scroll_table_L1+00H
		STA	  !scroll_table_L1+10H
		STA	  !scroll_table_L2+00H
		STA	  !scroll_table_L2+10H
		STZ	  !scroll_table_en+00H
		STZ	  !scroll_table_en+10H
;
;=============== Initialize window HDMA ================================
Set_scrlDMA200
		LDX	  #04H
Set_scrlDMA210
		LDA	  !Scroll_HDMA,X
		STA	  !DMA_1,X	      ; Set DMA #2 parameter
		DEX
		BPL	  Set_scrlDMA210
		STZ	  !DMA_1+7	      ; Set Data bank
		RTS
;
;=============== Scroll HDMA parameter =================================
;
Scroll_HDMA	BYTE	  00000011B	      ; DMA control parameter
		BYTE	  0DH		      ; DMA B address low
		BYTE	  LOW  scroll_table_1 ; DMA A address low
		BYTE	  HIGH scroll_table_1 ; DMA A address high
		BYTE	  BANK scroll_table_1 ; DMA A address bank
;
;************************************************************************
;*		 Initialize game window					*
;************************************************************************
;
		MEM8
		IDX8
;
;=============== Set game window table =================================
Game_window
		LDX	  !Window_length
Game_window110
		LDA	  !Window_data_0,X
		STA	  !window_table,X
		DEX
		BPL	  Game_window110
;
;=============== Initialize window HDMA ================================
Game_window200
		LDX	  #04H
Game_window210	LDA	  !Window_HDMA,X
		STA	  !DMA_2,X	      ; Set DMA #2 parameter
		DEX
		BPL	  Game_window210
;-----------------------------------------------------------------------
		LDA	  #08H
		STA	  !DMA_2+7	      ; Set Data bank
;
;=============== Initialize register ===================================
Game_window300
		LDA	  #00101000B
		STA	  <window_control+2   ; color window 1 inside on
		LDA	  #40H
		STZ	  !Window_range+2     ; window 2 left position
		STA	  !Window_range+3     ; window 2 right position
		LDA	  #10100001B
		STA	  <spot_switch	      ; Set spot color add/sub switch
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; memory 16 bit mode
		LDA	  #window_table
		STA	  <window_address     ; Set window table address
		LDA	  #Shadow_window_1&0FFFFH
		STA	  !shadow_address     ; Set shadow data address
		STZ	  <window_control+0
		STZ	  !Window_pile	      ; Set window logic ( OR )
		STZ	  <window_main	      ; window main/sub off
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Window HDMA parameter =================================
;
Window_HDMA	BYTE	  01000001B	      ; DMA control parameter
		BYTE	  26H		      ; DMA B address low
		BYTE	  LOW  window_table   ; DMA A address low
		BYTE	  HIGH window_table   ; DMA A address high
		BYTE	  BANK window_table   ; DMA A address bank
;
;************************************************************************
;*		 Initialize gradation					*
;************************************************************************
;
		MEM8
		IDX8
Gradation
		LDA	  <color_addsub	      ; set gradation mode
		BMI	  Gradation150	      ; if ( sub mode )
;-----------------------------------------------------------------------
		LDX	  #04H
Gradation110	LDA	  !Gradation_HDMA1,X
		STA	  !DMA_3,X	      ; Set HDMA #3 parameter
		DEX
		BPL	  Gradation110
		RTS
;-----------------------------------------------------------------------
Gradation150	LDX	  #04H
Gradation160	LDA	  !Gradation_HDMA2,X
		STA	  !DMA_3,X	      ; Set HDMA #3 parameter
		DEX
		BPL	  Gradation160
		RTS
;
;=============== Gradation DMA parameter ===============================
;
Gradation_HDMA1 BYTE	  00000000B	      ; DMA control parameter
		BYTE	  32H		      ; DMA B address low
		BYTE	  LOW  Gradation_add  ; DMA A address low
		BYTE	  HIGH Gradation_add  ; DMA A address high
		BYTE	  BANK Gradation_add  ; DMA A address bank
;
Gradation_HDMA2 BYTE	  00000000B	      ; DMA control parameter
		BYTE	  32H		      ; DMA B address low
		BYTE	  LOW  Gradation_sub  ; DMA A address low
		BYTE	  HIGH Gradation_sub  ; DMA A address high
		BYTE	  BANK Gradation_sub  ; DMA A address bank
;
;************************************************************************
;*		 Move back screen					*
;************************************************************************
;
		MEM8
		IDX8
Move_backscrn
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Check exception process ===============================
Move_baksrn100
		LDA	  <exception_flag
		LSR	  A		      ; CY = exception flag
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !car_angle
		BCC	  Move_baksrn500      ; if ( normal mode )
;-----------------------------------------------------------------------
		LDA	  <exception_flag
		AND	  #0000000000001000B  ; Camera pan mode ?
		BNE	  Move_baksrn300      ; yes.
;
;=============== Camera horizontal scroll ==============================
Move_baksrn200
		LDA	  !car_locatex_h
		LSR	  A
		CLC
		ADC	  !car_locatex_h
		SEC
		SBC	  <back_zelo_angle
		BCS	  Move_baksrn500
		ADC	  #192*256
		BRA	  Move_baksrn500
;
;=============== Camera pan ============================================
Move_baksrn300
		LDA	  <back_offs_angle
		CLC
		ADC	  !car_angle
		CMP	  #192*256
		BCC	  Move_baksrn500
		SBC	  #192*256
;
;=============== Set BG1 scroll ========================================
Move_baksrn500
		STA	  <back_angle
		STA	  !Dividend
		LDX	  #03H
		STX	  !Divisor
		NOP
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  <scroll_selector
		EOR	  #00010000B
		STA	  <scroll_selector
		TAY
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		NOP
;-----------------------------------------------------------------------
		LDA	  !Divide
		LSR	  A
		LSR	  A
		LSR	  A
		EOR	  #0FFFFH
		SEC			      ; ( = NOT	   A )
		ADC	  !Divide
;-----------------------------------------------------------------------
		LSR	  A
		LSR	  A
		LSR	  A
		LSR	  A
		PHA
		AND	  #00FFH
		STA	  <BG1_scroll_h	      ; Set BG2 scroll H
		STA	  !scroll_table_H1,Y  ; Set BG2 scroll H ( for H-DMA )
;-----------------------------------------------------------------------
		PLA
		XBA
		AND	  #0003H
		TAX
		LDA	  !BG1_vertical,X
		AND	  #00FFH
		STA	  <BG1_scroll_v	      ; Set BG2 scroll V
		STA	  !scroll_table_V1,Y  ; Set BG2 scroll V ( for H-DMA )
;
;=============== Set BG2 scroll ========================================
Move_baksrn600
		LDA	  <back_angle+0
		ASL	  A
		ASL	  A
		XBA
		AND	  #00FFH
		STA	  <BG2_scroll_h	      ; Set BG1 scroll V
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  <back_angle+1
		ASL	  A
		ROL	  A
		ROL	  A
		AND	  #00000011B
		TAX
		LDA	  !BG2_vertical,X
		STA	  <BG2_scroll_v+0     ; Set BG1 scroll V low
		STZ	  <BG2_scroll_v+1     ; Set BG1 scroll V high
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=======================================================================
;
BG1_vertical	BYTE	  024H
BG2_vertical	BYTE	  05CH,094H,0CCH
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Meter display routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Meter initialize 1					*
;************************************************************************
;
		MEM8
		IDX8
Meter_initial1
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Initial BG3 register ==================================
Meter_init100
		LDA	  #01110100B
		STA	  !Screen_bank+2      ; Set BG3 screen base address
		LDA	  #00000111B
		STA	  !Screen_segment+1   ; Set BG3 name base address
;-----------------------------------------------------------------------
		STZ	  !Scroll_H+4	      ; Set BG3 scroll h
		STZ	  !Scroll_H+4
		STZ	  !Scroll_V+4	      ; Set BG3 scroll v
		STZ	  !Scroll_V+4
;
;=============== Set BG3 character =====================================
Meter_init200
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDX	  #10000000B
		STX	  !Screen_step	      ; Set VRAM inc. mode
		LDA	  #7000H
		STA	  !Screen_address     ; Set VRAM address
		LDA	  #Meterchar_DMA1
		JSR	  Start_DMA	      ; Start DMA
;-----------------------------------------------------------------------
		LDA	  #7C00H
		STA	  !Screen_address     ; Set VRAM address
		LDA	  #Meterchar_DMA2
		JSR	  Start_DMA	      ; Start DMA
;
;=============== Clear BG3 screen ======================================
Meter_init300
		LDA	  #2100H
		STA	  !safe_rank_char
		STA	  <00H		      ; Set clear code
;-----------------------------------------------------------------------
		LDX	  #10000000B
		STX	  !Screen_step	      ; Set VRAM address auto inc.
		LDA	  #7400H
		STA	  !Screen_address     ; Set VRAM address
		LDA	  #Meterclr_DMA1
		JSR	  Start_DMA	      ; Start DMA
;-----------------------------------------------------------------------
		LDX	  #00000000B
		STX	  !Screen_step	      ; Set VRAM address auto inc.
		LDA	  #7400H
		STA	  !Screen_address     ; Set VRAM address
		LDA	  #Meterclr_DMA2
		JSR	  Start_DMA	      ; Start DMA
;
;=============== Initial BG3 screen ====================================
Meter_init400
		LDX	  #10000000B
		STX	  !Screen_step	      ; Set VRAM address auto inc.
		LDA	  #7440H
		STA	  !Screen_address     ; Set VRAM address
		LDA	  #Meterscrn_DMA
		JSR	  Start_DMA	      ; Start DMA
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDY	  #07FH
		LDX	  #0FEH
;-----------------------------------------------------------------------
Meter_init410	LDA	  >Meter_screen,X
		STA	  !meter_buffer,Y     ; Set meter buffer
		DEX
		DEX
		DEY
		BPL	  Meter_init410
;
;=============== Display best time =====================================
Meter_init500
		LDA	  <play_mode	      ; free run mode ?
		BEQ	  Meter_init550	      ; no.
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  #7462H
		STA	  !Screen_address     ; Set VRAM address
		LDA	  #Best_time_DMA1
		JSR	  Start_DMA	      ; Start DMA
		LDA	  #7482H
		STA	  !Screen_address     ; Set VRAM address
		LDA	  #Best_time_DMA2
		JSR	  Start_DMA	      ; Start DMA
		LDA	  #74A2H
		STA	  !Screen_address     ; Set VRAM address
		LDA	  #Best_time_DMA3
		JSR	  Start_DMA	      ; Start DMA
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mide
		LDA	  #27H
		STA	  !meter_buffer+24H
		INC	  A
		STA	  !meter_buffer+25H
		INC	  A
		STA	  !meter_buffer+26H
		INC	  A
		STA	  !meter_buffer+43H
		INC	  A
		STA	  !meter_buffer+46H
		JSR	  Disp_best_time
;-----------------------------------------------------------------------
		LDA	  #11111100B
		STA	  !OAM_sub+05H
		BRA	  Meter_init600
;-----------------------------------------------------------------------
Meter_init550	JSR	  Initial_score
		LDA	  #25H
		STA	  !safe_rank_char+1
		LDA	  #8AH
		STA	  !safe_rank_char+0

;=============== Initial shield meter ==================================
Meter_init600
		LDA	  #0B0H
		STA	  !shield_meter+0     ; Set left value of window
		LDA	  #0EFH
		STA	  !shield_meter+1     ; Set right value of window
		JSR	  Change_color200     ; Set shield meter color
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== DMA parameter =========================================
;
Meterchar_DMA1	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  18H		      ; DMA B address low
		WORD	  0C000H	      ; DMA A address low,high
		BYTE	  0CH		      ; DMA A bank
		WORD	  02C0H		      ; Number of data
;
Meterchar_DMA2	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  18H		      ; DMA B address low
		WORD	  0C2C0H	      ; DMA A address low,high
		BYTE	  0CH		      ; DMA A bank
		WORD	  00C0H		      ; Number of data
;
Meterscrn_DMA	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  18H		      ; DMA B address low
		WORD	  Meter_screen&0FFFFH	      ; DMA A address low,high
		BYTE	  BANK Meter_screen   ; DMA A bank
		WORD	  0100H		      ; Number of data
;
Meterclr_DMA1	BYTE	  00001000B	      ; DMA control parameter
		BYTE	  19H		      ; DMA B address low
		WORD	  00001H	      ; DMA A address low,high
		BYTE	  00H		      ; DMA A bank
		WORD	  0100H		      ; Number of data
;
Meterclr_DMA2	BYTE	  00001000B	      ; DMA control parameter
		BYTE	  18H		      ; DMA B address low
		WORD	  00000H	      ; DMA A address low,high
		BYTE	  00H		      ; DMA A bank
		WORD	  0100H		      ; Number of data
;
Best_time_DMA1	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  18H		      ; DMA B address low
		WORD	  Best_screen_1	      ; DMA A address low,high
		BYTE	  BANK Best_screen_1  ; DMA A bank
		WORD	  000AH		      ; Number of data
;
Best_time_DMA2	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  18H		      ; DMA B address low
		WORD	  Best_screen_2	      ; DMA A address low,high
		BYTE	  BANK Best_screen_2  ; DMA A bank
		WORD	  000EH		      ; Number of data
;
Best_time_DMA3	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  18H		      ; DMA B address low
		WORD	  Best_screen_3	      ; DMA A address low,high
		BYTE	  BANK Best_screen_3  ; DMA A bank
		WORD	  000CH		      ; NUmber of data
;
;************************************************************************
;*		 Meter initialize 2					*
;************************************************************************
;
		MEM8
		IDX8
Meter_initial2
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Initialize meter object ===============================
Meter_init800
		LDA	  #0FFH
		STA	  <message_pointer    ; Reset message pointer
		LDA	  #050H
		STA	  <message_length     ; Set message length max
		JSR	  Clear_message	      ; Clear message
		JSR	  MeterOAM_init	      ; Meter OAM initialize
		JSR	  Change_color200     ; Set shield meter color
;-----------------------------------------------------------------------
		LDA	  #0FFH
		STA	  !mycar_rank_flag    ; change my car rank ?
		STZ	  !rank_disp_count    ; Clear rank display counter
		JSR	  Disp_rank	      ; Display rank
;
;=============== Initialize map point ==================================
Meter_init900
		LDY	  <mycar_number
		LDA	  !Dot_attribute,Y
		STA	  !OAM_main+53H	      ; Set my car point attribute
		LDA	  #00H
		JSR	  Display_map2	      ; Map point display
;-----------------------------------------------------------------------
		LDY	  !rival_number
		LDA	  !Dot_attribute,Y
		STA	  !OAM_main+57H	      ; Set my car point attribute
		LDA	  #02H
		JSR	  Display_map2	      ; Map point display
;-----------------------------------------------------------------------
		PLP
		RTS
;=======================================================================
Dot_attribute	BYTE	  00110111B,00110001B,00110101B,00110011B
;
;************************************************************************
;*		 Initial meter OAM					*
;************************************************************************
;
		MEM16
		IDX8
MeterOAM_init
		REP	  #00100000B	      ; Memory 16 bit mode
		LDX	  #7EH
MeterOAM_init10 LDA	  >Meter_OAM_main,X
		STA	  !OAM_main+50H,X
		DEX
		DEX
		BPL	  MeterOAM_init10
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDX	  #0CH
MeterOAM_init20 LDA	  >Meter_OAM_sub,X
		STA	  !OAM_sub,X
		DEX
		BPL	  MeterOAM_init20
;-----------------------------------------------------------------------
		LDA	  #01H
		STA	  <spark_counter
		JSR	  Laser_spark
		LDA	  #11101010B
		STA	  !OAM_sub+10H
;-----------------------------------------------------------------------
		LDA	  <play_mode	      ; free run mode ?
		BEQ	  MeterOAM_init40     ; no.
		LDA	  #11111100B
		STA	  !OAM_sub+05H	      ; Clear retry counter
		LDA	  #11111111B
		STA	  !OAM_sub+0CH	      ; Clear RANK display
		LDA	  !rival_number
		BMI	  MeterOAM_init30
		LDA	  #00001100B
		TRB	  !OAM_sub+05H	      ; Dislay rival dot
MeterOAM_init30 RTS
;-----------------------------------------------------------------------
MeterOAM_init40 LDY	  <mycar_number
		LDA	  !Car_style_color,Y
		STA	  !OAM_main+5BH
;-----------------------------------------------------------------------
;		LDX	  <game_level
;		LDA	  >Time_disp_color,X
;		STA	  >OAM_main+5FH
;		LDX	  #2CH
;eterOAM_init50 STA	  >OAM_main+83H,X
;		DEX
;		DEX
;		DEX
;		DEX
;		BPL	  MeterOAM_init50
		RTS
;-----------------------------------------------------------------------
Car_style_color BYTE	  00111001B,00111011B,00111101B,00111111B
;
;=============== Meter OAM main data ===================================
Meter_OAM_main	EQU	  0BEC34H
Map_OAM_main	EQU	  0BEC44H
Meter_OAM_sub	EQU	  0BECB4H
;
;************************************************************************
;*		 Meter control						*
;************************************************************************
;
		MEM8
		IDX8
Meter_control
		PHP
		SEP	  #00110000B
		LDA	  <meter_switch
		BMI	  Meter_control20
;-----------------------------------------------------------------------
		JSR	  Disp_speed
		JSR	  Display_score
		JSR	  Change_color
		LDA	  !special_demo	      ; special demo ?
		BNE	  Meter_control20     ; yes.
		LDA	  <exception_flag
		BIT	  #00010000B	      ; ending demo ?
		BNE	  Meter_control20
		BIT	  #11101100B	      ; other exception ?
		BNE	  Meter_control10     ; yes.
		JSR	  Display_map
		JSR	  Disp_rank
Meter_control10 JSR	  Disp_time
;-----------------------------------------------------------------------
Meter_control20 PLP
		RTS
;
;************************************************************************
;*		 Display time						*
;************************************************************************
;
		MEM8
		IDX8
Disp_time
		LDX	  #07AH
		LDA	  <time_counter+0
		JSR	  Disp_time210
		LDX	  #08EH
		LDA	  <time_counter+1
		JSR	  Disp_time200
		LDX	  #0A2H
		LDA	  <time_counter+2
;
;=============== Display number ========================================
Disp_time200
		TAY
		LSR	  A
		LSR	  A
		LSR	  A
		LSR	  A
		CLC
		ADC	  #080H
		STA	  !OAM_main+0,X
		ADC	  #010H
		STA	  !OAM_main+4,X
		TYA
;-----------------------------------------------------------------------
Disp_time210	AND	  #0FH
		CLC
		ADC	  #080H
		STA	  !OAM_main+8,X
		ADC	  #010H
		STA	  !OAM_main+12,X
Disp_rank999	RTS
;
;************************************************************************
;*		 Display rank						*
;************************************************************************
;
		MEM8
		IDX8
Disp_rank
		LDA	  !mycar_rank
		CMP	  #21		      ; rank >= 21 ?
		BCC	  Disp_rank10	      ; no.
		LDA	  #20
;-----------------------------------------------------------------------
Disp_rank10	STA	  !Dividend+0
		STZ	  !Dividend+1
		LDA	  #10
		STA	  !Divisor
;-----------------------------------------------------------------------
		LDA	  <play_mode	      ; free run mode ?
		BNE	  Disp_rank999	      ; yes.
;
;=============== Check rank change =====================================
Disp_rank100
		LDA	  <exception_flag
		BNE	  Disp_rank110
		LDA	  !mycar_rank_flag    ; change my car rank ?
		BEQ	  Disp_rank300	      ; no.
;-----------------------------------------------------------------------
Disp_rank110	LDA	  #00100000B
		TSB	  !alarm_priority     ; Set SOUND
		STZ	  !mycar_rank_flag    ; Clear rank trigger
;
;=============== Display rank number ===================================
Disp_rank200
		CLC
		LDA	  !Divide+0
		BEQ	  Disp_rank210
;-----------------------------------------------------------------------
		ADC	  #80H
		STA	  !OAM_main+0C2H
		ADC	  #10H
		STA	  !OAM_main+0C6H
;-----------------------------------------------------------------------
		STZ	  <rank_display_sw    ; Set OAM sub ( 4 char )
		BRA	  Disp_rank220
Disp_rank210	LDA	  #00001111B
		STA	  <rank_display_sw    ; Set OAM sub ( 2 char )
;-----------------------------------------------------------------------
Disp_rank220	LDA	  !Rest+0
		ADC	  #80H
		STA	  !OAM_main+0CAH
		ADC	  #10H
		STA	  !OAM_main+0CEH
;
;=============== Check display rank ====================================
Disp_rank300
		JSR	  Disp_rank900
		LDY	  !mycar_rank
		CPY	  #03H		      ; Goal clear rank ?
		BCC	  Disp_rank310	      ; yes.
		CPY	  !clear_rank	      ; My car rank out ?
		BCS	  Disp_rank330	      ; yes.
		INY
		INY
		CPY	  !clear_rank	      ; My car rank warning ?
		BCS	  Disp_rank320	      ; yes.
;------------------------------------------------------------ Rank in --
Disp_rank310	LDY	  #00110001B	      ; IY = attribute ( Green )
		LDA	  #00H
		STA	  !rank_disp_count
		BRA	  Disp_rank400	      ; Display rank
;------------------------------------------------------- Rank worning --
Disp_rank320	LDA	  !rank_warnning
		BNE	  Disp_rank325
		LDA	  #00100000B
		TSB	  <message_flag
Disp_rank325	INC	  !rank_disp_count
		LDA	  !rank_disp_count
		LDY	  #00110101B	      ; IY = attribute ( Yellow )
		CMP	  #09H
		BCC	  Disp_rank500	      ; Clear rank
		CMP	  #29H
		BCC	  Disp_rank400	      ; Display rank
		STZ	  !rank_disp_count
		BRA	  Disp_rank500	      ; Clear rank
;----------------------------------------------------------- Rank out --
Disp_rank330	LDA	  !rank_warnning
		BNE	  Disp_rank335
		LDA	  #00100000B
		TSB	  <message_flag
Disp_rank335	INC	  !rank_disp_count
		LDA	  !rank_disp_count
		LDY	  #00110011B	      ; IY = attribute ( Red )
		CMP	  #09H
		BCC	  Disp_rank500	      ; Clear rank
		CMP	  #19H
		BCC	  Disp_rank400	      ; Display rank
		STZ	  !rank_disp_count
		BRA	  Disp_rank500	      ; Clear rank
;
;=============== Display rank ==========================================
Disp_rank400
		CMP	  #0AH
		BNE	  Disp_rank410
		LDA	  #00010000B
		TSB	  !alarm_priority     ; Set SOUND
;-----------------------------------------------------------------------
Disp_rank410	STY	  !OAM_main+0C3H
		STY	  !OAM_main+0C7H
		STY	  !OAM_main+0CBH
		STY	  !OAM_main+0CFH
		LDA	  <rank_display_sw
		STA	  !OAM_sub+0CH	      ; Set OAM sub
		RTS
;
;=============== Clear rank ============================================
Disp_rank500
		LDA	  #11111111B
		STA	  !OAM_sub+0CH	      ; Reset OAM sub
Disp_rank590	RTS
;
;=============== Frashing safe rank ====================================
Disp_rank900
		LDA	  <frame_counter
		AND	  #00011111B
		BNE	  Disp_rank910
;-----------------------------------------------------------------------
		LDA	  !safe_rank_char+1
		EOR	  #00001100B
		STA	  !safe_rank_char+1
Disp_rank910	RTS
;
;************************************************************************
;*		 Display speed						*
;************************************************************************
;
		MEM8
		IDX8
Disp_speed
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Set display speed value ===============================
Disp_speed100
		LDX	  !anime_car_index
		LDY	  !car_speed+1,X
		STY	  !Multiplicand
		STY	  !Multiplier	      ; Calculate H * H
		NOP			      ; 2 cycle
		NOP			      ; 2 cycle
		LDY	  !car_speed+0,X      ; 4 cycle
		LDA	  !Multiply+0
;-----------------------------------------------------------------------
		STY	  !Multiplicand
		STY	  !Multiplier	      ; Calculate L * L
		XBA			      ; 2 cycle
		NOP			      ; 2 cycle
		LDY	  !car_speed+1,X      ; 4 cycle
		LDA	  !Multiply+1
;-----------------------------------------------------------------------
		STY	  !Multiplier	      ; Calculate H * L
		NOP			      ; 2 cycle
		MEM16
		REP	  #00100000B	      ; 3 cycle : Memory 16 bit mode
		LSR	  A		      ; 2 cycle
		CLC			      ; 2 cycle
		ADC	  !Multiply
;-----------------------------------------------------------------------
		LSR	  A
		LSR	  A
		CLC
		ADC	  !car_speed,X
		STA	  <work1
		LSR	  A
		LSR	  A
;
;=============== Display speed =========================================
Disp_speed200
		STA	  <work0
		XBA
		TAX
		CPX	  #0AH		      ; Meter over ?
		BCS	  Disp_speed300	      ; yes.
		STX	  !meter_buffer+58H   ; 100
;-----------------------------------------------------------------------
		LDA	  <work0
		AND	  #00FFH
		STA	  <work0
		LDA	  <work1
		AND	  #03FFH
		CLC
		ADC	  <work0
		ASL	  A
;
		STA	  <work0	      ; Acc *= 10
		XBA
		TAX
		STX	  !meter_buffer+59H   ; 10
;-----------------------------------------------------------------------
		LDA	  <work0
		AND	  #00FFH
		STA	  <work0
		ASL	  A
		ASL	  A
		ADC	  <work0
		ASL	  A		      ; Acc *= 10
;
		XBA
		TAX
		STX	  !meter_buffer+5AH   ; 1
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== Meter over flow =======================================
Disp_speed300
		LDX	  #09H
		STX	  !meter_buffer+58H   ; 100
		STX	  !meter_buffer+59H   ; 10
		STX	  !meter_buffer+5AH   ; 1
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Change meter color					*
;************************************************************************
;
		MEM8
		IDX16
Change_color
		LDA	  <mycar_damage+1
		CMP	  #02H		      ; damage < 200H ?
		BCS	  Change_color400     ; no.
;-----------------------------------------------------------------------
		LDA	  <frame_counter
		AND	  #00001111B	      ; red timming ?
		BEQ	  Change_color200     ; yes.
		CMP	  #00001000B	      ; white timming ?
		BNE	  Change_color300     ; no.
;
;=============== Set shield meter color WHITE ==========================
Change_color100
		REP	  #00010000B	      ; Index 16 bit mode
		LDA	  #00100011B
		STA	  !meter_addsub
		LDA	  #00111000B
		STA	  !meter_color_R
		LDA	  #01011000B
		STA	  !meter_color_G
		LDA	  #10011000B
		STA	  !meter_color_B
		LDX	  #0110001100011000B
		STX	  !color_buffer+06H
;-----------------------------------------------------------------------
		SEP	  #00010000B	      ; Index 8 bit mode
		RTS
;
;=============== Set shield meter color RED ============================
Change_color200
		LDA	  !course_colnum
		ASL	  A
		ASL	  A
		TAX
		LDA	  >Meter_wind_col+0,X
		STA	  !meter_addsub
		LDA	  >Meter_wind_col+1,X
		STA	  !meter_color_R
		LDA	  >Meter_wind_col+2,X
		STA	  !meter_color_G
		LDA	  >Meter_wind_col+3,X
		STA	  !meter_color_B
		REP	  #00010000B	      ; Index 16 bit mode
		LDX	  #0000000000011000B
		STX	  !color_buffer+06H
;-----------------------------------------------------------------------
Change_color300 SEP	  #00010000B	      ; Index 8 bit mode
		RTS
;
;=============== Check meter color =====================================
Change_color400
		LDA	  !meter_color_B
		CMP	  #10011000B	      ; color == white ?
		BEQ	  Change_color200     ; yes.
		RTS
;
;************************************************************************
;*		 Display map						*
;************************************************************************
;
		MEM8
		IDX8
Display_map
		LDA	  <frame_counter
Display_map2	AND	  #00000010B
		TAY			      ; IY = work pointer
		ASL	  A
		TAX			      ; IX = OAM pointer
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !car_locatey_h,Y
		ASL	  A
		XBA
		AND	  #003FH
		CLC
		ADC	  !map_offset_h
		STA	  !OAM_main+50H,X     ; Set position H
;-----------------------------------------------------------------------
		LDA	  !car_locatex_h,Y
		ASL	  A
		XBA
		AND	  #0003FH
		EOR	  #0FFFFH
		SEC
		ADC	  !map_offset_v
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		STA	  !OAM_main+51H,X     ; Set position V
		RTS
;
;************************************************************************
;*		 Display shield meter					*
;************************************************************************
;
		MEM8
		IDX8
Display_shield
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Set shield meter ======================================
Disp_shield100
		LDY	  <mycar_damage+1     ; Damage < 0 ?
		BMI	  Disp_shield200      ; yes.
		CPY	  #08H		      ; No damage ?
		BEQ	  Disp_shield300      ; yes.
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  <mycar_damage
		ASL	  A
		ASL	  A
		ASL	  A		      ; Acc = damage value ( CY = 0 )
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		XBA
		PHA
		ADC	  #0AFH
		STA	  !shield_meter+1     ; Set window right side
;-----------------------------------------------------------------------
		PLA
		AND	  #00000111B
		EOR	  #11111111B
		SEC
		ADC	  #18H
		STA	  !meter_buffer+16H,Y
		STA	  !meter_buffer+36H,Y
		CPY	  <shield_status
		BEQ	  Disp_shield190
		BCS	  Disp_shield120
;------------------------------------------------------- Shield down ---
Disp_shield110	LDA	  #19H
		STA	  !meter_buffer+17H,Y
		STA	  !meter_buffer+37H,Y
		BRA	  Disp_shield180
;------------------------------------------------------- Shield up -----
Disp_shield120	LDA	  #10H
		STA	  !meter_buffer+15H,Y
		STA	  !meter_buffer+35H,Y
;-----------------------------------------------------------------------
Disp_shield180	STY	  <shield_status
Disp_shield190	PLP
		RTS
;
;=============== Set shield meter 0 ====================================
Disp_shield200
		LDA	  #18H
		STA	  !meter_buffer+16H
		STA	  !meter_buffer+36H
		LDA	  #0B0H
		STA	  !shield_meter+1     ; Set parameter
;-----------------------------------------------------------------------
		STZ	  <shield_status      ; shield_status = 0
		PLP
		RTS
;
;=============== Set shield meter max ==================================
Disp_shield300
		LDA	  #10H
		STA	  !meter_buffer+1DH
		STA	  !meter_buffer+3DH
		LDA	  #0EFH
		STA	  !shield_meter+1     ; Set parameter
;-----------------------------------------------------------------------
		LDA	  #07H
		STA	  <shield_status      ; shield_status = 7
		PLP
		RTS
;
;************************************************************************
;*		 Display score						*
;************************************************************************
;
		MEM16
		IDX8
Initial_score
		PHP
		BRA	  Disp_score210
;-----------------------------------------------------------------------
Display_score	PHP
		REP	  #00100000B	      ; Memory 16 bit mode
		SEP	  #00011000B	      ; Index 8 bit,BCD mode
;-----------------------------------------------------------------------
		LDA	  !score_disp_buff
		CMP	  !score_buffer	      ; score chnage ?
		BEQ	  Disp_score290	      ; no.
;
;=============== Change display score ==================================
Disp_score100
		CLC
		ADC	  #0001H	      ; Score += 10
		STA	  !score_disp_buff
		CMP	  #1000H
		BEQ	  Disp_score110
		CMP	  #2000H
		BEQ	  Disp_score110
		CMP	  #3000H
		BNE	  Disp_score205
;-----------------------------------------------------------------------
Disp_score110	INC	  <retry_counter
		JSR	  Display_retry
;
;=============== Display score =========================================
Disp_score200
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #10000000B
		BRA	  Disp_score206
Disp_score205	SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #01000000B
Disp_score206	TSB	  !alarm_priority     ; Set SOUND
;-----------------------------------------------------------------------
Disp_score210	LDY	  #022H
		LDA	  !score_disp_buff+1
		JSR	  Display_BCD2
		LDY	  #024H
		LDA	  !score_disp_buff+0
		JSR	  Display_BCD2
;-----------------------------------------------------------------------
Disp_score290	PLP
		RTS
;
;************************************************************************
;*		 Display best time					*
;************************************************************************
;
		MEM8
		IDX16
Disp_best_time
		REP	  #00010000B	      ; Index 16 bit mode
		LDX	  !backup_pointer
;-----------------------------------------------------------------------
		LDY	  #041H
		LDA	  >backup_buffer+0,X
		JSR	  Display_BCD1
		LDY	  #044H
		LDA	  >backup_buffer+1,X
		JSR	  Display_BCD2
		LDY	  #047H
		LDA	  >backup_buffer+2,X
		JSR	  Display_BCD2
		SEP	  #00010000B	      ; Index 8 bit mode
		RTS
;
;************************************************************************
;*		 Display number ( BCD )					*
;************************************************************************
;
		MEM8
		IDX8
Display_BCD2
		PHA
		LSR	  A
		LSR	  A
		LSR	  A
		LSR	  A
		STA	  !meter_buffer+0,Y   ; Set meter buffer
		PLA
;-----------------------------------------------------------------------
Display_BCD1	AND	  #0FH
		STA	  !meter_buffer+1,Y   ; Set meter buffer
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Message display routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Display course number					*
;************************************************************************
;
		MEM8
		IDX8
Display_course1
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		JSR	  Disp_course300      ; Set course name
		JSR	  Disp_course700      ; Display course name
		PLP
		RTS
;-----------------------------------------------------------------------
Display_course2 PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		LDA	  <demo_flag
		BNE	  Display_course4
		JSR	  Disp_course100      ; Display select message
		JSR	  Disp_course300      ; Set course name
		JSR	  Disp_course700      ; Display course name
Display_course4 PLP
		RTS
;-----------------------------------------------------------------------
Display_course3 PHP
		SEP	  #00110000B
		JSR	  Disp_course300      ; Set course name
		JSR	  Disp_course800      ; Display course name
		PLP
		RTS
;
;=============== Display message =======================================
Disp_course100
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  #Select_message1
		JSR	  Print_OBJmsg	      ; Display "COURSE SELECT"
		LDA	  #Select_message2
		JSR	  Print_OBJmsg	      ; Display "PUSH SELECT"
;
;=============== Display map ===========================================
Disp_course200
		LDX	  #1EH
Disp_course210	LDA	  >Map_OAM_main,X
		STA	  !OAM_main+1E0H,X
		DEX
		DEX
		BPL	  Disp_course210
;-----------------------------------------------------------------------
		LDA	  #1010101010101010B
		STA	  !OAM_sub+1EH
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Check course number ===================================
Disp_course300
		MEM8
		LDA	  !course_number      ; Acc = course number
		LDX	  !course_change      ; IX  = course change status
;
;=============== Set course name string ================================
Disp_course400
		ASL	  A
		TAY
		LDA	  !Course_name+0,Y
		STA	  <work0+0
		LDA	  !Course_name+1,Y
		STA	  <work0+1	      ; work0.w = course name data address
;-----------------------------------------------------------------------
		LDY	  #00H
Disp_course410	LDA	  (<work0),Y
		STA	  !string_buffer,Y    ; Set string to buffer
		BEQ	  Disp_course500
		INY
		BRA	  Disp_course410
;
;=============== Set change number ( I,II,III ) ========================
Disp_course500
		TXA			      ; short course ( I ) ?
		BEQ	  Disp_course600      ; yes.
;-----------------------------------------------------------------------
		LDA	  !string_buffer-1,Y
		INC	  A		      ; long course ok ?
		BNE	  Disp_course600      ; no.
		LDA	  #RO
		STA	  !string_buffer-1,Y  ; Set long course ( II )
		LDA	  !course_number
		CMP	  #7		      ; course 8 ?
		BNE	  Disp_course600      ; no.
;-----------------------------------------------------------------------
		TXA
		AND	  #00000001B
		BNE	  Disp_course600
		STA	  !string_buffer+1,Y
		LDA	  #RO
		STA	  !string_buffer,Y
;
;=============== Set display attribute =================================
Disp_course600
		LDA	  <game_scene
		CLC
		ADC	  #_1
		STA	  !string_buffer+3    ; Set scene number
;-----------------------------------------------------------------------
		LDY	  #00H
		LDA	  <play_mode	      ; free mode ?
		BNE	  Disp_course610      ; yes.
		LDY	  <game_level
Disp_course610	LDA	  !Course_attrib,Y
		STA	  !string_buffer+2    ; Set display attribute
		RTS
;
;=============== Display string normal =================================
Disp_course700
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  #string_buffer
Disp_course710	JSR	  Print_OBJmsg
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Display string ( best time display ) ==================
Disp_course800
		REP	  #00100000B	      ; Memory 16 bit mode
		LDY	  <game_world
		LDX	  !Course_attrib,Y
		STX	  !string_buffer+5    ; Set display attribute
		LDA	  #5010H
		STA	  !string_buffer+3    ; Set display point
		LDA	  #string_buffer+3
		BRA	  Disp_course710
;
;=======================================================================
Course_name	WORD	  Course_name_01
		WORD	  Course_name_02
		WORD	  Course_name_03
		WORD	  Course_name_04
		WORD	  Course_name_05
		WORD	  Course_name_06
		WORD	  Course_name_07
		WORD	  Course_name_08
		WORD	  Course_name_09
;-----------------------------------------------------------------------
Course_name_01	BYTE	  054H,053H,00000001B
		BYTE	  _1,DT,SP,_B,_I,_G,SP,_B,_L,_U,_E,0
Course_name_02	BYTE	  058H,053H,00000001B
		BYTE	  _2,DT,SP,_S,_I,_L,_E,_N,_C,_E,0
Course_name_03	BYTE	  04CH,053H,00000001B
		BYTE	  _3,DT,SP,_S,_A,_N,_D,SP,_O,_C,_E,_A,_N,0
Course_name_04	BYTE	  044H,053H,00000001B
		BYTE	  _4,DT,SP,_P,_O,_R,_T,SP,_T,_O,_W,_N,SP,RO,SP,0
Course_name_05	BYTE	  040H,053H,00000001B
		BYTE	  _5,DT,SP,_D,_E,_A,_T,_H,SP,_W,_I,_N,_D,SP,RO,SP,0
Course_name_06	BYTE	  040H,053H,00000001B
		BYTE	  _6,DT,SP,_R,_E,_D,SP,_C,_A,_N,_Y,_O,_N,SP,RO,SP,0
Course_name_07	BYTE	  04CH,053H,00000001B
		BYTE	  _7,DT,SP,_F,_I,_R,_E,SP,_F,_I,_E,_L,_D,0
Course_name_08	BYTE	  044H,053H,00000001B
		BYTE	  _8,DT,SP,_M,_U,_T,_E,SP,_C,_I,_T,_Y,SP,RO,SP,0
Course_name_09	BYTE	  040H,053H,00000001B
		BYTE	  _9,DT,SP,_W,_H,_I,_T,_E,SP,_L,_A,_N,_D,SP,RO,SP,0
;-----------------------------------------------------------------------
Select_message1 BYTE	  04CH,030H,00110101B
		BYTE	  _C,_O,_U,_R,_S,_E,SP,_S,_E,_L,_E,_C,_T,0
Select_message2 BYTE	  054H,0C0H,00110111B
		BYTE	  _P,_U,_S,_H,SP,_S,_E,_L,_E,_C,_T,0
;-----------------------------------------------------------------------
Course_attrib	BYTE	  00110001B,00110101B,00110011B
;
;************************************************************************
;*		 Display "READY"					*
;************************************************************************
;
		MEM16
		IDX16
Display_READY
		PHP
		REP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  #0014H-1	      ; Acc = number of bytes
		LDX	  #READY_message&0FFFFH      ; IX = source address
		LDY	  #OAM_main	      ; IY = destination address
		MVN	  #0BH,#00H	      ; block move
;-----------------------------------------------------------------------
		LDA	  #1111111010101010B
		STA	  !OAM_sub
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #180
		STA	  <startup_timer
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Display "GO"						*
;************************************************************************
;
		MEM16
		IDX16
Display_GO
		PHP
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		LDA	  <VRAMbuf_pointer
		AND	  #00FFH
		ASL	  A
		ASL	  A
		ASL	  A
		ADC	  #VRAMwte_buffer
		TAY
		LDX	  #Disp_GO_data
		LDA	  #000FH
		MVN	  #00H,#00H	      ; block move
;-----------------------------------------------------------------------
		LDA	  #0020H-1	      ; Acc = number of bytes
		LDX	  #GO_message&0FFFFH	      ; IX = source address
		LDY	  #OAM_main	      ; IY = destination address
		MVN	  #0BH,#00H	      ; block move
;-----------------------------------------------------------------------
		LDA	  #1010101010101010B
		STA	  !OAM_sub
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; memory 8 bit mode
		LDA	  #05H
		STA	  <startup_timer
		STZ	  <startup_count
;-----------------------------------------------------------------------
		LDA	  <VRAMbuf_pointer
		CLC
		ADC	  #02H
		STA	  <VRAMbuf_pointer
;-----------------------------------------------------------------------
		PLP
		RTS
;-----------------------------------------------------------------------
Disp_GO_data	WORD	  4660H,0B221H+2,000FH,0040H
		WORD	  4760H,0B263H+2,000FH,0040H
;
;************************************************************************
;*		 Clear "GO"						*
;************************************************************************
;
		MEM8
		IDX8
Clear_GO
		LDA	  <startup_timer      ; Startup_timer == 0 ?
		BEQ	  Clear_GO100	      ; yes.
;-----------------------------------------------------------------------
		DEC	  <startup_timer
		RTS
;
;=============== Clear "GO" check ======================================
Clear_GO100
		LDA	  <startup_count
		CMP	  #6CH		      ; clear "GO" already ?
		BCC	  Clear_GO200	      ; no.
		BNE	  Clear_GO190
;-----------------------------------------------------------------------
		INC	  <startup_count
		LDA	  #00000001B
		TRB	  <message_flag	      ; free message OAM
		STZ	  !OAM_sub+00H
		STZ	  !OAM_sub+01H
		LDA	  <demo_flag	      ; demo mode ?
		BNE	  Clear_GO180	      ; yes.
		LDA	  #06H
		STA	  <sound_status_0     ; Music start
Clear_GO180	DEC	  !jump_pose_flag     ; jump pose transfer ok
Clear_GO190	RTS
;
;=============== Clear "GO" process ====================================
Clear_GO200
		LDX	  <startup_count
		LDA	  >GO_clear_data+0,X
		STA	  !OAM_main+000H
		STA	  !OAM_main+004H
		CLC
		ADC	  #10H
		STA	  !OAM_main+008H
		STA	  !OAM_main+00CH
		CLC
		ADC	  #10H
		STA	  !OAM_main+010H
		STA	  !OAM_main+014H
;-----------------------------------------------------------------------
		LDA	  >GO_clear_data+1,X
		STA	  !OAM_main+018H
		STA	  !OAM_main+01CH
;-----------------------------------------------------------------------
		LDA	  >GO_clear_data+2,X
		STA	  !OAM_sub+00H
		LDA	  >GO_clear_data+3,X
		STA	  !OAM_sub+01H
;-----------------------------------------------------------------------
		INX
		INX
		INX
		INX
		STX	  <startup_count
		RTS
;
;=============== "GO" clear data =======================================
;
GO_clear_data	EQU	  0AEF80H	      ; "GO" message clear data
READY_message	EQU	  0BEC00H	      ; "READY" message OAM data
GO_message	EQU	  0BEC14H	      ; "GO" message OAM data
;
;************************************************************************
;*		 Display message					*
;************************************************************************
;
		MEM8
		IDX8
Display_message
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  !special_demo	      ; special demo ?
		BNE	  Disp_message250     ; yes.
		LDA	  <exception_flag     ; exception ?
		BEQ	  Disp_message200     ; no.
		LDA	  #11111000B
		TRB	  <message_flag+0
;
;=============== Check message flag ====================================
Disp_message200
		LDX	  #0FFH
		LDA	  #00000001B
Disp_message210
		INX
		BIT	  <message_flag+0
		BNE	  Disp_message300
		ASL	  A
		BCC	  Disp_message210
;-----------------------------------------------------------------------
		INX
		LDA	  #00000000B
		LDY	  <message_flag+1
		BNE	  Disp_message300
;-----------------------------------------------------------------------
		LDA	  <message_length
		BEQ	  Disp_message290
		JSR	  Clear_message
;-----------------------------------------------------------------------
Disp_message290 LDA	  #0FFH
		STA	  <message_pointer    ; Reset message pointer
		PLP
		RTS
;-----------------------------------------------------------------------
Disp_message250 JSR	  Special_roll
		BRA	  Disp_message290
;
;=============== Message display entry =================================
Disp_message300
		STA	  <message_status
		CPX	  #01H		      ; Display "YOU WON" ?
		BNE	  Disp_message310     ; no.
		LDA	  <play_mode	      ; free mode ?
		BEQ	  Disp_message310     ; no.
		LDA	  !rival_number	      ; rival noting ?
		BPL	  Disp_message310     ; no.
		LDX	  #9
;-----------------------------------------------------------------------
Disp_message310 LDA	  !Message_table,X
		TAX
		LDA	  !Message_data+0,X
		STA	  <jump_address+0
		LDA	  !Message_data+1,X
		STA	  <jump_address+1     ; Set process address
		JMP	  (!jump_address)
;
;************************************************************************
;*		 No display ( OAM request )				*
;************************************************************************
;
		MEM8
		IDX8
No_display
		CPX	  <message_pointer
		BEQ	  No_display120
;-----------------------------------------------------------------------
		STX	  <message_pointer
		LDA	  <message_length
		BEQ	  No_display110
		JSR	  Clear_message
No_display110	LDA	  #50H
		STA	  <message_length
;-----------------------------------------------------------------------
No_display120	PLP
		RTS
;
;************************************************************************
;*		 Display message ( ALWAYS mode )			*
;************************************************************************
;
		MEM8
		IDX8
Disp_always
		CPX	  <message_pointer
		BEQ	  Disp_always200
;
;=============== Start ALWAYS ==========================================
Disp_always100
		STZ	  <message_counter    ; Clear message counter
		STX	  <message_pointer
		JSR	  Print_message
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== Wait ALWAYS ===========================================
Disp_always200
		INC	  <message_counter
		PLP
		RTS
;
;************************************************************************
;*		 Display message ( LAP mode )				*
;************************************************************************
;
		MEM8
		IDX8
Disp_lap
		CPX	  <message_pointer
		BEQ	  Disp_level200
;
;=============== Start LAP =============================================
Disp_lap100
		LDA	  #128
		STA	  <message_counter    ; Set message counter
		STX	  <message_pointer
		JSR	  Print_message
;-----------------------------------------------------------------------
		LDA	  #85H
		SEC
		SBC	  !round_counter+0
		STA	  !OAM_main+02H
		ORA	  #00010000B
		STA	  !OAM_main+06H
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Display message ( LEVEL mode )				*
;************************************************************************
;
		MEM8
		IDX8
Disp_level
		CPX	  <message_pointer
		BEQ	  Disp_level200
;
;=============== Start LEVEL ===========================================
Disp_level100
		LDA	  #128
		STA	  <message_counter    ; Set message counter
		STX	  <message_pointer
		JSR	  Print_message
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== Wait LEVEL ============================================
Disp_level200
		DEC	  <message_counter
		BNE	  Disp_level210
		JSR	  Clear_message	      ; Clear message
		LDA	  <message_status
		TRB	  <message_flag	      ; Reset message flag
		JMP	  Disp_message290
;-----------------------------------------------------------------------
Disp_level210	PLP
		RTS
;
;************************************************************************
;*		 Display message ( FLASH mode )				*
;************************************************************************
;
		MEM8
		IDX8
Disp_flash
		CPX	  <message_pointer
		BEQ	  Disp_flash200
;
;=============== Start FLASH ===========================================
Disp_flash100
		LDA	  #9FH
		STA	  <message_counter    ; Set message counter
		STX	  <message_pointer
		JSR	  Print_message
;-----------------------------------------------------------------------
		LDA	  <demo_flag	      ; demo mode ?
		BNE	  Disp_flash110	      ; yes.
		LDA	  #00010000B
		TSB	  !alarm_priority     ; Set SOUND
;-----------------------------------------------------------------------
Disp_flash110	PLP
		RTS
;
;=============== Wait FLASH ============================================
Disp_flash200
		LDA	  <message_counter
		BEQ	  Disp_flash230
;-----------------------------------------------------------------------
		AND	  #00011111B
		BEQ	  Disp_flash210
		CMP	  #10
		BEQ	  Disp_flash220
;-----------------------------------------------------------------------
		DEC	  <message_counter
		BRA	  Disp_flash290
;-----------------------------------------------------------------------
Disp_flash210	JSR	  Print_message
		LDA	  <demo_flag	      ; demo mode ?
		BNE	  Disp_flash215	      ; yes.
		LDA	  #00010000B
		TSB	  !alarm_priority     ; Set SOUND
Disp_flash215	DEC	  <message_counter
		BRA	  Disp_flash290
;-----------------------------------------------------------------------
Disp_flash220	JSR	  Clear_message
		DEC	  <message_counter
		BRA	  Disp_flash290
;-----------------------------------------------------------------------
Disp_flash230	LDA	  <message_status
		TRB	  <message_flag	      ; Reset message flag
		JMP	  Disp_message290
;-----------------------------------------------------------------------
Disp_flash290	PLP
		RTS
;
;************************************************************************
;*		 Display message ( FINAL mode )				*
;************************************************************************
;
		MEM8
		IDX8
Disp_final
		CPX	  <message_pointer
		BEQ	  Disp_final200
;
;=============== Start FINAL ===========================================
Disp_final100
		LDA	  #09FH
		STA	  <message_counter    ; Set message counter
		STX	  <message_pointer
		JSR	  Print_message
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== Wait FINAL ============================================
Disp_final200
		LDA	  <message_counter
		BEQ	  Disp_final230
;-----------------------------------------------------------------------
		AND	  #00011111B
		BEQ	  Disp_final210
		CMP	  #10
		BEQ	  Disp_final220
;-----------------------------------------------------------------------
		DEC	  <message_counter
		BRA	  Disp_final290
;-----------------------------------------------------------------------
Disp_final210	JSR	  Print_message
		DEC	  <message_counter
		BRA	  Disp_final290
;-----------------------------------------------------------------------
Disp_final220	JSR	  Clear_message
		DEC	  <message_counter
		BRA	  Disp_final290
;-----------------------------------------------------------------------
Disp_final230	LDA	  <message_status
		TRB	  <message_flag	      ; Reset message flag
		JMP	  Disp_message290
;-----------------------------------------------------------------------
Disp_final290	PLP
		RTS
;
;************************************************************************
;*		 Display message ( LIMIT mode )				*
;************************************************************************
;
		MEM8
		IDX8
Disp_limit
		CPX	  <message_pointer
		BEQ	  Disp_limit200
;
;=============== Start LIMIT ===========================================
Disp_limit100
		INC	  !rank_warnning      ; Set flag
		LDA	  #9FH
		STA	  <message_counter    ; Set message counter
		STX	  <message_pointer
		JSR	  Print_message
		JSR	  Disp_limit300
;-----------------------------------------------------------------------
Disp_limit110	PLP
		RTS
;
;=============== Wait FLASH ============================================
Disp_limit200
		LDA	  <message_counter
		BEQ	  Disp_limit230
;-----------------------------------------------------------------------
		AND	  #00011111B
		BEQ	  Disp_limit210
		CMP	  #10
		BEQ	  Disp_limit220
;-----------------------------------------------------------------------
		DEC	  <message_counter
		BRA	  Disp_limit290
;-----------------------------------------------------------------------
Disp_limit210	JSR	  Print_message
		JSR	  Disp_limit300
		DEC	  <message_counter
		BRA	  Disp_limit290
;-----------------------------------------------------------------------
Disp_limit220	JSR	  Clear_message
		DEC	  <message_counter
		BRA	  Disp_limit290
;-----------------------------------------------------------------------
Disp_limit230	LDA	  <message_status
		TRB	  <message_flag	      ; Reset message flag
		JMP	  Disp_message290
;-----------------------------------------------------------------------
Disp_limit290	PLP
		RTS
;
;=======================================================================
Disp_limit300
		LDA	  !clear_rank
		CMP	  #16
		BEQ	  Disp_limit330
		CMP	  #11
		BEQ	  Disp_limit320
;-----------------------------------------------------------------------
Disp_limit310	DEC	  A
		ORA	  #10000000B
		STA	  !OAM_main+2AH
		ORA	  #00010000B
		STA	  !OAM_main+2EH
		LDA	  #0F0H
		STA	  !OAM_main+31H
		STA	  !OAM_main+35H
		BRA	  Disp_limit330
;-----------------------------------------------------------------------
Disp_limit320	LDA	  #80H
		STA	  !OAM_main+32H
		LDA	  #90H
		STA	  !OAM_main+36H
;-----------------------------------------------------------------------
Disp_limit330	LDA	  #00110101B
		STA	  !OAM_main+2BH
		STA	  !OAM_main+2FH
		STA	  !OAM_main+33H
		STA	  !OAM_main+37H
		RTS
;
;************************************************************************
;*		 Print message						*
;************************************************************************
;
		MEM8
		IDX8
Print_message
		LDY	  <message_pointer
		LDA	  !Message_data+2,Y
		STA	  <prtmsg_pos_h	      ; Set print H position
		LDA	  !Message_data+3,Y
		XBA			      ; Breg = attribute
		LDX	  #00H		      ; IX  = OAM pointer
;-----------------------------------------------------------------------
Print_message10 LDA	  !Message_data+4,Y   ; Get message data
		BEQ	  Print_message30     ; if ( end of string )
		CMP	  #SP
		BEQ	  Print_message20     ; if ( space )
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
		STA	  !OAM_main+2,X	      ; Set character upper
		ADC	  #10H
		STA	  !OAM_main+6,X	      ; Set character lower
		SEP	  #00100000B	      ; Memory 8 bit mode
		MEM8
;-----------------------------------------------------------------------
		LDA	  <prtmsg_pos_h
		STA	  !OAM_main+0,X	      ; Set H position upper
		STA	  !OAM_main+4,X	      ; Set H position lower
		LDA	  #48H
		STA	  !OAM_main+1,X	      ; Set V position upper
		LDA	  #50H
		STA	  !OAM_main+5,X	      ; Set V position lower
;-----------------------------------------------------------------------
		TXA
		CLC
		ADC	  #08H
		TAX
;-----------------------------------------------------------------------
Print_message20 LDA	  <prtmsg_pos_h
		CLC
		ADC	  #08H
		STA	  <prtmsg_pos_h
		INY
		BRA	  Print_message10
;-----------------------------------------------------------------------
Print_message30 TXY
		BRA	  Clear_message10
;
;************************************************************************
;*		 Clear message						*
;************************************************************************
;
		MEM8
		IDX8
Clear_message
		LDX	  #00H
		LDY	  #00H
;-----------------------------------------------------------------------
Clear_message10 LDA	  #0F0H
Clear_message20 CPX	  <message_length
		BCS	  Clear_message30
		STA	  !OAM_main+1,X
		INX
		INX
		INX
		INX
		BRA	  Clear_message20
;-----------------------------------------------------------------------
Clear_message30 STY	  <message_length
		RTS
;
;************************************************************************
;*		 Message data						*
;************************************************************************
;
Message_table	BYTE	  Message_data_0-Message_data
		BYTE	  Message_data_1-Message_data
		BYTE	  Message_data_2-Message_data
		BYTE	  Message_data_3-Message_data
		BYTE	  Message_data_4-Message_data
		BYTE	  Message_data_5-Message_data
		BYTE	  Message_data_6-Message_data
		BYTE	  Message_data_7-Message_data
		BYTE	  Message_data_8-Message_data
		BYTE	  Message_data_9-Message_data
;-----------------------------------------------------------------------
Message_data	EQU	  $
Message_data_0	WORD	  No_display
;-----------------------------------------------------------------------
Message_data_1	WORD	  Disp_always
		BYTE	  64H,00110001B
		BYTE	  _Y,_O,_U,SP,_W,_O,_N,0
;-----------------------------------------------------------------------
Message_data_2	WORD	  Disp_always
		BYTE	  60H,00110011B
		BYTE	  _Y,_O,_U,SP,_L,_O,_S,_T,0
;-----------------------------------------------------------------------
Message_data_3	WORD	  Disp_final
		BYTE	  5CH,00110101B
		BYTE	  _F,_I,_N,_A,_L,SP,_L,_A,_P,0
;-----------------------------------------------------------------------
Message_data_4	WORD	  Disp_lap
		BYTE	  54H,00110101B
		BYTE	  _0,SP,_L,_A,_P,_S,SP,_L,_E,_F,_T,0
;-----------------------------------------------------------------------
Message_data_5	WORD	  Disp_limit
		BYTE	  60H,00110001B
		BYTE	  _L,_I,_M,_I,_T,SP,_1,_5,0
;-----------------------------------------------------------------------
Message_data_6	WORD	  Disp_flash
		BYTE	  58H,00110011B
		BYTE	  _P,_O,_W,_E,_R,SP,_D,_O,_W,_N,0
;-----------------------------------------------------------------------
Message_data_7	WORD	  Disp_flash
		BYTE	  64H,00110011B
		BYTE	  _R,_E,_V,_E,_R,_S,_E,0
;-----------------------------------------------------------------------
Message_data_8	WORD	  Disp_flash
		BYTE	  5CH,00110011B
		BYTE	  _P,_U,_S,_H,SP,_S,_T,_A,_R,_T,0
;-----------------------------------------------------------------------
Message_data_9	WORD	  Disp_always
		BYTE	  64H,00110001B
		BYTE	  _G,_O,_A,_L,SP,_I,_N,0
;
;************************************************************************
;*		 Display rank number window				*
;************************************************************************
;
		MEM8
		IDX8
Set_rank_window
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=======================================================================
Set_rank_win100
		LDX	  #0BH
Set_rank_win110 LDA	  !window_table,X
		STA	  !rank_window,X
		DEX
		BPL	  Set_rank_win110
;-----------------------------------------------------------------------
		LDY	  !goal_in_rank
		LDX	  !Window_pointer,Y   ; IX = source pointer
		LDA	  !Window_length,Y
		TAY			      ; IY = destination pointer
Set_rank_win120 LDA	  !Window_data_0,X
		STA	  !rank_window+0CH,Y
		DEX
		DEY
		BPL	  Set_rank_win120
;-----------------------------------------------------------------------
		LDA	  #00110001B
		STA	  <spot_switch
		JSR	  Frash_rank_wind
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  #rank_window
		STA	  <window_address     ; Set rank window
		PLP
		RTS
;
;************************************************************************
;*		 Frash rank window color				*
;************************************************************************
;
		MEM8
		IDX8
Frash_rank_wind
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <frame_counter
		AND	  #00001000B	      ; White timming ?
		BNE	  Frash_rank200	      ; no.
;
;=============== Set white coloe =======================================
Frash_rank100
		LDA	  #11101111B
		STA	  !spot_color_B	      ; Set white color
		PLP
		RTS
;
;=============== Set rank display color ================================
Set_rank_color
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
Frash_rank200	LDX	  !goal_in_rank
		DEX
		LDA	  !Rank_window_col,X  ; Set rank window color
		STA	  !spot_color_B
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=======================================================================
Rank_window_col BYTE	  00101111B,10001111B,01001111B
;
;************************************************************************
;*		 Display retry count					*
;************************************************************************
;
		MEM16
		IDX16
Display_retry
		PHP
		REP	  #00111000B	      ; Memory,Index 16 bit mode
;
;=============== Set VRAM write pointer ================================
Disp_retry100
		LDA	  <retry_counter
Disp_retry110	AND	  #00FFH
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		TAX
		LDA	  #5BC0H
		STA	  !VRAMwte_buffer+0
		BRA	  Set_small_ch110
;
;************************************************************************
;*		 Set small character					*
;************************************************************************
;
		MEM16
		IDX16
Set_small_char
		PHP
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;
;=============== Set VRAM write pointer ================================
Set_small_ch100
		AND	  #00FFH
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		TAX
		ADC	  #5400H
		STA	  !VRAMwte_buffer+0
;-----------------------------------------------------------------------
Set_small_ch110 LDA	  #string_buffer
		STA	  !VRAMwte_buffer+2
		STZ	  !VRAMwte_buffer+4
		LDA	  #0020H
		STA	  !VRAMwte_buffer+6
;
;=============== Set character data ====================================
Set_small_ch200
		LDY	  #0000H
Set_small_ch210 LDA	  >0EDE00H,X
		STA	  !string_buffer,Y
		INX
		INX
		INY
		INY
		CPY	  #0010H
		BNE	  Set_small_ch210
;-----------------------------------------------------------------------
		LDA	  #0000H
Set_small_ch220 STA	  !string_buffer,Y
		INY
		INY
		CPY	  #0020H
		BNE	  Set_small_ch220
;
;=============== exit ==================================================
Set_small_ch300
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #01H
		STA	  <VRAMbuf_pointer
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Set my car style					*
;************************************************************************
;
		MEM8
		IDX8
Set_mycar_style
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		LDA	  #10000000B
		STA	  !Screen_step	      ; Set VRAM inc. mode
;-----------------------------------------------------------------------
		LDA	  <mycar_number
		ORA	  #00001000B
		STA	  !DMA_0+4
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  #01801H
		STA	  !DMA_0+0
		LDA	  #0EAA0H
		STA	  !DMA_0+2
		LDX	  #00000001B
;-----------------------------------------------------------------------
		LDA	  #5C80H
		STA	  !Screen_address     ; Set VRAM address
		LDA	  #0040H
		STA	  !DMA_0+5	      ; Set data length
		STX	  !DMA_burst	      ; Start DMA
;-----------------------------------------------------------------------
		LDA	  #5D80H
		STA	  !Screen_address     ; Set VRAM address
		LDA	  #0040H
		STA	  !DMA_0+5	      ; Set data length
		STX	  !DMA_burst	      ; Start DMA
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Display repair counter					*
;************************************************************************
;
		MEM8
		IDX8
Disp_repair_cnt
		PHP
		SEP	  #00110000B	      ; Memory 8 bit mode
		LDA	  <play_mode	      ; battle mode
		BNE	  Disp_repair_ct2     ; no.
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  >Meter_OAM_main+08H
		STA	  !OAM_main+1F8H
		LDA	  >Meter_OAM_main+0AH
		STA	  !OAM_main+1FAH
		LDA	  >Meter_OAM_main+0CH
		STA	  !OAM_main+1FCH
		LDA	  >Meter_OAM_main+0EH
		STA	  !OAM_main+1FEH
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  !OAM_sub+1FH
		AND	  #00101111B
		STA	  !OAM_sub+1FH	      ; Set OAM sub
		LDY	  <mycar_number
		LDA	  !Car_style_color,Y
		STA	  !OAM_main+1FBH      ; Set car color
;-----------------------------------------------------------------------
Disp_repair_ct2 PLP
		RTS
;
;************************************************************************
;*		 Back screen data					*
;************************************************************************
;
Backscrn_bank	EQU	  000FH		      ;			 F B
Back_screen	WORD	  53CEH,5C1CH	      ; Course 1 :Screen A-A
		WORD	  4F9AH,573EH	      ; Course 2 :Screen B-B
		WORD	  504BH,580BH	      ; Course 3 :Screen C-C
		WORD	  5269H,59ADH	      ; Course 4 :Screen D-D
		WORD	  52FBH,5B28H	      ; Course 5 :Screen E-E
		WORD	  53CEH,5B28H	      ; Course 6 :Screen F-E
		WORD	  5476H,5D74H	      ; Course 7 :Screen G-G
		WORD	  4E00H,553CH	      ; Course 8 :Screen A-A
		WORD	  4E00H,553CH	      ; Course 9 :Screen A-A
;
;=============== Meter window color ====================================
Meter_wind_col	EQU	  0FC220H
;
;			    switch     red	green	   blue
;eter_wind_col	BYTE	  00100011B,00111111B,01000000B,10000000B  ; Course 1
;		BYTE	  00100011B,00111000B,01000000B,10000000B  ; Course 3-B
;		BYTE	  10100011B,00100000B,01010000B,10001000B  ; Course 3-A
;		BYTE	  00100011B,00111000B,01000000B,10000000B  ; 4
;		BYTE	  10100011B,00100000B,01010000B,10001000B  ; 5
;		BYTE	  00100011B,00111111B,01000000B,10000000B  ; 6
;		BYTE	  00100011B,00111000B,01000000B,10000000B  ; 7
;		BYTE	  00100011B,00111111B,01000000B,10000000B  ; 8-A
;		BYTE	  10100011B,00100000B,01010000B,10001000B  ; 9
;		BYTE	  10100011B,00100000B,01010000B,10001000B  ; 8-B
;		BYTE	  00100011B,00111000B,01000000B,10000000B  ; 8-C
;
;************************************************************************
;*		 Meter screen data					*
;************************************************************************
;
Meter_screen	EQU	  0AEC00H	      ; Meter screen data
;-----------------------------------------------------------------------
Best_screen_1	WORD	  02100H,02100H,02027H,02028H,02029H
Best_screen_2	WORD	  02800H,0282AH,02800H,02800H,0282BH,02800H,02800H
Best_screen_3	WORD	  02100H,02100H,02100H,02100H,02100H,02100H
;
;************************************************************************
;*		 Game window and gradation HDMA data			*
;************************************************************************
;
Window_open	EQU	  08EEB0H	      ; window open data
Window_close	EQU	  08EEB2H	      ; window close data
Shadow_window_1 EQU	  08EEB4H	      ; my car shadow pattern 1
Shadow_window_2 EQU	  08EED4H	      ; my car shadow pattern 2
Shadow_window_3 EQU	  08EEF4H	      ; my car shadow pattern 3
Number_window10 EQU	  08EF14H	      ; rank number pattern '1'
Number_window11 EQU	  08EF24H
Number_window12 EQU	  08EF26H
Number_window20 EQU	  08EF32H	      ; rank number pattern '2'
Number_window21 EQU	  08EF42H
Number_window22 EQU	  08EF44H
Number_window23 EQU	  08EF66H
Number_window24 EQU	  08EF68H
Number_window30 EQU	  08EF74H	      ; rank number pattern '3'
Number_window31 EQU	  08EF8AH
Gradation_sub	EQU	  08EF9CH	      ; Gradation HDMA data ( color sub )
Gradation_add	EQU	  08EFC9H	      ; Gradation HDMA data ( color add )
;
;=============== Window HDMA table pointer =============================
;
Window_length	BYTE	  21,12,18,18
Window_pointer	BYTE	  Window_data_0-Window_data_0+20
		BYTE	  Window_data_1-Window_data_0+12
		BYTE	  Window_data_2-Window_data_0+18
		BYTE	  Window_data_3-Window_data_0+18
;
;=============== Window HDMA table data #0 =============================
Window_data_0
		BYTE	  00H+13H	      ; 00H
		WORD	  Window_close&0FFFFH	      ; Window close
		BYTE	  00H+0AH	      ; 14H
		WORD	  shield_meter	      ; Shield meter
		BYTE	  00H+0EH	      ; 1EH
		WORD	  Window_close&0FFFFH	      ; Window close
		BYTE	  00H+2AH	      ; 2CH
		WORD	  Window_open&0FFFFH	      ; Window open
		BYTE	  00H+30H
		WORD	  Window_close&0FFFFH	      ; Window close
		BYTE	  00H+2BH	      ; 56H
		WORD	  Window_close&0FFFFH	      ; Window close
		BYTE	  80H+10H	      ; ADH
		WORD	  Shadow_window_1&0FFFFH     ; My car air window
		BYTE	  00H
;
;=============== Window HDMA table data #1 =============================
Window_data_1
		BYTE	  00H+02H
		WORD	  Window_close&0FFFFH
		BYTE	  80H+08H
		WORD	  Number_window10&0FFFFH
		BYTE	  00H+33H
		WORD	  Number_window11&0FFFFH
		BYTE	  80H+06H
		WORD	  Number_window12&0FFFFH
		BYTE	  00H
;
;=============== Window HDMA table data #2 =============================
Window_data_2
		BYTE	  00H+02H
		WORD	  Window_close&0FFFFH
		BYTE	  80H+08H
		WORD	  Number_window20&0FFFFH
		BYTE	  00H+0DH
		WORD	  Number_window21&0FFFFH
		BYTE	  80H+11H
		WORD	  Number_window22&0FFFFH
		BYTE	  00H+15H
		WORD	  Number_window23&0FFFFH
		BYTE	  80H+06H
		WORD	  Number_window24&0FFFFH
		BYTE	  00H
;
;=============== Window HDMA table data #3 =============================
Window_data_3
		BYTE	  00H+02H
		WORD	  Window_close&0FFFFH
		BYTE	  80H+08H
		WORD	  Number_window20&0FFFFH
		BYTE	  00H+11H
		WORD	  Number_window21&0FFFFH
		BYTE	  80H+0BH
		WORD	  Number_window30&0FFFFH
		BYTE	  00H+14H
		WORD	  Number_window21&0FFFFH
		BYTE	  80H+09H
		WORD	  Number_window31&0FFFFH
		BYTE	  00H
;
		END
