;************************************************************************
;*	 EN_INIT_1	   -- enemy car Initialize Routine ---		*
;************************************************************************
		INCLUDE	  RP5A22:
		INCLUDE	  RP5C77:
		INCLUDE	  BUFFER
		INCLUDE	  VARIABLE
		INCLUDE	  WORK
;-----------------------------------------------------------------------
		EXT	  OAM_PTR_07
		EXT	  OAM_PTR_00
		EXT	  Check_enemy
		EXT	  Open_drive_data
		EXT	  Change_point
		EXT	  Set_mycar_OAM
;
		EXT	  Accele_datptr
;
		EXT	  Phase_check
		EXT	  Angle_CALC
		EXT	  mark_point_x,mark_point_y,view_point_x,view_point_y
		EXT	  calc_angle
;
		EXT	  Rival_speed_data,Zako_speed_data,Rival_MAX_data
		EXT	  Rival_handle_data,Zako_handle_12,Zako_handle_34
		EXT	  Rival_pase_data
;
		EXT	  Crash_pase_data
		EXT	  Set_distance_data
;-----------------------------------------------------------------------
		GLB	  Initial_OBJchar
		GLB	  Enemy_initial
;-----------------------------------------------------------
;		COMN
;-----------------------------------------------------------
tempolary	EQU	    00H
;
data_address	EQU	    08H
;
point_x_addr	EQU	    10H
point_y_addr	EQU	    12H
line_address	EQU	    16H
status_address	EQU	    18H
flag_address	EQU	    1AH
pos_x_address	EQU	    1CH
pos_y_address	EQU	    1EH
area_address	EQU	    20H
color_address	EQU	    22H
drive_0_addr	EQU	    24H
drive_1_addr	EQU	    26H
drive_2_addr	EQU	    28H
lane_0_addr	EQU	    2AH
lane_1_addr	EQU	    2CH
lane_2_addr	EQU	    2EH
table_counter	EQU	    30H			;2byte
free_level	EQU	    32H			;
counter		EQU	    0AH
;
;========================================================================
;		SET OBJ CHARACTER FOR INITIARISE
;=============================================================================
		EXTEND
		PROG
		MEM8
		IDX8
Initial_OBJchar
		PHP
		SEP	  #00110000B
		LDA	  #00000010B
		STA	  <OBJchar_status     ; Set OBJ status
		LDA	  #10000000B
		STA	  !Screen_step	      ; Set VRAM address auto inc.
;-----------------------------------------------------------------------
Set_car_char
Nomal_pose_set
		LDA	  #4AH
		STZ	  !Screen_address+0   ; Set VRAM address low
		STA	  !Screen_address+1   ; Set VRAM address high
;
		LDX	  #06H
Set_nomal_001
		LDA	  >Nomal_pose_data,X
		STA	  !DMA_0,X	      ; Set DMA parameter
		DEX
		BPL	  Set_nomal_001
;
		LDA	  <mycar_number
		CLC
		ADC	  #08H
		STA	  !DMA_0+04
;
		LDA	  #00000001B
		STA	  !DMA_burst	      ; Start DMA channel #0
;--------------------------------------------------------------------------
Bank_pose_set
		LDY	  #01H		      ; DMA burst data	use ch.0
		LDX	  #00H
;
		STZ	  !DMA_0+2
		LDA	  #0FCH
		STA	  !DMA_0+3
Car_bank_loop
		LDA	  >My_car_bank,X
		STA	  !Screen_address+00H	  ;VRAM address
		INX
		LDA	  >My_car_bank,X
		STA	  !Screen_address+01H
		INX
		LDA	  >My_car_bank,X	 ;Number of data
		STA	  !DMA_0+5
		STZ	  !DMA_0+6
		INX
		STY	  !DMA_burst
		CPX	  #24H
		BNE	  Car_bank_loop
Bank_plus_alpha
		LDA	  <mycar_number
		BIT	  #00000001B
		BNE	  No_plus_alpha
		LSR	  A
		XBA
		LDA	  #00H
		MEM16
		REP	  #00100000B
		CLC
		ADC	  #0FC00H
		STA	  <data_address
		LDX	  #09H
		STX	  !DMA_0+04
		LDX	  #00H
		LDA	  #40C0H
		PHA
Plus_alpha_loop
		PLA
		STA	  !Screen_address+0	  ;VRAM address
		ADC	  #0100H		  ;Carry=clear
		PHA
		LDA	  >Plus_alpha_data,X
		ADC	  <data_address		  ;Carry=clear
		STA	  !DMA_0+2
		LDA	  #040H
		STA	  !DMA_0+05		  ;Set data length
		STY	  !DMA_burst	      ; Start DMA channel #0
		INX
		INX
		CPX	  #10H
		BNE	  Plus_alpha_loop
;----Remain---------------------------------------------------------------
		LDX	  #0BH
		STX	  !DMA_0+04
		LDX	  #040H
		STX	  !DMA_0+05
		PLA
		STA	  !Screen_address+0	  ;VRAM address
		PHA
;
		LDA	  #0FC00H
		STA	  !DMA_0+02
		STY	  !DMA_burst	      ; Start DMA channel #0
;-------------------------------------------------------------------------
		LDA	  #0FE00H
		STA	  !DMA_0+02
;
		PLA
		ADC	  #00100H		  ;Carry=clear
		STA	  !Screen_address+0	  ;VRAM address
		STX	  !DMA_0+05		  ;X=40H
;
		STY	  !DMA_burst	      ; Start DMA channel #0
No_plus_alpha
		PLP
		RTS
;=============================================================================
;		Enemy control Initialize Routin
;=============================================================================
Enemy_initial
		PHP
		JSR		Open_drive_data
Set_angle_data
		MEM16
		IDX16
		REP		#00110000B
		LDX		#00H
		TXY
Set_angle_loop
		JSR		Set_angle_sub
		MEM16
		IDX16
		LDA		<calc_angle
		STA		!area_angle,X
		INY
		INY
		INX
		CPX		#0FFH
		BNE		Set_angle_loop
;--------------------------------------------------------------------------
;		SET   DATA ADDRESS FOR COURSE CHANGE
;---------------------------------------------------------------------------------
		MEM8
		IDX8
Car_initial
		SEP		#00110000B
		PEA		0002H
		PLB
		LDA		<mycar_number
		ASL		A
		ASL		A
		TAX
		LDY		#00H
Car_param_loop
		PHX
		CLC
		LDA		!Initial_set_x,X
		ADC		!position_x-02H
		STA		!car_locatex_h+00H,Y
;
		LDA		#00H
		STA		!car_angle+00H,Y
		STA		!car_scrl_angle+00H,Y
		STA		!car_course+00H,Y
		STA		!car_speed+00H,Y
		STA		!car_speed+01H,Y
		STA		!car_handle+01H,Y
		STA		!car_flag+00H,Y
		STA		!car_control+00H,Y
;
		DEC		A
		STA		!car_handle+00H,Y
		STA		!old_pointer+00H,Y
		ADC		!position_x-01H
		STA		!car_locatex_h+01H,Y
;
		LDA		!Set_select_data,X
		STA		!car_selecta,Y
		TAX
		ASL		A
		ORA		#00001000B
		STA		!car_char_num+01H,Y
		LDA		Car_char_num_data-02H,Y
		STA		!car_char_num+00H,Y
;
		LDA		!position_y-02H
		SBC		#030H
		ADC		!Initial_set_y,X
		STA		!car_locatey_h+00H,Y
		LDA		!position_y-01H
		STA		!car_locatey_h+01H,Y
;
		LDA		>Accele_datptr,X
		STA		!accele_perform,Y
;
		LDA		<area_number
		STA		!car_area,Y
		LDA		#030H
		STA		!car_angle+01H,Y
		STA		!car_scrl_angle+01H,Y
		STA		!area_speed+01H,Y
		LDA		#LOW OAM_PTR_07
		STA		!car_table_addr+00H,Y
		LDA		#HIGH OAM_PTR_07
		STA		!car_table_addr+01H,Y
;
		LDA		<play_mode
		BNE		Next_carparam
		LDA		#80H
		STA		!car_flag+00H,Y
		LDA		#04H
		STA		!car_flag+01H,Y
Next_carparam
		PLX
		INX
		INY
		INY
		CPY		#0AH
		BEQ		End_param_loop
		JMP		Car_param_loop
End_param_loop
		STZ		!car_flag+08H
		STZ		!car_flag+0AH
		LDA		#088H
		STA		!car_flag+00H
;-----------------------------------------------------------------------------
		LDA		<play_mode
		BEQ		Initial_ZAKO
		STZ		!car_locatex_l+01H
		STZ		!car_locatey_l+01H
		LDA		#00001000B
		STA		!car_char_num+01H
		LDA		!position_x-02H
		SEC
		SBC		#0010H
		STA		!car_locatex_h+00H
		LDA		!position_x-01H
		STA		!car_locatex_h+01H
;Set_freerival
		LDA		!rival_number
		BMI		No_rival_free
		STA		!car_selecta+02H
		LDA		#0AH
		STA		!car_char_num+03H
		LDA		#080H
		STA		!car_flag+02H
		LDA		#00CH
		STA		!car_flag+03H
		LDA		!car_locatex_h+00H
		STA		!car_locatex_h+02H
		LDA		!car_locatex_h+01H
		STA		!car_locatex_h+03H
		LDA		!position_y-02H
		SBC		#010H
		STA		!car_locatey_h+02H
		ADC		#020H
		STA		!car_locatey_h+00H
		LDA		!position_y-01H
		STA		!car_locatey_h+01H
		STA		!car_locatey_h+03H
		BRA		Initial_others
No_rival_free
		LDA		!position_y-02H
		STA		!car_locatey_h+00H
		LDA		!position_y-01H
		STA		!car_locatey_h+01H
		BRA		Initial_others
;------------------------------------------------------------------------------
Initial_ZAKO			  ;X=08H
		LDA		#0C0H
		STA		!car_flag+08H
		STZ		!car_flag+09H
		STZ		!car_number+08H
		LDA		#60H
		STA		!car_char_num+08H
		STZ		!car_char_num+09H
		LDA		#01H
		STA		!car_course+08H
		LDA		#13H
		STA		!accele_perform+08H
		LDA		!position_x-02H
		SEC
		SBC		#60H
		STA		!car_locatex_h+08H
		LDA		!position_x-01H
		SBC		#01H
		STA		!car_locatex_h+09H
		LDA		!position_y-02H
		STA		!car_locatey_h+08H
		LDA		!position_y-01H
		STA		!car_locatey_h+09H
;-------------------------------------------------------------------------------
Initial_others
		LDA		<game_level
		ASL		A
		ASL		A
		CLC
		ADC		<game_level
		ADC		<game_scene
		LDX		<game_world
		ADC		!Pase_level_data,X
		TAX
		LDA		!Rival_pase_data,X
		STA		<tempolary
		LDA		!Crash_pase_data,X
		STA		!crash_pase
		STA		!crash_counter
		LDA		#04H
		LDX		#00H
Set_pase_loop
		STZ		!car_pase_data,X
		LSR		<tempolary
		BCC		Slow_pase
		STA		!car_pase_data,X
Slow_pase
		INX
		CPX		#05H
		BNE		Set_pase_loop
;----------------------------------------------------------------------------------
;Set mycar rank
		LDX		!car_selecta+0
		STZ		!car_stack
		STZ		!late_number
		STZ		!set_pointa
		STZ		!zako_pointa
		STZ		!car_number+08H
		LDA		<play_mode
		BNE		Set_free_rank
		LDA		!Select_rank,X
		STA		!mycar_rank
		LDA		#03H
		STA		!rival_flag
		STZ		!rival_late
		LDX		#00H
Set_ranking_loop
		LDA		!car_selecta,X
		TAY
		LDA		!Select_rank,Y
		DEC		A
		ASL		A
		TAY
		TXA
		STA		!car_ranking+00H,Y
		LDA		!car_area,X
		STA		!rank_priority+0,Y
		LDA		#0FFH
		STA		!rank_priority+1,Y
		INX
		INX
		CPX		#08H
		BNE		Set_ranking_loop
;
		LDA		#08H
		STA		!car_ranking+08H
		LDA		!car_area+08H
		STA		!rank_priority+08H
		LDA		#0FFH
		STA		!rank_priority+09H
		BRA		Set_initial_buffer
Set_free_rank
		LDA		#01H
		STA		!mycar_rank
		LDX		#00H
Set_free_rank_loop
		TXA
		STA		!car_ranking,X
		STA		!rank_priority+00H,X
		STA		!rank_priority+01H,X
		INX
		INX
		CPX		#0CH
		BNE		Set_free_rank_loop
;---------------------------------------------------------------
Set_initial_buffer
		MEM8
		IDX8
		LDX		#00H
INIT_buffer_loop
		STZ		!Area_line,X
		LDA		#010H
		STA		!Area_status,X
		LDA		#03H
		STA		!Area_drive,X
		LDA		#030H
		STA		!Area_angle,X
		LDA		!position_x-02H
		STA		!Mark_point_x+00H,X
		LDA		!position_x-01H
		STA		!Mark_point_x+01H,X
		STA		<start_room_x
		LDA		!car_locatey_h+00H,X
		STA		!Mark_point_y+00H,X
		LDA		!car_locatey_h+01H,X
		STA		!Mark_point_y+01H,X
		LDA		!position_y-01H
		STA		<start_room_y
;
		INX
		INX
		CPX		#0AH
		BNE		INIT_buffer_loop
;
		MEM16
		IDX16
		REP		#00110000B
;
		LDA		!car_area
		AND		#00FFH
		DEC		A
		ASL		A
		TAY
		LDA		!position_x,Y
		STA		!Behind_x
		LDA		!position_y,Y
		STA		!Behind_y
;
		MEM8
		IDX8
		SEP		#00110000B
		STZ		!Behind_status
		LDA		#030H
		STA		!Behind_angle
;
		STZ		!special_EX_flag
		STZ		!EX_counter
		LDA		#04H
		STA		!EX_timer
		STA		!stop_counter
		STA		!mycar_pose
		STZ		!nearcar_sub
		LDA		<play_mode
		BEQ		Set_speed_sub
		LDX		!rival_number
		LDA		>Accele_datptr,X
		STA		!accele_perform+02H
;
		LDA		Rival_free_speed,X
		PHA
		AND		#00001111B
		STA		!rival_free_speed
		PLA
		ROL		A
		ROL		A
		ROL		A
		AND		#00000011B
		STA		<game_level
Set_speed_sub
		MEM16
		IDX8
		REP		#00100000B
;
		LDA		<game_level
		ASL		A
		CLC
		ADC		<game_level
		TAX
		LDY		#00H
Set_RV_speed_loop
		LDA		!Rival_speed_data,X
		JSR		Four_time_sub
		STA		!RV_speed_table,Y
		INX
		INY
		INY
		CPY		#06H
		BNE		Set_RV_speed_loop
;
		LDA		<game_level
		ASL		A
		ASL		A
		ASL		A
		TAX
		LDY		#00H
Set_RV_MAX_loop
		LDA		!Rival_MAX_data,X
		JSR		Four_time_sub
		STA		!RV_max_speed,Y
		INX
		INY
		INY
		CPY		#10H
		BNE		Set_RV_MAX_loop
;
		LDA		<game_level
		JSR		Four_time_sub
		TAX
		LDY		#00H
Set_ZK_speed_loop
		LDA		!Zako_speed_data,X
		JSR		Four_time_sub
		STA		!ZK_speed_table,Y
		INX
		INY
		INY
		CPY		#20H
		BNE		Set_ZK_speed_loop
;
		LDA		<game_level
		ASL		A
		ASL		A
		TAX
		LDY		#00H
Set_handle_loop
		LDA		!Rival_handle_data,X
		JSR		Four_time_sub
		STA		!RV_handle_table,Y
		LDA		!Zako_handle_12,X
		JSR		Four_time_sub
		STA		!ZK_handle_table+00H,Y
		LDA		!Zako_handle_34,X
		JSR		Four_time_sub
		STA		!ZK_handle_table+08H,Y
		PHX
		TXA
		ASL		A
		TAX
		LDA		!Set_distance_data,X
		STA		!distance_check,Y
		PLX
		INX
		INY
		INY
		CPY		#08H
		BNE		Set_handle_loop
		PLB
;
		JSR		Set_mycar_OAM
;
		PLP
		RTS
;---------------------------------------------------------------------------
Four_time_sub
		MEM16
		AND		#00FFH
		ASL		A
		ASL		A
		ASL		A
		ASL		A
		RTS
;---------------------------------------------------------------------------------
Set_angle_sub
		MEM16
		IDX16
;
		LDA		!position_x+0,Y
		STA		<mark_point_x
		LDA		!position_y+0,Y
		STA		<mark_point_y
		LDA		!position_x-2,Y
		STA		<view_point_x
		LDA		!position_y-2,Y
		STA		<view_point_y
;
		PHY
;
		JSR		Phase_check
		JSR		Angle_CALC
;
		MEM16
		IDX16
		REP		#00110000B
;
		PLY
		RTS
;-------------------------------------------------------------------------------
		ORG	  2FF85H
;------------------------------------------------------------------------------
;=============== DMA parameter =========================================
Nomal_pose_data
		BYTE	  00000001B
		BYTE	  18H		      ; DMA B address low
		WORD	  0F000H	      ; DMA A address low,high
		BYTE	  0000H		      ; dummy
		WORD	  0C00H		      ; Number of data
;=============== Mycar BANK pose Address & data length =====================
My_car_bank
		WORD	  40E0H
		BYTE	  0040H
		WORD	  42E0H
		BYTE	  0040H
		WORD	  44E0H
		BYTE	  0040H
		WORD	  46E0H
		BYTE	  0040H
		WORD	  48E0H
		BYTE	  0040H
		WORD	  5000H
		BYTE	  00C0H
		WORD	  41E0H
		BYTE	  0040H
		WORD	  43E0H
		BYTE	  0040H
		WORD	  45E0H
		BYTE	  0040H
		WORD	  47E0H
		BYTE	  0040H
		WORD	  49E0H
		BYTE	  0040H
		WORD	  5100H
		BYTE	  00C0H
Plus_alpha_data
		WORD	  000H,200H,040H,240H,080H,280H,0C0H,2C0H
;----------------------------------------------------------------
Initial_set_x					     ;minus data
		;	  Blue Green Gold Red	   ;
		BYTE	  0E2H,0D8H,0DCH,0ECH ;(-2); My car= Blue(00)
		BYTE	  0D4H,0CEH,0DEH,0ECH	   ;	   =Green(01)
		BYTE	  0C6H,0CEH,0D8H,0E8H	   ;	   = Gold(02)
		BYTE	  0EEH,0D8H,0E0H,0E6H	   ;	   =  Red(03)
Initial_set_y					   ;  plus  data
		;	  Blue Green Gold Red	   ;
		BYTE	  020H,040H,060H,000H
Car_char_num_data
		WORD	  000H,020H,040H
;---------------------------------------------------------------------------
Rival_free_speed
;		    (blue)(green)(gold)(red)  00=Lv.1  40=Lv.2 80=Lv.3
;					      0,4,or8
		BYTE	  88H,44H,80H,40H
;----------------------------------------------------------------------
Set_select_data
		BYTE		00H,02H,01H,03H		 ;car num=0
		BYTE		01H,02H,00H,03H
		BYTE		02H,01H,00H,03H
		BYTE		03H,02H,01H,00H
Set_rank_data
		WORD		08H,08H,04H,00H
Select_rank
		BYTE		002H,003H,004H,001H
Pase_level_data
		BYTE		000,015,030

		END
