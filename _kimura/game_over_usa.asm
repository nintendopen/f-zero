;************************************************************************
;*	 GAME_OVER	   -- game over module --			*
;************************************************************************
;
		INCLUDE	  RP5A22:
		INCLUDE	  RP5C77:
		INCLUDE	  ALPHA
		INCLUDE	  ALPHA2
		INCLUDE	  VARIABLE
		INCLUDE	  BUFFER
;
;=============== Cross reference =======================================
;
		EXT	  Calc_lap_time,CALC_LAP_TIME,Copy_time_rank,Change_BCD
		EXT	  Write_VRAM,Clear_OAM,Select_sound,CHANGE_BCD,Return_long
		EXT	  Disp_repair_cnt,Set_best_time,Set_rank_window,Set_rank_color
		EXT	  Display_retry,Select_sound2,CHECK_START,Set_small_char
		EXT	  Roll_data,Staff_roll_data,SCENE_INIT,TRANS_MINICAR,CLEAR_OAM
		EXT	  Reset_carcolor
;
;=============== Cross definition ======================================
;
		GLB	  Check_pause,Game_pause,Title_demo_end,Special_roll
		GLB	  Over_entry,Master_status,Check_start,Meter_off
;-----------------------------------------------------------------------
		GLB	  Print_OBJsrn,Print_OBJmsg
		GLB	  Print_OBJchr,Put_OBJchr
		GLB	  Display_roll,Trans_car_char
		GLB	  string_table,string_pointer,Inc_OAM_pointer
		GLB	  cursor_pos_h,cursor_pos_v
;
;=============== Define local variable =================================
;
work0		EQU	  0000H		      ; 2 byte	:work 0
work1		EQU	  0002H		      ; 2 byte	:work 1
time_temp	EQU	  0010H		      ; 3 byte	:Time temprary
;
;=============== Define external variable ==============================
;
string_table	EQU	  0038H		      ; 2 byte	:String table address
string_pointer	EQU	  003AH		      ; 2 byte	:String data pointer
cursor_pos_h	EQU	  003CH		      ; 1 byte	:Print location h
cursor_pos_v	EQU	  003DH		      ; 1 byte	:Print location v
lap_disp_status EQU	  00F0H		      ; 1 byte	:Lap display status
lap_disp_pross	EQU	  00F1H		      ; 2 byte	:Lap display process
Backup_RAM	EQU	  700000H	      ; Backup RAM address
;
;=======================================================================
;
		PROG
		EXTEND
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Game pause routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Check pause						*
;************************************************************************
;
		MEM8
		IDX8
Check_pause
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <demo_flag	      ; demo mode ?
		BNE	  Check_pause10	      ; yes.
		LDA	  <exception_flag     ; exception ?
		BNE	  Check_pause20	      ; yes. ( not pause )
;-----------------------------------------------------------------------
Check_pause10	LDA	  <stick_trigger+1
		AND	  #00010000B	      ; push START ?
		BEQ	  Check_pause20	      ; no. ( not pause )
		LDA	  <demo_flag	      ; demo end ?
		BNE	  Title_demo_end      ; yes.
		BRA	  Check_pause100      ; no. ( pause start )
;-----------------------------------------------------------------------
Check_pause20	PLP
		CLC
		RTS
;
;=============== Title demo end ========================================
Title_demo_end
		LDA	  #05H
		STA	  <end_status	      ; Demo end
		LDA	  #04H
		STA	  <game_process	      ; Set game exit mode
		STZ	  <game_status	      ; Clear game status
		PLP
		RTS
;
;=============== Store OAM data ========================================
Check_pause100
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		JSR	  Select_sound2
;-----------------------------------------------------------------------
		LDA	  #004FH	      ; Acc = number of bytes
		LDX	  #OAM_main+000H      ; IX  = source address
		LDY	  #string_buffer+00H  ; IY  = destination address
		MVN	  #00H,#00H
;-----------------------------------------------------------------------
		LDA	  #001FH	      ; Acc = number of bytes
		LDX	  #OAM_main+0D0H      ; IX  = source address
		LDY	  #string_buffer+50H  ; IY  = destination address
		MVN	  #00H,#00H
;-----------------------------------------------------------------------
		LDA	  #000FH	      ; Acc = number of bytes
		LDX	  #OAM_sub	      ; IX  = source address
		LDY	  #string_buffer+70H  ; IY  = destination address
		MVN	  #00H,#00H
;
;=============== Clear OAM sub =========================================
Check_pause150
		IDX8
		SEP	  #00010000B	      ; Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  #1111111111111111B
		TAY
		STA	  !OAM_sub+00H
		STA	  !OAM_sub+02H
		STY	  !OAM_sub+04H
		STA	  !OAM_sub+0DH
;
;=============== Set pause mesage ======================================
Check_pause200
		STZ	  <OAM_mainptr	      ; Set OAM main pointer
		LDX	  #00000000B
		STX	  <OAM_subptr	      ; Set OAM sub pointer
		LDX	  #00000011B
		STX	  <OAM_submask	      ; Set OAM sub mask data
		LDA	  #Pause_table_1
		JSR	  Print_OBJsrn	      ; Print screen
;-----------------------------------------------------------------------
		LDA	  #00D0H
		STA	  <OAM_mainptr	      ; Set OAM main pointer
		LDX	  #0DH
		STX	  <OAM_subptr	      ; Set OAM sub pointer
		LDX	  #00000011B
		STX	  <OAM_submask	      ; Set OAM sub mask data
		LDA	  #Pause_table_2
		JSR	  Print_OBJsrn	      ; Print screen
;-----------------------------------------------------------------------
		LDX	  #0EH
Check_pause210	LDA	  !Pause_cursor,X
		STA	  !OAM_main+0E0H,X
		DEX
		DEX
		BPL	  Check_pause210
;-----------------------------------------------------------------------
		LDX	  #00000000B
		STX	  !OAM_sub+0EH	      ; Set OAM sub
;
;=============== Change PAUSE mode =====================================
Check_pause300
		LDX	  #05H
		STX	  <game_process	      ; Set game pause mode
		PLP
		SEC
		RTS
;
;=============== Pause message =========================================
;
Pause_table_1	WORD	  Pause_message_1
		WORD	  Pause_message_2
		WORD	  0
Pause_table_2	WORD	  Pause_message_3
		WORD	  0
;-----------------------------------------------------------------------
;			  posH posV VHPPCCC
Pause_message_1 BYTE	  058H,050H,00110001B
		BYTE	  _G,_I,_V,_E,SP,_U,_P,SP,0A9H,00H
Pause_message_2 BYTE	  050H,070H,00110011B
		BYTE	  _Y,_E,_S,0
Pause_message_3 BYTE	  098H,070H,00110111B
		BYTE	  _N,_O,0
;-----------------------------------------------------------------------
;			  posH posV char VHPPCCC
Pause_cursor	BYTE	  08CH,070H,0AAH,00110101B
		BYTE	  08CH,078H,0AAH,10110101B
		BYTE	  0A8H,070H,0BAH,00110101B
		BYTE	  0A8H,078H,0BAH,10110101B
;
;************************************************************************
;*		 Game pause entry					*
;************************************************************************
;
		MEM8
		IDX8
Game_pause
		LDA	  <stick_trigger+1
		BIT	  #00010000B	      ; push START ?
		BNE	  Game_pause300	      ; yes.
;-----------------------------------------------------------------------
		LSR	  A		      ; push RIGHT ?
		BCS	  Game_pause110	      ; yes.
		LSR	  A		      ; push LEFT ?
		BCC	  Game_pause200	      ; no.
;
;=============== Cursor move ===========================================
Game_pause100
		LDA	  #048H		      ; Cursor == YES.
		BRA	  Game_pause120
Game_pause110	LDA	  #08CH		      ; Cursor == NO.
;-----------------------------------------------------------------------
Game_pause120	CMP	  !OAM_main+0E0H
		BEQ	  Game_pause200
		STA	  !OAM_main+0E0H
		STA	  !OAM_main+0E4H
		CLC
		ADC	  #1CH
		STA	  !OAM_main+0E8H
		STA	  !OAM_main+0ECH
		JSR	  Select_sound2
;
;=============== Cursor blink ==========================================
Game_pause200
		LDX	  #00000000B	      ; IX = OAM sub data
		LDA	  <frame_counter
		AND	  #00001000B	      ; Cursor display timming ?
		BNE	  Game_pause210	      ; yes.
		LDX	  #11111111B
Game_pause210	STX	  !OAM_sub+0EH	      ; Set OAM sub
		RTS
;
;=============== Give up check =========================================
Game_pause300
		JSR	  Select_sound2
		LDA	  !OAM_main+0E0H
		CMP	  #048H
		BEQ	  Game_pause400
;-----------------------------------------------------------------------
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		LDA	  #004FH	      ; Acc = number of bytes
		LDX	  #string_buffer+00H  ; IX  = destination address
		LDY	  #OAM_main+000H      ; IY  = source address
		MVN	  #00H,#00H
;-----------------------------------------------------------------------
		LDA	  #001FH	      ; Acc = number of bytes
		LDX	  #string_buffer+50H  ; IX  = destination address
		LDY	  #OAM_main+0D0H      ; IY  = source address
		MVN	  #00H,#00H
;-----------------------------------------------------------------------
		LDA	  #000FH	      ; Acc = number of bytes
		LDX	  #string_buffer+70H  ; IX  = destination address
		LDY	  #OAM_sub	      ; IY  = source address
		MVN	  #00H,#00H
;
;=============== DEBUG mode check ======================================
;
		MEM8
		IDX8
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		STZ	  <param5
		STZ	  <still_flag
;--------------------------------------------- Check process meter -----
;;;;		LDA	  <stick_status+1
;;;;		AND	  #00100000B
;;;;		BEQ	  Game_pause310
;;;;		LDA	  #0FFH
;;;;		STA	  <param5
;--------------------------------------------- Check still mode --------
;;;;_pause310	LDA	  <stick_status+0
;;;;		ASL	  A
;;;;		BPL	  Game_pause320
;;;;		LDA	  #0FFH
;;;;		STA	  <still_flag
;--------------------------------------------- Check Final LAP ---------
;;;;_pause320	LDA	  <stick_status+0
;;;;		BPL	  Game_pause330
;
;;;;		LDA	  #03H
;;;;		STA	  <final_debug
;;;;		STA	  >round_counter+0
;;;;		INC	  A
;;;;		STA	  >round_counter+2
;;;;		STA	  >round_counter+4
;;;;		STA	  >round_counter+6
;;;;		LDX	  #08H
;;;;		LDA	  #00H
;;;;_pause325	STA	  >time_buffer,X
;;;;		DEX
;;;;		BPL	  Game_pause325
;-----------------------------------------------------------------------
Game_pause330	LDA	  #03H
		STA	  <game_process	      ; Set game play mode
		RTS
;
;=============== Give up ===============================================
Game_pause400
		PHK
		JSR	  Copy_time_rank
		LDA	  !round_counter
		STA	  !rast_round
		LDA	  #80H
		STA	  !over_rank
		STZ	  !over_status
;;;;		DEC	  <retry_counter
		JSR	  Display_retry
;-----------------------------------------------------------------------
		LDA	  <play_mode
		ASL	  A
		INC	  A
		STA	  <end_status
		LDA	  #04H
		STA	  <game_process	      ; Set game exit mode
		STZ	  <game_status	      ; Clear game status
		LDA	  #10000000B
		TSB	  <sound_status_0
		LDA	  #02H
		STA	  <fade_flag
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\ Game over / clear routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Game over / clear entry				*
;************************************************************************
;
		MEM8
		IDX8
Over_entry
		LDA	  <game_process
		ASL	  A
		TAX
		JMP	  (!Entry_vector,X)
;-----------------------------------------------------------------------
Entry_vector	WORD	  Battle_over	      ; Battle mode game over  ( end = 1 )
		WORD	  Battle_clear	      ; Battle mode game clear ( end = 2 )
		WORD	  Free_over	      ; Free mode game clear   ( end = 3 )
		WORD	  Free_clear	      ; Free mode game over    ( end = 4 )
		WORD	  Demo_mode_exit      ; Demo end	       ( end = 5 )
		WORD	  Over_exit	      ; Game over exit
		WORD	  Staff_roll	      ; Staff roll display
;
;=============== Staff roll display entry ==============================
Staff_roll
		PHK
		PER	  3
		JMP	  >STAFF_ROLL
		RTS
;
;************************************************************************
;*		 Game over / clear exit					*
;************************************************************************
;
		MEM8
		IDX8
Over_exit
		STZ	  <part_flag	      ; Clear pert mode change flag
		STZ	  <pers_flag	      ; Clear pers flag
		STZ	  <meter_switch	      ; Clear meter switch
		STZ	  <window_flag	      ; Clear window flag
		STZ	  <colwte_flag	      ; Clear color write flag
;-----------------------------------------------------------------------
Over_exit10	LDX	  <game_status
		LDA	  !Exit_mode,X
		STA	  <game_mode	      ; Set GAME mode
		LDA	  !Exit_process,X
		STA	  <game_process	      ; Set game process
		STZ	  <game_status	      ; Clear game status
		RTS
;=======================================================================
Exit_mode	BYTE	  2,0,2,1,0
Exit_process	BYTE	  0,0,1,0,2
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Battle mode game over \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Game over entry					*
;************************************************************************
;
		MEM8
		IDX8
Battle_over
		LDA	  <game_status
		ASL	  A
		TAX
		JMP	  (!Battle_ovr_vec,X)
;-----------------------------------------------------------------------
Battle_ovr_vec	WORD	  Meter_off	      ; Game screen blanking
		WORD	  Lap_disp_init	      ; Lap display initial
		WORD	  Lap_disp_main	      ; Lap display main
		WORD	  Lap_disp_wait	      ; Lap dipslay wait
		WORD	  Battle_ovr_init     ; Game over initial
		WORD	  Battle_ovr_main     ; Game over main
;
;************************************************************************
;*		 Initialize title screen				*
;************************************************************************
;
		MEM8
		IDX8
Battle_ovr_init
		LDA	  <retry_counter      ; retry OK ?
		BEQ	  Bat_ovr_init200     ; no.
;
;=============== Initial Game over screen ==============================
Bat_ovr_init100
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		JSR	  Clear_OAM	      ; Clear OAM
		JSR	  Disp_repair_cnt     ; Display repair count
		LDA	  #05C50H	      ; cur_h=40H, cur_v=5CH
		STA	  <cursor_pos_h	      ; Set cursor position
		LDA	  #03773H
		JSR	  Put_OBJchr	      ; Put charatcer ( cursor char )
		LDA	  #Battle_over_msg
		JSR	  Print_OBJsrn	      ; Print screen
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		STZ	  <menu_select
		INC	  <game_status	      ; Next process
Bat_ovr_init190 RTS
;
;=============== Cannot retry ( demo exit ) ============================
Bat_ovr_init200
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		JSR	  Clear_OAM	      ; Clear OAM
		LDA	  #Game_over_msg_4
		JSR	  Print_OBJmsg	      ; Display "GAME OVER"
		MEM8
		SEP	  #00100000B
		LDA	  #00000011B
		STA	  <meter_switch
		STZ	  <initial_timer
		INC	  <game_status	      ; Next process
		RTS
;
;=============== Game over message =====================================
;
Battle_over_msg WORD	  Game_over_msg_1,Game_over_msg_3,0
;-----------------------------------------------------------------------
Game_over_msg_1 BYTE	  060H,058H,00110111B
		BYTE	  _T,_R,_Y,SP,_A,_G,_A,_I,_N,0		  ; 04H
Game_over_msg_3 BYTE	  060H,080H,00110011B
		BYTE	  _E,_N,_D,SP,_G,_A,_M,_E,0
Game_over_msg_4 BYTE	  05CH,060H,00110011B
		BYTE	  _G,_A,_M,_E,SP,_O,_V,_E,_R,0
;
;************************************************************************
;*		 Game over main routine					*
;************************************************************************
;
		MEM8
		IDX8
Battle_ovr_main
		LDA	  <retry_counter      ; retry OK ?
		BNE	  Bat_ovr_main200     ; yes.
;
;=============== Game over =============================================
Bat_ovr_main000
		JSR	  Check_start	      ; push START ?
		BCS	  Bat_ovr_main010     ; yes.
		DEC	  <initial_timer
		BNE	  Bat_ovr_main020
;-----------------------------------------------------------------------
Bat_ovr_main010 LDA	  #10000000B
		TSB	  <sound_status_0
		JMP	  Demo_mode_exit2
Bat_ovr_main020 RTS
;
;=============== Check controller ======================================
Bat_ovr_main200
		JSR	  Check_start	      ; push START ?
		BCS	  Bat_ovr_main400     ; yes.
		LDA	  <stick_trigger+1
		AND	  #00101100B	      ; push UP or DOWN or SELECT ?
		BEQ	  Bat_ovr_main900     ; no.
;
;=============== Cursor move ===========================================
Bat_ovr_main300
		BIT	  #00001000B	      ; push UP ?
		BNE	  Bat_ovr_main320     ; yes.
		BIT	  #00000100B	      ; push DOWN ?
		BNE	  Bat_ovr_main330     ; yes.
;-----------------------------------------------------------------------
Bat_ovr_main310 LDX	  <menu_select	      ; Cursor == TRY AGAIN ?
		BEQ	  Bat_ovr_main330     ; yes.
Bat_ovr_main320 LDX	  #00H		      ; Cursor = TRY AGAIN
		BRA	  Bat_ovr_main350
Bat_ovr_main330 LDX	  #01H		      ; Cursor = GAME OVER
;-----------------------------------------------------------------------
Bat_ovr_main350 CPX	  <menu_select
		BEQ	  Bat_ovr_main360
		STX	  <menu_select	      ; Set cursor pos.V
		LDA	  !Over_cursor,X
		STA	  !OAM_main+1	      ; Set Object pos.V
		JSR	  Select_sound
Bat_ovr_main360 RTS
;
;=============== Exit main routine =====================================
Bat_ovr_main400
		JSR	  Select_sound
		LDA	  #10000000B
		TSB	  <sound_status_0
		LDA	  #02H
		STA	  <fade_flag	      ; Set fade out mode
		LDA	  <menu_select
		STA	  <game_status
		LDA	  #05H
		STA	  <game_process	      ; Next process
Bat_ovr_main900 RTS
;
;=============== Demo mode exit ( return title ) =======================
Demo_mode_exit
		LDA	  !special_demo	      ; special demo ?
		BEQ	  Demo_mode_exit2     ; no.
;-----------------------------------------------------------------------
		DEC	  <demo_flag	      ; demo_flag = 1
		LDA	  #04H		      ; Return best display
		LDY	  <screen_contrast    ; screen blanking ?
		BMI	  Demo_mode_exit3     ; yes.
		LDY	  #02H
		STY	  <fade_flag	      ; Set fade out mode
		BRA	  Demo_mode_exit3
;-----------------------------------------------------------------------
Demo_mode_exit2 LDA	  #02H
		STA	  <fade_flag	      ; Set fade out mode
		LDA	  #01H
Demo_mode_exit3 STA	  <game_status	      ; Return title
		LDA	  #05H
		STA	  <game_process	      ; Exit process
		RTS
;
;=======================================================================
Over_cursor	BYTE	  05CH,084H
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Battle mode course clear \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Course clear entry					*
;************************************************************************
;
		MEM8
		IDX8
Battle_clear
		LDA	  <game_status
		ASL	  A
		TAX
		JMP	  (!Battle_clr_vec,X)
;-----------------------------------------------------------------------
Battle_clr_vec	WORD	  Meter_off	      ; Meter off
		WORD	  Lap_disp_init	      ; Lap display initialize
		WORD	  Lap_disp_main	      ; Lap display main
		WORD	  Lap_disp_wait	      ; Lap display wait
		WORD	  Battle_clr_main     ; Check start botton
;
;************************************************************************
;*		 Battle mode clear main					*
;************************************************************************
;
		MEM8
		IDX8
Battle_clr_main
		INC	  <retry_counter
		INC	  <game_scene
		LDA	  <game_scene
		CMP	  #05H		      ; Last course ?
		BEQ	  Bat_clr_main120     ; yes.
;-----------------------------------------------------------------------
Bat_clr_main110 STZ	  <game_status	      ; Try next scene
		BRA	  Bat_clr_main130
Bat_clr_main120 LDA	  #04H
		STA	  <game_status	      ; Return best display
;-----------------------------------------------------------------------
Bat_clr_main130 LDA	  #02H
		STA	  <fade_flag	      ; Set fade_out mode
		LDA	  #10000000B
		TSB	  <sound_status_0
		LDA	  #05H
		STA	  <game_process
Bat_clr_main190 RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Free mode game over \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Free mode game over entry				*
;************************************************************************
;
		MEM8
		IDX8
Free_over
		LDA	  <game_status
		ASL	  A
		TAX
		JMP	  (!Free_over_vec,X)
;-----------------------------------------------------------------------
Free_over_vec	WORD	  Meter_off	      ; Meter off
		WORD	  Lap_disp_init	      ; Lap display initialize
		WORD	  Lap_disp_main	      ; Lap display main
		WORD	  Lap_disp_wait	      ; Lap dipslay wait
		WORD	  Retry_sel_init      ; Retry select initial
		WORD	  Retry_sel_main      ; Retry select main
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Free mode game clear \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Free mode game clear entry				*
;************************************************************************
;
		MEM8
		IDX8
Free_clear
		LDA	  <game_status
		ASL	  A
		TAX
		JMP	  (!Free_clear_vec,X)
;-----------------------------------------------------------------------
Free_clear_vec	WORD	  Meter_off	      ; Meter off
		WORD	  Lap_disp_init	      ; Lap display initial
		WORD	  Lap_disp_main	      ; Lap display main
		WORD	  Lap_disp_wait	      ; Lap dipslay wait
		WORD	  Best_disp_init      ; Best time display initial
		WORD	  Best_disp_main      ; Best time display main
		WORD	  Best_disp_wait      ; Best time display wait
		WORD	  Retry_sel_init      ; Retry select initial
		WORD	  Retry_sel_main      ; Retry select main
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Lap time display process \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Lap display initial					*
;************************************************************************
;
		MEM8
		IDX8
Lap_disp_init
		JSR	  Clear_OAM	      ; Clear OAM
		LDX	  #00H
		JSR	  Reset_carcolor
		JSR	  Disp_repair_cnt     ; Display repair count
;
;=============== Calculate lap time ====================================
Lap_dsp_init100
		LDA	  <game_scene
		PHK
		JSR	  Calc_lap_time	      ; Calculate lap time
;-----------------------------------------------------------------------
		LDA	  <game_process
		LSR	  A		      ; Game clear ?
		BCC	  Lap_dsp_init200     ; no.
		PHK
		JSR	  Set_best_time	      ; Set best time
;
;=============== Display message =======================================
Lap_dsp_init200
		LDA	  <game_process
		AND	  #00000010B
		TAX
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !Lap_disp_table1,X
		JSR	  Print_OBJmsg
		SEP	  #00100000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
		LDA	  #01111111B
		TRB	  <sound_status_0
		LDA	  #32
		STA	  <initial_timer      ; Set wait timer
		STZ	  <initial_count      ; Set LAP counter
		INC	  <game_status
		RTS
;
;=============== Display message data ==================================
;
Lap_disp_table1 WORD	  Lap_disp_msg_10,Lap_disp_msg_11
;-----------------------------------------------------------------------
Lap_disp_msg_10 BYTE	  048H,020H,00110011B		; color RED
		BYTE	  _L,_A,_P,SP,SP,SP,SP,SP,SP,SP,_R,_A,_N,_K,0
Lap_disp_msg_11 BYTE	  068H,020H,00110011B		; color RED
		BYTE	  _R,_E,_S,_U,_L,_T,_S,0
;
;************************************************************************
;*		 Lap display main					*
;************************************************************************
;
		MEM8
		IDX8
Lap_disp_main
		LDA	  #05H
		STA	  <sound_status_0
		DEC	  <initial_timer
		LDA	  <initial_timer      ; Display timming ?
		BNE	  Lap_dsp_main590     ; no.
		LDA	  !rast_round	      ; no round ?
		BMI	  Lap_dsp_main300     ; yes.
		CMP	  <initial_count      ; counter == rast_round ?
		BEQ	  Lap_dsp_main300     ; yes.
;-----------------------------------------------------------------------
		LDA	  <initial_count
		ASL	  A
		TAY			      ; IY = message data table pointer
;-----------------------------------------------------------------------
		LDA	  <game_process
		BIT	  #00000010B	      ; free mode ?
		BNE	  Lap_dsp_main200     ; yes.
;
;=============== Lap display ( battle mode ) ===========================
Lap_dsp_main100
		CPY	  #0AH		      ; display "TOTAL" ?
		BNE	  Lap_dsp_main110     ; no.
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  #Lap_disp_msg_30
		JSR	  Print_OBJmsg	      ; Display "TOTAL"
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
Lap_dsp_main110 LDX	  <initial_count
		LDY	  !Lap_disp_pos_v1,X
		LDX	  #48H		      ; Acc = display pos.H
		JSR	  Disp_lap_time	      ; Display lap time
		JSR	  Disp_lap_rank	      ; Display lap rank
		BRA	  Lap_dsp_main500
;
;=============== Lap display ( free mode ) =============================
Lap_dsp_main200
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		CPY	  #00H		      ; Display "LAP" ?
		BNE	  Lap_dsp_main210     ; no.
		LDA	  #Lap_disp_msg_20
		JSR	  Print_OBJmsg	      ; Print "LAP"
		LDY	  #00H
Lap_dsp_main210 LDA	  !Lap_disp_table2,Y
		JSR	  Print_OBJmsg
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDX	  <initial_count
		LDY	  !Lap_disp_pos_v2,X
		LDX	  #80H		      ; Acc = display pos.H
		JSR	  Disp_lap_time
		BRA	  Lap_dsp_main500
;
;=============== Display game over message =============================
Lap_dsp_main300
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDX	  !over_status
		LDA	  !Lap_disp_table3,X
		JSR	  Print_OBJmsg
		JSR	  Disp_over_rank
		SEP	  #00100000B	      ; Memory 8 bit mode
		BRA	  Lap_dsp_main580
;
;=============== Set timer =============================================
Lap_dsp_main500
		MEM8
		LDA	  #16
		STA	  <initial_timer      ; Set wait timer
		INC	  <initial_count      ; Increment LAP counter
		LDA	  <initial_count
		CMP	  #06H		      ; display end ?
		BNE	  Lap_dsp_main590     ; no.
Lap_dsp_main580 INC	  <game_status
Lap_dsp_main590 RTS
;
;=============== Lap time display pos.V ================================
Lap_disp_pos_v1 BYTE	  030H,048H,060H,078H,090H,0B8H
Lap_disp_pos_v2 BYTE	  040H,058H,070H,088H,0A0H,0C0H
;=============== Free mode message =====================================
Lap_disp_table2 WORD	  Lap_disp_msg_21,Lap_disp_msg_22
		WORD	  Lap_disp_msg_23,Lap_disp_msg_24
		WORD	  Lap_disp_msg_25,Lap_disp_msg_26
;-----------------------------------------------------------------------
Lap_disp_table3 WORD	  Lap_disp_msg_40,Lap_disp_msg_41
		WORD	  Lap_disp_msg_42,Lap_disp_msg_43
;-----------------------------------------------------------------------
Lap_disp_table4 WORD	  Lap_disp_msg_50,Lap_disp_msg_51
		WORD	  Lap_disp_msg_52,Lap_disp_msg_53
		WORD	  Lap_disp_msg_54
;-----------------------------------------------------------------------
Lap_disp_msg_20 BYTE	  048H,040H,00110011B		; color RED
		BYTE	  _L,_A,_P,0
Lap_disp_msg_21 BYTE	  068H,040H,00110001B		; coolor GREEN
		BYTE	  _1,DT,0
Lap_disp_msg_22 BYTE	  068H,058H,00110111B		; color BLUE
		BYTE	  _2,DT,0
Lap_disp_msg_23 BYTE	  068H,070H,00110001B		; color GREEN
		BYTE	  _3,DT,0
Lap_disp_msg_24 BYTE	  068H,088H,00110111B		; color BLUE
		BYTE	  _4,DT,0
Lap_disp_msg_25 BYTE	  068H,0A0H,00110001B		; color GREEN
		BYTE	  _5,DT,0
Lap_disp_msg_26 BYTE	  048H,0C0H,00110011B		; color RED
		BYTE	  _T,_O,_T,_A,_L,DT,0
Lap_disp_msg_30 BYTE	  048H,0A8H,00110011B		; color RED
		BYTE	  _T,_O,_T,_A,_L,0
Lap_disp_msg_40 BYTE	  068H,0C0H,00110101B		; color YELLOW
		BYTE	  _G,_I,_V,_E,SP,_U,_P,0
Lap_disp_msg_41 BYTE	  058H,0C0H,00110101B		; color YELLOW
		BYTE	  _C,_R,_A,_S,_H,_E,_D,SP,_O,_U,_T,0
Lap_disp_msg_42 BYTE	  05CH,0C0H,00110101B		; color YELLOW
		BYTE	  _R,_A,_N,_K,_E,_D,SP,_O,_U,_T,0
Lap_disp_msg_43 BYTE	  058H,0C0H,00110101B		; color YELLOW
		BYTE	  _O,_U,_T,SP,_O,_F,SP,_T,_I,_M,_E,0
Lap_disp_msg_50 BYTE	  0B8H,030H,00110101B
		BYTE	  2AH,TR,_1,_5,3AH,0
Lap_disp_msg_51 BYTE	  0B8H,048H,00110101B
		BYTE	  2AH,TR,_1,_0,3AH,0
Lap_disp_msg_52 BYTE	  0B8H,060H,00110101B
		BYTE	  2AH,TR,_7,3AH,0
Lap_disp_msg_53 BYTE	  0B8H,078H,00110101B
		BYTE	  2AH,TR,_5,3AH,0
Lap_disp_msg_54 BYTE	  0B8H,090H,00110101B
		BYTE	  2AH,TR,_3,3AH,0
;
;************************************************************************
;*		 Lap display wait					*
;************************************************************************
;
		MEM8
		IDX8
Lap_disp_wait
		JSR	  Check_start	      ; push START ?
		BCC	  Lap_dsp_wait100     ; no.
;-----------------------------------------------------------------------
		INC	  <game_status
		RTS
;
;=============== Flashing renew best time ==============================
Lap_dsp_wait100
		LDA	  !best_time_renew    ; Best time renew ?
		BNE	  Lap_dsp_wait200     ; no.
		LDX	  #5
		JSR	  Lap_dsp_wait500     ; Set attribute
;-----------------------------------------------------------------------
		IDX16
		REP	  #00010000B	      ; Index 16 bit mode
		LDY	  !lap_time_point+10  ; IY = OAM main pointer
		LDX	  #000CH	      ; IX = length
		JSR	  Lap_dsp_wait600     ; Set OAM data
;
;=============== Flashing renew best lap ===============================
Lap_dsp_wait200
		IDX8
		SEP	  #00010000B	      ; Index 8 bit mode
		LDX	  !best_lap_renew     ; Best lap renew ?
		BEQ	  Lap_dsp_wait210     ; no.
;-----------------------------------------------------------------------
		DEX
		JSR	  Lap_dsp_wait500     ; Set attribute
		TXA
		ASL	  A
		TAX
		IDX16
		REP	  #00010000B	      ; Index 16 bit mode
		LDY	  !lap_time_point,X
		LDX	  #000CH
		JSR	  Lap_dsp_wait600
		SEP	  #00010000B	      ; Index 8 bit mode
;-----------------------------------------------------------------------
Lap_dsp_wait210 RTS
;
;=============== Set OBJ attribute =====================================
Lap_dsp_wait500
		IDX8
		LDA	  <frame_counter
		AND	  #00001000B
		BNE	  Lap_dsp_wait510
		LDA	  !Lap_disp_attr,X
		BRA	  Lap_dsp_wait520
Lap_dsp_wait510 LDA	  #00110011B
Lap_dsp_wait520 STA	  <work0	      ; work0 = OBJ attribute
		RTS
;
;=============== Set attribute into OAM ================================
Lap_dsp_wait600
		IDX16
		LDA	  <work0
Lap_dsp_wait610 STA	  !OAM_main+3,Y
		INY
		INY
		INY
		INY
		DEX
		BNE	  Lap_dsp_wait610
		RTS
;
;************************************************************************
;*		 Display lap time					*
;************************************************************************
;
		MEM8
		IDX8
Disp_lap_time
		STX	  <cursor_pos_h	      ; Set cursor position h
		STY	  <cursor_pos_v	      ; Set cursor position v
;-----------------------------------------------------------------------
		LDA	  <initial_count
		ASL	  A
		TAX
		ADC	  <initial_count
		TAY			      ; IY = time data pointer
;-----------------------------------------------------------------------
		LDA	  !OAM_mainptr+0
		STA	  !lap_time_point+0,X
		LDA	  !OAM_mainptr+1
		STA	  !lap_time_point+1,X
;-----------------------------------------------------------------------
		LDX	  <initial_count      ; IX = parameter pointer
		LDA	  !Lap_disp_attr,X
		XBA			      ; B_reg = attribute code
;-----------------------------------------------------------------------
		LDA	  !lap_time_buffer+0,Y
		STA	  <time_temp+0	      ; Set minute
		LDA	  !lap_time_buffer+1,Y
		STA	  <time_temp+1	      ; Set second
		LDA	  !lap_time_buffer+2,Y
		STA	  <time_temp+2	      ; Set 1/100 second
;-----------------------------------------------------------------------
		JSR	  Print_time	      ; Display lap time
		RTS
;
;=======================================================================
Lap_disp_attr	BYTE	  031H,037H,031H,037H,031H,035H
;
;************************************************************************
;*		 Display lap rank					*
;************************************************************************
;
		MEM8
		IDX8
Disp_lap_rank
		LDX	  <initial_count
		LDA	  !rank_buffer,X
		CMP	  #10
		BCS	  Disp_lap_rnk200
;
;=============== Display rank ( < 9 ) ==================================
Disp_lap_rnk100
		LDX	  #0B0H
		STX	  <cursor_pos_h	      ; Set cursor position h
		JSR	  Print_time150
		RTS
;
;=============== Display rank ( 10 < ) =================================
Disp_lap_rnk200
		LDX	  #0A8H
		STX	  <cursor_pos_h	      ; Set cursor position h
		PHK
		JSR	  Change_BCD
		JSR	  Print_time100
		RTS
;
;************************************************************************
;*		 Display over rank					*
;************************************************************************
;
		MEM16
		IDX8
Disp_over_rank
		LDX	  !over_status
		CPX	  #04H
		BNE	  Dsp_ovr_rank110
		LDX	  !over_rank
		CPX	  #20
		BEQ	  Dsp_ovr_rank200
;
;=============== Display over rank =====================================
Dsp_ovr_rank100
		LDA	  <initial_count
		DEC	  A
		ASL	  A
		TAX
		LDA	  !Lap_disp_table4,X
		JSR	  Print_OBJmsg
Dsp_ovr_rank110 RTS
;
;=============== Display rank 20 =======================================
Dsp_ovr_rank200
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
		LDX	  <initial_count
		BPL	  Dsp_ovr_rank210
		LDX	  #00H
Dsp_ovr_rank210 LDA	  !Lap_disp_pos_v1,X
		STA	  <cursor_pos_v	      ; Set cursor position v
		LDA	  #20
		JMP	  Disp_lap_rnk200     ; Display lap rank
;
;************************************************************************
;*		 Check start						*
;************************************************************************
;
		MEM8
		IDX8
Check_start
		LDA	  <stick_trigger+0    ; push A ?
		BMI	  Check_start10	      ; yes.
		LDA	  <stick_trigger+1
		AND	  #10010000B	      ; push START or B ?
		BEQ	  Check_start20	      ; no.
		JSR	  Select_sound
;-----------------------------------------------------------------------
Check_start10	SEC
		RTS
;-----------------------------------------------------------------------
Check_start20	CLC
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Display best time routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Initialize best time display				*
;************************************************************************
;
		MEM16
		IDX8
Best_disp_init
		REP	  #00100000B	      ; Memory 16 bit mode
		SEP	  #00010000B	      ; Index 8 bit mode
;-----------------------------------------------------------------------
		JSR	  Clear_OAM	      ; Clear OAM
		LDA	  #Disp_best_msg_1
		JSR	  Print_OBJmsg	      ; Display "BEST 5"
;-----------------------------------------------------------------------
		IDX16
		REP	  #00010000B	      ; Index 8 bit mode
		LDA	  #0007FH
		LDX	  #0CD80H
		LDY	  #color_buffer+180H
		MVN	  #0FH,#00H	      ; Transfer car color
;-----------------------------------------------------------------------
		MEM8
		IDX8
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		LDA	  #32
		STA	  <initial_timer
		STZ	  <initial_count
;-----------------------------------------------------------------------
		INC	  <game_status
		RTS
;
;=============== Display message data ==================================
;
Disp_best_tbl	WORD	  Disp_best_msg_2
		WORD	  Disp_best_msg_3
		WORD	  Disp_best_msg_4
		WORD	  Disp_best_msg_5
		WORD	  Disp_best_msg_6
		WORD	  Disp_best_msg_7
;
;			  posH posV VHPPCCC
Disp_best_msg_1 BYTE	  028H,038H,00110011B		; color RED
		BYTE	  _B,_E,_S,_T,SP,_5,0
Disp_best_msg_2 BYTE	  068H,038H,00110001B		; color GREEN
		BYTE	  _1,DT,0
Disp_best_msg_3 BYTE	  068H,050H,00110111B		; color BLUE
		BYTE	  _2,DT,0
Disp_best_msg_4 BYTE	  068H,068H,00110001B
		BYTE	  _3,DT,0
Disp_best_msg_5 BYTE	  068H,080H,00110111B
		BYTE	  _4,DT,0
Disp_best_msg_6 BYTE	  068H,098H,00110001B
		BYTE	  _5,DT,0
Disp_best_msg_7 BYTE	  028H,0B8H,00110011B		; color RED
		BYTE	  _B,_E,_S,_T,SP,_L,_A,_P,0
;
;************************************************************************
;*		 Best time display main					*
;************************************************************************
;
		MEM8
		IDX8
Best_disp_main
		DEC	  <initial_timer
		LDA	  <initial_timer
		BNE	  Bst_dsp_main200
;
;=============== Display message =======================================
Bst_dsp_main100
		LDA	  <initial_count
		ASL	  A
		TAY
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !Disp_best_tbl,Y
		JSR	  Print_OBJmsg	      ; Display "BEST ?"
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		JSR	  Disp_best_time
;-----------------------------------------------------------------------
		LDA	  #16
		STA	  <initial_timer
		INC	  <initial_count
		LDA	  <initial_count
		CMP	  #06H
		BNE	  Bst_dsp_main190
		INC	  <game_status
Bst_dsp_main190 RTS
;
;=============== Transfer car character ================================
Bst_dsp_main200
		CMP	  #05H
		BCS	  Bst_dsp_main210
		LDX	  <initial_count
		BNE	  Bst_dsp_main210
		JSR	  Trans_car_char
Bst_dsp_main210 RTS
;
;************************************************************************
;*		 Best display wait					*
;************************************************************************
;
		MEM8
		IDX8
Best_disp_wait
		JSR	  Check_start	      ; push START ?
		BCC	  Bst_dsp_wait100     ; no.
;-----------------------------------------------------------------------
		INC	  <game_status	      ; Game over mode
		RTS
;
;=============== Flashing renew best time ==============================
Bst_dsp_wait100
		LDX	  !best_time_renew
		CPX	  #5		      ; Best time renew ?
		BCS	  Bst_dsp_wait200     ; no.
;-----------------------------------------------------------------------
		JSR	  Lap_dsp_wait500     ; Set attribute
		TXA
		ASL	  A
		TAX
		IDX16
		REP	  #00010000B	      ; Index 16 bit mode
		LDY	  !Renew_time_OAM,X
		LDX	  #000FH	      ; IX = length
		JSR	  Lap_dsp_wait600     ; Set OAM
;
;=============== Flashing renew best lap ===============================
Bst_dsp_wait200
		IDX8
		SEP	  #00010000B	      ; Index 8 bit mode
		LDA	  !best_lap_renew     ; Best lap renew ?
		BEQ	  Bst_dsp_wait210     ; no.
;-----------------------------------------------------------------------
		LDX	  #5
		JSR	  Lap_dsp_wait500     ; Set attribute
		IDX16
		REP	  #00010000B	      ; Index 16 bit mode
		LDY	  #01A0H
		LDX	  #000CH
		JSR	  Lap_dsp_wait600     ; Set OAM
;-----------------------------------------------------------------------
		SEP	  #00010000B	      ; Index 8 bit mode
Bst_dsp_wait210 RTS
;
;=======================================================================
Renew_time_OAM	WORD	  0028H,0068H,00A8H,00E8H,0128H
;
;************************************************************************
;*		 Transfer car character					*
;************************************************************************
;
		MEM8
		IDX8
Trans_car_char
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		DEC	  A
		ORA	  #00001000B
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		AND	  #00FFH
		STA	  !VRAMwte_buffer+04H
		STA	  !VRAMwte_buffer+0CH ; Set data bank
;-----------------------------------------------------------------------
		AND	  #0003H
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		ORA	  #5000H
		STA	  !VRAMwte_buffer+00H
		ORA	  #0100H
		STA	  !VRAMwte_buffer+08H ; Set VRAM address
;-----------------------------------------------------------------------
		LDA	  #0EAA0H
		STA	  !VRAMwte_buffer+02H
		LDA	  #0EAE0H
		STA	  !VRAMwte_buffer+0AH ; Set data address
;-----------------------------------------------------------------------
		LDA	  #00040H
		STA	  !VRAMwte_buffer+06H
		STA	  !VRAMwte_buffer+0EH ; Set data length
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #02H
		STA	  <VRAMbuf_pointer
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Display best time					*
;************************************************************************
;
		MEM8
		IDX8
Disp_best_time
		LDA	  <initial_count
		ASL	  A
		ADC	  <initial_count
		CMP	  #0FH
		BNE	  Disp_best_time2
		ASL	  A
;-----------------------------------------------------------------------
		MEM16
		IDX16
Disp_best_time2 REP	  #00110001B	      ; Memory,Index 16 bit mode
		AND	  #00FFH
		ADC	  !backup_pointer
		TAX
;
;=============== Display best time =====================================
Disp_best_tm100
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  >backup_buffer+0,X
		STA	  <time_temp+0
		LDA	  >backup_buffer+1,X
		STA	  <time_temp+1
		LDA	  >backup_buffer+2,X
		STA	  <time_temp+2
;
;=============== Display time ==========================================
Disp_best_tm200
		IDX8
		SEP	  #00010000B	      ; Index 8 bit mode
		LDX	  <initial_count
		LDA	  #80H
		STA	  <cursor_pos_h	      ; Set display pos.H
		LDA	  !Best_disp_pos_v,X
		STA	  <cursor_pos_v	      ; Set display pos.V
		LDA	  !Lap_disp_attr,X
		XBA			      ; B_reg = attribute code
		JSR	  Print_time
;
;=============== Display car type ======================================
Disp_best_tm400
		LDA	  #0C0H
		STA	  <cursor_pos_h
		DEC	  <cursor_pos_v
		DEC	  <cursor_pos_v
		LDA	  <time_temp+0
		BPL	  Disp_best_tm420
		AND	  #00110000B
		LSR	  A
		LSR	  A
		LSR	  A
		LSR	  A
		TAY
		LDA	  !Best_car_color,Y
;-----------------------------------------------------------------------
Disp_best_tm410 XBA
		LDA	  !Best_car_char,Y
		JSR	  Put_big_char
		RTS
;-----------------------------------------------------------------------
Disp_best_tm420 JSR	  Inc_OAM_pointer
		RTS
;
;=======================================================================
Best_disp_pos_v BYTE	  038H,050H,068H,080H,098H,0B8H
Best_car_color	BYTE	  00111001B,00111011B,00111101B,00111111B
Best_car_char	BYTE	  00H,02H,04H,06H
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Retry select routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Initialize retry select screen				*
;************************************************************************
;
		MEM16
		IDX8
Retry_sel_init
		REP	  #00100000B	      ; Memory 16 bit mode
		JSR	  Clear_OAM	      ; Clear OAM
		LDA	  #05C40H	      ; cur_h=40H, cur_v=5CH
		STA	  <cursor_pos_h	      ; Set cursor position
		LDA	  #03773H
		JSR	  Put_OBJchr	      ; Put charatcer ( cursor char )
		LDA	  #Retry_msg_table    ; Acc = message table address
		JSR	  Print_OBJsrn	      ; Print screen
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		STZ	  <menu_select
		INC	  <game_status	      ; Next process
		RTS
;
;=============== Game over message =====================================
;
Retry_msg_table WORD	  Retry_message_5,Retry_message_4,Retry_message_3
		WORD	  Retry_message_2,Retry_message_1,0
;-----------------------------------------------------------------------
Retry_message_1 BYTE	  060H,020H,00110101B
		BYTE	  _S,_E,_L,_E,_C,_T,0
Retry_message_2 BYTE	  050H,058H,00110011B
		BYTE	  _T,_R,_Y,SP,_A,_G,_A,_I,_N,0
Retry_message_3 BYTE	  050H,070H,00110111B
		BYTE	  _E,_N,_D,SP,_G,_A,_M,_E,0
Retry_message_4 BYTE	  050H,088H,00110001B
		BYTE	  _C,_H,_A,_N,_G,_E,SP,_C,_O,_U,_R,_S,_E,0
Retry_message_5 BYTE	  050H,0A0H,00110101B
		BYTE	  _C,_H,_A,_N,_G,_E,SP,_C,_A,_R,0
;
;************************************************************************
;*		 Game over main routine					*
;************************************************************************
;
		MEM8
		IDX8
Retry_sel_main
		JSR	  Check_start	      ; push START ?
		BCS	  Retry_main200	      ; yes.
		LDA	  <stick_trigger+1
		AND	  #00101100B	      ; push UP or DOWN or SELECT ?
		BEQ	  Retry_main900	      ; no.
;
;=============== Cursor move ===========================================
Retry_main100
		BIT	  #00001000B	      ; push UP ?
		BNE	  Retry_main120	      ; yes.
		BIT	  #00000100B	      ; push DOWN ?
		BNE	  Retry_main130
;-------------------------------------------------------- Push SELECT --
Retry_main110	LDX	  <menu_select
		CPX	  #03H		      ; cursor == max ?
		BNE	  Retry_main140	      ; no.
		LDX	  #00H
		BRA	  Retry_main150
;------------------------------------------------------------ Push UP --
Retry_main120	LDX	  <menu_select	      ; cursor == 0 ?
		BEQ	  Retry_main160	      ; yes.
		DEX			      ; cursor up
		BRA	  Retry_main150
;-----------------------------------------------------------------------
Retry_main130	LDX	  <menu_select
		CPX	  #03H
		BEQ	  Retry_main160
Retry_main140	INX			      ; cursor down
;-----------------------------------------------------------------------
Retry_main150	STX	  <menu_select	      ; Set cursor pos.V
		LDA	  !Retry_cursor,X
		STA	  !OAM_main+1	      ; Set Object pos.V
		JSR	  Select_sound
Retry_main160	RTS
;
;=============== Exit main routine =====================================
Retry_main200
		JSR	  Select_sound
		LDA	  #02H
		STA	  <fade_flag	      ; Set fade out mode
		LDA	  #10000000B
		TSB	  <sound_status_0
		LDA	  <menu_select
		STA	  <game_status
		LDA	  #05H
		STA	  <game_process	      ; Next process
Retry_main900	RTS
;=======================================================================
Retry_cursor	BYTE	  05CH,074H,08CH,0A4H
;
;************************************************************************
;*		 Meter off						*
;************************************************************************
;
		MEM8
		IDX8
Meter_off
		PHK
		PER	  3
		JMP	  >Meter_off100
		RTS
;
;=======================================================================
		DATA
Meter_off100	LDA	  <screen_contrast    ; screen blanking ( game lost ) ?
		BPL	  Meter_off110	      ; no.
		STZ	  <window_flag
		LDA	  #10000111B
		STA	  <meter_switch
		BRA	  Meter_off120
;-----------------------------------------------------------------------
Meter_off110	LDA	  #10000000B
		TSB	  <meter_switch
;-----------------------------------------------------------------------
Meter_off120	LDA	  <play_mode	      ; free mode ?
		BEQ	  Meter_off200	      ; no.
		LDA	  #00000100B
		TRB	  <meter_switch
		BRA	  Meter_off300
;
;=============== Meter buffer clear ====================================
Meter_off200
		LDX	  #21H
Meter_off210	STZ	  !meter_buffer,X
		DEX
		BPL	  Meter_off210
;-----------------------------------------------------------------------
		LDX	  #27H
Meter_off220	STZ	  !meter_buffer,X
		INX
		BPL	  Meter_off220
;-----------------------------------------------------------------------
		LDX	  #07H
Meter_off230	LDA	  >Meter_off_data,X
		STA	  !VRAMwte_buffer,X
		DEX
		BPL	  Meter_off230
		LDA	  #82H
		STA	  <VRAMbuf_pointer    ; Set VRAM buffer pointer
;
;=======================================================================
Meter_off300
		STZ	  <objwte_flag	      ; OAM buffer normal
		PHK
		JSR	  CLEAR_OAM
		LDA	  #00001111B
		STA	  <screen_contrast
		INC	  <game_status
		RTL
;
;=======================================================================
Meter_off_data	BYTE	  40H,74H,22H,00000001B
		BYTE	  67H,74H,59H,00000001B
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\  Print OBJECT message routines \\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Print time						*
;************************************************************************
;
		PROG
		MEM8
		IDX8
Print_time
		PHX
		LDA	  <time_temp+0
		JSR	  Print_time150	      ; Print minute
		LDA	  #0A8H
		JSR	  Print_time200	      ; Print '''
		LDA	  <time_temp+1
		JSR	  Print_time100	      ; Print second
		LDA	  #0B8H
		JSR	  Print_time200	      ; print '"'
		LDA	  <time_temp+2
		JSR	  Print_time100	      ; Print 1/100 secound
		PLX
		RTS
;
;=============== Print digit ===========================================
Print_time100
		PHA
		LSR	  A
		LSR	  A
		LSR	  A
		LSR	  A
		CLC
		ADC	  #080H
		JSR	  Print_OBJchr	      ; Print one block
		JSR	  Print_time210	      ; Cursor next
		PLA
;-----------------------------------------------------------------------
Print_time150	AND	  #0FH
		CLC
		ADC	  #080H
		JSR	  Print_OBJchr	      ; Print one block
		JSR	  Print_time210	      ; Cursor next
		RTS

;=============== Print 1 character =====================================
Print_time200
		JSR	  Put_OBJchr	      ; Print one character
Print_time210	LDA	  <cursor_pos_h
		CLC
		ADC	  #08H
		STA	  <cursor_pos_h	      ; cursor_h += 8
		RTS
;
;************************************************************************
;*		 Print screen						*
;************************************************************************
;				    Entry     Acc.w = string table pointer
		MEM16
		IDX8
Print_OBJsrn
		LDY	  #00H		      ; IY = table offset
		STA	  <string_table	      ; Store string table pointer
;-----------------------------------------------------------------------
Print_OBJsrn10	LDA	  (<string_table),Y    ; Get string pointer
		BEQ	  Print_OBJsrn20      ; if ( end of table )
		PHY
		JSR	  Print_OBJmsg	      ; Print string
		PLY
		INY
		INY
		BRA	  Print_OBJsrn10
Print_OBJsrn20	RTS
;
;************************************************************************
;*		 Print message						*
;************************************************************************
;				    Entry     Acc.w = string pointer
		MEM16
		IDX8
Print_OBJmsg
		PHP
		LDY	  #00H		      ; IY = character pointer
		STA	  <string_pointer     ; Store string pointer
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  (<string_pointer),Y
		STA	  <cursor_pos_h	      ; Set cursor position h
		INY
		LDA	  (<string_pointer),Y
		STA	  <cursor_pos_v	      ; Set cursor position v
		INY
		LDA	  (<string_pointer),Y
		XBA			      ; B_reg = attribute
		INY
;-----------------------------------------------------------------------
Print_OBJmsg10	LDA	  (<string_pointer),Y  ; Get message data
		BEQ	  Print_OBJmsg40      ; if ( end of string )
		CMP	  #0FFH
		BEQ	  Print_OBJmsg20      ; if ( space )
		CMP	  #0FEH		      ; number 'I' ?
		BEQ	  Print_OBJmsg30      ; yes.
		JSR	  Print_OBJchr	      ; Print one character
Print_OBJmsg20	LDA	  <cursor_pos_h
		CLC
		ADC	  #08H
		STA	  <cursor_pos_h	      ; Increment cursor_x
		INY
		BRA	  Print_OBJmsg10
;-----------------------------------------------------------------------
Print_OBJmsg30	LDA	  #_I
		JSR	  Print_OBJchr	      ; Print one character
		LDA	  <cursor_pos_h
		CLC
		ADC	  #05H
		STA	  <cursor_pos_h	      ; Increment cursor_x
		INY
		BRA	  Print_OBJmsg10
;-----------------------------------------------------------------------
Print_OBJmsg40	PLP
		RTS
;
;************************************************************************
;*		 Print character					*
;************************************************************************
;				    Entry     Acc.b = character code
		MEM8
		IDX8
Print_OBJchr
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		CMP	  #60H
		BCS	  Print_OBJchr200
;
;=============== Print one character mode ==============================
Print_OBJchr100
		ORA	  #10000000B
		TAX
		CMP	  #09BH
		BEQ	  Print_OBJchr210     ; Display lower character
		CMP	  #0ABH
		BEQ	  Print_OBJchr210     ; Display lower character
;-----------------------------------------------------------------------
		PHX
		JSR	  Put_OBJchr	      ; Display upper character
		PLX
		CPX	  #0AAH
		BEQ	  Print_OBJchr110
		CPX	  #0BAH
		BNE	  Print_OBJchr230
;-----------------------------------------------------------------------
Print_OBJchr110 XBA
		EOR	  #10000000B
		XBA
		LDA	  <cursor_pos_v
		PHA
		CLC
		ADC	  #08H
		STA	  <cursor_pos_v	      ; Increment cursor_v
		TXA
		JSR	  Put_OBJchr	      ; Display lower character
		XBA
		EOR	  #10000000B
		XBA
		BRA	  Print_OBJchr220
;
;=============== Print two character mode ==============================
Print_OBJchr200
		PHA
		JSR	  Put_OBJchr	      ; Display upper character
		PLX
;-----------------------------------------------------------------------
Print_OBJchr210 LDA	  <cursor_pos_v
		PHA
		CLC
		ADC	  #08H
		STA	  <cursor_pos_v	      ; Increment cursor_v
		TXA
		ADC	  #10H		      ; Acc = lower character code
		JSR	  Put_OBJchr	      ; Display lower character
;-----------------------------------------------------------------------
Print_OBJchr220 PLA
		STA	  <cursor_pos_v	      ; Restore cursor_v position
;-----------------------------------------------------------------------
Print_OBJchr230 PLP
		RTS
;
;************************************************************************
;*		 Put character						*
;************************************************************************
;				    Entry     Acc.b = character number
		MEM8
		IDX16
Put_OBJchr
		PHP
		SEP	  #00100000B	      ; Memory 8 bit mode
		REP	  #00010000B	      ; Index 16 bit mode
;
;=============== Set OAM main ==========================================
Put_OBJchr100
		LDX	  <OAM_mainptr	      ; IX = OAM main pointer
		STA	  !OAM_main+2,X	      ; Set character number
		XBA
		STA	  !OAM_main+3,X	      ; Set attribute status
		XBA
;-----------------------------------------------------------------------
		LDA	  <cursor_pos_h
		STA	  !OAM_main+0,X	      ; Set display H position
		LDA	  <cursor_pos_v
		STA	  !OAM_main+1,X	      ; Set display V position
;
;===============Set OAM sub ============================================
Put_OBJchr200
		IDX8
		SEP	  #00010000B	      ; Index 8 bit mode
		LDX	  <OAM_subptr	      ; IX  = OAM sub pointer
		LDA	  <OAM_submask	      ; Acc = OAM sub mask data
		EOR	  #11111111B
		AND	  !OAM_sub,X
		STA	  !OAM_sub,X	      ; Set OAM sub
		JMP	  Inc_OAM_point1
;
;************************************************************************
;*		 Put big character					*
;************************************************************************
;
;				    Entry     Acc.b = character number
		MEM8
		IDX8
Put_big_char
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDX	  <OAM_subptr	      ; IX  = OAM sub pointer
		PHX
		LDX	  <OAM_submask	      ; Acc = OAM sub mask data
		PHX
		JSR	  Put_OBJchr
;-----------------------------------------------------------------------
		PLA			      ; Acc = OAM sub mask data
		PLX			      ; IX  = OAM sub pointer
		AND	  #10101010B
		ORA	  !OAM_sub,X
		STA	  !OAM_sub,X	      ; Set OAM sub
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Increment OAM pointer					*
;************************************************************************
;
		MEM8
		IDX16
Inc_OAM_pointer
		PHP
		SEP	  #00100000B	      ; Memory 8 bit mode
Inc_OAM_point1	REP	  #00010000B	      ; Index 16 bit mode
;-----------------------------------------------------------------------
		LDX	  <OAM_mainptr	      ; IX = OAM main pointer
		INX
		INX
		INX
		INX
		STX	  <OAM_mainptr	      ; Store OAM main pointer
;-----------------------------------------------------------------------
		LDA	  <OAM_submask
		ASL	  A		      ; Shift left OAM sub mask data
		ASL	  A		      ;	  two time
		BCC	  Inc_OAM_point2      ; if ( not over flow )
		LDA	  #00000011B
		INC	  <OAM_subptr	      ; Increment OAM sub pointer
Inc_OAM_point2	STA	  <OAM_submask	      ; Store OAM sub mask data
;-----------------------------------------------------------------------
		PLP
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Display ending roll \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Display roll entry					*
;************************************************************************
;
		MEM8
		IDX8
Display_roll
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		PHK
		PER	  3
		JMP	  >Display_roll100
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=======================================================================
;
		DATA
SET_RANK_WINDOW PEA	  Return_long
		JMP	  >Set_rank_window
SET_RANK_COLOR	PEA	  Return_long
		JMP	  >Set_rank_color
SET_SMALL_CHAR	PEA	  Return_long
		JMP	  >Set_small_char
;
;=============== Check wait timer ======================================
;
Display_roll100
		LDA	  !roll_timer	      ; timer == 0 ?
		BEQ	  Display_roll200     ; yes.
		DEC	  !roll_timer	      ; decrement timer
		RTL
;
;=============== Check roll data =======================================
Display_roll200
		LDX	  !roll_pointer
		LDA	  >Roll_data,X	     ; normal character ?
		BPL	  Roll_disp_char      ; yes.
		CMP	  #0F0H		      ; display time ?
		BCC	  Roll_disp_time      ; yes.
;-----------------------------------------------------------------------
		AND	  #00001111B
		ASL	  A
		TAX
		JMP	  (!Roll_vector,X)     ; Index jump
Roll_vector	WORD	  Roll_mov_cursor     ; F0: move cursor right
		WORD	  Roll_set_point      ; F1: set display point
		WORD	  Roll_set_attrib     ; F2: set display attribute
		WORD	  Roll_set_timer      ; F3: set wait timer
		WORD	  Roll_OAM_clear      ; F4: clear roll OAM
		WORD	  Roll_calculate      ; F5: calculate lap time
		WORD	  Roll_data_reset     ; F6: reset roll data pointer
		WORD	  Roll_dsp_course     ; F7: display course number
		WORD	  Roll_dsp_window     ; F8: display rank window
		WORD	  Roll_data_end	      ; F9: roll data end
;
;=============== Display time ==========================================
Roll_disp_time
		BIT	  #01000000B
		BNE	  Roll_disp_rank
;-----------------------------------------------------------------------
		AND	  #00111111B
		LSR	  A
		TAX
		LDA	  !lap_time_buffer,X
		BCC	  Roll_disp_rank1
		BRA	  Roll_disp_rank2
;
;=============== Display rank ==========================================
Roll_disp_rank
		AND	  #00111111B
		LSR	  A
		PHP
		TAX
		LDA	  !rank_buffer,X
		PHK
		JSR	  CHANGE_BCD
		PLP
		BCS	  Roll_disp_rank2
		CMP	  #10H
		BCC	  Roll_mov_cursor
;-----------------------------------------------------------------------
Roll_disp_rank1 LSR	  A
		LSR	  A
		LSR	  A
		LSR	  A
Roll_disp_rank2 AND	  #00001111B
		ORA	  #01000000B
;
;=============== Display character =====================================
Roll_disp_char
		JSR	  Roll_set_char
;
;=============== Move cursor right =====================================
Roll_mov_cursor
		LDA	  !roll_cursor_x
		CLC
		ADC	  #08H
		STA	  !roll_cursor_x
		INC	  !roll_pointer
		RTL
;
;=============== Display course number =================================
Roll_dsp_course
		LDA	  !roll_course
		JSR	  Roll_set_char
		LDA	  !roll_cursor_y
		CLC
		ADC	  #08H
		STA	  !roll_cursor_y
		LDA	  !roll_course
		ORA	  #00010000B
		JSR	  Roll_set_char
;-----------------------------------------------------------------------
		INC	  !roll_pointer
		RTL
;
;=============== Display rank window ===================================
Roll_dsp_window
		LDA	  !rank_buffer+5
		STA	  !goal_in_rank
		PHK
		JSR	  SET_RANK_WINDOW
		PHK
		JSR	  SET_RANK_COLOR
		INC	  !roll_pointer
		RTL

;=============== Set display point =====================================
Roll_set_point
		LDX	  !roll_pointer
		INX
		LDA	  >Roll_data,X
		STA	  !roll_cursor_x
		INX
		LDA	  >Roll_data,X
		STA	  !roll_cursor_y
		INX
		STX	  !roll_pointer
		JMP	  Display_roll200
;
;=============== Set display attribute =================================
Roll_set_attrib
		LDX	  !roll_pointer
		INX
		LDA	  >Roll_data,X
		STA	  !roll_attribute
		INX
		STX	  !roll_pointer
		JMP	  Display_roll200
;
;=============== Set wait timer ========================================
Roll_set_timer
		LDX	  !roll_pointer
		INX
		LDA	  >Roll_data,X
		STA	  !roll_timer
		INX
		STX	  !roll_pointer
		RTL
;
;=============== Roll data end =========================================
Roll_data_end
		MEM16
		REP	  #00100000B
;-----------------------------------------------------------------------
		LDA	  <ending_timer
		CMP	  #003CH
		BCC	  Roll_data_end2
		LDA	  #003CH
		STA	  <ending_timer
;-----------------------------------------------------------------------
Roll_data_end2	SEP	  #00100000B
		RTL
;
;=============== Clear roll OAM ========================================
Roll_OAM_clear
		MEM8
		LDA	  #0F0H
		LDX	  #0FCH
Roll_OAM_clear2 STA	  !OAM_main+1,X
		DEX
		DEX
		DEX
		DEX
		CPX	  #0FCH
		BNE	  Roll_OAM_clear2
;-----------------------------------------------------------------------
		LDX	  #0FH
Roll_OAM_clear3 STZ	  !OAM_sub,X
		DEX
		BPL	  Roll_OAM_clear3
;-----------------------------------------------------------------------
		LDA	  #11100000B
		STA	  !spot_color_B
		INC	  !roll_pointer
		STZ	  <OAM_mainptr
		RTL
;
;=============== Reset roll data =======================================
Roll_data_reset
		LDA	  !roll_counter
		CMP	  #05H
		BEQ	  Roll_dat_reset2
		STZ	  !roll_pointer
		RTL
;-----------------------------------------------------------------------
Roll_dat_reset2 INC	  !roll_pointer
		RTL
;
;=============== Calculate lap time ====================================
Roll_calculate
		LDA	  !roll_counter
		STA	  <game_scene	      ; Set game scene
		PHK
		PER	  3
		JMP	  >CALC_LAP_TIME     ; Calculate lap time
		LDA	  #04H
		STA	  <game_scene	      ; Reset game scene
;-----------------------------------------------------------------------
		LDA	  !roll_counter
		ASL	  A
		ASL	  A
		ASL	  A
		TAX
		LDY	  #00H
Roll_calculate2 LDA	  !course_rank,X
		STA	  !rank_buffer,Y      ; Set lap rank
		INX
		INY
		CPY	  #6
		BNE	  Roll_calculate2
;-----------------------------------------------------------------------
		INC	  !roll_counter
		LDA	  !roll_counter
		ORA	  #10000000B
		STA	  !roll_course
		INC	  !roll_pointer
		RTL
;
;=============== Set character to OAM ==================================
Roll_set_char
		LDX	  <OAM_mainptr
		STA	  !OAM_main+2,X
		LDA	  !roll_attribute
		STA	  !OAM_main+3,X
		LDA	  !roll_cursor_x
		STA	  !OAM_main+0,X
		LDA	  !roll_cursor_y
		STA	  !OAM_main+1,X
		INX
		INX
		INX
		INX
		STX	  <OAM_mainptr
		RTS			      ; ( not long )
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Staff roll display routines \\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Free mode game over entry				*
;************************************************************************
;
		DATA
		MEM8
		IDX8
STAFF_ROLL
		LDA	  <game_status
		ASL	  A
		TAX
		JMP	  (!Staff_roll_vec,X)
;-----------------------------------------------------------------------
Staff_roll_vec	WORD	  Staff_roll_init     ; Staff roll initialize
		WORD	  Staff_roll_disp     ; Staff roll display
		WORD	  Staff_roll_end      ; Staff roll end check
;
;************************************************************************
;*		 Staff roll initialize					*
;************************************************************************
;
		MEM8
		IDX8
Staff_roll_init
		LDA	  <game_level
		CMP	  #02H		      ; Expert level ?
		BEQ	  Staff_init10	      ; yes.
		JMP	  Staff_roll_end2     ; return title mode
;-----------------------------------------------------------------------
Staff_init10	LDA	  !master_level	      ; master level ?
		BEQ	  Staff_init100	      ; no.
		JMP	  Special_init	      ; special demo initialize
;
;=============== Clear OAM =============================================
Staff_init100
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		LDA	  #0F700H
		STA	  !OAM_main+0
		LDA	  #03100H
		STA	  !OAM_main+2
		LDA	  #512-4-1
		LDX	  #OAM_main+0
		LDY	  #OAM_main+4
		MVN	  #00H,#00H
;-----------------------------------------------------------------------
		LDA	  #0000H
		STA	  !OAM_sub+0
		LDA	  #32-2-1
		LDX	  #OAM_sub+0
		LDY	  #OAM_sub+2
		MVN	  #00H,#00H
;
;=============== Initialize work =======================================
Staff_init200
		STZ	  <OAM_mainptr
		STZ	  !roll_counter
		MEM8
		IDX8
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		LDA	  #20H
		STA	  !roll_timer
;-----------------------------------------------------------------------
		LDA	  >backup_buffer+1FAH
		STA	  !staff_abort_flg
		LDX	  <game_world
		LDA	  >backup_buffer+1FAH
		ORA	  >Master_status,X
		STA	  >backup_buffer+1FAH
		STA	  >Backup_RAM+1FAH
;-----------------------------------------------------------------------
		LDA	  #00010000B
		STA	  !Through_screen     ; Display OBJ only
		LDA	  #00001111B
		STA	  <screen_contrast    ; Set screen contrast
		INC	  <game_status
		RTL
;=======================================================================
Master_status	BYTE	  00010001B,00100010B,01000100B
;
;************************************************************************
;*		 Staff roll display main				*
;************************************************************************
;
		MEM8
		IDX16
Staff_roll_disp
		REP	  #00010000B	      ; Index 16 bit mode
;-----------------------------------------------------------------------
		LDA	  <frame_counter
		AND	  #00000011B
		BNE	  Staff_disp230
;
;=============== Scroll up =============================================
Staff_disp100
		LDX	  #01FCH
		LDA	  #0F7H
Staff_disp110
		CMP	  !OAM_main+1,X
		BEQ	  Staff_disp120
		DEC	  !OAM_main+1,X
Staff_disp120	DEX
		DEX
		DEX
		DEX
		BPL	  Staff_disp110
;
;=============== Display string ========================================
Staff_disp200
		DEC	  !roll_timer	      ; Display timming ?
		BNE	  Staff_disp230	      ; no.
		LDA	  #4
		STA	  <sound_status_0
;-----------------------------------------------------------------------
		LDX	  !roll_counter	      ; IX = roll data pointer
		LDA	  >Staff_roll_data,X
		BEQ	  Staff_disp300
		STA	  !roll_cursor_x      ; Set cursor position x
		INX
;-----------------------------------------------------------------------
Staff_disp210	LDA	  >Staff_roll_data,X
		BEQ	  Staff_disp220	      ; if ( end of string )
		JSR	  Staff_putchar	      ; Put character
		INX
		BRA	  Staff_disp210
;-----------------------------------------------------------------------
Staff_disp220	INX
		LDA	  >Staff_roll_data,X
		STA	  !roll_timer
		INX
		STX	  !roll_counter
;-----------------------------------------------------------------------
Staff_disp230	LDA	  !staff_abort_flg
		BEQ	  Staff_disp240
		JSR	  CHECK_START	      ; push START ?
		BCS	  Staff_roll_end1     ; yes.
;-----------------------------------------------------------------------
Staff_disp240	SEP	  #00110000B	      ; Memory,Index 8 bit mode
		RTL
;
;=============== End of display ========================================
Staff_disp300
		INC	  <game_status
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		RTL
;
;************************************************************************
;*		 Check staff roll end					*
;************************************************************************
;
		MEM8
		IDX8
Staff_roll_end
		JSR	  CHECK_START	      ; push START ?
		BCC	  Staff_roll_end3     ; no.
;-----------------------------------------------------------------------
Staff_roll_end1 SEP	  #00010000B	      ; Index 8 bit mode
		LDA	  #01H
		STA	  <demo_flag
		LDA	  #02H
		STA	  <fade_flag
;-----------------------------------------------------------------------
Staff_roll_end2 LDA	  #05H
		STA	  <game_process
		LDA	  #04H
		STA	  <game_status	      ; Return best display
		LDA	  #10000000B
		TSB	  <sound_status_0
Staff_roll_end3 RTL
;
;************************************************************************
;*		 Put character						*
;************************************************************************
;
		MEM8
		IDX16
Staff_putchar
		CMP	  #0FFH		      ; Acc = Space code ?
		BEQ	  Staff_putchar10     ; yes.
;-----------------------------------------------------------------------
		LDY	  <OAM_mainptr	      ; IX = OAM main pointer
		STA	  !OAM_main+2,Y	      ; Set display character
		LDA	  !roll_cursor_x
		STA	  !OAM_main+0,Y	      ; Set display position x
		LDA	  #0F0H
		STA	  !OAM_main+1,Y	      ; Set display position y
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
		TYA
		ADC	  #0004H
		AND	  #01FFH
		STA	  <OAM_mainptr	      ; Set OAM main pointer
		SEP	  #00100000B	      ; Memory 16 bit mode
		MEM8
;-----------------------------------------------------------------------
Staff_putchar10 LDA	  !roll_cursor_x
		CLC
		ADC	  #08H
		STA	  !roll_cursor_x
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Special demo routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Special demo initial					*
;************************************************************************
;
		MEM8
		IDX8
Special_init
		LDA	  <mycar_number
		STA	  !special_car	      ; Set special demo car
		STZ	  <mycar_number	      ; Clear my car number
		LDA	  #0FFH
		STA	  !rival_number
;-----------------------------------------------------------------------
		LDA	  #01H
		STA	  <play_mode	      ; Set free mode
		LDA	  <game_world
		TAX
		INC	  A
		STA	  !special_demo	      ; Set special demo flag
		LDA	  >Demo_scene,X
		STA	  <game_scene
		INC	  <demo_flag	      ; Set demo flag
;
;=============== Initial title screen ==================================
Special_init100
		PHK
		JSR	  SCENE_INIT	      ; Set game screen
		JSR	  TRANS_MINICAR	      ; Transfer mini car character
		STZ	  <part_flag
		STZ	  <meter_switch
		LDA	  #02H
		STA	  <pers_flag
		LDA	  #40H
		STA	  <screen_zoom+0
		LDA	  #01H
		STA	  <screen_zoom+1
;-----------------------------------------------------------------------
		STZ	  <window_color_sw
		LDA	  #10010000B
		STA	  <color_add_sw
		LDA	  #00000100B
		STA	  !const_color_R
		STA	  !const_color_G
		STA	  !const_color_B
;
;=============== Initialize window HDMA ================================
Special_init200
		LDX	  #04H
Special_init210 LDA	  >Window_HDMA,X
		STA	  !DMA_7,X	      ; Set DMA #2 parameter
		DEX
		BPL	  Special_init210
;-----------------------------------------------------------------------
		LDA	  #02H
		STA	  <window_flag	      ; Set window flag
		LDA	  #00000011B
		STA	  <window_control+0   ; Set window switch
		LDA	  #00000001B
		STA	  <window_main	      ; Window BG1 on
		LDA	  #10000000B
		STA	  <HDMA_switch	      ; HDMA #7 on
;
;=============== Work initialize =======================================
Special_init300
		INC	  <demo_flag	      ; demo_flag = 2
		STZ	  !demo_status
		STZ	  !demo_back_data
		STZ	  !demo_data_point
		STZ	  !round_counter
		STZ	  !demo_contrast
;-----------------------------------------------------------------------
		LDA	  #00110001B
		STA	  !roll_attribute
		STZ	  !roll_pointer
		STZ	  !roll_counter
		STZ	  !roll_timer
;-----------------------------------------------------------------------
		LDA	  #80H
		STA	  <sound_status_0
		LDA	  #02H
		STA	  <game_mode	      ; Set PLAY mode
		DEC	  A
		STA	  <game_process	      ; Clear process counter
		STA	  <game_status	      ; Clear game status
;-----------------------------------------------------------------------
		INC	  <fade_step	      ; fade step = 2
		STZ	  <fade_flag	      ; no fade in/out
		RTL
;
;=============== Window HDMA parameter =================================
;
Window_HDMA	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  26H		      ; DMA B address low
		BYTE	  LOW  Window_data    ; DMA A address low
		BYTE	  HIGH Window_data    ; DMA A address high
		BYTE	  BANK Window_data    ; DMA A address bank
;-----------------------------------------------------------------------
Window_data	BYTE	  040H
		BYTE	  001H,000H
		BYTE	  040H
		BYTE	  000H,0FFH
		BYTE	  050H
		BYTE	  000H,0FFH
		BYTE	  001H
		BYTE	  001H,000H
		BYTE	  000H
;-----------------------------------------------------------------------
Demo_scene	BYTE	  4,7,8
;
;************************************************************************
;*		 Display special roll					*
;************************************************************************
;
		PROG
		MEM8
		IDX8
Special_roll
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		PHK
		PER	  3
		JMP	  >Special_roll100
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== Check display timming =================================
;
		DATA
Special_roll100
		LDA	  !roll_counter
		CMP	  #01111001B
		BNE	  Special_roll300
;-----------------------------------------------------------------------
		LDA	  <frame_counter
		AND	  #00000111B
		BNE	  Special_roll190
		LDA	  !roll_timer	      ; timer == 0 ?
		BEQ	  Special_roll200     ; yes.
		DEC	  !roll_timer	      ; decrement timer
Special_roll190 RTL
;
;=============== Check roll data =======================================
Special_roll200
		LDX	  !roll_pointer
		LDA	  >Roll_data2,X	     ; normal character ?
		BMI	  Special_roll210     ; no.
;-----------------------------------------------------------------------
		XBA
		LDA	  #00000010B
		TSB	  <sound_status_2
		XBA
		JMP	  Roll_disp_char      ; yes.
;-----------------------------------------------------------------------
Special_roll210 AND	  #00001111B
		ASL	  A
		TAX
		JMP	  (!Sp_roll_vector,X)  ; Index jump
Sp_roll_vector	WORD	  Roll_mov_cursor     ; F0: move cursor right
		WORD	  Special_roll500     ; F1: set display point
		WORD	  Special_roll600     ; F2: set wait timer
		WORD	  Roll_OAM_clear      ; F3: clear roll OAM
		WORD	  Special_roll700     ; F4: roll data end
		WORD	  Special_roll400     ; F5: screen fade out
;
;=============== Screen fade in and set character ======================
Special_roll300
		STZ	  <sound_status_0
		CMP	  #2EH
		BCS	  Special_roll310
;-----------------------------------------------------------------------
		PHK
		JSR	  SET_SMALL_CHAR
;-----------------------------------------------------------------------
Special_roll310 LDA	  !roll_counter
		BNE	  Special_roll320
		INC	  !roll_counter
		JMP	  Roll_OAM_clear
;-----------------------------------------------------------------------
Special_roll320 INC	  !roll_counter
		LSR	  A
		LSR	  A
		LSR	  A
		STA	  <screen_contrast
		RTL
;
;=============== Screen fade out =======================================
Special_roll400
		LDA	  #10000000B
		TSB	  <sound_status_0
		DEC	  <screen_contrast
		BNE	  Special_roll410
		LDA	  #10000000B
		STA	  <screen_contrast
		INC	  !roll_pointer
Special_roll410 RTL
;
;=============== Set display point =====================================
Special_roll500
		LDX	  !roll_pointer
		INX
		LDA	  >Roll_data2,X
		STA	  !roll_cursor_x
		INX
		LDA	  >Roll_data2,X
		STA	  !roll_cursor_y
		INX
		STX	  !roll_pointer
		RTL
;
;=============== Set wait timer ========================================
Special_roll600
		LDX	  !roll_pointer
		INX
		LDA	  >Roll_data2,X
		STA	  !roll_timer
		INX
		STX	  !roll_pointer
		RTL
;
;=============== End of special demo ===================================
Special_roll700
		LDA	  #07H
		STA	  <mycar_damage+1
		RTL
;
;************************************************************************
;*		 Special roll data					*
;************************************************************************
;
Roll_data2	BYTE	  000H,0F1H,10H,20H
		BYTE	  TT,HH,EE,0F0H,RR,AA,CC,EE,0F0H,II,SS,0F0H
		BYTE	  OO,VV,EE,RR,LN,LN,LN
		BYTE	  0F2H,018H,0F3H,0F1H,10H,20H
		BYTE	  YY,OO,UU,0F0H,AA,RR,EE,0F0H,AA,NN,0F0H
		BYTE	  FF,6DH,ZZ,EE,RR,OO,0F0H,MM,AA,SS,TT,EE,RR,6CH
		BYTE	  0F2H,018H,0F3H,0F1H,10H,20H
		BYTE	  GG,OO,OO,DD,BB,YY,EE
		BYTE	  0F2H,018H,0F3H,0F1H,10H,20H
		BYTE	  FF,RR,OO,MM,0F0H,CC,AA,PP,TT,AA,II,NN,0F0H
		BYTE	  FF,AA,LL,CC,OO,NN
		BYTE	  0F1H,10H,30H
		BYTE	  AA,NN,DD,0F0H,TT,HH,EE,0F0H,FF,6DH,ZZ,EE,RR,OO
		BYTE	  0F0H,CC,RR,EE,WW,LN
		BYTE	  0F2H,048H,0F5H,0F4H
;
		END
