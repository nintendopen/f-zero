;************************************************************************
;*	 EN_INIT_3	   -- Open drive data sub routin ---		*
;************************************************************************
		INCLUDE	  BUFFER
		INCLUDE	  VARIABLE
		INCLUDE	  WORK
;-----------------------------------------------------------------------
		EXT	  Check_enemy
		EXT	  Course_address
		EXT	  Free_address
		EXT	  Free_jump_data
		EXT	  Battle_jump_data
		EXT	  Crash_pase_data
		EXT	  Change_point
;
		EXT	  Phase_check
		EXT	  Angle_CALC
		EXT	  mark_point_x,mark_point_y,view_point_x,view_point_y
;-----------------------------------------------------------------------
		GLB	  Open_drive_data
;-----------------------------------------------------------
;		COMN
;-----------------------------------------------------------
tempolary	EQU	    00H
;
address_data	EQU	    10H
point_x_addr	EQU	    10H
point_y_addr	EQU	    12H
status_address	EQU	    14H
drive_0_addr	EQU	    16H
drive_1_addr	EQU	    18H
drive_2_addr	EQU	    1AH
;
code_data	EQU	    24H
open_data	EQU	    26H
data_address	EQU	    26H
start_area	EQU	    28H
start_pos_x	EQU	    2AH
start_pos_y	EQU	    2CH
end_area	EQU	    2EH
;
basic_address	EQU	    30H
point_counter	EQU	    32H
;============================================================================
;		OPEN COURSE DATA FROM ROM TO RAM
;===========================================================================
		PROG
		extend
Open_drive_data
		MEM8
		IDX8
		SEP		#00110000B
;
		PEA		0002H
		PLB
;
		STZ		!jump_checker
		LDA		<play_mode
		BEQ		Open_battle
Open_free
		LDA		<game_scene
		TAX
		LDA		>Free_jump_data,X
		STA		!check_jump_address
		TXA
		ASL		A
		TAX
		MEM16
		REP		#00100000B
		LDA		!Free_address,X
		BRA		Set_open_data
Open_battle
		MEM8
		LDA		<game_world
		ASL		A
		ASL		A
		CLC
		ADC		<game_world
		ADC		<game_scene
		TAX
		LDA		>Crash_pase_data,X
		STA		!crash_pase
		LDA		>Battle_jump_data,X
		STA		!check_jump_address
		TXA
		ASL		A
		TAX
		MEM16
		REP		#00100000B
		LDA		!Course_address,X
Set_open_data
		STA		<basic_address
		STZ		<point_counter
		STZ		!branch_flag
		IDX16
		REP		#00010000B
		LDY		#00H
Open_data_loop
		LDA		(<basic_address),Y
		STA		<code_data
		INY
		LDX		#00H
Set_data_loop
		LDA		(<basic_address),Y
		STA		<open_data,X
		INY
		INY
		INX
		INX
		CPX		#08H
		BNE		Set_data_loop
;
		LDA		<code_data
		AND		#00FFH
		BEQ		End_open_loop
		CMP		#00FFH
		BEQ		Open_branch
		PHY
		JSR		Open_the_data
		PLY
		BRA		Open_data_loop
Open_branch
		LDA		<point_counter
		PHA
		INC		A
		STA		!branch_addr
		STA		<point_counter
		JSR		Open_the_data
		PLA
		STA		<point_counter
End_open_loop
		LDA		<point_counter
		DEC		A
		STA		<area_number
		PLB
		RTS
;----------------------------------------------------------------------------------
Open_the_data
		MEM16
		IDX16
		LDY		#00H
Set_address_loop
		TYX
		LDA		(<data_address),Y
		STA		<address_data,X
		INY
		INY
		CPY		#0CH
		BNE		Set_address_loop
;
		LDA		<start_area
		PHA
		AND		#00FFH
		TAY
		PLA
		XBA
		AND		#00FFH
		STA		<end_area
;
		LDA		<point_counter
		ASL		A
		TAX
		LDA		<start_pos_x
		STA		!position_x-02H,X
		LDA		<start_pos_y
		STA		!position_y-02H,X
Open_loop
		MEM8
		IDX8
		SEP		#00110000B
		LDX		<point_counter
		LDA		(<status_address),Y
		STA		!area_status,X
		BIT		#00100000B
		BEQ		No_branch_bit
		LDA		!branch_flag
		BMI		No_branch_bit
		DEC		!branch_flag
		STX		!branch_area
No_branch_bit
		LDA		(<drive_0_addr),Y
		STA		!drive_data_0,X
		LDA		(<drive_1_addr),Y
		STA		!drive_data_1,X
		LDA		(<drive_2_addr),Y
		STA		!drive_data_2,X
		MEM16
		IDX16
		REP		#00110000B
		TXA
		ASL		A
		TAX
		LDA		(<point_x_addr),Y
		JSR		Set_pos_sub
		ADC		!position_x-02H,X
		STA		!position_x+00H,X
		LDA		(<point_y_addr),Y
		JSR		Set_pos_sub
		ADC		!position_y-02H,X
		STA		!position_y+00H,X
;
		CPY		<end_area
		BEQ		Open_loop_RTS
		INC		<point_counter
		INY
		BNE		Open_loop
Open_loop_RTS
		INC		<point_counter
		RTS
;-----------------------------------------------------------------------------
		MEM16
Set_pos_sub
		AND		#00FFH
		CMP		#0080H
		BCC		No_minus_sub
		ORA		#0FF00H
No_minus_sub
		ASL		A
		ASL		A
		ASL		A
		CLC
		RTS
		end
