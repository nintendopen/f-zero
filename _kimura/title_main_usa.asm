;************************************************************************
;*	 TITL_MAIN	   -- title & car select module --		*
;************************************************************************
;
		INCLUDE	  RP5A22:
		INCLUDE	  RP5C77:
		INCLUDE	  ALPHA
		INCLUDE	  ALPHA2
		INCLUDE	  VARIABLE
		INCLUDE	  BUFFER
;
;=============== Cross reference =======================================
;
		EXT	  Return_long,Start_DMA,Set_course_data,Scene_init
		EXT	  Write_character,Write_VRAM,Engine_sound,Trans_car_char
		EXT	  Clear_OAM,Setting_CAR,Set_OAM_address,Inc_OAM_pointer
		EXT	  TRANSFER_OBJECT,TRANS_SEL_CHAR,EXPAND_SELECT,EXPAND_BACK
		EXT	  Map_data_number,Course_color,Back_screen,Display_course3
		EXT	  Map_char_offset,ERASE_RECORD,Boot_APU,Select_sound
		EXT	  TRANS_TITLE_CHR,TITLE_OBJECT,TITLE_SUB,Print_OBJmsg,Put_OBJchr
		EXT	  Backup_pointer,Master_status,Course8_color
;
;=============== Cross definition ======================================
;
		GLB	  Title_entry
		GLB	  Select_entry,CHECK_START,SCENE_INIT,CLEAR_OAM
;
;=============== Define local variable =================================
;
work0		EQU	  0000H		      ; 2 byte	:work 0
string_table	EQU	  0010H
string_pointer	EQU	  0012H
;-----------------------------------------------------------------------
cursor_pos_h	EQU	  003CH		      ; 1 byte	:Print location h
cursor_pos_v	EQU	  003DH		      ; 1 byte	:Print location v
;
;=============== Define static variable ================================
;
demo_wait_timer EQU	  00F0H		      ; 2 byte	:Title demo wait timer
;-----------------------------------------------------------------------
yes_no_status	EQU	  00F0H		      ; 1 byte	:My car select check ( YES or NO )
open_close_flag EQU	  00F1H		      ; 1 byte	:Window open close flag
APU_port0	EQU	  2140H
;
;=======================================================================
;
		PROG
		EXTEND
;
;************************************************************************
;*		 Title select routine interface				*
;************************************************************************
;
;=============== Title routine entry ===================================
Title_entry
		PHK
		PER	  3
		JMP	  >TITLE_ENTRY
		RTS
;=============== Select routine entry ==================================
Select_entry
		PHK
		PER	  3
		JMP	  >SELECT_ENTRY
		RTS

;=======================================================================
;
		DATA
;
WRITE_VRAM	PEA	  Return_long
		JMP	  >Write_VRAM
WRITE_CHARACTER PEA	  Return_long
		JMP	  >Write_character
ENGINE_SOUND	PEA	  Return_long
		JMP	  >Engine_sound
CLEAR_OAM	PEA	  Return_long
		JMP	  >Clear_OAM
SETTING_CAR	PEA	  Return_long
		JMP	  >Setting_CAR
SET_OAM_ADDRESS PEA	  Return_long
		JMP	  >Set_OAM_address
TRANS_CAR_CHAR	PEA	  Return_long
		JMP	  >Trans_car_char
START_DMA	PEA	  Return_long
		JMP	  >Start_DMA
INC_OAM_POINTER PEA	  Return_long
		JMP	  >Inc_OAM_pointer
DISPLAY_COURSE3 PEA	  Return_long
		JMP	  >Display_course3
SET_COURSE_DATA PEA	  Return_long
		JMP	  >Set_course_data
BOOT_APU	PEA	  Return_long
		JMP	  >Boot_APU
SCENE_INIT	PEA	  Return_long
		JMP	  >Scene_init
SELECT_SOUND	PEA	  Return_long
		JMP	  >Select_sound
PRINT_OBJMSG	PEA	  Return_long
		JMP	  >Print_OBJmsg
PUT_OBJCHR	PEA	  Return_long
		JMP	  >Put_OBJchr
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\ Title routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Game title entry					*
;************************************************************************
;
		MEM8
		IDX8
TITLE_ENTRY
		LDA	  <game_process
		ASL	  A
		TAX
		JMP	  (!Title_vector,X)    ; Jump mode process
;-----------------------------------------------------------------------
Title_vector	WORD	  Title_init	      ; Initialize title screen
		WORD	  Title_main	      ; Title main routine
		WORD	  Best_init	      ; Best time display initialize
		WORD	  Best_menu	      ; Best screen select menu
		WORD	  Best_display	      ; Best screen display
		WORD	  Best_select	      ; Best screen select
		WORD	  Erase_data	      ; Erase best time data
;
;************************************************************************
;*		 Check start						*
;************************************************************************
;
		MEM8
		IDX8
CHECK_START
		LDA	  <stick_trigger+0    ; push A ?
		BMI	  Check_start10	      ; yes.
		LDA	  <stick_trigger+1
		AND	  #10010000B	      ; push START or B ?
		BEQ	  Check_start20	      ; no.
		PHK
		JSR	  SELECT_SOUND
;-----------------------------------------------------------------------
Check_start10	SEC
		RTS
;-----------------------------------------------------------------------
Check_start20	CLC
		RTS
;
;************************************************************************
;*		 Initialize title screen				*
;************************************************************************
;
		MEM8
		IDX8
Title_init
		LDA	  <demo_flag	      ; demo mode ?
		BNE	  Title_init100	      ; yes.
		INC	  <demo_flag
;
;=============== Initial title work ====================================
Title_init100
		STZ	  !special_demo
		STZ	  <mycar_number	      ; Clear my car number
		LDA	  #0FFH
		STA	  !rival_number	      ; Clear rival car number
;-----------------------------------------------------------------------
		STZ	  <retry_counter      ; Set retry counter
		STZ	  !master_level
		STZ	  <menu_select
		LDA	  #06H
		STA	  <game_scene
		LDA	  #01H
		STA	  <play_mode
;-----------------------------------------------------------------------
		LDX	  #05H
Title_init110	STZ	  !score_disp_buff,X
		DEX
		BPL	  Title_init110
;
;=============== Initial title screen ==================================
Title_init200
		PHK
		JSR	  SCENE_INIT	      ; Set game screen
		PHK
		JSR	  TRANS_TITLE_CHR     ; Transfer title character
;-----------------------------------------------------------------------
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		LDA	  !color_buffer+4EH
		STA	  !color_buffer+5CH   ; Clear start line
		STA	  !color_buffer+5EH   ; Clear start line
;-----------------------------------------------------------------------
		LDX	  #0C2E0H
		LDY	  #color_buffer+100H
		LDA	  #007FH
		MVN	  #0FH,#00H	      ; Transfer title color
;-----------------------------------------------------------------------
		LDX	  #TITLE_OBJECT
		LDY	  #OAM_main
		LDA	  #00DFH
		MVN	  #0BH,#00H	      ; Transfer title OAM main
;-----------------------------------------------------------------------
		LDX	  #TITLE_SUB
		LDY	  #OAM_sub
		LDA	  #000DH
		MVN	  #0BH,#00H	      ; Transfer title OAM sub
;-----------------------------------------------------------------------
		MEM8
		IDX8
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		JSR	  Search_backup
		STA	  <course_select
		CMP	  #0FH
		BNE	  Title_init300
		LDA	  #11111110B
		STA	  !OAM_sub+08H	      ; Clear "RECORD"
		LDA	  #00001011B
		STA	  !OAM_sub+09H	      ; Clear "S"
;
;=============== Work initialize =======================================
Title_init300
		LDX	  #01FH
		LDA	  #0FFH
Title_init310	STA	  !best_time_rank,X
		DEX
		BPL	  Title_init310
;-----------------------------------------------------------------------
		LDA	  <sound_status_0
		BPL	  Title_init320
		LDA	  #01111111B
		TRB	  <sound_status_0
;-----------------------------------------------------------------------
Title_init320	LDA	  #02H
		STA	  <demo_flag
		INC	  <fade_step	      ; fade step = 2
		INC	  <game_process	      ; Set game process
		STZ	  <game_status
		RTL
;
;************************************************************************
;*		 Title main routine					*
;************************************************************************
;
		MEM8
		IDX8
Title_main
		LDA	  #04H
		STA	  <sound_status_0     ; Start TITLE sound
;-----------------------------------------------------------------------
		LDA	  <stick_trigger+0    ; push A ?
		BMI	  Title_main300	      ; yes.
		LDA	  <stick_trigger+1
		BIT	  #10010000B	      ; push START or B ?
		BNE	  Title_main300	      ; yes.
		AND	  #00101100B	      ; cursor move ?
		BNE	  Title_main100	      ; yes.
;--------------------------------------------------- Check demo start --
		INC	  <demo_wait_timer+0
		BNE	  Title_main10
		INC	  <demo_wait_timer+1
		LDA	  <demo_wait_timer+1
		CMP	  #05H		      ; demo start time ?
		BNE	  Title_main10	      ; no.
		JMP	  Title_main500	      ; demo start
Title_main10	JMP	  Title_main900
;
;=============== Cursor move ===========================================
Title_main100
		LDX	  #02H
		LDY	  <course_select
		CPY	  #0FH		      ; Display "RECORD" ?
		BNE	  Title_main105	      ; yes.
		DEX
;-----------------------------------------------------------------------
Title_main105	STX	  <work0
		LDY	  #00H
		LDX	  <menu_select
;-----------------------------------------------------------------------
		BIT	  #00001000B	      ; push UP ?
		BNE	  Title_main120	      ; yes.
		BIT	  #00000100B	      ; push DOWN ?
		BNE	  Title_main130	      ; yes.
;-------------------------------------------------------- Push SELECT --
Title_main110	TXA
		CMP	  <work0	      ; cursor == work0 ?
		BNE	  Title_main140	      ; no.
		LDA	  #00H		      ; cursor == 0
		BRA	  Title_main150
;------------------------------------------------------------ Push UP --
Title_main120	TXA			      ; cursor == 0 ?
		BEQ	  Title_main900	      ; yes.
		DEC	  A		      ; cursor up
		BRA	  Title_main150
;---------------------------------------------------------- Push DOWN --
Title_main130	TXA
		CMP	  <work0	      ; cursor == work0 ?
		BEQ	  Title_main900	      ; yes.
Title_main140	INC	  A		      ; cursor down
;-----------------------------------------------------------------------
Title_main150	STA	  <menu_select
		ASL	  A
		ADC	  <menu_select	      ; *3
		ASL	  A
		ASL	  A
		ASL	  A		      ; *24
		ADC	  #68H
		STA	  !OAM_main+0CDH      ; Set cursor position v
		ADC	  #08H
		STA	  !OAM_main+0D1H      ; Set cursor position v
;-----------------------------------------------------------------------
		PHK
		JSR	  SELECT_SOUND
		STZ	  <demo_wait_timer+0  ; Reset demo wait timer
		STZ	  <demo_wait_timer+1  ; Reset demo wait timer
		PHK
		JSR	  Title_main910
		JMP	  Title_main900
;
;=============== Exit title ( car select ) =============================
Title_main300
		PHK
		JSR	  SELECT_SOUND
		LDA	  <menu_select
		CMP	  #02H		      ; display best time ?
		BEQ	  Title_main320	      ; yes.
;-----------------------------------------------------------------------
		STA	  <play_mode	      ; Set play mode
		LDA	  #03H
		STA	  <retry_counter      ; Set retry counter
		STZ	  <game_level
		STZ	  <game_world
		STZ	  <game_scene
		STZ	  <demo_flag
		STZ	  <mycar_number	      ; Clear my car number
		STZ	  !rival_number	      ; Clear rival car number
		STZ	  <demo_wait_timer+0  ; Reset demo wait timer
		STZ	  <demo_wait_timer+1  ; Reset demo wait timer
;------------------------------------------------ Set car select mode --
Title_main310	LDA	  #02H
		STA	  <fade_flag	      ; Set fade_out mode
		INC	  <game_mode	      ; Set SELECT mode
		STZ	  <game_process	      ; Clear process counter
		RTL
;-----------------------------------------------------------------------
Title_main320	LDA	  #02H
		STA	  <fade_flag	      ; Set fade_out mode
		LDA	  #10000000B
		TSB	  <sound_status_0     ; sound fade out
		LDA	  #01H
		STA	  <demo_flag
		STZ	  <play_mode	      ; Clear play mode
		INC	  <game_process	      ; Best time display mode
		RTL
;
;=============== Demo entry ============================================
Title_main500
		STZ	  !demo_status
		STZ	  !demo_back_data
		STZ	  !demo_data_point
		LDA	  #03H
		STA	  !round_counter
		LDA	  #02H
		STA	  <game_mode	      ; Set PLAY mode
		DEC	  A
		STA	  <game_process	      ; Clear process counter
		STA	  <game_status	      ; Clear game status
		RTL
;
;=============== Menu message flashing =================================
Title_main900
		LDX	  <menu_select	      ; IX = menu_select
		LDA	  <frame_counter
		AND	  #00000100B
		LSR	  A
		LSR	  A
		TAY			      ; IY = attribute_select
;-----------------------------------------------------------------------
Title_main910	LDA	  !Title_flash_atr,Y
		XBA
		LDY	  !Title_OAM_len,X    ; IY  = data length
		LDA	  !Title_OAM_ptr,X
		TAX			      ; IX  =  OAM pointer
		XBA			      ; Acc = attribute code
;-----------------------------------------------------------------------
Title_main920	STA	  !OAM_main+3,X
		INX
		INX
		INX
		INX
		DEY
		BNE	  Title_main920
		RTL
;
;=======================================================================
		PROG
Title_OAM_ptr	BYTE	  060H,074H,084H
Title_OAM_len	BYTE	  005H,004H,004H
Title_flash_atr BYTE	  00110011B,00110101B
;
;************************************************************************
;*		 Search backup course					*
;************************************************************************
;
		DATA
		MEM8
		IDX16
Search_backup
		PHP
		SEP	  #00100000B	      ; Memory 8 bit mode
		REP	  #00010000B	      ; Index 16 bit mode
;-----------------------------------------------------------------------
Search_backup10 LDY	  #0000H
Search_backup20 LDX	  !Backup_pointer,Y   ; IX = best time pointer
		LDA	  >backup_buffer,X   ; data avilable ?
		BMI	  Search_backup30     ; yes.
		INY
		INY
		CPY	  #001EH
		BNE	  Search_backup20
;-----------------------------------------------------------------------
Search_backup30 TYA
		LSR	  A
;-----------------------------------------------------------------------
		PLP
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\	Best time display routines  \\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Best time display initialize				*
;************************************************************************
;
		MEM8
		IDX8
Best_init
		LDA	  <pers_flag	      ; pers flag enable ?
		BEQ	  Best_init100	      ; no.
		JMP	  Clear_flag	      ; Clear game flag
;
;=============== Initialize screen =====================================
Best_init100
		STZ	  <play_mode
		JSR	  Set_mode1	      ; Set BG mode 1
		JSR	  Set_best_char	      ; Set Best display character
		JSR	  Set_best_color      ; Set Best display color
		JSR	  Clear_best_scrn     ; Clear best screen
		JSR	  Disp_best_menu
		LDA	  #00010111B
		STA	  !Through_screen+0   ; Display BG1,BG2,BG3,OBJ
;
;=============== Check start course ====================================
Best_init200
		LDA	  <course_select
		ASL	  A
		TAY
		REP	  #00010000B	      ; Index 16 bit mode
		LDX	  !Backup_pointer,Y   ; IX = best time pointer
		LDA	  >backup_buffer,X   ; data avilable ?
		SEP	  #00010000B	      ; Index 8 bit mode
		BMI	  Best_init210	      ; if ( data avilable )
		LDA	  #0FH
		STA	  <course_select      ; Set exit
;-----------------------------------------------------------------------
Best_init210	LDA	  #00H
		JSR	  Best_menu810	      ; Clear cursor
		JSR	  Best_menu900	      ; Display cursor
;-----------------------------------------------------------------------
Best_init230	LDA	  <sound_status_0
		CMP	  #05H
		BEQ	  Best_init240
		LDA	  #01111111B
		TRB	  <sound_status_0     ; music off
Best_init240	LDA	  #01H
		STA	  <fade_flag	      ; Set fade_in mode
		INC	  <game_process
		RTL
;
;************************************************************************
;*		 Best menu select					*
;************************************************************************
;
		MEM8
		IDX16
Best_menu
		REP	  #00010000B	      ; Index 16 bit mode
		LDA	  #05H
		STA	  <sound_status_0     ; Music start
		LDX	  #0058H
		JSR	  Frash_mark
;-----------------------------------------------------------------------
		IDX8
		SEP	  #00010000B	      ; Index 8 bit mode
		JSR	  CHECK_START	      ; push START ?
		BCS	  Best_menu300	      ; yes.
		LDA	  <stick_trigger+1
		ASL	  A
		ASL	  A		      ; push SELECT ?
		BMI	  Best_menu150	      ; yes.
		ASL	  A
		ASL	  A		      ; push UP ?
		BMI	  Best_menu100	      ; yes.
		ASL	  A		      ; push DOWN ?
		BMI	  Best_menu150	      ; yes.
		ASL	  A		      ; push LEFT ?
		BMI	  Best_menu200	      ; yes.
		ASL	  A		      ; push RIGHT ?
		BMI	  Best_menu250	      ; yes.
		RTL
;
;=============== Cursor up =============================================
Best_menu100
		JSR	  Best_menu800	      ; Clear cursor
		JSR	  Best_menu500	      ; Search back course
		JSR	  Best_menu900	      ; Display cursor
		RTL
;
;=============== Cursor down ===========================================
Best_menu150
		JSR	  Best_menu800	      ; Clear cursor
		JSR	  Best_menu600	      ; Search next course
Best_menu190	JSR	  Best_menu900	      ; Display cursor
		RTL
;
;=============== Cursor left ===========================================
Best_menu200
		JSR	  Best_menu800	      ; Clear cursor
		LDA	  <course_select
		SEC
		SBC	  #10
		JSR	  Best_menu700
		JSR	  Best_menu900	      ; Display cursor
		RTL
;
;=============== Cursor right ==========================================
Best_menu250
		JSR	  Best_menu800	      ; Clear cursor
		LDA	  <course_select
		CLC
		ADC	  #10
		JSR	  Best_menu700
		JSR	  Best_menu900	      ; Display cursor
		RTL
;
;=============== Display best time =====================================
Best_menu300
		LDA	  <course_select
		CMP	  #0FH
		BEQ	  Best_menu400
;-----------------------------------------------------------------------
		LDX	  #0FFH
		SEC
Best_menu310	INX
		SBC	  #5
		BCS	  Best_menu310
;-----------------------------------------------------------------------
		ADC	  #5
		STA	  <game_scene
		STX	  <game_world
;-----------------------------------------------------------------------
		LDA	  #02H
		STA	  <fade_flag	      ; Set fade_out mode
		PHK
		JSR	  SELECT_SOUND
		INC	  <game_process
		RTL
;
;=============== Return title ==========================================
Best_menu400
		PHK
		JSR	  SELECT_SOUND
		LDA	  #02H
		STA	  <fade_flag	      ; Set fade_out mode
		LDA	  #10000000B
		TSB	  <sound_status_0
		STZ	  <game_mode
		STZ	  <game_process
		STZ	  <game_status
		RTL
;
;=============== Search back course ====================================
Best_menu500
		PHK
		JSR	  SELECT_SOUND
		DEC	  <course_select
		LDA	  <course_select
		BPL	  Best_menu510
		LDA	  #0FH
		STA	  <course_select
		RTS
;-----------------------------------------------------------------------
Best_menu510	ASL	  A
		TAY
		REP	  #00010000B	      ; Index 16 bit mode
		LDX	  !Backup_pointer,Y
		LDA	  >backup_buffer,X
		SEP	  #00010000B	      ; Index 8 bit mode
		BPL	  Best_menu500
		RTS
;
;=============== Search next course ====================================
Best_menu600
		PHK
		JSR	  SELECT_SOUND
		INC	  <course_select
		LDA	  <course_select
		CMP	  #0FH
		BEQ	  Best_menu620
		CMP	  #10H
		BNE	  Best_menu610
		LDA	  #00H
		STA	  <course_select
;-----------------------------------------------------------------------
Best_menu610	ASL	  A
		TAY
		REP	  #00010000B	      ; Index 16 bit mode
		LDX	  !Backup_pointer,Y
		LDA	  >backup_buffer,X
		SEP	  #00010000B	      ; Index 8 bit mode
		BPL	  Best_menu600
Best_menu620	RTS
;
;=============== Check backup RAM ======================================
Best_menu700
		CMP	  #10H
		BCS	  Best_menu720
		CMP	  #0FH
		BEQ	  Best_menu710
;-----------------------------------------------------------------------
		ASL	  A
		TAY
		REP	  #00010000B	      ; Index 16 bit mode
		LDX	  !Backup_pointer,Y   ; IX = best time pointer
		LDA	  >backup_buffer,X   ; data avilable ?
		SEP	  #00010000B	      ; Index 8 bit mode
		BPL	  Best_menu720
		TYA
		LSR	  A
Best_menu710	STA	  <course_select
		PHK
		JSR	  SELECT_SOUND
Best_menu720	RTS

;=============== Set cursor 1 ==========================================
Best_menu800
		LDA	  <course_select
Best_menu810	ASL	  A
		TAX
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  >Best_cursor_pos,X
		STA	  !VRAMwte_buffer+8
		LDA	  #Best_cursor_chr+0
		LDY	  #08H
		BRA	  Best_menu910
;
;=============== Set cursor 2 ==========================================
Best_menu900
		LDA	  <course_select
		ASL	  A
		TAX
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  >Best_cursor_pos,X
		STA	  !VRAMwte_buffer+0
		LDA	  #Best_cursor_chr+2
		LDY	  #00H
;-----------------------------------------------------------------------
Best_menu910	STA	  !VRAMwte_buffer+2,Y
		LDA	  #0003H
		STA	  !VRAMwte_buffer+4,Y
		LDA	  #0002H
		STA	  !VRAMwte_buffer+6,Y
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #02H
		STA	  <VRAMbuf_pointer
		RTS
;
;=======================================================================
Best_cursor_chr BYTE	  000H,00000001B      ; Clear character
		BYTE	  05CH,00000100B      ; Cursor character
;-----------------------------------------------------------------------
Best_cursor_pos WORD	  20A1H,20E1H,2121H,2161H,21A1H
		WORD	  2241H,2281H,22C1H,2301H,2341H
		WORD	  20B0H,20F0H,2130H,2170H,21B0H,2250H
;
;************************************************************************
;*		 Best screen display					*
;************************************************************************
;
		MEM8
		IDX8
Best_display
		JSR	  Best_display300
		PHK
		JSR	  SET_COURSE_DATA     ; Set course code
		JSR	  Best_display200     ; Initialize OAM
		JSR	  Set_back_screen     ; Set back screen
		JSR	  Set_back_color      ; Set back color
		JSR	  Disp_best_time      ; Display message
		JSR	  Disp_best_mark
		PHK
		JSR	  DISPLAY_COURSE3     ; Display course name
;
;=============== Display under bar =====================================
Best_display100
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDX	  #06H
		LDA	  #2182H
		STA	  !Screen_address
		LDA	  #0C19H
Best_display110 STA	  !Screen_write
		INX
		LDY	  !string_buffer,X    ; string end ?
		BNE	  Best_display110     ; no.
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #01H
		STA	  <fade_flag	      ; Set fade_in mode
		INC	  <game_process
		RTL
;
;=============== Initialize OAM ========================================
Best_display200
		MEM16
		IDX16
		PHK
		JSR	  CLEAR_OAM	      ; Clear OAM
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		LDX	  #0EE68H
		LDY	  #00200H
		LDA	  #0002BH
		MVN	  #0BH,#00H	      ; Transfer car display OAM
;-----------------------------------------------------------------------
		LDX	  #0EC44H
		LDY	  #003E0H
		LDA	  #0001FH
		MVN	  #0BH,#00H	      ; Transfer map display OAM
;-----------------------------------------------------------------------
		LDA	  #1010101010101010B
		STA	  !OAM_sub+1EH
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		RTS
;
;=============== Set game_world & game_scene ===========================
Best_display300
		MEM8
		IDX8
		LDA	  <course_select
		LDX	  #0FFH
		SEC
Best_display310 INX
		SBC	  #5
		BCS	  Best_display310
;-----------------------------------------------------------------------
		ADC	  #5
		STA	  <game_scene
		STX	  <game_world
		RTS
;
;************************************************************************
;*		 Best screen select					*
;************************************************************************
;
		MEM8
		IDX16
Best_select
		REP	  #00010000B	      ; Index 16 bit mode
		LDX	  #0100H
		JSR	  Frash_mark
;-----------------------------------------------------------------------
		IDX8
		SEP	  #00010000B	      ; Index 8 bit mode
		JSR	  CHECK_START	      ; push START ?
		BCS	  Best_select300      ; yes.
		LDA	  <stick_trigger+1
		ASL	  A
		ASL	  A		      ; push SELECT ?
		BMI	  Best_select200      ; yes.
		ASL	  A
		ASL	  A		      ; push UP ?
		BMI	  Best_select100      ; yes.
		ASL	  A		      ; push DOWN ?
		BMI	  Best_select110      ; yes.
		RTL
;
;=============== Push UP ===============================================
Best_select100
		JSR	  Search_backup
		CMP	  #0FH
		BEQ	  Best_select130
Best_select105	JSR	  Best_menu500
		LDA	  <course_select
		CMP	  #0FH
		BEQ	  Best_select105
		BRA	  Best_select120
;-----------------------------------------------------------------------
Best_select110	JSR	  Search_backup
		CMP	  #0FH
		BEQ	  Best_select130
Best_select115	JSR	  Best_menu600
		LDA	  <course_select
		CMP	  #0FH
		BEQ	  Best_select115
;-----------------------------------------------------------------------
Best_select120	LDA	  #02H
		STA	  <fade_flag	      ; Set fade_out mode
		PHK
		JSR	  SELECT_SOUND
		DEC	  <game_process
Best_select130	RTL
;
;=============== Data erase check start ================================
Best_select200
		LDA	  #3
		LDY	  #0
		JSR	  Display_erase	      ; Display erase message
		LDA	  #01H
		STA	  <menu_select
		INC	  <game_process
		RTL
;
;=============== Exit best time display mode ===========================
Best_select300
		LDA	  #02H
		STA	  <fade_flag	      ; Set fade_out mode
		STZ	  <game_mode
		LDA	  #02H
		STA	  <game_process
		STZ	  <game_status
		RTL
;
;************************************************************************
;*		 Data erase check					*
;************************************************************************
;
		MEM8
		IDX16
Erase_data
		REP	  #00010000B	      ; Index 16 bit mode
		LDX	  #0100H
		JSR	  Frash_mark
;-----------------------------------------------------------------------
		IDX8
		SEP	  #00010000B	      ; Index 8 bit mode
		LDA	  <stick_trigger+1
		BIT	  #00010000B	      ; push START ?
		BNE	  Erase_data200	      ; yes.
		BIT	  #00001000B	      ; push UP ?
		BNE	  Erase_data100	      ; yes.
		BIT	  #00000100B	      ; push DOWN ?
		BNE	  Erase_data150	      ; yes.
		RTL
;
;===============Select menu ============================================
Erase_data100
		LDA	  <menu_select
		BEQ	  Erase_data110
		LDA	  #2
		LDY	  #48
		JSR	  Display_erase
		STZ	  <menu_select
Erase_data110	RTL
;-----------------------------------------------------------------------
Erase_data150	LDA	  <menu_select
		DEC	  A
		BEQ	  Erase_data160
		LDA	  #2
		LDY	  #64
		JSR	  Display_erase
		LDA	  #01H
		STA	  <menu_select
Erase_data160	RTL
;
;=============== Erase data ============================================
Erase_data200
		LDA	  <menu_select
		BNE	  Erase_data300
;-----------------------------------------------------------------------
		PHK
		JSR	  SELECT_SOUND
		LDA	  <game_scene
		PHK
		PER	  3
		JMP	  >ERASE_RECORD
;-----------------------------------------------------------------------
		LDA	  <game_world
		ASL	  A
		ASL	  A
		ADC	  <game_world
		ADC	  <game_scene
		TAY
		LDA	  #0FFH
		STA	  !best_time_rank,Y
		STA	  !best_lap_flag,Y
;-----------------------------------------------------------------------
		LDA	  #02H
		STA	  <fade_flag	      ; Set fade_out mode
		DEC	  <game_process
		DEC	  <game_process
		RTL
;
;=============== Erase abort ===========================================
Erase_data300
		LDA	  #3
		LDY	  #24
		JSR	  Display_erase	      ; Clear erase message
		DEC	  <game_process
		RTL
;
;************************************************************************
;*		 Frash best mark					*
;************************************************************************
;
		MEM8
		IDX16
Frash_mark
		LDA	  <frame_counter
		AND	  #00000111B
		BNE	  Frash_mark_exit
		LDA	  !OAM_main+03H,X
		EOR	  #00000010B
		STA	  !OAM_main+03H,X
		STA	  !OAM_main+07H,X
		STA	  !OAM_main+0BH,X
		STA	  !OAM_main+0FH,X
		STA	  !OAM_main+13H,X
Frash_mark_exit RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\	My car celect routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 My car Select entry					*
;************************************************************************
;
		MEM8
		IDX8
SELECT_ENTRY
		LDA	  <game_process	      ; initialize process
		BEQ	  Select_initial
;-----------------------------------------------------------------------
		LDX	  <open_close_flag
		BNE	  Window_process
		DEC	  A
		ASL	  A
		TAX
		JMP	  (!Select_vector,X)   ; Jump mode process
;-----------------------------------------------------------------------
Select_vector	WORD	  Mycar_select	      ; Mycar select mode
		WORD	  Check_mycar_sel     ; Mycar check mode
		WORD	  Change_screen	      ; Exchange BG1 to BG2
		WORD	  Rival_select	      ; Rival select mode
		WORD	  Series_select	      ; Series select mode
		WORD	  Class_celect	      ; Class select mode
		WORD	  Exit_select	      ; Exit select mode
;
;************************************************************************
;*		 Message window control					*
;************************************************************************
;
		MEM8
		IDX8
Window_process
		DEX
		BNE	  Window_close
;
;=============== Open message window ===================================
Window_open
		LDA	  <window_flag
		BNE	  Window_open100
		JSR	  Window_initial      ; Window initialize
		RTL
;-----------------------------------------------------------------------
Window_open100	LDX	  #02H
Window_open110	DEC	  !window_table+00H   ; decrement window upper
		INC	  !window_table+03H   ; increment window hight 1
		INC	  !window_table+06H   ; increment window hight 2
		DEC	  !window_table+04H   ; decrement window left side 1
		DEC	  !window_table+07H   ; decrement window left side 2
		INC	  !window_table+05H   ; increment window right side 1
		INC	  !window_table+08H   ; increment window right side 2
		DEX
		BNE	  Window_open110
;-----------------------------------------------------------------------
		LDA	  !window_table+04H
		CMP	  #5BH
		BNE	  Window_open120
		STZ	  <open_close_flag
Window_open120	RTL
;
;=============== Close message window ==================================
Window_close
		LDA	  !window_table+04H
		CMP	  #0ABH
		BEQ	  Window_close200
;-----------------------------------------------------------------------
Window_close100 LDX	  #02H
Window_close110 INC	  !window_table+00H   ; increment window upper
		DEC	  !window_table+03H   ; decrement window hight 1
		DEC	  !window_table+06H   ; decrement window hight 2
		INC	  !window_table+04H   ; increment window left side 1
		INC	  !window_table+07H   ; increment window left side 2
		DEC	  !window_table+05H   ; decrement window right side 1
		DEC	  !window_table+08H   ; decrement window right side 2
		DEX
		BNE	  Window_close110
		RTL
;-----------------------------------------------------------------------
Window_close200 STZ	  <window_control+0
		STZ	  <window_main
		STZ	  <HDMA_switch
		STZ	  <window_flag
		STZ	  <open_close_flag
		RTL
;
;************************************************************************
;*		 Initialize celect screen				*
;************************************************************************
;
		MEM8
		IDX8
Select_initial
		LDA	  <pers_flag	      ; pers flag enable ?
		BEQ	  Select_initial2     ; no.
		JMP	  Clear_flag	      ; Clear game flag
;-----------------------------------------------------------------------
Select_initial2 JSR	  Set_mode1	      ; Set BG mode 1
		JSR	  Set_character	      ; Set BG character
		JSR	  Set_color	      ; Set color generator
		PHK
		JSR	  CLEAR_OAM	      ; Clear OAM
		LDA	  #01H
		STA	  <objwte_flag	      ; Set OBJ write mode
;
;=============== Display screen ========================================
Select_init100
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		LDA	  #0000FH	      ; Acc = data bank
		LDX	  #032B0H	      ; IX  = data offset
		LDY	  #00000H	      ; IY  = VRAM write address
		PHK
		JSR	  EXPAND_SELECT	      ; Set screen data
		LDA	  #0000FH	      ; Acc = data bank
		LDX	  #038B0H	      ; IX  = data offset
		LDY	  #01000H	      ; IY  = VRAM write address
		PHK
		JSR	  EXPAND_SELECT	      ; Set screen data
;-----------------------------------------------------------------------
		IDX8
		SEP	  #00010000B	      ; Index 8 bit mode
		LDA	  <play_mode
		AND	  #00FFH
		XBA
		STA	  <BG1_scroll_h	      ; Set BG1 scroll H
		LDA	  #0100H
		STA	  <BG1_scroll_v	      ; Set BG1 scroll V
;
;=============== Initialize work =======================================
Select_init300
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
		LDX	  <mycar_number
		LDA	  >Mycar_sel_data,X
		STA	  <menu_select	      ; Set menu selector
		JSR	  Change_car_type
		JSR	  Set_windowcolor
;-----------------------------------------------------------------------
		LDA	  #00010011B
		STA	  !Through_screen+0   ; Display BG1,BG2,OBJ
		STZ	  <open_close_flag
		LDA	  #01H
		STA	  <fade_flag	      ; Set fade_in mode
		INC	  <game_process	      ; Next process
		LDA	  #04H
		STA	  <sound_status_0     ; Start SELECT sound
		RTL
;
;************************************************************************
;*		 Change BG2 screen segment				*
;************************************************************************
;
		MEM16
		IDX8
Change_screen
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  #0000001100010011B
		STA	  !Screen_bank+0      ; Set BG1,BG2 SC base address
;-----------------------------------------------------------------------
		LDX	  <play_mode	      ; free mode ?
		BNE	  Change_scrn200      ; yes.
;
;=============== Initialize battle mode ================================
Change_scrn100
		LDA	  #5870H
		STA	  !OAM_main+0
		LDA	  #732FH
		STA	  !OAM_main+2
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDX	  #00H
		JSR	  Flashing_attrib
		STZ	  <menu_select
		INC	  <game_process
		BRA	  Change_scrn210
;
;=============== Initialize free mode ==================================
Change_scrn200
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDX	  !rival_number
		INX
		LDA	  >Rival_menu,X
		STA	  <menu_select
		JSR	  Change_rival	      ; Set rival car
;-----------------------------------------------------------------------
Change_scrn210	INC	  <game_process
		LDA	  #01H
		STA	  <open_close_flag    ; Window open
		RTL
;-----------------------------------------------------------------------
Rival_menu	BYTE	  04H,00H,01H,02H,03H
;
;************************************************************************
;*		 My car select						*
;************************************************************************
;
		MEM8
		IDX8
Mycar_select
		JSR	  CHECK_START	      ; push START ?
		BCS	  Mycar_select200     ; yes.
		LDA	  <stick_trigger+1
		BIT	  #00100000B	      ; Push SELECT ?
		BNE	  Mycar_select100     ; yes.
		BIT	  #00001000B	      ; Push UP ?
		BNE	  Mycar_select110     ; yes.
		BIT	  #00000100B	      ; Push DOWN ?
		BNE	  Mycar_select120     ; yes.
		RTL
;
;=============== My car select =========================================
Mycar_select100
		LDX	  <menu_select
		CPX	  #03H		      ; cursor == 3 ?
		BNE	  Mycar_select130     ; no.
		LDX	  #00H		      ; cursor == 0
		BRA	  Mycar_select150
;------------------------------------------------------------ Push UP --
Mycar_select110 LDX	  <menu_select	      ; cursor == 0 ?
		BEQ	  Mycar_select160     ; yes.
		DEX			      ; cursor up
		BRA	  Mycar_select150
;---------------------------------------------------------- Push DOWN --
Mycar_select120 LDX	  <menu_select
		CPX	  #03H		      ; cursor == 3 ?
		BEQ	  Mycar_select160     ; yes.
Mycar_select130 INX			      ; cursor down
;-----------------------------------------------------------------------
Mycar_select150 STX	  <menu_select
		LDA	  >Mycar_sel_data,X
		STA	  <mycar_number
		PHK
		JSR	  SELECT_SOUND
		JSR	  Change_car_type
		JSR	  Set_windowcolor
Mycar_select160 RTL
;-----------------------------------------------------------------------
Mycar_sel_data	BYTE	  00H,02H,01H,03H
;
;=============== End my car select mode ================================
Mycar_select200
		STZ	  <yes_no_status
		JSR	  Display_YES_NO      ; Display "NO"
		STZ	  !car_speed+0
		STZ	  !car_speed+1
		LDA	  #06H
		STA	  <initial_timer
		LDA	  #01H
		STA	  <open_close_flag    ; Message window open
		INC	  <game_process	      ; Next process
		RTL
;
;************************************************************************
;*		 Check my car select					*
;************************************************************************
;
		MEM8
		IDX8
Check_mycar_sel
		JSR	  Graph_open
		JSR	  CHECK_START	      ; push START ?
		BCS	  Check_mycar200      ; yes.
		LDA	  <stick_trigger+1
		BIT	  #00100000B	      ; push SELECT
		BNE	  Check_mycar100      ; yes.
		LSR	  A		      ; push RIGHT ?
		BCS	  Check_mycar110      ; yes.
		LSR	  A		      ; push LEFT ?
		BCS	  Check_mycar120      ; yes.
		BRA	  Check_mycar160
;
;=============== Select YES or NO ======================================
Check_mycar100
		LDA	  <yes_no_status      ; cursor == NO ?
		BNE	  Check_mycar120      ; yes.
;-----------------------------------------------------------------------
Check_mycar110	LDA	  #02H		      ; Cursor = NO
		BRA	  Check_mycar150
Check_mycar120	LDA	  #00H		      ; Cursor = YES
;-----------------------------------------------------------------------
Check_mycar150	CMP	  <yes_no_status
		BEQ	  Check_mycar160
		STA	  <yes_no_status
		PHK
		JSR	  SELECT_SOUND
Check_mycar160	JSR	  Display_YES_NO      ; Display "YES" or "NO"
		RTL
;
;=============== Change next process ===================================
Check_mycar200
		JSR	  Display_YES_NO2     ; Display "YES" or "NO"
		JSR	  Change_car_type     ; restore car style
		LDA	  <yes_no_status      ; cursor == "YES"
		BEQ	  Check_mycar220      ; yes.
;-----------------------------------------------------------------------
Check_mycar210	DEC	  <game_process	      ; Set process counter
		LDA	  #02H
		STA	  <open_close_flag    ; Message window close
		RTL			      ; Return mycar select
;-----------------------------------------------------------------------
Check_mycar220	STZ	  <window_control+0
		STZ	  <window_main
		STZ	  <HDMA_switch
		STZ	  <window_flag	      ; Reset window
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  <BG2_scroll_h
		STA	  <BG1_scroll_h	      ; Set BG1 scroll H
		LDA	  <BG2_scroll_v
		STA	  <BG1_scroll_v	      ; Set BG1 scroll H
		LDA	  #0000H
		LDX	  <play_mode	      ; Free mode ?
		BNE	  Check_mycar230      ; yes.
		LDA	  #0100H
Check_mycar230	STA	  <BG2_scroll_h	      ; Set BG2 scroll H
		STZ	  <BG2_scroll_v	      ; Set BG2 scroll V
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		INC	  <game_process	      ; Set process counter
		RTL
;
;=============== Accele graph window open ==============================
Graph_open
		LDA	  !window_table+17H
		CMP	  #0E1H
		BEQ	  Graph_open10
		INC	  !window_table+17H
Graph_open10	JSR	  Disp_car_style
		LDA	  #10000000B
		PHK
		JSR	  ENGINE_SOUND
		LDA	  #10000000B
		TSB	  !sound_status_1
		RTS			      ; ( not long )
;
;************************************************************************
;*		 Rival select						*
;************************************************************************
;
		MEM8
		IDX8
Rival_select
		LDA	  #11101000B
		STA	  !OAM_sub+0	      ; Cursor on
;-----------------------------------------------------------------------
		JSR	  CHECK_START	      ; push A ?
		BCS	  Rival_select900     ; yes.
		LDA	  <stick_trigger+1
		BIT	  #00100000B	      ; Push SELECT ?
		BNE	  Rival_select100     ; yes.
		LSR	  A		      ; Push RIGHT ?
		BCS	  Rival_select500
		LSR	  A		      ; Push LEFT ?
		BCS	  Rival_select400
		LSR	  A		      ; Push DOWN ?
		BCS	  Rival_select300     ; yes.
		LSR	  A		      ; Push UP ?
		BCS	  Rival_select200     ; yes.
		RTL
;
;=============== Push SELECT ===========================================
Rival_select100
		LDA	  <menu_select
		INC	  A
		CMP	  #05H		      ; ++cursor == 5 ?
		BNE	  Rival_select600     ; no.
		LDA	  #00H		      ; cursor == 0
		BRA	  Rival_select600
;
;=============== Push UP ===============================================
Rival_select200
		LDA	  <menu_select
		CMP	  #04H		      ; cursor == 4 ?
		BNE	  Rival_select250     ; no.
		LDA	  #02H		      ; cursor = 2
		BRA	  Rival_select600
;-----------------------------------------------------------------------
Rival_select250 AND	  #00000001B	      ; cursor up
		BRA	  Rival_select600
;
;=============== Push DOWN =============================================
Rival_select300
		LDA	  <menu_select
		CMP	  #02H		      ; cursor >= 2 ?
		BCC	  Rival_select350     ; no.
		LDA	  #04H
		BRA	  Rival_select600
;-----------------------------------------------------------------------
Rival_select350 ORA	  #00000010B	      ; cursor down
		BRA	  Rival_select600
;
;=============== Push LEFT =============================================
Rival_select400
		LDA	  <menu_select
		AND	  #00000110B	      ; cursor left
		BRA	  Rival_select600
;
;=============== Push RIGHT ============================================
Rival_select500
		LDA	  <menu_select
		CMP	  #04H
		BEQ	  Rival_select600
		ORA	  #00000001B	      ; cursor right
;
;=============== Set cursor position ===================================
Rival_select600
		CMP	  <menu_select
		BEQ	  Rival_select610
		STA	  <menu_select
		PHK
		JSR	  SELECT_SOUND
		JSR	  Change_rival
Rival_select610 RTL
;
;=============== End of rival select ===================================
Rival_select900
		LDX	  <menu_select
		LDA	  >Rival_num_data,X
		STA	  !rival_number	      ; Set rival car number
		LDA	  #02H
		STA	  <fade_flag	      ; Set fade_out mode
		LDA	  #07H
		STA	  <game_process	      ; Set process counter
		LDA	  #10000000B
		TSB	  <sound_status_0     ; Set SOUND fade out
		RTL
;
;-----------------------------------------------------------------------
Rival_num_data	BYTE	  00H,01H,02H,03H,0FFH
;
;************************************************************************
;*		 Series select						*
;************************************************************************
;
		MEM8
		IDX8
Series_select
		LDA	  #11111100B
		STA	  !OAM_sub+0	      ; Cursor on
;-----------------------------------------------------------------------
		JSR	  CHECK_START	      ; push START ?
		BCS	  Series_sel200	      ; yes.
		LDA	  <stick_trigger+1    ; Push B ?
		ASL	  A
		ASL	  A		      ; Push SELECT ?
		BMI	  Series_sel100	      ; yes.
		ASL	  A
		ASL	  A		      ; Push UP ?
		BMI	  Series_sel110	      ; yes.
		ASL	  A		      ; Push DOWN ?
		BMI	  Series_sel120	      ; yes.
;-----------------------------------------------------------------------
		LDX	  #00H
		JSR	  Flashing_attrib
		RTL
;
;=============== Series select =========================================
Series_sel100
		LDA	  <menu_select
		CMP	  #02H		      ; cursor == 2 ?
		BNE	  Series_sel130	      ; no.
		LDA	  #00H		      ; cursor == 0
		BRA	  Series_sel150
;------------------------------------------------------------ Push UP --
Series_sel110	LDA	  <menu_select	      ; cursor == 0 ?
		BEQ	  Series_sel160	      ; yes.
		DEC	  A		      ; cursor up
		BRA	  Series_sel150
;---------------------------------------------------------- Push DOWN --
Series_sel120	LDA	  <menu_select
		CMP	  #02H		      ; cursor == 2 ?
		BEQ	  Series_sel160	      ; yes.
Series_sel130	INC	  A		      ; cursor down
;-----------------------------------------------------------------------
Series_sel150	STA	  <menu_select
		PHK
		JSR	  SELECT_SOUND
		JSR	  Change_series
Series_sel160	RTL
;
;=============== End of select =========================================
Series_sel200
		MEM8
		IDX8
		LDA	  <menu_select
		STA	  <game_world	      ; Set game world
		ASL	  A
		ASL	  A
		ADC	  <menu_select
		STA	  <course_select
		LDA	  <game_level
		STA	  <menu_select
;-----------------------------------------------------------------------
		LDA	  #0A8H
		STA	  !OAM_main+1	      ; Set cursor position v
		LDX	  #04H
		JSR	  Flashing_attrib
		INC	  <game_process	      ; Set process counter
		RTL
;
;************************************************************************
;*		 Class select						*
;************************************************************************
;
		MEM8
		IDX8
Class_celect
		LDY	  #02H
		LDX	  <game_world
		LDA	  >backup_buffer+1FAH
		AND	  >Master_status,X
		CMP	  >Master_status,X
		BNE	  Class_celect10
		INY
Class_celect10	STY	  <work0
;-----------------------------------------------------------------------
		LDA	  <stick_trigger+0    ; push A ?
		BMI	  Class_select300     ; yes.
		LDA	  <stick_trigger+1    ; Push B ?
		BMI	  Class_select300     ; yes.
		ASL	  A
		ASL	  A		      ; Push SELECT ?
		BMI	  Class_select100     ; yes.
		ASL	  A		      ; Push START ?
		BMI	  Class_select300     ; yes.
		ASL	  A		      ; Push UP ?
		BMI	  Class_select110     ; yes.
		ASL	  A		      ; Push DOWN ?
		BMI	  Class_select120     ; yes.
;-------------------------------------------------------------- DEBUG --
;;;;		XBA
;;;;		LDA	  <stick_status+0     ; Push L,R ?
;;;;		AND	  #00110000B
;;;;		BEQ	  Class_select10      ; no.
;;;;		XBA
;;;;		ASL	  A		      ; Push LEFT ?
;;;;		BMI	  Class_select510     ; yes.
;;;;		ASL	  A		      ; Push RIGHT ?
;;;;		BMI	  Class_select520     ; yes.
;;;;		BRA	  Class_select500
;-----------------------------------------------------------------------
Class_select10	LDX	  #04H
		JSR	  Flashing_attrib
		RTL
;
;=============== Class select ==========================================
Class_select100
		LDA	  <menu_select
		CMP	  <work0	      ; cursor == 2 ?
		BNE	  Class_select130     ; no.
		LDA	  #00H		      ; cursor == 0
		BRA	  Class_select150
;------------------------------------------------------------ Push UP --
Class_select110 LDA	  <menu_select	      ; cursor == 0 ?
		BEQ	  Class_select160     ; yes.
		DEC	  A		      ; cursor up
		BRA	  Class_select150
;---------------------------------------------------------- Push DOWN --
Class_select120 LDA	  <menu_select
		CMP	  <work0	      ; cursor == 2 ?
		BEQ	  Class_select160     ; yes.
Class_select130 INC	  A		      ; cursor down
;-----------------------------------------------------------------------
Class_select150 STA	  <menu_select
		PHK
		JSR	  SELECT_SOUND
		JSR	  Change_class
Class_select160 RTL
;
;=============== End of select =========================================
Class_select300
		MEM8
		IDX8
		PHK
		JSR	  SELECT_SOUND
;-----------------------------------------------------------------------
		LDA	  <menu_select
		STA	  <game_level	      ; Set game level
		CMP	  #03H
		BNE	  Class_select310
		DEC	  <game_level
		DEC	  !master_level
;-----------------------------------------------------------------------
Class_select310 LDA	  #02H
		STA	  <fade_flag	      ; Set fade_out mode
		LDA	  #07H
		STA	  <game_process	      ; Set process counter
		LDA	  #10000000B
		TSB	  <sound_status_0     ; Set SOUND fade out
		RTL
;
;=============== Select scene ( for DEBUG ) ============================
;lass_select500
;		LDA	  <game_scene
;		BRA	  Class_select540
;-----------------------------------------------------------------------
;lass_select510 LDA	  <game_scene
;		BEQ	  Class_select530
;		DEC	  A
;		BRA	  Class_select530
;-----------------------------------------------------------------------
;lass_select520 LDA	  <game_scene
;		CMP	  #04H
;		BEQ	  Class_select530
;		INC	  A
;-----------------------------------------------------------------------
;lass_select530 STA	  <game_scene
;lass_select540 CLC
;		ADC	  #0C1H
;		STA	  >OAM_main+12H
;		ADC	  #010H
;		STA	  >OAM_main+16H
;		LDA	  #00110000B
;		STA	  >OAM_main+13H
;		STA	  >OAM_main+17H
;		LDA	  #0D0H
;		STA	  >OAM_main+10H
;		STA	  >OAM_main+14H
;		LDA	  #090H
;		STA	  >OAM_main+11H
;		ADC	  #008H
;		STA	  >OAM_main+15H
;		LDA	  #11110000B
;		STA	  >OAM_sub+01H
;		RTL
;
;************************************************************************
;*		 Exit select mode					*
;************************************************************************
;
		MEM8
		IDX8
Exit_select
;;;;		STZ	  <sound_status_0     ; Set SOUND
		STZ	  <window_control+0
		STZ	  <window_main
		STZ	  <HDMA_switch
		STZ	  <window_flag
		STZ	  <objwte_flag
;-----------------------------------------------------------------------
		INC	  <game_mode	      ; Set PLAY mode
		LDA	  <play_mode
		STA	  <game_process	      ; Clear process counter
		STZ	  <game_status	      ; Clear game status
		RTL
;
;************************************************************************
;*		 Display YES or NO					*
;************************************************************************
;
		MEM8
		IDX8
Display_YES_NO
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <frame_counter
		AND	  #00000100B
		BRA	  Display_YES_NO3
;-----------------------------------------------------------------------
Display_YES_NO2 PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		LDA	  #00000000B
;-----------------------------------------------------------------------
Display_YES_NO3 ORA	  <yes_no_status
		TAY			      ; IY = data address pointer
		LDA	  <menu_select	      ; Acc = car number
		ASL	  A
		TAX			      ; IX = write address pointer
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  >YES_NO_wte_addr,X
		STA	  !VRAMwte_buffer+0   ; Set VRAM write address
		TYX
		LDA	  >YES_NO_dat_addr,X
		STA	  !VRAMwte_buffer+2   ; Set data address
		LDA	  #0003H
		STA	  !VRAMwte_buffer+4   ; Set data bank
		LDA	  #000EH
		STA	  !VRAMwte_buffer+6   ; Set data length
;-----------------------------------------------------------------------
		LDY	  #01H
		STY	  <VRAMbuf_pointer
		PLP
		RTS			      ; ( not long )
;
;=======================================================================
YES_NO_wte_addr WORD	  12F2H,16F2H,1AF2H,1EF2H
YES_NO_dat_addr WORD	  YES_BG_data,NO_BG_data,YES_CLR_data,NO_CLR_data
;-----------------------------------------------------------------------
YES_BG_data	WORD	  082AH,082BH,082CH,082FH,00FFH,0C2DH,0C2EH
NO_BG_data	WORD	  0C2AH,0C2BH,0C2CH,00FFH,482FH,082DH,082EH
YES_CLR_data	WORD	  0C2AH,0C2BH,0C2CH,082FH,00FFH,0C2DH,0C2EH
NO_CLR_data	WORD	  0C2AH,0C2BH,0C2CH,00FFH,482FH,0C2DH,0C2EH
;
;************************************************************************
;*		 Set window frame color					*
;************************************************************************
;
		MEM8
		IDX8
Set_windowcolor
		LDA	  <mycar_number
		ASL	  A
		ADC	  <mycar_number
		ASL	  A
;-----------------------------------------------------------------------
Set_windowcol10 TAX			      ; IX = data pointer
		LDY	  #00H		      ; IY = loop counter
Set_windowcol20 LDA	  >Window_col_data,X
		STA	  !color_buffer+06H,Y ; Set window frame color
		INX
		INY
		CPY	  #06H
		BNE	  Set_windowcol20
		RTS			      ; ( not long )
;=======================================================================
Window_col_data WORD	  7FEBH,7E80H,7520H   ; CAR 0
		WORD	  4BEFH,1B49H,0240H   ; CAR 1
		WORD	  53FFH,033FH,0213H   ; CAR 2
		WORD	  765DH,60D8H,0018H   ; CAR 3
;
;************************************************************************
;*		 Change SERIES & CLASS attribute			*
;************************************************************************
;
		MEM8
		IDX8
Flashing_attrib
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		STX	  <work0
		LDA	  <frame_counter
		AND	  #00001000B
		LSR	  A
		LSR	  A
		ORA	  <work0
		TAX
;-----------------------------------------------------------------------
		LDA	  #83H
		STA	  <VRAMbuf_pointer
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		LDA	  >Flash_attr_addr,X
		TAX
		LDY	  #VRAMwte_buffer
		LDA	  #11
		MVN	  #03H,#00H
;-----------------------------------------------------------------------
		PLP
		RTS			      ; ( not long )
;
;=======================================================================
Flash_attr_addr WORD	  Flash_attr1,Flash_attr2,Flash_attr3,Flash_attr4
;-----------------------------------------------------------------------
Flash_attr1	BYTE	  00FH,05H,06H,04H
		BYTE	  04FH,06H,05H,04H
		BYTE	  0B0H,06H,08H,0CH
;-----------------------------------------------------------------------
Flash_attr2	BYTE	  00FH,05H,06H,08H
		BYTE	  04FH,06H,05H,04H
		BYTE	  0B0H,06H,08H,0CH
;-----------------------------------------------------------------------
Flash_attr3	BYTE	  00FH,05H,06H,04H
		BYTE	  04FH,06H,05H,04H
		BYTE	  0B0H,06H,08H,08H
;-----------------------------------------------------------------------
Flash_attr4	BYTE	  00FH,05H,06H,04H
		BYTE	  04FH,06H,05H,08H
		BYTE	  0B0H,06H,08H,08H
;
;************************************************************************
;*		 Change series						*
;************************************************************************
;
		MEM8
		IDX8
Change_series
		PHP
		SEP	  #00110000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <menu_select
		ASL	  A
		TAX
		ASL	  A
		ASL	  A
		ASL	  A
		ADC	  #58H
		STA	  !OAM_main+1	      ; Set cursor position v
;-----------------------------------------------------------------------
		LDA	  #83H
		STA	  <VRAMbuf_pointer    ; Set attr write mode / 3 data
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		LDA	  >Series_attr_adr,X
		TAX
		LDY	  #VRAMwte_buffer
		LDA	  #11
		MVN	  #03H,#00H		; CHANGE
;-----------------------------------------------------------------------
		PLP
		RTS			      ; ( not long )
;
;=======================================================================
Series_attr_adr WORD	  Series_attrib1,Series_attrib2,Series_attrib3
;-----------------------------------------------------------------------
Series_attrib1	BYTE	  070H,05H,0DH,08H
		BYTE	  0B0H,05H,0CH,0CH
		BYTE	  0F0H,05H,0BH,0CH
;-----------------------------------------------------------------------
Series_attrib2	BYTE	  070H,05H,0DH,0CH
		BYTE	  0B0H,05H,0CH,08H
		BYTE	  0F0H,05H,0BH,0CH
;-----------------------------------------------------------------------
Series_attrib3	BYTE	  070H,05H,0DH,0CH
		BYTE	  0B0H,05H,0CH,0CH
		BYTE	  0F0H,05H,0BH,08H
;
;************************************************************************
;*		 Change class						*
;************************************************************************

		MEM8
		IDX8
Change_class
		PHP
		SEP	  #00110000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <menu_select
		ASL	  A
		TAX
		LDA	  #01H
		STA	  <VRAMbuf_pointer
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		LDA	  >Class_tbl_addr,X
		TAX
		LDY	  #VRAMwte_buffer
		LDA	  #7
		MVN	  #03H,#00H		; CHANGE
;-----------------------------------------------------------------------
		PLP
		RTS			      ; ( not long )
;
;=======================================================================
Class_tbl_addr	WORD	  Class_table1,Class_table2
		WORD	  Class_table3,Class_table4
;-----------------------------------------------------------------------
Class_table1	WORD	  06B0H,Class_message1,0003H,0010H
Class_table2	WORD	  06B0H,Class_message2,0003H,0010H
Class_table3	WORD	  06B0H,Class_message3,0003H,0010H
Class_table4	WORD	  06B0H,Class_message4,0003H,0010H
;-----------------------------------------------------------------------
Class_message1	WORD	  08BFH,08C6H,08C8H,08CAH,08CEH,08CEH,08C6H,08D3H
Class_message2	WORD	  08D4H,08D5H,08AFH,08CEH,08C5H,08AFH,08D3H,08C5H
Class_message3	WORD	  08C6H,08D9H,08D1H,08C6H,08D3H,08D5H,08FFH,08FFH
Class_message4	WORD	  08CFH,08AFH,08D4H,08D5H,08C6H,08D3H,08FFH,08FFH
;
;************************************************************************
;*		 Change rival car type					*
;************************************************************************
;
		MEM8
		IDX8
Change_rival
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <menu_select
		ASL	  A
		TAX			      ; IX = data pointer
;
;=============== Set VRAM write buffer =================================
Change_rival100
		LDA	  #81H
		STA	  <VRAMbuf_pointer
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
		LDA	  >Rival_nothing+0
		STA	  !VRAMwte_buffer+0   ; Set VRAM write buffer
		LDA	  >Rival_nothing+2
		STA	  !VRAMwte_buffer+2   ; Set VRAM write buffer
;
;=============== Set cursor position ===================================
Change_rival200
		LDA	  >Rival_cursor,X
		STA	  !OAM_main+0	      ; Set cursor position
		ADC	  #0FB10H
		STA	  !OAM_main+4	      ; Set rival car position 1
		CLC
		ADC	  #0008H
		STA	  !OAM_main+8	      ; Set rival car position 2
;
;=============== Set cursor character ( select rival ) =================
Change_rival300
		LDA	  #732FH
		STA	  !OAM_main+2	      ; Set cursor character
		LDA	  >Rival_car_char,X
		BEQ	  Change_rival400
		STA	  !OAM_main+6	      ; Set rival car character 1
		INC	  A
		STA	  !OAM_main+10	      ; Set rival car character
		PLP
		RTS			      ; ( not long )
;
;=======================================================================
Change_rival400
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #0F0H
		STA	  !OAM_main+5
		STA	  !OAM_main+9
;-----------------------------------------------------------------------
		LDA	  #00001000B
		STA	  !VRAMwte_buffer+3
		PLP
		RTS			      ; ( not long )
;
;=======================================================================
Rival_cursor	WORD	  5C78H,5CA8H,7C78H,7CA8H,9878H
Rival_car_char	WORD	  3860H,3A63H,3C66H,3E69H,0000H
;-----------------------------------------------------------------------
Rival_nothing	BYTE	  71H,02H,08H,00001100B
;
;************************************************************************
;*		 Change display car type				*
;************************************************************************
;
		MEM8
		IDX8
Change_car_type
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDX	  <menu_select
		LDA	  >Carsel_BG_scc_H,X
		STA	  <BG2_scroll_h+1     ; Set BG2 scroll H high
		STZ	  <BG2_scroll_h+0     ; Set BG2 scroll H low
		LDA	  >Carsel_BG_scc_V,X
		STA	  <BG2_scroll_v+1     ; Set BG2 scroll H high
		STZ	  <BG2_scroll_v+0     ; Set BG2 scroll H low
;-----------------------------------------------------------------------
		LDA	  #08H
		STA	  <initial_count
		BRA	  Disp_car_style2
;
;=======================================================================
Carsel_BG_scc_H BYTE	  00H,01H,00H,01H
Carsel_BG_scc_V BYTE	  00H,00H,01H,01H
;
;************************************************************************
;*		 Display car style					*
;************************************************************************
;
		MEM8
		IDX8
Disp_car_style
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		DEC	  <initial_timer      ; Display timming ?
		BNE	  Disp_car_style9     ; no.
;------------------------------------------------------ Set car style --
		LDA	  #06H
		STA	  <initial_timer      ; Reset timer
		DEC	  <initial_count      ; Decrement car style number
		LDA	  <initial_count
		CMP	  #0F3H		      ; car style number over ?
		BNE	  Disp_car_style2     ; no.
		LDA	  #00BH
		STA	  <initial_count      ; Reset car style number
;----------------------------------------------------- Display my car --
		MEM16
Disp_car_style2 REP	  #00100000B	      ; Memory 16 bit mode
		AND	  #00FFH	      ; Acc.H = size ; Acc.L = pose
		PHA
		LDX	  <menu_select
		LDA	  >Disp_car_posy,X   ; IY = position y
		TAY
		LDX	  #34H		      ; IX = position x
		PLA
		PHK
		JSR	  SETTING_CAR	      ; Set my car pattern
		PHK
		JSR	  SET_OAM_ADDRESS     ; Set OAM data
;-----------------------------------------------------------------------
Disp_car_style9 PLP
		RTS			      ; ( not long )
;
;=======================================================================
Disp_car_posy	BYTE	  04CH,074H,09CH,0C4H
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\  Game screen initial routines \\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Clear game flag					*
;************************************************************************
;
		MEM8
Clear_flag
		STZ	  <part_flag	      ; Clear pert mode change flag
		STZ	  <pers_flag	      ; Clear pers flag
		STZ	  <window_flag	      ; Clear window flag
		STZ	  <colwte_flag	      ; Clear color write flag
		RTL
;
;************************************************************************
;*		 Set BG screen mode 1					*
;************************************************************************
;
		MEM8
		IDX8
Set_mode1
		LDA	  #00000001B
		STA	  !Screen_mode	      ; Set BG mode
		STZ	  !Screen_mosaic      ; Reset mosaic
;-----------------------------------------------------------------------
		LDA	  #00000011B
		STA	  !Screen_bank+0      ; Set BG1 SC base address
		LDA	  #00010011B
		STA	  !Screen_bank+1      ; Set BG2 SC base address
		LDA	  #00100011B
		STA	  !Screen_bank+2      ; Set BG3 SC base address
		LDA	  #00110011B
		STA	  !Screen_segment+0   ; Set BG1,BG2 name base address
		LDA	  #01000100B
		STA	  !Screen_segment+1   ; Set BG3,BG4 name base address
		LDA	  #00000010B
		STA	  <OBJchar_status
;-----------------------------------------------------------------------
		LDA	  #00000010B
		STA	  !Color_control      ; set indirect color mode
;-----------------------------------------------------------------------
		LDA	  #00010001B
		STA	  !Through_screen+0   ; Display BG1,OBJ
		STZ	  !Through_screen+1   ; no sub screen
		RTS			      ; ( not long )
;
;************************************************************************
;*		 Set BG character data to VRAM				*
;************************************************************************
;
		MEM16
		IDX16
Set_character
		PHP
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;
;=============== Set BG character ======================================
Set_character10
		LDX	  #4400H	      ; IX = VRAM top address
		LDY	  #0000H	      ; IY = data offset
		PHK
		JSR	  TRANSFER_OBJECT     ; Set OBJ character 1
;-----------------------------------------------------------------------
		LDX	  #3000H	      ; IX = VRAM write address
		PHK
		JSR	  TRANS_SEL_CHAR      ; Set BG character and screen
;-----------------------------------------------------------------------
		LDX	  #5000H	      ; IX = VRAM write address
		PHK
		JSR	  TRANS_SEL_CHAR      ; Set BG character and screen
;
;=============== Set car character =====================================
Set_character20
		LDA	  #001FH	      ; Acc = data length
		LDX	  #Mycar_character    ; IX  = source address
		LDY	  #chrwte_length      ; IY  = destination address
		MVN	  #03H,#00H		; CHANGE
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #04H
		STA	  <chrwte_status      ; Set character write status
		PHK
		JSR	  WRITE_CHARACTER
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B
		LDA	  #001FH	      ; Acc = data length
		LDX	  #Rival_character    ; IX  = source address
		LDY	  #chrwte_length      ; IY  = destination address
		MVN	  #03H,#00H
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #04H
		STA	  <chrwte_status      ; Set character write status
		PHK
		JSR	  WRITE_CHARACTER
;-----------------------------------------------------------------------
		PLP			      ; Memory,Index 8 bit mode
		RTS			      ; ( not long )
;
;=======================================================================
;
Mycar_character WORD	  00100H,00100H,00100H,00100H	; Character write length
		WORD	  04200H,04280H,04400H,04480H	; Character write address
		WORD	  00008H,00009H,0000AH,0000BH	; Character data bank
		WORD	  0D5E0H,0D5E0H,0D5E0H,0D5E0H	; Chanacter data address
;-----------------------------------------------------------------------
Rival_character WORD	  00060H,00060H,00060H,00060H	; Character write length
		WORD	  04600H,04630H,04660H,04690H	; Character write address
		WORD	  00008H,00009H,0000AH,0000BH	; Character data bank
		WORD	  0E860H,0E860H,0E860H,0E860H	; Character data address
;
;************************************************************************
;*		 Set color data into Color Generator			*
;************************************************************************
;
		MEM8
		IDX8
Set_color
		LDA	  #01H
		STA	  <colwte_flag	      ; Set color write flag
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		STZ	  !color_buffer	      ; Null color
;-----------------------------------------------------------------------
		LDA	  #000FDH
		LDX	  #0E102H
		LDY	  #color_buffer+002H
		MVN	  #0EH,#00H	      ; Transfer BG color
;-----------------------------------------------------------------------
		LDA	  #000FFH
		LDX	  #0CD00H
		LDY	  #color_buffer+100H
		MVN	  #0FH,#00H	      ; Transfer OBJ color
;-----------------------------------------------------------------------
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		RTS			      ; ( not long )
;
;************************************************************************
;*		 Set Best display character to VRAM			*
;************************************************************************
;
		MEM16
		IDX8
Set_best_char
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
		SEP	  #00010000B	      ; Index 8 bit mode
;
;=============== Set BG character ======================================
Set_best_char10
		LDX	  #10000000B
		STX	  !Screen_step	      ; Set VRAM inc. mode
		LDA	  #3000H
		STA	  !Screen_address     ; Set VRAM address
		LDA	  #Best_char_DMA1
		PHK
		JSR	  START_DMA	      ; Start DMA
		LDA	  #Best_char_DMA2
		PHK
		JSR	  START_DMA	      ; Start DMA
		LDA	  #Best_char_DMA3
		PHK
		JSR	  START_DMA	      ; Start DMA
;
;=============== Set OBJ character =====================================
Set_best_char20
		IDX16
		REP	  #00010000B	      ; Index 16 bit mode
		LDX	  #5000H	      ; IX = VRAM top address
		LDY	  #0000H	      ; IY = data offset
		PHK
		JSR	  TRANSFER_OBJECT     ; Set OBJ character 1
;-----------------------------------------------------------------------
		MEM8
		IDX8
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		LDA	  #04H
Set_best_char21 PHA
		PHK
		JSR	  TRANS_CAR_CHAR
		PHK
		JSR	  WRITE_VRAM
		PLA
		DEC	  A
		BNE	  Set_best_char21
;
;=============== Set null character ====================================
Set_best_char30
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDA	  #47F0H
		STA	  !Screen_address     ; Set VRAM address
		LDX	  #20H
Set_best_char32 STZ	  !Screen_write	      ; Clear character
		DEX
		BNE	  Set_best_char32
;-----------------------------------------------------------------------
		PLP			      ; Memory,Index 8 bit mode
		RTS			      ; ( not long )
;
;************************************************************************
;*		 Set best display color					*
;************************************************************************
;
		MEM16
		IDX16
Set_best_color
		JSR	  Set_color	      ; Set BG,OBJ color
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		LDA	  #0001DH
		LDX	  #0DDC2H
		LDY	  #color_buffer+002H
		MVN	  #0EH,#00H	      ; Transfer Best color
;-----------------------------------------------------------------------
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		RTS
;
;************************************************************************
;*		 Clear best screen					*
;************************************************************************
;
		MEM8
		IDX8
Clear_best_scrn
		PHP
		SEP	  #00110000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
		PHK
		JSR	  CLEAR_OAM
		LDA	  #10000000B
		STA	  !Screen_step	      ; Set VRAM inc. mode
;
;=============== Clear screen main =====================================
Clr_best_scn100
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		LDY	  #0000H	      ; IY  = VRAM address
		LDA	  #0180H	      ; Acc = clear code
		JSR	  Clr_best_srn300
		LDY	  #1000H	      ; IY  = VRAM address
		JSR	  Clr_best_srn300
;
;=============== Set scroll register ===================================
Clr_best_srn200
		STZ	  <BG1_scroll_h
		STZ	  <BG1_scroll_v
		STZ	  <BG2_scroll_h
		STZ	  <BG2_scroll_v
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; Memory 8 bit mode
		STZ	  !Scroll_H+4
		STZ	  !Scroll_H+4
		STZ	  !Scroll_V+4
		STZ	  !Scroll_V+4
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== Clear screen sub ======================================
Clr_best_srn300
		STY	  !Screen_address
		LDY	  #0400H
Clr_best_srn310 STA	  !Screen_write
		DEY
		BNE	  Clr_best_srn310
		RTS
;
;************************************************************************
;*		 Display best time menu					*
;************************************************************************
;
		MEM16
		IDX16
Disp_best_menu
		PHP
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;
;=============== Initial menu screen ===================================
Dsp_best_men100
		LDA	  #0000CH	      ; Acc = data bank
		LDX	  #06A00H	      ; IX  = data offset
		LDY	  #02000H	      ; IY  = VRAM write address
		PHK
		JSR	  EXPAND_SELECT	      ; Set screen data
;
;=============== Clear message =========================================
Dsp_best_men200
		LDY	  #001CH
Dsp_best_men210 LDX	  !Backup_pointer,Y   ; IX = best time pointer
		LDA	  >backup_buffer-1,X ; data avilable ?
		BMI	  Dsp_best_men230     ; yes.
;-----------------------------------------------------------------------
		TYX
		LDA	  >Best_cursor_pos,X
		INC	  A
		INC	  A
		INC	  A
		STA	  !Screen_address     ; Set message top address
;-----------------------------------------------------------------------
		LDX	  #11
		LDA	  #0100H
Dsp_best_men220 STA	  !Screen_write	      ; Clear message
		DEX
		BNE	  Dsp_best_men220
;-----------------------------------------------------------------------
Dsp_best_men230 DEY
		DEY
		BPL	  Dsp_best_men210
;
;=============== Print OBJ message =====================================
Dsp_best_men300
		SEP	  #00010000B	      ; Index 8 bit mode
		LDA	  #Best_menu_msg
		PHK
		JSR	  PRINT_OBJMSG
;
;=============== Print OBJ mark ========================================
Dsp_best_men400
		MEM8
		IDX8
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		LDX	  #0EH
Dsp_best_men410 LDA	  !best_time_rank,X
		AND	  !best_lap_flag,X
		INC	  A
		BEQ	  Dsp_best_men420
		LDA	  >Best_menu_mkx,X
		STA	  <cursor_pos_h
		LDA	  >Best_menu_mky,X
		STA	  <cursor_pos_v
		LDA	  #00110011B
		XBA
		LDA	  #63H
		PHX
		PHK
		JSR	  PUT_OBJCHR
		PLX
Dsp_best_men420 DEX
		BPL	  Dsp_best_men410
;-----------------------------------------------------------------------
		PLP
		RTS
;=======================================================================
Best_menu_mkx	BYTE	  17H,17H,17H,17H,17H
		BYTE	  17H,17H,17H,17H,17H
		BYTE	  8FH,8FH,8FH,8FH,8FH
Best_menu_mky	BYTE	  027H,037H,047H,057H,067H
		BYTE	  08FH,09FH,0AFH,0BFH,0CFH
		BYTE	  027H,037H,047H,057H,067H
;
;************************************************************************
;*		 Set back screen					*
;************************************************************************
;
		MEM8
		IDX8
Set_back_screen
		LDA	  #10000000B
		STA	  !Screen_step
;
;=============== Transfer SC1 SC2 ======================================
Set_bak_scrn100
		LDA	  !course_number
		ASL	  A
		ASL	  A
		TAX
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		PHX
		LDY	  !Back_screen+0,X
		PHK
		JSR	  EXPAND_BACK
		STZ	  !Screen_address
		LDA	  #Back_scrn_DMA
		PHK
		JSR	  START_DMA
;-----------------------------------------------------------------------
		PLX
		LDY	  !Back_screen+2,X
		PHK
		JSR	  EXPAND_BACK
		LDA	  #1000H
		STA	  !Screen_address
		LDA	  #Back_scrn_DMA
		PHK
		JSR	  START_DMA
;
;=============== Transfer SC3 ==========================================
Set_bak_scrn200
		LDA	  #0000FH	      ; Acc = data bank
		LDX	  #040B0H	      ; IX  = data offset
		LDY	  #02000H	      ; IY  = VRAM write address
		PHK
		JSR	  EXPAND_SELECT	      ; Set screen data
;
;=============== Transfer 4 bit character ==============================
Set_bak_scrn300
		LDA	  !course_code
		AND	  #00FFH
		ASL	  A
		TAX
		LDA	  >Map_char_offset,X
		TAY
		LDX	  #5000H
		PHK
		JSR	  TRANSFER_OBJECT     ; Transfer map character
;
;=============== Set world name ========================================
Set_bak_scrn400
		MEM8
		IDX8
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <game_world
		ASL	  A
		ASL	  A
		ASL	  A
		TAX
		LDY	  #00H
		LDA	  >World_name_data,X
		STA	  <work0
;-----------------------------------------------------------------------
Set_bak_scrn410 LDA	  >World_name_data+1,X
		STA	  !string_buffer+2,Y  ; Set string buffer
		INX
		INY
		CPY	  <work0
		BNE	  Set_bak_scrn410
;-----------------------------------------------------------------------
		LDA	  #0FEH
		STA	  !string_buffer+2,Y
		INC	  A
		STA	  !string_buffer+4,Y
		LDA	  <game_scene
		INC	  A
		STA	  !string_buffer+3,Y
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  #21A2H
		STA	  !string_buffer+0
		LDA	  #string_buffer
		JSR	  Print_BGmsg
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		RTS
;
;=======================================================================
World_name_data BYTE	  7,00001100B,4AH,4DH,48H,46H,47H,53H
		BYTE	  6,00001000B,50H,54H,44H,44H,4DH,0
		BYTE	  5,00000000B,4AH,48H,4DH,46H
;
;************************************************************************
;*		 Set best color						*
;************************************************************************
;
		MEM8
		IDX8
Set_back_color
		LDA	  !course_number
		CMP	  #7
		BNE	  Set_back_col10
		LDX	  !course_change
		LDA	  !Course8_color,X
Set_back_col10	ASL	  A
		TAY
;-----------------------------------------------------------------------
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		LDA	  #00DFH
		LDX	  !Course_color,Y
		LDY	  #color_buffer+20H
		MVN	  #0FH,#00H	      ; Set course color
;-----------------------------------------------------------------------
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		RTS
;
;************************************************************************
;*		 Window initialize					*
;************************************************************************
;
		MEM8
		IDX8
Window_initial
		LDA	  #02H
		STA	  <window_flag	      ; Set window flag
;
;=============== Initialize window table ===============================
Window_init100
		LDX	  #12
Window_init110	LDA	  >Message_window,X
		STA	  !window_table+00H,X
		LDA	  >Meter_window,X
		STA	  !window_table+10H,X
		DEX
		BPL	  Window_init110
;
;=============== Initialize window HDMA ================================
Window_init200
		LDX	  #04H
Window_init210	LDA	  >Window_HDMA1,X
		STA	  !DMA_7,X	      ; Set DMA #2 parameter
		LDA	  >Window_HDMA2,X
		STA	  !DMA_6,X	      ; Set DMA #2 parameter
		DEX
		BPL	  Window_init210
;
;=============== Set window work =======================================
Window_init300
		LDA	  <game_process
		CMP	  #02H
		BNE	  Window_init320
;-----------------------------------------------------------------------
Window_init310	LDA	  #10000010B
		STA	  <window_control+0   ; Set window switch
		LDA	  #00000011B
		STA	  <window_main	      ; Window BG1,BG2 on
		LDA	  #11000000B
		STA	  <HDMA_switch	      ; HDMA #6,#7 on
		RTS			      ; ( not long )
;-----------------------------------------------------------------------
Window_init320	LDA	  #00000010B
		STA	  <window_control+0   ; Set window switch
		LDA	  #00000001B
		STA	  <window_main	      ; Window BG1 on
		LDA	  #10000000B
		STA	  <HDMA_switch	      ; HDMA #7 on
		RTS			      ; ( not long )
;
;=============== Window HDMA initial data ==============================
;
Window_HDMA1	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  26H		      ; DMA B address low
		WORD	  window_table+00H    ; DMA A address low,high
		BYTE	  00H		      ; DMA A bank
;-----------------------------------------------------------------------
Window_HDMA2	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  28H		      ; DMA B address low
		WORD	  window_table+10H    ; DMA A address low,high
		BYTE	  00H		      ; DMA A bank
;
;=============== Window table initial data =============================
;
Message_window	BYTE	  06EH
		BYTE	  001H,000H
		BYTE	  001H
		BYTE	  0ABH,0ACH
		BYTE	  001H
		BYTE	  0ABH,0ACH
		BYTE	  001H
		BYTE	  001H,000H
		BYTE	  000H
;-----------------------------------------------------------------------
Meter_window	BYTE	  040H
		BYTE	  001H,000H
		BYTE	  047H
		BYTE	  001H,000H
		BYTE	  028H
		BYTE	  078H,0E2H
		BYTE	  001H
		BYTE	  001H,000H
		BYTE	  000H
;
;************************************************************************
;*		 Display best time					*
;************************************************************************
;
		MEM8
		IDX16
Disp_best_time
		PHP
		SEP	  #00100000B	      ; Memory 8 bit mode
		REP	  #00010000B	      ; Index 16 bit mode
;
;=============== Initial best time buffer ==============================
Disp_best100
		LDX	  #0008H
Disp_best110	LDA	  !Time_disp_data,X
		STA	  !string_buffer+2,X  ; Set string buffer
		DEX
		BPL	  Disp_best110
;
;=============== Display best record ===================================
Disp_best200
		LDX	  !backup_pointer     ; IX = best time pointer
		LDY	  #0000H	      ; IY = display address pointer
;-----------------------------------------------------------------------
Disp_best210	LDA	  >backup_buffer,X
		JSR	  Disp_best_car
		LDA	  >backup_buffer,X   ; data avilable ?
		BPL	  Disp_best240	      ; no.
		JSR	  Change_barcolor
;-----------------------------------------------------------------------
		LDA	  !Time_disp_addr+0,Y
		STA	  !string_buffer+0    ; Set display address low
		LDA	  !Time_disp_addr+1,Y
		STA	  !string_buffer+1    ; Set display address high
;-----------------------------------------------------------------------
Disp_best220	PHY
		LDA	  >backup_buffer+0,X
		AND	  #0FH
		STA	  !string_buffer+03H
		LDA	  >backup_buffer+1,X
		LDY	  #0005H
		JSR	  Disp_best500
		LDA	  >backup_buffer+2,X
		LDY	  #0008H
		JSR	  Disp_best500
;-----------------------------------------------------------------------
Disp_best230	PHX
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		SEP	  #00010000B	      ; Index 8 bit mode
		LDA	  #string_buffer
		JSR	  Print_BGmsg
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		REP	  #00010000B	      ; Index 16 bit mode
		PLX
		PLY
;-----------------------------------------------------------------------
Disp_best240	INX
		INX
		INX
		INY
		INY
		CPY	  #0016H
		BNE	  Disp_best210
;-----------------------------------------------------------------------
Disp_best250	PLP
		RTS
;
;=============== Set time into string buffer ===========================
Disp_best500
		MEM8
		PHA
		LSR	  A
		LSR	  A
		LSR	  A
		LSR	  A
		STA	  !string_buffer+0,Y
;-----------------------------------------------------------------------
		PLA
		AND	  #0FH
		STA	  !string_buffer+1,Y
		RTS
;
;************************************************************************
;*		 Display best time car					*
;************************************************************************
;
		MEM8
		IDX8
Disp_best_car
		PHX
		PHY
		SEP	  #00010000B	      ; Index 8 bit mode
;-----------------------------------------------------------------------
		AND	  #11110000B	      ; no data ?
		BEQ	  Dsp_best_car300     ; yes.
;
;=============== Set OAM main ==========================================
Dsp_best_car100
		AND	  #00110000B
		LSR	  A
		LSR	  A
		LSR	  A
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		AND	  #00FFH
		TAX
;-----------------------------------------------------------------------
		LDY	  <OAM_mainptr
		LDA	  !Best_car_data,X
		STA	  !OAM_main+2,Y	      ; Set character
;
;=============== Set OAM sub ===========================================
Dsp_best_car200
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDY	  <OAM_subptr
		LDA	  <OAM_submask
		AND	  #01010101B
		EOR	  #11111111B
		AND	  !OAM_sub,Y
		STA	  !OAM_sub,Y
;
;=============== Increment OAM pointer =================================
Dsp_best_car300
		PHK
		JSR	  INC_OAM_POINTER
		REP	  #00010000B		; Index 16 bit mode
		PLY
		PLX
		RTS
;
;************************************************************************
;*		 Display best time mark					*
;************************************************************************
;
		MEM8
		IDX8
Disp_best_mark
		LDA	  #063H
		STA	  !OAM_main+102H
		STA	  !OAM_main+106H
		LDA	  #00110011B
		STA	  !OAM_main+103H
		STA	  !OAM_main+107H
;
;=============== Display best time mark ================================
Disp_best_mk100
		LDA	  <game_world
		ASL	  A
		ASL	  A
		ADC	  <game_world
		ADC	  <game_scene
		TAY
		LDX	  !best_time_rank,Y
		BMI	  Disp_best_mk200
;-----------------------------------------------------------------------
		LDA	  >Best_time_mkx,X
		STA	  !OAM_main+100H
		LDA	  >Best_time_mky,X
		STA	  !OAM_main+101H
		LDA	  #11111100B
		STA	  !OAM_sub+10H
;
;=============== Display best lap mark =================================
Disp_best_mk200
		LDX	  !best_lap_flag,Y
		BMI	  Disp_best_mk210
		LDA	  #0FH
		STA	  !OAM_main+104H
		LDA	  #87H
		STA	  !OAM_main+105H
		LDA	  #00001100B
		TRB	  !OAM_sub+10H
Disp_best_mk210 RTS
;
;=======================================================================
Best_time_mkx	BYTE	  08FH,087H,07FH,077H,06FH,067H,05FH,057H,04FH,03FH
Best_time_mky	BYTE	  03FH,04FH,05FH,06FH,07FH,08FH,09FH,0AFH,0BFH,0CFH
;
;************************************************************************
;*		 Change under bar color					*
;************************************************************************
;
		MEM8
		IDX16
Change_barcolor
		LDA	  >backup_buffer,X
		AND	  #01000000B	      ; Battle mode ?
		BNE	  Change_barcl290     ; no.
;
;=============== Change number color ===================================
Change_barcl100
		PHX
		LDA	  #10000000B
		STA	  !Screen_step	      ; Set screen write step
		CPY	  #0014H
		BEQ	  Change_barcl210
;-----------------------------------------------------------------------
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !Time_disp_addr,Y
		DEC	  A
		DEC	  A
		STA	  !Screen_address     ; Set screen address
		CPY	  #0012H
		BNE	  Change_barcl110
;-----------------------------------------------------------------------
		DEC	  A
		STA	  !Screen_address
		SEP	  #00100000B	      ; Memory 8 bit mode
		STZ	  !Screen_write+1
;-----------------------------------------------------------------------
Change_barcl110 SEP	  #00100000B	      ; Memory 8 bit mode
		STZ	  !Screen_write+1
;
;=============== Change bar color ======================================
Change_barcl200
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !Time_disp_addr,Y
		CLC
		ADC	  #001DH
		STA	  !Screen_address
		LDX	  #000DH
		BRA	  Change_barcl220
;-----------------------------------------------------------------------
Change_barcl210 REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !Time_disp_addr,Y
		CLC
		ADC	  #001FH
		STA	  !Screen_address
		LDX	  #000BH
;-----------------------------------------------------------------------
		MEM8
Change_barcl220 SEP	  #00100000B	      ; Memory 8 bit mode
Change_barcl230 STZ	  !Screen_write+1
		DEX
		BNE	  Change_barcl230
;-----------------------------------------------------------------------
		PLX
Change_barcl290 RTS
;
;=======================================================================
;
		PROG
Time_disp_data	BYTE	  00001000B,00H,2AH,00H,00H,2BH,00H,00H,0FFH
Time_disp_addr	WORD	  2115H,2154H,2193H,21D2H,2211H
		WORD	  2250H,228FH,22CEH,230DH,234CH,2223H
Best_car_data	WORD	  3900H,3B02H,3D04H,3F06H
;
;************************************************************************
;*		 Display erase message					*
;************************************************************************
;
		DATA
		MEM16
		IDX16
Display_erase
		STA	  <VRAMbuf_pointer
		REP	  #00110001B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		TYA
		ADC	  #Erase_msg_tbl
		TAX
		LDY	  #VRAMwte_buffer
		LDA	  #23
		MVN	  #03H,#00H
;-----------------------------------------------------------------------
		PHK
		JSR	  SELECT_SOUND
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		RTS
;
;=======================================================================
Erase_msg_tbl	WORD	  22D9H,Erase_message1,0003H,000CH    ; 00
		WORD	  231BH,Erase_message2,0003H,0006H
		WORD	  235AH,Erase_message3,0003H,0006H
;-----------------------------------------------------------------------
		WORD	  22D9H,Erase_message4,0003H,000CH    ; 24
		WORD	  231BH,Erase_message4,0003H,0006H
		WORD	  235AH,Erase_message4,0003H,0006H
;-----------------------------------------------------------------------
		WORD	  231AH,Erase_message3,0003H,0002H    ; 48
		WORD	  235AH,Erase_message4,0003H,0002H
;-----------------------------------------------------------------------
		WORD	  231AH,Erase_message4,0003H,0002H    ; 64
		WORD	  235AH,Erase_message3,0003H,0002H
;-----------------------------------------------------------------------
Erase_message1	WORD	  0044H,0051H,0040H,0052H,0044H,005BH
Erase_message2	WORD	  0058H,0044H,0052H
Erase_message3	WORD	  045CH,004DH,004EH
Erase_message4	WORD	  0100H,0100H,0100H,0100H,0100H,0100H
;
;************************************************************************
;*		 Print message						*
;************************************************************************
;
		MEM16
		IDX8
Print_BGmsg
		PHP
		LDY	  #00H		      ; IY = character pointer
		TYX			      ; IX = length counter
		STA	  <string_pointer     ; Store string pointer
;-----------------------------------------------------------------------
		LDA	  (<string_pointer),Y
		STA	  !Screen_address
		INY
		INY
		MEM8
		SEP	  #00100000B
		LDA	  #10000000B
		STA	  !Screen_step
		LDA	  (<string_pointer),Y
		XBA			      ; B_reg = attribute
		INY
;-----------------------------------------------------------------------
Print_BGmsg10	LDA	  (<string_pointer),Y
		CMP	  #0FFH		      ; end of string ?
		BEQ	  Print_BGmsg20	      ; yes.
		INY
		INX
		STA	  !Screen_write+0     ; Write character
		XBA
		STA	  !Screen_write+1     ; Write attribute
		XBA
		BRA	  Print_BGmsg10
;-----------------------------------------------------------------------
Print_BGmsg20	PLP
		RTS
;
;************************************************************************
;*		 DMA parameters						*
;************************************************************************
;
		PROG
;
Best_menu_msg	BYTE	  90H,0B0H,00110111B
		BYTE	  _R,_E,_C,_O,_R,_D,_S,SP,_M,_E,_N,_U,0
;-----------------------------------------------------------------------
Best_char_DMA1	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  18H		      ; DMA B address low
		WORD	  0E000H	      ; DMA A address low,high
		BYTE	  0FH		      ; DMA A bank
		WORD	  2000H		      ; Number of data
;-----------------------------------------------------------------------
Best_char_DMA2	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  18H		      ; DMA B address low
		WORD	  0C000H	      ; DMA A address low,high
		BYTE	  0CH		      ; DMA A bank
		WORD	  0400H		      ; Number of data
;-----------------------------------------------------------------------
Best_char_DMA3	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  18H		      ; DMA B address low
		WORD	  0DEA0H	      ; DMA A address low,high
		BYTE	  0EH		      ; DMA A bank
		WORD	  0300H		      ; Number of data
;-----------------------------------------------------------------------
Back_scrn_DMA	BYTE	  00000001B	      ; DMA control parameter
		BYTE	  18H		      ; DMA B address low
		WORD	  3000H		      ; DMA A address low,high
		BYTE	  7FH		      ; DMA A bank
		WORD	  01C0H		      ; Number of data
;
		END
