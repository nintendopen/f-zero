*************************************************************************
*	int cmpbuf( buf,dat,len )					*
*	char	*buf,*dat ;						*
*	int	len ;							*
*************************************************************************
*	Programmed by Y.Nishida			[ Apr.10, 1990 ]	*

		.globl		_cmpbuf
		.set		buf,8
		.set		dat,12
		.set		len,16
		.set		autosize,0

		.text
_cmpbuf:
		link		fp,#autosize
		clr.l		d0			/*  d0.l = data number		*/
		move.l		(len,fp),d1		/*  d1.l = data length		*/
		movea.l		(dat,fp),a0		/*  a0.l = data address		*/
		movea.l		(buf,fp),a1		/*  a1.l = buffer address	*/
repeat:
		move.l		(a0),d2
		cmp.l		(a1),d2
		bne.w		skip
		move.l		(4,a0),d2
		cmp.l		(4,a1),d2
		bne.w		skip
		move.l		(8,a0),d2
		cmp.l		(8,a1),d2
		bne.w		skip
		move.l		(12,a0),d2
		cmp.l		(12,a1),d2
		bne.w		skip
		move.l		(16,a0),d2
		cmp.l		(16,a1),d2
		bne.w		skip
		move.l		(20,a0),d2
		cmp.l		(20,a1),d2
		bne.w		skip
		move.l		(24,a0),d2
		cmp.l		(24,a1),d2
		bne.w		skip
		move.l		(28,a0),d2
		cmp.l		(28,a1),d2
		beq.w		exit_func
skip:
		adda.l		#32,a1
		addq.l		#1,d0
		cmp.l		d1,d0
		blt.w		repeat
exit_func:
		unlk		fp
		rts
