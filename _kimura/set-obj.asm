;************************************************************************
;*	 SET_OBJ	 -- object set module --			*
;************************************************************************
		INCLUDE	  BUFFER
		INCLUDE	  VARIABLE
		INCLUDE	  WORK
;-----------------------------------------------------------------------
		GLB	  Setting_OBJ
		GLB	  Setting_CAR
		GLB	  Disp_car
		GLB	  Set_OAMbuff
		GLB	  top_priority
;-----------------------------------------------------------------------
		EXT	  Set_disp_pos
		EXT	  Check_enemy
		EXT	  Calc_distance
		EXT	  RIVAL_address,ZAKO_address
		EXT	  RIVAL_length,ZAKO_length
		EXT	  Car_VRAM_address
		EXT	  OBJ_point_address
		EXT	  PTR_000,PTR_001,PTR_002
		EXT	  PTR_100,PTR_101,PTR_102
		EXT	  OAM_PTR_00,OAM_PTR_07
		EXT	  CRASH_PTR
		EXT	  Sequence_1,Sequence_2,Sequence_3,Sequence_4
		EXT	  Sequence_5,Sequence_6,Sequence_7,Sequence_8
		EXT	  Sequence_9,Sequence_A
		EXT	  EX_parts_data
		EXT	  EX_block
		EXT	  Init_smoke_h,Init_smoke_v,Init_speed_h,Init_speed_v
		EXT	  Stand_smoke_h,Stand_smoke_v
		EXT	  BIT_CHECK_DATA,CAR_TABLE_DATA
;==========================================================================
;		Local variable
;==========================================================================-
tempolary	EQU	  0000H
;
plus_v_pos	EQU	  0010H
counter		EQU	  0012H
my_car_color	EQU	  0014H
;
data_address	EQU	  0018H
position_h	EQU	  001AH
position_v	EQU	  001CH
position	EQU	  001AH
character	EQU	  001EH
attribute	EQU	  0020H
char_addr	EQU	  0022H			; 3 byte
angle_sign	EQU	  0024H
pose_data	EQU	  0025H
car_pose_pointer EQU	  0026H
write_char_index EQU	  0028H
Sub_pointer	EQU	  002AH
block_number	EQU	  002CH
MSB_H		EQU	  002EH
;
OAM_pointer	EQU	  0028H			;2byte
big_block	EQU	  002AH			;1  :
small_block	EQU	  002BH			;  :
parts_h		EQU	  002CH			;  :
block_h		EQU	  002DH			;  :
block_v		EQU	  002EH			;  :
;
disp_dif_h	EQU	  0030H			;2byte
disp_dif_v	EQU	  0032H			;2byte
block_no_cont	EQU	  0034H
;
enemy_status	EQU	  0036H
;
stand_smoke_h	EQU	  0030H
stand_smoke_v	EQU	  0032H
smoke_address	EQU	  0034H
basic_smoke_h	EQU	  0036H
basic_smoke_v	EQU	  0038H
basic_speed_h	EQU	  003AH
basic_speed_v	EQU	  003CH
reverse_h	EQU	  003EH
;=======================================================================
		EXTEND
		PROG
;=======================================================================
;		Setting OAM to OBJ data
;==========================================================================
		MEM8
		IDX8
Setting_OBJ
		PHP
		SEP	  #00110000B
;
		LDA		!enemy_counter
		INC		A
		CMP		#06H
		BCC		Not_over_counter
		LDA		#01H
Not_over_counter
		STA		!enemy_counter
;-----------------------------------------------------------------------------
Check_explosion
		LDA		<explosion_count
		CMP		#07
		BCC		No_explosion
		BNE		Check_after_EX
Start_explosion
		JSR		Chenge_buffer
		MEM8
		IDX8
		SEP		#00110000B
		LDX		#0E1H
		LDA		#080H
Clear_Buffer_loop
		STA		!31EH,X
		DEX
		BNE		Clear_Buffer_loop
		LDA		#0A0H
		STA		!car_flag+0AH
;
		LDX		#0AH
Clear_Flag_loop
		STZ		!car_flag-01H,X
		DEX
		BNE		Clear_Flag_loop
		LDX		#11H
		LDA		#01010101B
Clear_sub_loop
		STA		!40EH,X
		DEX
		BNE		Clear_sub_loop
;
		LDA		!car_BGdata+00H
		CMP		#0D0H
		BCC		Not_special_EX
		LDA		!check_jump_address
		CMP		#04H
		BCS		Not_special_EX
		DEC		!special_EX_flag
		LDA		#0CH
		STA		!EX_counter
		LDA		#028H
		STA		!plus_V_data
		BRA		Start_SP_EX
Not_special_EX
		STZ		!plus_V_data
;-----------------------------------------------------------------------
Check_after_EX
		LDA		!special_EX_flag
		BEQ		No_special_EX
		BPL		No_special_EX
		DEC		<explosion_count
Start_SP_EX
		LDA		#11100000B
		STA		!spot_color_B
;
		JSR		Special_EX_sub
;
		DEC		!EX_counter
		BNE		Cont_special_EX
		LDA		#01H
		STA		!special_EX_flag
No_special_EX
		LDA		<explosion_count
		CMP		#47
		BCS		After_explosion
		JSR		Explosion_CONT
Cont_special_EX
		PLP
		RTS
;---------------------------------------------------------------------------
No_explosion
		JSR		Check_enemy
		JSR		My_car_cont
		JSR		Disp_car
		JSR		Set_write_char
		JSR		Set_OAMbuff
		PLP
		RTS
;---------------------------------------------------------------------------
After_explosion
		BNE		Go_to_after_EX
		BRA		Initial_smoke
Go_to_after_EX
		JSR		Check_enemy
		MEM8
		IDX8
		LDA		!car_flag+0AH
		BIT		#00001000B
		BEQ		No_smoke
		JSR		Set_smoke
No_smoke
		PLP
		RTS
;-------------------------------------------------------------------------
		MEM16
		IDX8
Initial_smoke
		REP		#00100000B
		LDY		#00H
Smoke_L_loop
		LDX		#00H
		LDA		!special_EX_flag
		AND		#00FFH
		BEQ		No_special_smoke
		LDX		#08H
No_special_smoke
Smoke_M_loop
		LDA		>Init_speed_h,X
		AND		#00FFH
		STA		<basic_speed_h
		LDA		>Init_speed_v,X
		AND		#00FFH
		STA		<basic_speed_v
		LDA		>Stand_smoke_h-01H,X
		STA		<stand_smoke_h
		LDA		>Stand_smoke_v-01H,X
		STA		<stand_smoke_v
		LDA		>Init_smoke_h-01H,X
		STA		<basic_smoke_h
		CPY		#040H
		BCC		No_reverse_h
		LDA		<stand_smoke_h
		ADC		!Smoke_width-01H,X
		STA		<stand_smoke_h
No_reverse_h
		LDA		>Init_smoke_v-01H,X
		STA		<basic_smoke_v
		PHX
		LDX		#00H
Smoke_S_loop
		LDA		!Smoke_block_h-01H,X
		ADC		<basic_smoke_h
		STA		!smoke_pos_h+00H,Y
		LDA		<stand_smoke_h
		STA		!stand_pos_h+00H,Y
		LDA		!Smoke_block_v-01H,X
		ADC		<basic_smoke_v
		STA		!smoke_pos_v+00H,Y
		LDA		<basic_smoke_v
		LDA		<stand_smoke_v
		STA		!stand_pos_v+00H,Y
		LDA		!basic_speed_h
		STA		!stand_speed_h+00H,Y
		LDA		!basic_speed_v
		STA		!stand_speed_v+00H,Y
		INY
		INY
		INX
		CPX		#04H
		BNE		Smoke_S_loop
End_S_loop
		PLX
		INX
		CPX		#08H
		BEQ		End_M_loop
		CPX		#10H
		BEQ		End_M_loop
		JMP		Smoke_M_loop
End_M_loop
		CPY		#050H
		BCS		End_set_loop
		LDY		#040H
		JMP		Smoke_L_loop
End_set_loop
		LDX		#00H
		LDA		#0011010100001110B
Init_char_loop
		STA		!2F2H,X
		INX
		INX
		INX
		INX
		CPX		#0F0H
		BNE		Init_char_loop
		PLP
		RTS
;----------------------------------------------------------------------------
Smoke_block_h
		BYTE		0FCH,0F8H,0F4H,0F8H
		BYTE		0FCH,0FDH,0FCH,0FBH
Smoke_block_v
		BYTE		000H,0F8H,0F0H,0E8H
		BYTE		000H,0F8H,0F0H,0E8H
;----------------------------------------------------------------------------------
Smoke_width
		BYTE		001H,001H,002H,002H
		BYTE		003H,003H,004H,004H
		BYTE		000H,000H,001H,001H
		BYTE		001H,002H,002H,002H
;------------------------------------------------------------------------------
Nomal_h		BYTE		0F8H,0F8H,0E8H,0E8H,008H,008H
Nomal_v		BYTE		0E0H,0F0H,0E0H,0F0H,0E0H,0F0H
;--------------------------------------------------------------------------
Setting_CAR
;				Entry	     A:pose B:size
;					     X:hpos Y:vpos
		MEM16
		IDX8
		PHP
		REP		#00100000B
		SEP		#00010000B
		STX		<position_h+0
		STY		<position_v+0
		STY		!OAM_priority+1
		STZ		<position_v+1
;
		LDX		#0AH
		STX		!OAM_priority+0
		LDX		#02H
		STX		!OAM_counter
;
		MEM8
		SEP		#00100000B
		STZ		<position_h+1
		STZ		<angle_sign
;
		BIT		#10000000B
		BEQ		Pose_plus
		DEC		<angle_sign
		EOR		#0FFH
		INC		A
Pose_plus
		STA		<tempolary	;pose_data
		XBA
		STA		!car_size+0AH
		TAX
		CLC
		LDA		!Size_point_data,X
		ADC		<tempolary
		TAX
		LDA		>OBJ_point_address,X
		STA		<car_pose_pointer
;
		LDA		#0A8H
		STA		!car_flag+0AH
;
		LDA		<mycar_number
		STA		!car_number+0AH
		ASL		A
		ORA		#00001000B
		STA		<character+01H
		LDA		<play_mode
		BEQ		Grandprix_char
		LDA		<game_mode
		BIT		#00000001B
		BNE		Grandprix_char
		LDA		!car_char_num+01H
		STA		<character+01H
Grandprix_char
		LDA		#080H
		STA		<character+00H
;
		LDX		#0AH
		STX		<write_char_index
;
		JSR		Set_write_parameter
;
		MEM16
		REP		#00100000B
		LDA		!car_table_addr+0AH
		STA		<data_address
;
		TXY
;
		JSR		Go_OAM_buffer
;
		PLP
		RTS
;--------------------------------------------------------------------------
		MEM8
		IDX8
Special_EX_sub
		LDA		!EX_counter
		TAY
		LSR		A
		TAX
		LDA		SP_v_pos,Y
		TAY
		LDA		SP_size,X
		XBA
		LDA		#00H
		LDX		#080H
		JSR		Setting_CAR
		RTS
;--------------------------------------------------------------------------
SP_v_pos	BYTE		0CEH,0CCH,0CAH,0C8H
		BYTE		0C6H,0C4H,0C2H,0C0H,0BFH,0BCH,0BAH,0B8H,0B8H
SP_size		BYTE		03H,02H,02H,01H,01H,01H,01H,00H
;===========================================================================
Explosion_CONT			      ;A= explosion counter
		MEM8
		IDX8
		CMP		#46
		BNE		Not_end_EX
		LDX		#00H
		JMP		OAM_clear
Not_end_EX
		LDA		!EX_counter
		TAX
		LDA		!Sequence_data,X
		BPL		Set_EX_char
		LDA		!405H
		ORA		#00FH
		STA		!405H
		LDA		#0FFH
		STA		!406H
		STA		!407H
		LDX		#00H
		JMP		OAM_clear
Set_EX_char
		PHA
		TAX
		LDA		!Sequence_v,X
		CLC
		ADC		!car_display_v
		ADC		!plus_V_data
		STA		<position_v
		LDA		#080H
		STA		<position_h
		PLA
		ASL		A
		TAY
		LDA		Sequence_addr+0,Y
		STA		<data_address+0
		LDA		Sequence_addr+1,Y
		STA		<data_address+1
;
		LDY		#00H
		TYX
Set_EX_loop
		LDA		(<data_address),Y
		CMP		#0FFH
		BNE		Set_pattern_01
		JMP		Set_pattern_02
;-------------------------------------------------------------------------------
Set_pattern_01
		LDA		(<data_address),Y
		CMP		#0FFH
		BEQ		End_of_patern01
		STA		<tempolary+00H
		INY
		LDA		(<data_address),Y
		STA		<tempolary+02H
		INY
		PHY
		LDA		<tempolary+00H
		LSR		A
		LSR		A
		LSR		A
		LSR		A
		TAY
		LDA		!EX_h_data,Y
		CLC
		ADC		<position_h
		STA		!2C0H,X
		LDA		<tempolary+00H
		AND		#0FH
		TAY
		LDA		!EX_v_data,Y
		CLC
		ADC		<position_v
		JSR		Check_v_pos
		STA		!2C1H,X
;
		LDA		<tempolary+02H
		AND		#00111111B
		STA		!2C2H,X
		LDA		<tempolary+02H
		AND		#11000000B
		ORA		#00110111B
		STA		!2C3H,X
		PLY
		INX
		INX
		INX
		INX
		BRA		Set_pattern_01
End_of_patern01
		TXA
		LSR		A
		LSR		A
End_of_explosion
		LDX		#00H
EX_sub_loop
		PHA
		LDA		#0AAH
		STA		!040CH,X
		PLA
		SEC
		SBC		#04H
		CMP		#04H
		BCC		End_EX_sub
		INX
		BRA		EX_sub_loop
End_EX_sub
		INX
		TAY
		LDA		Sub_EX_data,Y
		STA		!040CH,X
		INX
OAM_clear
		LDA		#055H
EX_clear_loop
		STA		!040CH,X
		INX
		CPX		#014H
		BCC		EX_clear_loop
No_change_OBJ
		DEC		!EX_timer
		BNE		Cont_sequence
		INC		!EX_counter
		LDX		!EX_counter
		LDA		Sequence_timer,X
		STA		!EX_timer
Cont_sequence
		RTS
;-------------------------------------------------------------------------
Set_pattern_02
		MEM8
		IDX8
;
		STZ		<OAM_pointer+0
		STZ		<OAM_pointer+1
		INY
;
		LDA		(<data_address),Y
		STA		<big_block
		INY
PTR_02_big_loop
		LDA		(<data_address),Y
		TAX
		INY
		LDA		(<data_address),Y
		STA		<block_h
		INY
		LDA		(<data_address),Y
		STA		<block_v
		INY
;
		JSR		Set_parts
;
		DEC		<big_block
		BNE		PTR_02_big_loop
;
		MEM16
		REP		#00100000B
		LDA		<OAM_pointer
		LSR		A
		LSR		A
		MEM8
		SEP		#00100000B
;
		JMP		End_of_explosion
;-------------------------------------------------------------------------------
EX_h_data
		BYTE		00H,10H,20H,30H,40H,48H,0A8H,0B0H
		BYTE		0C0H,0D0H,0E0H,0F0H
EX_v_data
		BYTE		00H,08H,0FH,17H,1EH,20H,0D1H,0D3H
		BYTE		0D9H,0E1H,0E8H,0F0H,0F8H
;--------------------------------------------------------------------------
Set_parts
		MEM8
		IDX8
;
		PHY
		LDA		!EX_block,X
		STA		<small_block
		INX
Small_block_loop
		LDA		!EX_block,X
		TAY				 ;parts address
		INX
		LDA		!EX_block,X
		STA		<parts_h
		INX
;
		JSR		Set_parts_OAM
		DEC		<small_block
		BNE		Small_block_loop
;
		PLY
		RTS
;-------------------------------------------------------------------------
Set_parts_OAM
		MEM8
		IDX8
		PHX
		IDX16
		REP		#00010000B
		LDX		<OAM_pointer
		LDA		<parts_h
		CLC
		ADC		<block_h
		ADC		<position_h
		STA		!2C0H,X
		STA		!2C4H,X
		LDA		<block_v
		CLC
		ADC		<position_v
		JSR		Check_v_pos
		STA		!2C1H,X
		CLC
		ADC		#010H
		JSR		Check_v_pos
		STA		!2C5H,X
;
		MEM16
		REP		#00100000B
;
		LDA		EX_parts_data+0,Y
		STA		!2C2H,X
		LDA		EX_parts_data+2,Y
		STA		!2C6H,X
;
		TXA
		CLC
		ADC		#08H
		STA		<OAM_pointer
;
		MEM8
		IDX8
		SEP		#00110000B
;
		PLX
		RTS
;---------------------------------------------------------------------------
Check_v_pos
		CMP		#0F0H
		BCC		Not_over_F0
Abnomal_v_pos
		LDA		#0F0H
Nomal_v_pos
		RTS
Not_over_F0
		CMP		#060H
		BCC		Abnomal_v_pos
		RTS
;----------------------------------------------------------------------------
Sub_EX_data
       BYTE	 055H,056H,05AH,06AH
Sequence_data
       BYTE	 000H,001H,002H,003H,080H,002H,001H,004H,005H
       BYTE	 004H,005H,006H,007H,006H,007H,008H,008H,009H,009H,080H
Sequence_timer
       BYTE	 004H,004H,004H,002H,004H,002H,002H,002H,002H
       BYTE	 001H,001H,002H,002H,001H,001H,002H,002H,001H,001H,0FFH
Sequence_v
       BYTE	 0F0H,0F0H,0F0H,0F0H,0F8H,0FCH,000H,004H,008H,010H
Sequence_addr
       WORD	 Sequence_1,Sequence_2,Sequence_3,Sequence_4
       WORD	 Sequence_5,Sequence_6,Sequence_7,Sequence_8
       WORD	 Sequence_9,Sequence_A
;-------------------------------------------------------------------------
Set_smoke
		MEM16
		IDX8
		REP		#00100000B
		LDX		#00H
		LDA		!EX_counter
		INC		A
		AND		#003FH
		STA		!EX_counter
Set_move_loop
		LDA		!EX_counter
		AND		#0000000000100000B
		BEQ		Over_08H
		LDA		!smoke_pos_h,X
		CLC
		ADC		!stand_speed_h,X
		BRA		Set_h_pos
Over_08H
		LDA		!smoke_pos_h,X
		SEC
		SBC		!stand_speed_h,X
Set_h_pos
		STA		!smoke_pos_h,X
Set_v_pos
		LDA		!smoke_pos_v,X
		SEC
		SBC		!stand_speed_v,X
		STA		<tempolary
		SEC
		SBC		!stand_pos_v,X
		BCS		Go_on_move
		CMP		#0E700H
		BCS		Go_on_move
Over_v_pos
		LDA		!stand_pos_h,X
		STA		!smoke_pos_h,X
		LDA		!stand_pos_v,X
		BRA		Not_over_posv
Go_on_move
		LDA		<tempolary
Not_over_posv
		STA		!smoke_pos_v,X
		INX
		INX
		CPX		#78H
		BNE		Set_move_loop
;-----------------------------------------------------------------------------------------
Set_smoke_OAM
		MEM8
		SEP		#00100000B
		LDA		!car_position_h+0AH
		STA		<position_h
		LDA		!car_position_v+0AH
		STA		<position_v
		LDX		#00H
		LDY		#00H
Set_smoke_pos_loop
		LDA		smoke_pos_h+1,X
		CLC
		ADC		<position_h
		STA		!2E0H,Y
		LDA		!smoke_pos_v+1,X
		CLC
		ADC		<position_v
		STA		!2E1H,Y
		CMP		#0F0H
		BCC		Not_over_screen
		CMP		#0FCH
		BCS		Not_over_screen
		LDA		!stand_pos_v+1,X
		STA		!smoke_pos_v+1,X
		LDA		!stand_pos_h+1,X
		STA		!smoke_pos_h+1,X
Not_over_screen
		INX
		INX
		INY
		INY
		INY
		INY
		CPX		#080H
		BNE		Set_smoke_pos_loop
;-------------------------------------------------------------------------------
		LDX		#00H
Set_smoke_sub_loop
		STZ		!040EH,X
		INX
		CPX		#10H
		BNE		Set_smoke_sub_loop
		LDA		!special_EX_flag
		BNE		No_fire_oam
		JSR		Set_fire_sub
		RTS
No_fire_oam
		LDA		#55H
		STA		!041DH
		RTS
;------------------------------------------------------------------------------------
Set_fire_sub
		LDA		<frame_counter
		AND		#00000110B
		LSR		A
		TAX
		LDA		<position_h
		PHA
		PHA
		STA		!03DCH
		SEC
		SBC		#008H
		STA		!03D8H
		PLA
		ADC		F_pos_h+00H,X
		STA		!03D0H
		PLA
		ADC		F_pos_h+04H,X
		STA		!03D4H
		LDA		<position_v
		PHA
		PHA
		SEC
		SBC		#08H
		STA		!03DDH
		STA		!03D9H
		PLA
		ADC		F_pos_v+00H,X
		STA		!03D1H
		PLA
		ADC		F_pos_v+04H,X
		STA		!03D5H
		LDA		#00DH
		STA		!03D2H
		STA		!03D6H
		LDA		#1EH
		STA		!03DAH
		LDA		#1FH
		STA		!03DEH
		LDA		#37H
		STA		!03D3H
		STA		!03D7H
		LDA		#035H
		STA		!03DBH
		STA		!03DFH
Not_car_exist
Not_goll_in_demo
		RTS
F_pos_h
		BYTE		0FAH,0FEH,0FBH,0FDH
		BYTE		0FBH,0FFH,0FCH,0FEH
F_pos_v
		BYTE		0F4H,0F3H,0F4H,0F2H
		BYTE		0F3H,0F2H,0F3H,0F4H
;========================================================================
My_car_cont
		MEM8
		IDX8
		LDA		!car_flag+0
		BPL		Not_car_exist
Car_exists
		LDA		<exception_flag
		BIT		#00100000B
		BEQ		Not_goll_in_demo
Goll_in_demo
		STZ		!car_flag+0
Chenge_buffer
		LDA		#0AH
		STA		!priority_index
		STA		!anime_car_index
;
		LDA		#088H		   ;DEMO soukouchuu
		STA		!car_flag+0AH
		STA		!old_pointer+0AH
		STZ		!car_course+0AH
;
		STZ		!car_area+0AH
;
		STZ		Area_line+0AH
		LDA		#030H
		STA		Area_angle+0AH
		STZ		Area_status+0AH
		LDA		#03H
		STA		Area_drive+0AH
		LDA		#13H
		STA		!accele_perform+0AH
;
		LDA		<mycar_number
		STA		!car_selecta+0AH
		LDA		#080H
		STA		!car_char_num+0AH
		LDA		!car_char_num+01H
		STA		!car_char_num+0BH
;
		LDA		!car_angle+01H
		STA		!car_angle+0BH
		LDA		!car_scrl_angle+01H
		STA		!car_scrl_angle+0BH
;
		MEM16
		REP		#00100000B
		LDA		!car_locatex_h
		STA		!car_locatex_h+0AH
		LDA		!car_locatey_h
		STA		!car_locatey_h+0AH
		LDA		!Mark_point_x
		STA		!Mark_point_x+0AH
		LDA		!Mark_point_y
		STA		!Mark_point_y+0AH
		LDA		!car_speed
		STA		!car_speed+0AH
		LDA		!car_position_h
		STA		!car_position_h+0AH
		LDA		!car_position_v
		STA		!car_position_v+0AH
		RTS
;==========================================================================
;		Display car
;=========================================================================
		MEM8
		IDX8
Disp_car
		SEP	  #00110000B
		STZ	  <OAM_counter
		STZ	  !top_priority
		STZ	  !disp_number
		LDX	  #00H
Disp_car_loop
		LDA	  !car_flag+0,X
		BPL	  Disp_car130
		BIT	  #00001000B
		BEQ	  Disp_car130
		BIT	  #00000100B
		BNE	  Disp_car130
Car_on_the_screen
		INC	  !disp_number
		LDA	  !car_position_v,X	  ; Acc = display position v
		JSR	  Sort_OAMdata
Disp_car130
		INX
		INX
		CPX	  #0CH
		BNE	  Disp_car_loop
;
		LDA	  !disp_number
		BEQ	  No_disp_car
		LDX	  !OAM_priority+00H
		BNE	  Set_top_prio
		LDX	  !OAM_priority+02H
Set_top_prio
		STX	  !top_priority
		JSR	  Calc_distance
No_disp_car
		RTS
;************************************************************************
;*		 Sort OAM data						*
;************************************************************************
		MEM8
		IDX8
Sort_OAMdata
		LDY	  <OAM_counter	      ; IY = OAM_counter
		BEQ	  Sort_OAMdata200     ; if ( IY == 0 )
Sort_OAMdata110
		CMP	  !OAM_priority-1,Y   ; position v < *(priority-1) ?
		BCC	  Sort_OAMdata200     ; yes.
		XBA
		LDA	  !OAM_priority-2,Y
		STA	  !OAM_priority+0,Y   ; Change data number
		LDA	  !OAM_priority-1,Y
		STA	  !OAM_priority+1,Y   ; Change position v
		XBA
		DEY
		DEY
		BNE	  Sort_OAMdata110
;=============== Set data ==============================================
Sort_OAMdata200
		STA	  !OAM_priority+1,Y   ; Set position v
		TXA
		STA	  !OAM_priority+0,Y   ; Set data number
		INC	  <OAM_counter
		INC	  <OAM_counter
Not_first_enemy
		RTS
;========================================================================
;		Set write character
;==========================================================================
		MEM8
		IDX8
Set_write_char
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		LDA	  <chrwte_status
		BNE	  No_char_write
		LDX	  !priority_index
		BEQ	  No_priority
		DEC	  !enemy_counter
		BNE	  Not_over_EN_counter
		LDA	  #05H
		STA	  !enemy_counter
Not_over_EN_counter
		BRA	  Set_car_param
No_priority
		LDA	  !jump_pose_flag
		BEQ	  Not_trans_jump_pose
		STZ	  !jump_pose_flag
		MEM16
		REP	  #00100000B
		LDA	  #4800H
		STA	  !chrwte_address+00H	 ;VRAM address
		LDA	  #00C0H
		STA	  !chrwte_length+00H	 ;Trans length
		LDA	  #0EH
		STA	  !chrdat_bank
		LDA	  !mycar_number		  ;Rom bank
		ASL	  A
		TAY
		LDA	  !Jump_pose_address,Y
		STA	  !chrdat_address+00H	 ;Rom address
		INC	  !chrwte_status
No_char_write
Not_set_wteprm
SWC_RTS
		RTS
Jump_pose_address
		WORD	  0E200H,0E380H,0E500H,0E680H
Not_trans_jump_pose
		MEM8
		LDA	  !enemy_counter
		ASL	  A
		TAX
		LDA	  !car_flag+0,X
		AND	  #00001000B
		BEQ	  Not_set_wteprm
Set_car_param
		JSR	  Set_char_pointer
		BCC	  SWC_RTS
		STX	  <write_char_index
		JMP	  Set_write_parameter
;-----------------------------------------------------------------------
		MEM8
		IDX8
Set_char_pointer
		CPX	  !priority_index
		BEQ	  Kyousei_kakikae
		LDA	  !old_pointer,X
		CMP	  #0FFH
		BNE	  Not_kyousei_kakikae
Kyousei_kakikae
		JSR	  Check_pose_sub
		BRA	  Set_car_pose
Not_kyousei_kakikae
		JSR	  Check_pose_sub
;					     A:car pose angle
;					     Y:car pose
		SEC
		SBC	  !car_pose_angle,X
		BEQ	  No_change_pose
Change_car_pos
		BCC	  Pose_comp_minus
		CMP	  #00CH
		BCC	  Rotate_right
		BRA	  Rotate_left
Pose_comp_minus
No_iregular_pose
		CMP	  #0F4H
		BCC	  Rotate_right
;-------------------------------------------------------------------------
Rotate_left
		SEC
		LDA	  !handle_history,X
		ROL	  A
		STA	  !handle_history,X
		AND	  #00001111B
		CMP	  #00000101B
		BEQ	  No_change_pose
;
		LDA	  !car_pose_angle,X
		DEC	  A
		BPL	  Set_car_pose
		LDA	  #25
		BRA	  Set_car_pose
Rotate_right
		CLC
		LDA	  !handle_history,X
		ROL	  A
		STA	  !handle_history,X
		AND	  #00001111B
		CMP	  #00001010B
		BEQ	  No_change_pose
;
		LDA	  !car_pose_angle,X
		INC	  A
		CMP	  #26
		BNE	  Set_car_pose
		LDA	  #00H
Set_car_pose
		STA	  !car_pose_angle,X
		CMP	  #13
		BCC	  Car_pose_plus
		SEC
		SBC	  #25
Car_pose_plus
		STA	  !car_pose,X
No_change_pose
;-------------------------------------------------------------------------
		STZ	  <angle_sign
		LDA	  !car_pose,X
		BPL	  Plus_car_pose
		EOR	  #0FFH
		INC	  A
		DEC	  <angle_sign
Plus_car_pose
		STA	  <pose_data
Set_char_size
		LDA	  !car_position_v,X
		LDY	  #00H
Set_char_size10
		CMP	  !Size_check_data,Y
		BCS	  Set_char_size20
		INY
		CPY	  #08
		BNE	  Set_char_size10
Set_char_size20
		TYA
		STA	  !car_size,X
Set_S_data
		TAY
		CLC
		LDA	  Size_point_data,Y
		ADC	  <pose_data
		PHX
		TAX
		LDA	  >OBJ_point_address,X
		PLX
		CMP	  !old_pointer,X
		BEQ	  No_char_change
		STA	  !old_pointer,X
		STA	  <car_pose_pointer
		SEC
		RTS
No_char_change
		CLC
		RTS
;-----------------------------------------------------------------------
Check_pose_sub
		STZ	  <angle_sign
		LDA	  !car_angle+1	      ;My car angle
		SEC
		SBC	  !car_angle+1,X      ;Enemy car angle
Comp_angle
		BCS	  Set_chr_angle20     ;Angle minus?
		CMP	  #0A0H
		BCS	  Set_chr_angle10
		CLC
		ADC	  #0C0H
		BRA	  Set_chr_angle30
Set_chr_angle10
		EOR	  #0FFH
		INC	  A
		DEC	  <angle_sign
		BRA	  Set_chr_angle30
Set_chr_angle20
		CMP	  #060H
		BCC	  Set_chr_angle30
		DEC	  <angle_sign
		SEC
		SBC	  #0C0H
		EOR	  #0FFH
		INC	  A
Set_chr_angle30
		LSR	  A
		LSR	  A
		TAY
		LDA	  Angle_check_data,Y
		LDY	  <angle_sign
		BEQ	  Angle_sign_plus
		SEC
		SBC	  #25
		EOR	  #0FFH
		INC	  A
Angle_sign_plus
		RTS
;-----------------------------------------------------------------------
Size_point_data
		BYTE	  00,13,26,39,52,65,78,91,104,117
Angle_check_data
		BYTE	  00,01,02,03,04,05,06,06,07,07,08,08
		BYTE	  08,09,09,09,10,10,10,11,11,11,12,12,12
Size_check_data
		BYTE	  09CH,084H,06CH,05CH,04CH,046H,03CH,038H
;************************************************************************
;*		 Set table address					*
;************************************************************************
		MEM8
		IDX8
Set_write_parameter
		STZ		<tempolary
		LDA		!car_flag+00H,X
		BIT		#01000000B	;rival or zako ?
		BEQ		CTA_001
		LDA		#03H
		STA		<tempolary
CTA_001
		LDA		<car_pose_pointer
		LSR		A
		PHA
		AND		#00000111B
		TAY
		LDA		!BIT_CHECK_DATA,Y
		STA		<tempolary+02H
		PLA
		LSR		A
		LSR		A
		LSR		A
		TAY
		LDA		!POSE_COMP_DATA,Y
		BIT		<tempolary+02H
		BNE		CTA_002		 ;Front or rea pose
		LDA		<angle_sign
		BPL		CTA_004
		BRA		CTA_003
CTA_002
		INC		<tempolary
CTA_003
		INC		<tempolary
CTA_004
		LDY		<tempolary
		LDA		!Table_address_data,Y
		CLC
		ADC		!car_size,X
		ASL		A
		TAY
		LDA		!CAR_TABLE_DATA+00H,Y
		STA		!car_table_addr+00H,X
		LDA		!CAR_TABLE_DATA+01H,Y
		STA		!car_table_addr+01H,X
;***********************************************************************
;*		Set character address				       *
;***********************************************************************
		PEA		0002H
		PLB
		LDA		#01H
		STA		<chrwte_status
		LDA		!Car_VRAM_address+00H,X
		STA		!chrwte_address+00H ; Set char write address
		LDA		!Car_VRAM_address+01H,X
		STA		!chrwte_address+01H
		LDY		<car_pose_pointer
		LDA		!car_flag+00H,X
		BIT		#01000000B
		BNE		ZAKO_character
RIVAI_character
		LDA		!car_selecta,X
		CLC
		ADC		#08H
		STA		!chrdat_bank+00H  ; Set char data bank
		MEM16
		REP		#00100000B
		LDA		!RIVAL_address+00H,Y
		STA		!chrdat_address+00H
		TYA
		LSR		A
		TAY
		LDA		!RIVAL_length+00H,Y
		AND		#00FFH
		ASL		A
		STA		!chrwte_length+00H
		PLB
		RTS
ZAKO_character
		MEM8
		LDA		#07EH
		STA		!chrdat_bank+00H      ; Set char data bank
		MEM16
		REP		#00100000B
		LDA		!ZAKO_address+00H,Y
		STA		!chrdat_address+00H
		TYA
		LSR		A
		TAY
		LDA		!ZAKO_length+00H,Y
		AND		#00FFH
		ASL		A
		STA		!chrwte_length+00H
		PLB
		RTS
;----------------------------------------------------------------------------------
Table_address_data
		BYTE		000H,009H,012H,01BH,024H,02DH
POSE_COMP_DATA
		BYTE		00000001B
		BYTE		00110000B
		BYTE		00000000B
		BYTE		00000110B
		BYTE		11000000B
		BYTE		01100000B
		BYTE		00101100B
		BYTE		00000000B
;************************************************************************
;*		 Set OAM main						*
;************************************************************************
		MEM8
		IDX8
Set_OAMbuff
		SEP	  #00110000B	      ; Index 8 bit mode
;
		LDX	  #00H
		JSR	  Set_disp_pos
;
		LDA	  !car_display_h
		STA	  <position_h
		LDA	  !car_display_v
		STA	  <position_v
		JSR	  Set_mycar_buffer
		SEP	  #00110000B
		LDA	  <position_v
		STA	  !old_pos_v
;=============== OAM set enemy car loop=================================
Set_enemy_char
		LDX	  #02H
;-----------------------------------------------------------------------
Set_enemy_loop
Nomal_car
		LDA	  !car_flag+0,X
		BPL	  Next_buffer
		BIT	  #00001000B
		BEQ	  Next_buffer
		BIT	  #00000010B
		BNE	  Crashcar_disp
		BIT	  #00000001B
		BEQ	  Not_precrash
		LDA	  !car_crash_flag,X
		BEQ	  Not_precrash
		BRA	  Start_crash
Not_precrash
		JSR	  Set_disp_pos
;
		CPX	  <write_char_index
		BEQ	  Writecar_set
;
		LDA	  !old_disp_h,X
		BEQ	  Not_old_disp
		CMP	  #18H
		BCC	  Not_old_disp
		CMP	  #0E8H
		BCS	  Not_old_disp
		LDA	  !car_display_h,X
		CMP	  #18H
		BCC	  Not_old_disp
		CMP	  #0E8H
		BCS	  Not_old_disp
		SEC
		SBC	  !old_disp_h,X
		STA	  <disp_dif_h
		LDA	  !car_display_v,X
		SEC
		SBC	  !old_disp_v,X
		STA	  <disp_dif_v
		JSR	  Enemycar_no2
		BRA	  Next_buffer
Not_old_disp
Writecar_set
		JSR	  Enemycar_no1
Next_buffer
		LDA	  !car_display_h,X
		STA	  !old_disp_h,X
		LDA	  !car_display_v,X
		STA	  !old_disp_v,X
		MEM8
		IDX8
		INX
		INX
		CPX	  #0CH
		BCC	  Set_enemy_loop
End_set_buffer
		RTS
;************************************************************************
;*		 Set OAM block into OAM					*
;************************************************************************
Start_crash
		LDA	  !car_flag+00H,X
		ORA	  #00100010B
		STA	  !car_flag+00H,X
		LDA	  #0AH
		STA	  !car_counter,X
		STZ	  !round_counter,X
Crashcar_disp
		LDA	  !car_counter,X
		DEC	  A
		BNE	  Cont_crash
End_crash
		LDA	  !car_flag+00H,X
		ORA	  #00000100B
		AND	  #11011100B
		STA	  !car_flag+00H,X
		BRA	  Next_buffer
Cont_crash
		CMP	  #07H
		BNE	  No_white_color
		LDY	  #0FFH			  ;frash on
		STY	  !flash_color
		LDY	  #03H			  ;2 frame flashing
		STY	  !colwte_flag
No_white_color
		STA	  !car_counter,X
		AND	  #11111110B
		PHX
		TAX
		LDA	  >CRASH_PTR+00H,X
		STA	  <data_address+00H
		LDA	  >CRASH_PTR+01H,X
		STA	  <data_address+01H
		PLX
		LDA	  !car_position_v,X
		STA	  !position_v
		LDA	  !car_position_h+00H,X
		STA	  !position_h+00H
		LDA	  !car_position_h+01H,X
		STA	  !position_h+01H
		STZ	  <character+00H
		STZ	  <character+01H
		JSR	  Go_OAM_buffer
		BRA	  Next_buffer
;*******************************************************************************
Enemycar_no2
		MEM8
		LDA	  !block_num,X
		STA	  <block_number+00H
		MEM16
		IDX16
		REP	  #00110000B
		LDA	  !Buffer_address-02H,X
		TAY
Set_no2_loop
		MEM8
		SEP	  #00100000B
		LDA	  !0200H,Y
		CLC
		ADC	  <disp_dif_h
		STA	  !0200H,Y
		LDA	  !0201H,Y
		CLC
		ADC	  <disp_dif_v
		STA	  !0201H,Y
		INY
		INY
		INY
		INY
		DEC	  <block_number
		BNE	  Set_no2_loop
		MEM8
		IDX8
		SEP	  #00110000B
		RTS
;-----------------------------------------------------------------------------
		MEM8
		IDX8
Enemycar_no1
		LDA	  !car_table_addr+00H,X
		STA	  <data_address+00H
		LDA	  !car_table_addr+01H,X
		STA	  <data_address+01H
		LDA	  !car_display_h+00H,X
		STA	  <position_h+00H
		STA	  !old_disp_h,X
		LDA	  !car_display_h+01H,X
		STA	  <position_h+01H
		LDA	  !car_display_v,X
		STA	  <position_v+00H
		STA	  !old_disp_v,X
		LDA	  !car_char_num+00H,X
		STA	  <character+00H
		LDA	  !car_char_num+01H,X
		STA	  <character+01H
Go_OAM_buffer
		PHX
		MEM16
		IDX16
		REP	  #00110000B
		TXA
		AND	  #00FFH
		STA	  <Sub_pointer
		LDA	  !Buffer_address-02H,X
		TAX
Set_enemycar_100
		LDA	  #0008H
		STA	  <block_number
		STA	  <block_no_cont
		LDY	  #0002H	      ; IY = data pointer
		LDA	  (<data_address)      ; Acc = data length
		STA	  <attribute
		STZ	  <MSB_H
Enemycar_loop
		LDA	  (<data_address),Y
		CMP	  #0080H
		BEQ	  Skip_enemycar
;
		CLC
		ADC	  <position_h+0
		AND	  #01FFH
		CMP	  #0100H
		ROR	  <MSB_H
		LSR	  <MSB_H
;
		MEM8
		SEP	  #00100000B
		STA	  !200H,X
		INY
		INY
		LDA	  (<data_address),Y
		CLC
		ADC	  <position_v
		STA	  !201H,X
		INY
		MEM16
		REP	  #00100000B
		LDA	  (<data_address),Y
		ORA	  <character
		STA	  !202H,X
		DEC	  <block_number
		BEQ	  END_enemycar
		INY
		INY
		INX
		INX
		INX
		INX
		BRA	  Enemycar_loop
Skip_enemycar
		LDA	  #0008H
		SEC
		SBC	  <block_number
		STA	  <block_no_cont
Skip_enemy_loop
		STA	  !200H,X
;
		LSR	  <MSB_H
		LSR	  <MSB_H
		DEC	  <block_number
		BEQ	  END_enemycar
		INX
		INX
		INX
		INX
		BRA	  Skip_enemy_loop
END_enemycar
		LDA	  <attribute
		ORA	  <MSB_H
		LDX	  <Sub_pointer
		STA	  !0D80H,X
		MEM8
		IDX8
		SEP	  #00110000B
		PLX
		LDA	  <block_no_cont
		STA	  !block_num,X
		RTS
;*********************************************************************************
		MEM8
		IDX8
Set_mycar_buffer
		LDA		!special_demo
		BNE		Set_minicar
		JMP		Not_special_demo
;-------------------------------------------------------------------------------------------
Set_minicar
		LDA		!minicar_counter
		INC		A
		CMP		#03H
		BCC		Not_over_mini
		LDA		#00H
Not_over_mini
		STA		!minicar_counter
		AND		#00000001B
		STA		<tempolary+04H
;
		LDA		!special_car
		ASL		A
		STA		<tempolary+02H
		CLC
		ADC		!special_car
		STA		<tempolary
		LDX		#00H
		TXY
Minicar_loop
		LDA		#078H
		STA		!300H,X
		LDA		#080H
		STA		!30CH,X
		LDA		!Mini_v_data,Y
		CLC
		ADC		<tempolary+04H
		STA		!301H,X
		STA		!30DH,X
		TYA
		CLC
		ADC		<tempolary+00H
		STA		!302H,X
		STA		!30EH,X
		LDA		<tempolary+02H
		ORA		#00111001B
		STA		!303H,X
		ORA		#01000000B
		STA		!30FH,X
		INX
		INX
		INX
		INX
		INY
		CPY		#03H
		BNE		Minicar_loop
		STZ		!0D80H
		LDA		#01010000B
		STA		!0D81H
		LDA		<tempolary+02H
		CLC
		ADC		#0CH
		TAX
		LDA		!car_control+00H
		BIT		#00000001B
		BNE		Right_mini
		BIT		#00000010B
		BNE		Left_mini
		RTS
Right_mini
		STX		!030AH
		INX
		STX		!0316H
		LDA		<tempolary+02H
		ORA		#00111001B
		STA		!030BH
		STA		!0317H
		RTS
Left_mini
		STX		!0316H
		INX
		STX		!030AH
		LDA		<tempolary+02H
		ORA		#01111001B
		STA		!030BH
		STA		!0317H
		RTS
;--------------------------------------------------------------------------
Mini_v_data	BYTE		0A8H,0B0H,0B8H
Jump_pose;-------------------------------------------------------------------------------
Not_special_demo
		STZ		!plus_v_pos
		LDA		!car_canopy
		BPL		No_jump_pose
		LDA		#08H
		STA		<counter
		MEM16
		IDX16
		REP		#00110000B
		LDX		#jump_OAM
		LDY		#0000H
		MEM8
		SEP		#00100000B
		LDA		!mycar_pose
		BIT		#00100000B
		BNE		Old_is_jump
		LDA		#00100000B
		STA		!mycar_pose
		JSR		Set_4_param
Set_8_OAM	LDA		#0AAH
		STA		!0D80H
		STA		!0D81H
		RTS
Old_is_jump
		JMP		Move_vpos
No_jump_pose;---------------------------------------------------------------------
		LDA		!car_wing
		BPL		No_slide_pose
Slide_pose;--------------------------------------------------------------------------------------
		AND		#01111111B
		ASL		A
		TAX
		LDA		!oam_number+01H
		STA		<counter
		MEM16
		IDX16
		REP		#00110000B
		LDA		#slide_OAM
		CLC
		ADC		Slide_address,X
		TAX
		LDY		#0000H
		MEM8
		SEP		#00100000B
		LDA		#08H
		STA		<counter
		LDA		!mycar_pose
		BIT		#01000000B
		BNE		Old_is_slide
		LDA		!car_wing
		STA		!old_wing
		LDA		#01000000B
		STA		!mycar_pose
		JSR		Set_4_param
		JSR		Set_6_OAM
		BRA		Check_slide_02
Old_is_slide
		LDA		!car_wing
		CMP		!old_wing
		BEQ		Same_slide
		STA		!old_wing
		JSR		Set_2_param
		JSR		Move_vpos
Check_slide_02
		LDA		<mycar_number
		CMP		#02H
		BNE		No_slide_sub
		JMP		Slide_C_sub
Same_slide
		JMP		Move_vpos
Plus_bank_pos
		BYTE		01H,01H,01H,02H
No_slide_pose;--------------------------------------------------------------------------
		BEQ		SP_bank_01
		CMP		#01H
		BEQ		Bank_pose
		CMP		#09H
		BCC		Stand_pose
		BNE		SP_bank_03
		LDA		#02H
		BRA		Bank_pose
SP_bank_03
		LDA		#03H
SP_bank_01
		PHA
		LDX		<mycar_number
		LDA		!Plus_bank_pos,X
		STA		<plus_v_pos
		PLA
Bank_pose;-----------------------------------------------------------------------
		ASL		A
		TAX
		LDA		!oam_number+00H
		STA		<counter
		MEM16
		IDX16
		REP		#00110000B
		LDA		#bank_OAM
		CLC
		ADC		Bank_address,X
		TAX
		LDY		#0000H
;
		MEM8
		SEP		#00100000B
;
		LDA		!mycar_pose
		BMI		Old_is_bank
		LDA		#10000000B
		STA		!mycar_pose
		LDA		!car_wing
		STA		!old_wing
		JSR		Set_4_param
		LDA		<mycar_number
		BIT		#00000001B
		BNE		Set_6_OAM
Set_7_OAM	LDA		#0AAH
		STA		!0D80H
		LDA		#0AH
		STA		!0D81H
No_slide_sub
		RTS
Old_is_bank
		LDA		!car_wing
		CMP		!old_wing
		BEQ		No_change_bank
		STA		!old_wing
		JMP		Set_4_param
No_change_bank
		JMP		Move_vpos
;-------------------------------------------------------------------------------------
Set_6_OAM	LDA		#0AAH
		STA		!0D80H
		LDA		#05AH
		STA		!0D81H
		RTS
Stand_pose;------------------------------------------------------------------------
		LDA		!mycar_pose
		BEQ		Old_is_stand
		STZ		!mycar_pose
		JSR		Body_sub
		JSR		Wing_sub
		JSR		Canopy_sub
		JSR		Set_position
		BRA		Set_6_OAM
Old_is_stand
		LDA		!car_wing
		CMP		!old_wing
		BEQ		No_change_wing
		STA		!old_wing
		JSR		Wing_sub
No_change_wing
		LDA		!car_canopy
		CMP		!old_canopy
		BEQ		No_change_canopy
		STA		!old_canopy
		JSR		Canopy_sub
No_change_canopy
		JMP		Move_vpos
;--------------------------------------------------------------------------------
		MEM8
		IDX8
Canopy_sub;---------------------------------------------------------------------
		MEM8
		IDX8
		SEP		#00110000B
;
		LDA		!parts_number+01H  ;canopy OAM number
		BEQ		No_canopy_OAM
		STA		<counter
		LDX		!car_canopy
		LDA		!canopy_data_addr,X
		MEM16
		IDX16
		REP		#00110000B
		AND		#00FFH
		CLC
		ADC		#canopy_OAM
		TAX
		LDY		#0000H
		MEM8
		SEP		#00100000B
		JMP		Set_2_param
No_canopy_OAM
		RTS
Wing_sub;---------------------------------------------------------------------------
		MEM8
		IDX8
		SEP		#00110000B
		LDX		!car_wing
		LDA		!wing_data_addr-02H,X
		MEM16
		IDX16
		REP		#00110000B
		AND		#00FFH
		CLC
		ADC		#wing_OAM
		TAX
		LDY		!wing_set_addr
		MEM8
		SEP		#00100000B
		LDA		!parts_number+00H
		STA		<counter
		JMP		Set_2_param
Body_sub;--------------------------------------------------------------------------
		MEM16
		IDX16
		REP		#00110000B
		LDX		#body_OAM
		LDY		!body_set_addr
		MEM8
		SEP		#00100000B
		LDA		!parts_number+02H
		STA		<counter
		JMP		Set_2_param
Set_4_param;----------------------------------------------------------------------
		MEM8
		IDX16
		LDA		<00H,X
		CLC
		ADC		<position_h
		STA		!300H,Y
		LDA		<01H,X
		CLC
		ADC		<plus_v_pos
		ADC		<position_v
		STA		!301H,Y
		LDA		<02H,X
		STA		!302H,Y
		LDA		!03H,X
		STA		!303H,Y
		DEC		<counter
		BEQ		End_loop_4
		INX
		INX
		INX
		INX
		INY
		INY
		INY
		INY
		BRA		Set_4_param
Set_2_param;-----------------------------------------------------------------------------
		LDA		!02H,X
		STA		!302H,Y
		LDA		!03H,X
		STA		!303H,Y
		DEC		<counter
		BEQ		End_loop_2
		INX
		INX
		INX
		INX
		INY
		INY
		INY
		INY
		BRA		Set_2_param
End_loop_2
End_loop_4
End_set_pos
		RTS
Set_position;-------------------------------------------------------------------
		MEM8
		IDX8
		SEP		#00110000B
		LDX		#00H
		LDY		#00H
Set_pos_loop
		LDA		<position_h
		CLC
		ADC		!Nomal_h,X
		STA		!300H,Y
		LDA		<position_v
		CLC
		ADC		<plus_v_pos
		ADC		!Nomal_v,X
		STA		!301H,Y
		INX
		CPX		#08H
		BEQ		End_set_pos
		INY
		INY
		INY
		INY
		BRA		Set_pos_loop
Move_vpos;------------------------------------------------------------------------
		MEM8
		IDX8
		SEP		#00110000B
		LDA		<position_v
		SEC
		SBC		!old_pos_v
		STA		<tempolary
;
		LDX		#00H
Set_vpos_loop
		LDA		!301H,X
		CLC
		ADC		<tempolary
		STA		!301H,X
		INX
		INX
		INX
		INX
		CPX		#20H
		BNE		Set_vpos_loop
		RTS
;------------------------------------------------------------------------
		MEM8
		IDX8
Slide_C_sub
		LDA		#04AH
		STA		!0D81H
		LDA		#04H
		STA		!031AH
		LDA		<position_v
		CLC
		ADC		#0DAH
		STA		!0319H
		LDA		<car_wing
		BIT		#00000001B
		BEQ		Nomal_C_sub
		LDA		#070H
		STA		!318H
		LDA		!car_char_num+01H
		ORA		#01110001B
		STA		!031BH
		RTS
Nomal_C_sub
		LDA		#088H
		STA		!0318H
		LDA		!car_char_num+01H
		ORA		#00110001B
		STA		!031BH
		RTS
;-------------------------------------------------------------------------------
Slide_address	WORD		000H,018H
Bank_address	WORD		000H,020H,040H,060H
;-------------------------------------------------------------------------------
Buffer_address
		WORD		0120H,0140H
		WORD		0160H,0180H,01A0H

		END
