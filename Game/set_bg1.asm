;************************************************************************
;*	 SET_BG1	   -- game screen set module 1 --		*
;************************************************************************
;
		INCLUDE	  RP5A22:
		INCLUDE	  RP5C77:
		INCLUDE	  VARIABLE
		INCLUDE	  BUFFER
;
;=============== Cross reference =======================================
;
		EXT	  INIT_COURSE,BG_SCROLL
		EXT	  SET_GAMEPERS,SET_DEMOPERS
		EXT	  TRANSFER_ENEMY,TRANSFER_OBJECT,TRANSFER_PERS
		EXT	  EXPAND_BACK,EXPAND_SELECT,TRANS_SEL_CHAR
		EXT	  Map_data_number,Set_backup_bank
;
;=============== Cross definition ======================================
;
		GLB	  Init_gamescrn,Set_course_data
		GLB	  Init_demopers,Set_demopers
		GLB	  Init_gamepers,Set_gamepers
		GLB	  BG_scroll,Car_BGcheck,Set_crater
		GLB	  Transfer_enemy,Transfer_object,Transfer_pers
		GLB	  Expand_back,Expand_select,Trans_sel_char
		GLB	  Course_color,Course8_color
;
;=============== Define constant =======================================
;
object_color	EQU	  0FCD00H
Car_clear_color EQU	  0AEE80H
write_offset_x	EQU	  0AED00H
;
;=============== Define local variable =================================
;
work0		EQU	  00H		      ; 2 byte	:Work
work1		EQU	  02H		      ; 2 byte	:Work
;-----------------------------------------------------------------------
chr_address	EQU	  04H		      ; 3 byte	:character address
col_address	EQU	  04H		      ; 3 byte	:color address
char_bank	EQU	  07H		      ; 1 byte	:character bank
;-----------------------------------------------------------------------
loop_counter	EQU	  04H		      ; 2 byte	:loop counter
slit_pointer	EQU	  09H		      ; 3 byte	:Slit pointer
panel_pointer	EQU	  0CH		      ; 2 byte	:Panel pointer
room_offset	EQU	  0EH		      ; 2 byte	:room offset ( panel_y )
slit_offset	EQU	  10H		      ; 2 byte	:slit offset ( panel_x )
;-----------------------------------------------------------------------
home_locate_x	EQU	  12H		      ; 2 byte	:Screen home location x
home_locate_y	EQU	  14H		      ; 2 byte	:Screen home location y
room_count_x	EQU	  16H		      ; 2 byte	:Room counter x
room_count_y	EQU	  18H		      ; 2 byte	:Room counter y
room_counter	EQU	  18H		      ; 2 byte	:Room counter yx
;-----------------------------------------------------------------------
scrl_position_x EQU	  20H		      ; 2 byte
scrl_position_y EQU	  22H		      ; 2 byte
scrl_distance_x EQU	  24H		      ; 2 byte
scrl_distance_y EQU	  26H		      ; 2 byte
check_offset_x	EQU	  28H		      ; 2 byte
check_offset_y	EQU	  2AH		      ; 2 byte
;-----------------------------------------------------------------------
matrix_temp	EQU	  04H		      ; 4 byte	:Rotation matrix tempolary
;-----------------------------------------------------------------------
bomb_pointer	EQU	  20H		      ; 3 byte	:Bomb slit pointer
bomb_offset_x	EQU	  23H		      ; 2 byte	:Bomb write offset x
bomb_offset_y	EQU	  25H		      ; 2 byte	:Bomb write offset y
bomb_position_x EQU	  27H		      ; 2 byte	:Bomb write position x
bomb_position_y EQU	  29H		      ; 2 byte	:Bomb write position y
panel_address	EQU	  2BH		      ; 2 byte	:Panel address
;-----------------------------------------------------------------------
map_bank_number EQU	  20H		      ; 2 byte	:
data_pointer	EQU	  22H		      ; 4 byte	:
trans_counter	EQU	  26H		      ; 2 byte	:
course_status	EQU	  28H		      ; 1 byte	:
;
;=======================================================================
;
		PROG
		EXTEND
;
;************************************************************************
;*		 Bank 3 interface					*
;************************************************************************
;
Init_course	JMP	  >INIT_COURSE
BG_scroll	JMP	  >BG_SCROLL
Set_gamepers	JMP	  >SET_GAMEPERS
Set_demopers	JMP	  >SET_DEMOPERS
Transfer_pers	JMP	  >TRANSFER_PERS
Transfer_enemy	JMP	  >TRANSFER_ENEMY
Transfer_object JMP	  >TRANSFER_OBJECT
Expand_back	JMP	  >EXPAND_BACK
Expand_select	JMP	  >EXPAND_SELECT
Trans_sel_char	JMP	  >TRANS_SEL_CHAR
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Game screen initialize routines \\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Set course data					*
;************************************************************************
;
		MEM8
		IDX8
Set_course_data
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Initialize course work ================================
Set_course100
		LDA	  #15		      ; Acc = offset of free mode
		LDX	  <play_mode	      ; free mode ?
		BNE	  Set_course110	      ; yes.
		LDA	  <game_world
		ASL	  A
		ASL	  A		      ; ( CY=0 )
		ADC	  <game_world	      ; Acc = game_world * 5
Set_course110	CLC
		ADC	  <game_scene
		TAX
;-----------------------------------------------------------------------
		LDA	  >Map_data_number,X
		STA	  !map_data_code
		AND	  #0FH
		STA	  !course_number      ; Set course number
		TAY
		LDA	  >Map_data_number,X
		LSR	  A
		LSR	  A
		LSR	  A
		LSR	  A
		SEC
		SBC	  #0CH
		STA	  !course_change      ; Set course change switch
;-----------------------------------------------------------------------
		PHX
		PHY
		PHK
		JSR	  Set_backup_bank
		PLY
		PLX
;-----------------------------------------------------------------------
		LDA	  <game_mode	      ; Title mode ?
		BNE	  Set_course115	      ; no. ( trans slit )
		LDA	  <game_process	      ; Title initial process ?
		BNE	  Set_course400	      ; no. ( skip )
		LDA	  <demo_flag
		CMP	  #02H		      ; transfer already ?
		BEQ	  Set_course400	      ; yes.( skip )
;-----------------------------------------------------------------------
Set_course115	LDA	  >Map_data_number,X
		BIT	  #00010000B	      ; exception ( COURSE 8-C ) ?
		BEQ	  Set_course120	      ; no.
		LDY	  #09H
		AND	  #11100000B
Set_course120	LSR	  A
		LSR	  A
		LSR	  A
		LSR	  A
		ORA	  #40H
		STA	  <world_pointer+1    ; Set world data address high
		STZ	  <world_pointer+0    ; Set world data address low
;-----------------------------------------------------------------------
		TYX
		LDA	  >Map_change_offs,X
		STA	  <course_status      ; Set course status
;
;=============== Transfer map data =====================================
Set_course200
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		LDA	  !course_number
		AND	  #00FFH
		CMP	  #0001H
		BNE	  Set_course210
		INC	  A
Set_course210	ASL	  A
		ASL	  A
		ASL	  A
;-----------------------------------------------------------------------
		TAX
		LDY	  #Map_world_data&0FFFFH
		JSR	  Transfer_world      ; Transfer world,room data
		INX
		INX
		INX
		INX
		LDY	  #Map_slit_data&0FFFFH
		JSR	  Transfer_slit	      ; Transfer slit data
;
;=============== Set short course ======================================
Set_course300
		LDA	  #01FFH
		LDX	  #Map_world_data&0FFFFH	; IX = world main data address
		LDY	  #Map_world_sub&0FFFFH		; IY = world sub data address
		MVN	  #7FH,#7FH
;-----------------------------------------------------------------------
		PEA	  000CH
		PLB			      ; Data Bank Register = 0CH
		LDA	  <course_status
		AND	  #00FFH
		TAY
Set_course310	LDX	  !(Map_change_data&0FFFFH)+0,Y
		BMI	  Set_course320
		LDA	  !(Map_change_data&0FFFFH)+2,Y
		STA	  >Map_world_sub+00H,X
		LDA	  !(Map_change_data&0FFFFH)+4,Y
		STA	  >Map_world_sub+20H,X
		TYA
		CLC
		ADC	  #0006H
		TAY
		BRA	  Set_course310
;-----------------------------------------------------------------------
Set_course320	PLB
;
;=======================================================================
Set_course400
		MEM8
		IDX8
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  !course_number      ; Get course number
		CMP	  #03H
		BCC	  Set_course410
		CMP	  #06H
		BEQ	  Set_course410
		LDX	  !course_change
		BEQ	  Set_course410
;-----------------------------------------------------------------------
		CLC
		ADC	  !course_change      ; Get course change switch
		ADC	  #04H
Set_course410	STA	  !course_code
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Transfer world data					*
;************************************************************************
;
		MEM16
		IDX16
Transfer_world
		PHX
		JSR	  Trans_course100     ; Initialize pointer
Trans_world100	JSR	  Trans_course200     ; Set data bank
;-----------------------------------------------------------------------
Trans_world200	LDY	  #0000H
Trans_world210	LDA	  [<data_pointer],Y
		STA	  >7F0000H,X
		INX
		INX
		INY
		INY
		CPY	  #0010H
		BNE	  Trans_world210
		DEC	  <trans_counter
		BEQ	  Trans_world220
;-----------------------------------------------------------------------
		CLC
		TYA
		ADC	  <data_pointer
		STA	  <data_pointer
		BNE	  Trans_world200
;-----------------------------------------------------------------------
		JSR	  Trans_course300     ; Increment data bank
		BRA	  Trans_world100
;-----------------------------------------------------------------------
Trans_world220	PLX
		RTS
;
;************************************************************************
;*		 Transfer slit data					*
;************************************************************************
;
		MEM16
		IDX16
Transfer_slit
		PHX
		JSR	  Trans_course100     ; Initialize pointer
Trans_slit100	JSR	  Trans_course200     ; Set data bank
;-----------------------------------------------------------------------
Trans_slit200	LDY	  #0000H
Trans_slit210	LDA	  [<data_pointer],Y
		LSR	  A
		ROR	  <panel_address
		LSR	  A
		ROR	  <panel_address
		STA	  >7F0000H,X
		INX
		INX
		INY
		INY
		CPY	  #0010H
		BNE	  Trans_slit210
;-----------------------------------------------------------------------
		LDA	  <panel_address
		STA	  >7F0000H,X
		INX
		INX
		DEC	  <trans_counter
		BEQ	  Trans_slit220
;-----------------------------------------------------------------------
		CLC
		TYA
		ADC	  <data_pointer
		STA	  <data_pointer	      ; Increment data pointer
		BNE	  Trans_slit200
;-----------------------------------------------------------------------
		JSR	  Trans_course300     ; Increment data bank
		BRA	  Trans_slit100
;-----------------------------------------------------------------------
Trans_slit220	PLX
		RTS
;
;************************************************************************
;*		 Transfer course subroutines				*
;************************************************************************
;
		MEM16
		IDX16
;
;=============== Pointer initialize ====================================
Trans_course100
		LDA	  >Course_pointer+0,X
		STA	  <data_pointer+0
		LDA	  >Course_pointer+2,X
		STA	  <trans_counter
;-----------------------------------------------------------------------
		LDA	  #0000H
		ASL	  <data_pointer+0
		ROL	  A
		ASL	  <data_pointer+0
		ROL	  A
		ASL	  <data_pointer+0
		ROL	  A
		ASL	  <data_pointer+0
		ROL	  A
		STA	  <map_bank_number
		TYX
		RTS
;
;=============== Set data bank =========================================
Trans_course200
		TAY
		LDA	  !Map_data_bank,Y
		AND	  #00FFH
		STA	  <data_pointer+2
		RTS
;
;=============== Increment data bank ===================================
Trans_course300
		LDA	  #8000H
		STA	  <data_pointer
		INC	  <map_bank_number
		LDA	  <map_bank_number
		RTS
;
;************************************************************************
;*		 Initialize game screen					*
;************************************************************************
;
		MEM8
		IDX8
Init_gamescrn
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Initialize screen of mode 7 ===========================
Init_gamesrn100
		LDA	  #00001111B
		STA	  !Screen_mode	      ; Set BG mode 7
		STZ	  !Screen_flip	      ; BG mode 7 screen roop
		STZ	  !Screen_mosaic      ; disable mosaic
		LDA	  #00010011B
		STA	  !Through_screen+0   ; Set through main
		STZ	  !Through_screen+1   ; Set through sub
;-----------------------------------------------------------------------
		LDA	  #10100011B
		STA	  <color_add_sw
		LDA	  #00011111B
		STA	  !const_color_R      ; Set constant color red
		STA	  !const_color_G      ; Set constant color green
		STA	  !const_color_B      ; Set constant color blue
;
;=============== Set world pointer =====================================
;
		LDA	  !course_number
		TAX			      ; IX = course_number
		ASL	  A
		ADC	  !course_number
		TAY			      ; IY = course_number * 3
;
;=============== Set screen ============================================
Init_gamesrn300
		JSR	  Init_character      ; Initialize character
		JSR	  Init_color	      ; Initialize color buffer
		PHK
		JSR	  Init_course	      ; Initialize course data
		JSR	  Init_crater	      ; Initialize crater buffer
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Initialize mode 7 character				*
;************************************************************************
;
		MEM8
		IDX8
Init_character
		LDA	  #10000000B
		STA	  !Screen_step	      ; Set VRAM auto inc. mode
		STZ	  !Screen_address+0   ; Set VRAM address low
		STZ	  !Screen_address+1   ; Set VRAM address high
;
;=============== Set course character ==================================
Init_char100
		STZ	  <chr_address+0      ; Set character data address low
		LDA	  #80H
		STA	  <chr_address+1      ; Set character data address high
		LDA	  #0CH
		STA	  <chr_address+2      ; Set character data address bank
		LDA	  #0D0H		      ; Acc.b = number of characters
		JSR	  Init_char500	      ; Write VRAM
;
;=============== Set effect character ==================================
Init_char200
		LDA	  !course_number
		ASL	  A
		TAY			      ; IY = course_number * 2
;-----------------------------------------------------------------------
		LDA	  !Course_char+0,Y
		STA	  <chr_address+0      ; Set character data address low
		LDA	  !Course_char+1,Y
		STA	  <chr_address+1      ; Set character data address high
		LDA	  #0CH
		STA	  <chr_address+2      ; Set character data address bank
		LDA	  #30H		      ; Acc.b = number of characters
;
;=============== Write character to VRAM ===============================
Init_char500
		IDX16
		REP	  #00010000B	      ; Index 16 bit mode
		LDY	  #0000H	      ; IY = data pointer
		STA	  <work0	      ; work0.b = loop counter
;-----------------------------------------------------------------------
Init_char110	LDA	  [<chr_address],Y
		STA	  <char_bank	      ; Set character bank
		INY
		LDX	  #32
;-----------------------------------------------------------------------
Init_char120	LDA	  [<chr_address],Y
		LSR	  A
		LSR	  A
		LSR	  A
		LSR	  A
		ORA	  <char_bank
		STA	  !Screen_write+1     ; Write character data to VRAM
		LDA	  [<chr_address],Y
		AND	  #0FH
		ORA	  <char_bank
		STA	  !Screen_write+1     ; Write character data to VRAM
		INY
		DEX
		BNE	  Init_char120
;-----------------------------------------------------------------------
		DEC	  <work0
		BNE	  Init_char110
;-----------------------------------------------------------------------
		SEP	  #00010000B	      ; Index 8 bit mode
		RTS
;
;************************************************************************
;*		 Initialize color buffer				*
;************************************************************************
;
		MEM8
		IDX8
Init_color
		LDY	  !course_number
		CPY	  #7		      ; course 8 ?
		BNE	  Init_color10	      ; no.
		LDX	  !course_change
		LDY	  !Course8_color,X
;-----------------------------------------------------------------------
Init_color10	STY	  !course_colnum
		LDA	  !Gradation_mode,Y
		STA	  <color_addsub	      ; set gradation mode
		TYA
		ASL	  A
		TAY
;
;=============== Set course color ======================================
Init_color100
		MEM16
		IDX16
		REP	  #00110000B	      ; memory,Index 16 bit mode
;-----------------------------------------------------------------------
		LDA	  #00DFH
		LDX	  !Course_color,Y
		LDY	  #color_buffer+20H   ; Set course color data bank
		MVN	  #0FH,#00H
;
;=============== Set meter color =======================================
Init_color200
		LDA	  #001FH
		LDX	  #0DDE0H
		LDY	  #color_buffer+00H
		MVN	  #0EH,#00H
;
;=============== Set object color ======================================
Init_color300
		LDA	  #00FFH
		LDX	  #object_color&0FFFFH
		LDY	  #color_buffer+100H
		MVN	  #0FH,#00H
;-----------------------------------------------------------------------
		LDA	  #00FFH
		LDX	  #Car_clear_color&0FFFFH
		LDY	  #car_clear_buff
		MVN	  #0AH,#00H
;-----------------------------------------------------------------------
		LDA	  !special_demo	      ; special demo ?
		AND	  #00FFH
		BNE	  Init_color310	      ; yes.
		LDA	  <play_mode
		AND	  #00FFH	      ; free mode ?
		BEQ	  Init_color310	      ; no.
;-----------------------------------------------------------------------
		LDA	  <mycar_number
		LDX	  #(object_color&0FFFFH)+080H
		LDY	  #color_buffer+180H
		JSR	  Init_color900
		MVN	  #0FH,#00H
;-----------------------------------------------------------------------
		LDA	  !rival_number
		LDX	  #(object_color&0FFFFH)+080H
		LDY	  #color_buffer+1A0H
		JSR	  Init_color900
		MVN	  #0FH,#00H
;-----------------------------------------------------------------------
		LDA	  <mycar_number
		LDX	  #(Car_clear_color&0FFFFH)+80H
		LDY	  #car_clear_buff+080H
		JSR	  Init_color900
		MVN	  #0AH,#00H
;-----------------------------------------------------------------------
		LDA	  !rival_number
		LDX	  #(Car_clear_color&0FFFFH)+80H
		LDY	  #car_clear_buff+0A0H
		JSR	  Init_color900
		MVN	  #0AH,#00H
;-----------------------------------------------------------------------
Init_color310	LDA	  #00FFH
		LDX	  #color_buffer+100H
		LDY	  #car_color_buff
		MVN	  #00H,#00H
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		RTS
;
;=============== Calculate color data address ==========================
Init_color900
		STX	  <work0
		AND	  #0003H
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		ADC	  <work0
		TAX
		LDA	  #001FH
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Car BG check routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Car BG check entry					*
;************************************************************************
;
		MEM16
		IDX8
Car_BGcheck
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
		SEP	  #00010000B	      ; Index 8 bit mode
;
;=============== Get BG data ===========================================
Car_BGcheck200
		LDX	  #0AH
Car_BGcheck210	LDY	  !car_flag,X
		BPL	  Car_BGcheck220
;-----------------------------------------------------------------------
		LDA	  !car_locatex_h,X
		STA	  <home_locate_x      ; Set home location x
		LDA	  !car_locatey_h,X
		STA	  <home_locate_y      ; Set home location y
		JSR	  Search_map_chr
		AND	  #00FFH
		STA	  !car_BGdata,X	      ; Set BG data
Car_BGcheck220	DEX
		DEX
		BPL	  Car_BGcheck210
;-----------------------------------------------------------------------
		PLP			      ; Restore mode
		RTS
;
;************************************************************************
;*		 Search character code					*
;************************************************************************
;
		MEM16
		IDX16
Search_map_chr
		PHP
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		PEA	  007FH
		PLB			      ; Data Bank Register = 7FH
;
;=============== Shaerch slit ==========================================
Search_chr100
		LDA	  <home_locate_x+1
		AND	  #001FH
		STA	  <room_count_x
		LDA	  <home_locate_y
		AND	  #0F00H
		LSR	  A
		LSR	  A
		LSR	  A
		ORA	  <room_count_x
		TAY			      ; IY = room counter
;-----------------------------------------------------------------------
		LDA	  <home_locate_y
		AND	  #0000000011110000B
		LSR	  A
		LSR	  A
		LSR	  A
		STA	  <room_offset	      ; Set room offset ( panel_y * 2 )
;-----------------------------------------------------------------------
		LDA	  (<world_pointer),Y
		AND	  #00FFH	      ; Acc.w = room number
		XBA
		LSR	  A
		LSR	  A
		LSR	  A		      ; ( CY = 0 )
		ADC	  <room_offset
		TAY
		LDA	  !Map_room_data&0FFFFH,Y    ; Acc.w = slit address
		STA	  <slit_pointer	      ; Set slit pointer
;
;=============== Search panel ==========================================
Search_chr200
		LDA	  <home_locate_x
		AND	  #0000000011110000B
		LSR	  A
		LSR	  A
		LSR	  A
		TAY			      ; IY = slit offset ( panel_x * 2 )
		LDA	  (<slit_pointer),Y    ; Acc.b = quad address high
		STA	  <panel_pointer      ; Set panel pointer
;
;=============== Map search in panel ===================================
Search_chr300
		MEM8
		IDX8
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <home_locate_y+0
		AND	  #00001000B
		LSR	  A
		STA	  <work0	      ; work0.b = LSB of char_loc_y
		LDA	  <home_locate_x+0
		AND	  #00001000B
		ORA	  <work0
		LSR	  A
		LSR	  A
		TAY
		LDA	  (<panel_pointer),Y   ; Acc.b = character
;-----------------------------------------------------------------------
		PLB			      ; Data Bank Register = 00H
		PLP
		RTS
;
;************************************************************************
;*		 Search panel address					*
;************************************************************************
;
		MEM16
		IDX16
Search_panel
		PHP
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;
;=============== Vertical panel search =================================
Search_panel100
		LDA	  <home_locate_x+1
		AND	  #001FH
		STA	  <room_count_x
		LDA	  <home_locate_y
		AND	  #0F00H
		LSR	  A
		LSR	  A
		LSR	  A
		ORA	  <room_count_x
		TAY			      ; IY = room counter
;-----------------------------------------------------------------------
		LDA	  <home_locate_y
		AND	  #0000000011110000B
		LSR	  A
		LSR	  A
		LSR	  A
		STA	  <room_offset	      ; Set room offset ( panel_y * 2 )
;-----------------------------------------------------------------------
		LDA	  (<world_pointer),Y
		AND	  #00FFH	      ; Acc.w = room number
		XBA
		LSR	  A
		LSR	  A
		LSR	  A		      ; ( CY = 0 )
		ADC	  <room_offset
;
		TAY
		LDA	  !Map_room_data&0FFFFH,Y    ; Acc.w = slit address
		STA	  <slit_pointer	      ; Set slit pointer
;
;=============== Horizontal panel search ===============================
Search_panel200
		LDA	  <home_locate_x
		AND	  #0000000011110000B
		LSR	  A
		LSR	  A
		LSR	  A
		ADC	  <slit_pointer
		STA	  <panel_address
;-----------------------------------------------------------------------
		PLP
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ BG screen perspective routines \\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Initialize demo perspective				*
;************************************************************************
;
		MEM8
		IDX8
Init_demopers
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Set H DMA table =======================================
Ini_demopers100
		LDX	  #09H
Ini_demopers110 LDA	  !Demo_persdata_0,X
		STA	  !pers_tableA1,X
		STA	  !pers_tableA2,X
		LDA	  !Demo_persdata_1,X
		STA	  !pers_tableB1,X
		STA	  !pers_tableB2,X
		DEX
		BPL	  Ini_demopers110
;
;=============== Set H DMA parameter ===================================
Ini_demopers200
		LDX	  #04H
Ini_demopers210 LDA	  !HDMA_param1,X      ; Set DMA channel #4
		STA	  !DMA_4,X
		LDA	  !HDMA_param2,X      ; Set DMA channel #5
		STA	  !DMA_5,X
		LDA	  !HDMA_param3,X      ; Set DMA channel #6
		STA	  !DMA_6,X
		LDA	  !HDMA_param4,X      ; Set DMA channel #7
		STA	  !DMA_7,X
		DEX
		BPL	  Ini_demopers210
;
;=============== Set HDMA data bank ====================================
Ini_demopers300
		STZ	  <pers_angle	      ; Clear pers angle
		STZ	  <pers_databank_A
		STZ	  <pers_databank_D
		LDA	  #0EH
		STA	  <pers_databank_C
		LDA	  #7EH
		STA	  <pers_databank_B
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== Perspective H DMA table data ==========================
;
Demo_persdata_0 BYTE	  070H
		WORD	  No_pers_data_0
		BYTE	  070H
		WORD	  No_pers_data_0
		BYTE	  00H
;
Demo_persdata_1 BYTE	  0F0H
		WORD	  2000H
		BYTE	  0F0H
		WORD	  4000H
		BYTE	  00H
;-----------------------------------------------------------------------
No_pers_data_0	BYTE	  00H,00H	      ; Matrix = 0000H
;
;=============== Perspective H DMA data ================================
;
HDMA_param1	BYTE	  01000010B	      ; DMA control parameter
		BYTE	  1BH		      ; DMA B address low
		BYTE	  LOW  pers_tableA1   ; DMA A address low
		BYTE	  HIGH pers_tableA1   ; DMA A address high
		BYTE	  BANK pers_tableA1   ; DMA A bank
;
HDMA_param2	BYTE	  01000010B	      ; DMA control parameter
		BYTE	  1CH		      ; DMA B address low
		BYTE	  LOW  pers_tableB1   ; DMA A address low
		BYTE	  HIGH pers_tableB1   ; DMA A address high
		BYTE	  BANK pers_tableB1   ; DMA A bank
;
HDMA_param3	BYTE	  01000010B	      ; DMA control parameter
		BYTE	  1DH		      ; DMA B address low
		BYTE	  LOW  pers_tableB1   ; DMA A address low
		BYTE	  HIGH pers_tableB1   ; DMA A address high
		BYTE	  BANK pers_tableB1   ; DMA A bank
;
HDMA_param4	BYTE	  01000010B	      ; DMA control parameter
		BYTE	  1EH		      ; DMA B address low
		BYTE	  LOW  pers_tableA1   ; DMA A address low
		BYTE	  HIGH pers_tableA1   ; DMA A address high
		BYTE	  BANK pers_tableA1   ; DMA A bank
;
;************************************************************************
;*		 Initialize game perspective				*
;************************************************************************
;
		MEM8
		IDX8
Init_gamepers
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <pers_selector
		EOR	  #00010000B
		TAY			      ; IY = buffer selector

;=============== Set H DMA table =======================================
Ini_gamepers100
		LDX	  #00H
Ini_gamepers110 LDA	  !Game_persdata,X
		STA	  !pers_tableA1,Y
		STA	  !pers_tableB1,Y
		INX
		INY
		CPX	  #0AH
		BNE	  Ini_gamepers110
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== Perspective H DMA table data ==========================
;
Game_persdata	BYTE	  02FH
		WORD	  No_pers_data_1
		BYTE	  0F0H
		WORD	  8000H
		BYTE	  0C0H
		WORD	  8000H
		BYTE	  00H
;-----------------------------------------------------------------------
No_pers_data_1	BYTE	  00H,01H	      ; Matrix = 0100H
;
;************************************************************************
;*		 Initialize crater write buffer				*
;************************************************************************
;
		MEM16
		IDX16
Init_crater
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;
;=============== Set bomb slit to buffer ===============================
Init_crater100
		LDA	  !course_number
		AND	  #00FFH
		ASL	  A
		TAX
		LDA	  #0C00H
		STA	  <bomb_pointer+1     ; Set bomb slit bank
		LDA	  >Bomb_slit_table,X
		STA	  <bomb_pointer+0     ; Set bomb slit address
		LDA	  #7F00H
		STA	  <slit_pointer+1     ; Set slit data bank
;-----------------------------------------------------------------------
		LDY	  #0000H
		TYX
Init_crater110	LDA	  [<bomb_pointer],Y
		BEQ	  Init_crater200      ; if ( data end )
		STA	  <slit_pointer+0     ; Set slit address
		PHY
;-----------------------------------------------------------------------
		LDY	  #0000H
Init_crater120	LDA	  [<slit_pointer],Y
		STA	  >Bomb_slit_data,X
		INX
		INX
		INY
		INY
		CPY	  #32
		BNE	  Init_crater120
;-----------------------------------------------------------------------
		PLY
		INY
		INY
		BRA	  Init_crater110
;
;=============== Set crater panel data =================================
Init_crater200
		IDX8
		SEP	  #00010000B	      ; Index 8 bit mode
;-----------------------------------------------------------------------
		LDX	  #0EH
Init_crater210	LDA	  !Crater_pnldat,X
		STA	  >Crater_panel,X
		DEX
		DEX
		BPL	  Init_crater210
;
;=============== Set crater write buffer ===============================
Init_crater300
		LDX	  #12H
Init_crater310	LDA	  !Crater_tbldat,X
		STA	  !crater_table,X
		DEX
		DEX
		BPL	  Init_crater310
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Crater buffer initial data ============================
;
Crater_tbldat	BYTE	  000H,000H,0AEH,0AEH		  ; 1F80H
		BYTE	  000H,000H,0ACH,0CCH,0CDH,0ADH	  ; 1F84H
		BYTE	  000H,000H,0ACH,0CEH,0CFH,0ADH	  ; 1F8AH
		BYTE	  000H,000H,0AFH,0AFH		  ; 1F90H
;-----------------------------------------------------------------------
Crater_pnldat	BYTE	  09AH,0ACH,0AEH,0CCH
		BYTE	  0AEH,0CDH,09AH,0ADH
		BYTE	  0ACH,09AH,0CEH,0AFH
		BYTE	  0CFH,0AFH,0ADH,09AH
;
;************************************************************************
;*		 Set crater data					*
;************************************************************************
;
		MEM16
		IDX8
Set_crater
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
		SEP	  #00010000B	      ; Index 8 bit mode
;
;=============== Set crater write offset ===============================
Set_crater100
		LDA	  !car_BGdata+0,X
		AND	  #0001H
		ASL	  A
		STA	  <bomb_offset_x      ; Set bomb write offset x
;-----------------------------------------------------------------------
		LDA	  !car_BGdata+0,X
		AND	  #0002H
		LSR	  A
		XBA
		STA	  <bomb_offset_y      ; Set bomb write offset y
;
;=============== Set crater address ====================================
Set_crater200
		LDA	  !crater_pos_x
		LSR	  A
		LSR	  A
		LSR	  A
		SEC
		SBC	  <bomb_offset_x
		AND	  #007EH
		STA	  <bomb_position_x    ; Set bomb position x
;-----------------------------------------------------------------------
		LDA	  !crater_pos_y
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		SEC
		SBC	  <bomb_offset_y
		AND	  #3F00H
		STA	  <bomb_position_y    ; Set bomb position y
;-----------------------------------------------------------------------
		LDA	  <bomb_position_x
		INC	  A
		AND	  #007FH
		ORA	  <bomb_position_y
		STA	  !crater_table+00H   ; Set crater address 1
;-----------------------------------------------------------------------
		LDA	  <bomb_position_x
		ORA	  <bomb_position_y
		CLC
		ADC	  #0080H
		AND	  #3FFFH
		STA	  !crater_table+04H   ; Set crater address 2
;-----------------------------------------------------------------------
		CLC
		ADC	  #0080H
		AND	  #3FFFH
		STA	  !crater_table+0AH   ; Set crater address 4
;-----------------------------------------------------------------------
		LDA	  <bomb_position_x
		INC	  A
		AND	  #007FH
		ORA	  <bomb_position_y
		CLC
		ADC	  #0180H
		AND	  #3FFFH
		STA	  !crater_table+10H   ; Set crater address 5
;
;=============== Change slit data 1 ====================================
Set_crater300
		LDA	  !crater_pos_x
		LDY	  <bomb_offset_x
		BEQ	  Set_crater310
		SEC
		SBC	  #0010H
		AND	  #1FFFH
Set_crater310	STA	  <home_locate_x
;-----------------------------------------------------------------------
		LDA	  !crater_pos_y
		LDY	  <bomb_offset_y+1
		BEQ	  Set_crater320
		SEC
		SBC	  #0010H
		AND	  #1FFFH
Set_crater320	STA	  <home_locate_y
;-----------------------------------------------------------------------
		PEA	  007FH
		PLB			      ; Data Bank Register = 7FH
		JSR	  Search_panel
		LDY	  #00H
		LDA	  #(Crater_panel&0FFFFH)+00H
		STA	  (<panel_address),Y
		INY
		INY
		LDA	  #(Crater_panel&0FFFFH)+04H
		STA	  (<panel_address),Y
;
;=============== Change slit data 2 ====================================
Set_crater400
		LDA	  <home_locate_y
		CLC
		ADC	  #0010H
		STA	  <home_locate_y
;-----------------------------------------------------------------------
		JSR	  Search_panel
		LDY	  #00H
		LDA	  #(Crater_panel&0FFFFH)+08H
		STA	  (<panel_address),Y
		INY
		INY
		LDA	  #(Crater_panel&0FFFFH)+0CH
		STA	  (<panel_address),Y
		PLB			      ; Data Bank Register = 7FH
;
;=============== Set write flag ========================================
Set_crater500
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
		LDA	  #01H
		STA	  <crater_wteflg
		LDA	  #00001000B
		TSB	  <sound_status_2     ; Set SOUND
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Bomb slit data						*
;************************************************************************
;
Bomb_slit_table EQU	  0CE880H
Map_change_offs EQU	  0CE93EH
Map_change_data EQU	  0CE948H
;
;=======================================================================
;
;omb_slit_table WORD	  Bomb_slit_0	      ; 1
;		WORD	  Bomb_slit_2	      ; 2
;		WORD	  Bomb_slit_0	      ; 3
;		WORD	  Bomb_slit_0	      ; 4
;		WORD	  Bomb_slit_0	      ; 5
;		WORD	  Bomb_slit_6	      ; 6
;		WORD	  Bomb_slit_7	      ; 7
;		WORD	  Bomb_slit_8	      ; 1
;		WORD	  Bomb_slit_0	      ; 1
;=======================================================================
;omb_slit_2	WORD	  0A384H,0A3A4H,0A384H,0A3A4H
;		WORD	  0A384H,0A3A4H,0A384H,0A3A4H
;		WORD	  00000H
;-----------------------------------------------------------------------
;omb_slit_6	WORD	  090E8H,090E8H,0825EH,0825EH
;		WORD	  090CCH,090CCH,090CCH,090CCH
;		WORD	  08242H,08242H,08242H,08242H
;		WORD	  090E8H,090E8H,0825EH,0825EH
;		WORD	  00000H
;-----------------------------------------------------------------------
;omb_slit_7	WORD	  0C876H,0C876H,0C876H,0C876H
;		WORD	  0C88AH,0C88AH,0C88AH,0C88AH
;		WORD	  0C876H,0C876H,0C876H,0C876H
;		WORD	  0C88AH,0C88AH,0C88AH,0C88AH
;		WORD	  00000H
;-----------------------------------------------------------------------
;omb_slit_8	WORD	  0A34AH,07A74H,0A32EH,0A260H
;		WORD	  09E46H,0A358H,0ADCAH,0A33CH
;		WORD	  0A256H,09E3EH,0ADC2H,0A302H
;		WORD	  0A4C6H,0966CH,0A192H,0ADECH
;		WORD	  0A680H,0A2D0H,0A48AH,0A320H
;		WORD	  09812H,0A470H,0ADFCH,0A4AAH
;		WORD	  0A690H,0A2E2H,0A5DCH,0A3FEH
;		WORD	  0967AH,0A1A0H,0A61AH,0A66CH
;		WORD	  09E54H,0ADD8H,0BA0AH,0BA8CH
;		WORD	  0A35CH,093BCH,0A354H,093B4H
;omb_slit_0	WORD	  07A66H,097FEH,00000H
;
;************************************************************************
;*		 MAP data address					*
;************************************************************************
;
;ap_change_offs BYTE	  Map_change_data-Map_change_data  ; 1
;		BYTE	  Map_change_data-Map_change_data  ; 2
;		BYTE	  Map_change_data-Map_change_data  ; 3
;		BYTE	  Map_change_dat4-Map_change_data  ; 4
;		BYTE	  Map_change_dat5-Map_change_data  ; 5
;		BYTE	  Map_change_dat6-Map_change_data  ; 6
;		BYTE	  Map_change_data-Map_change_data  ; 7
;		BYTE	  Map_change_dat8-Map_change_data  ; 8
;		BYTE	  Map_change_data-Map_change_data  ; 9
;		BYTE	  Map_change_dat0-Map_change_data  ; 8'
;------------------------------------------------- Course change data --
;ap_change_data WORD	  0FFFFH
;ap_change_dat4 WORD	  00046H,05F0EH,01110H		; 1Fa0
;		WORD	  00086H,06026H,02928H		; 17a0
;		WORD	  0FFFFH
;ap_change_dat5 WORD	  000C6H,00000H,0182FH		; 05-D
;		WORD	  000D6H,00000H,0182EH		; 05-A
;		WORD	  0FFFFH
;ap_change_dat6 WORD	  00008H,00105H,00993H		; 46
;		WORD	  00048H,02694H,00127H		; 47
;		WORD	  0010CH,00163H,00163H		; 25
;		WORD	  0014AH,0927CH,00105H		; 26
;		WORD	  0014CH,07F91H,00005H		; 24
;		WORD	  00006H,00105H,09506H		; 44
;		WORD	  0FFFFH
;ap_change_dat8 WORD	  000C8H,03E3EH,05E3FH		; 0E
;		WORD	  000CAH,0413EH,0435FH		; 0F
;		WORD	  0010AH,06160H,06362H		; 11
;		WORD	  0014AH,06564H,0664FH		; 12
;		WORD	  0014EH,05252H,05353H		; 15
;		WORD	  00150H,05252H,0537BH		; 15b
;		WORD	  00152H,05252H,05353H		; 15
;		WORD	  0FFFFH
;ap_change_dat0 WORD	  000C8H,03E3EH,05E67H		; 0Ec
;		WORD	  000CAH,0413EH,0435FH		; 0F
;		WORD	  0010AH,06160H,06362H		; 11
;		WORD	  0014AH,06564H,0664FH		; 12
;		WORD	  0014CH,05251H,06853H		; 13b
;		WORD	  0014EH,06A69H,0176BH		; 14b
;		WORD	  0010EH,01717H,06D6CH		; 14c
;		WORD	  00110H,01717H,06F6EH		; 14d
;		WORD	  00150H,07170H,07217H		; 14e
;		WORD	  0018EH,07473H,07675H		; 14f
;		WORD	  00190H,07877H,07A79H		; 14g
;		WORD	  00152H,05252H,05353H		; 15
;		WORD	  0FFFFH
;
;=============== Course character data address =========================
;
Course_char	WORD	  0A100H	    ;  Course 1
		WORD	  0AD60H	    ;  Course 2 ( course 3-B )
		WORD	  0AD60H	    ;  Course 3
		WORD	  09AD0H	    ;  Course 4
		WORD	  0A100H	    ;  Course 5
		WORD	  0A730H	    ;  Course 6
		WORD	  0AD60H	    ;  Course 7
		WORD	  0B390H	    ;  Course 8
		WORD	  0B9C0H	    ;  Course 9
;
;=============== Course color data address =============================
;
Course8_color	BYTE	  07H,09H,0AH
;-----------------------------------------------------------------------
Course_color	WORD	  0C360H	    ; Course 1
		WORD	  0CC20H	    ; Course 3-B
		WORD	  0C440H	    ; Course 3-A
		WORD	  0C520H	    ; Course 4
		WORD	  0C600H	    ; Course 5
		WORD	  0C6E0H	    ; Course 6
		WORD	  0C7C0H	    ; Course 7
		WORD	  0C8A0H	    ; Course 8-A
		WORD	  0CB40H	    ; Course 9
		WORD	  0C980H	    ; Course 8-B
		WORD	  0CA60H	    ; Course 8-C
;-----------------------------------------------------------------------
Gradation_mode	BYTE	  00100011B	    ; Course 1	   add
		BYTE	  10100011B	    ; Course 3-B   sub
		BYTE	  00100011B	    ; Course 3-A   add
		BYTE	  10100011B	    ; Course 4	   sub
		BYTE	  00100011B	    ; Course 5	   add
		BYTE	  00100011B	    ; Course 6	   add
		BYTE	  10100011B	    ; Course 7	   sub
		BYTE	  00100011B	    ; Course 8-A   add
		BYTE	  00100011B	    ; Course 9	   add
		BYTE	  00100011B	    ; Course 8-B   add
		BYTE	  10100011B	    ; Course 8-C   sub
;
;=============== Map data bank =========================================
;
Map_data_bank	BYTE	  03H,04H,05H,06H,07H,0DH    ; for REAL
;;;;Map_data_bank   BYTE      78H,04H,05H,06H,07H,0DH	; for EMULATOR
;
		END
