;		BUFFER	 &   WORK
;------------------------------------------------------------------------------
;			       11FFH	;  don't use !!!!
position_x	EQU	       1200H	;  course  data X position
position_y	EQU	       1400H	;  course  data Y position
;
area_angle	EQU	       1600H	;  course  standard angle
area_status	EQU	       1700H	;  course  status for driving
;
drive_data_0	EQU	       1800H	;  course  status for driving
drive_data_1	EQU	       1900H	;  course  status for driving
drive_data_2	EQU	       1A00H	;  course  status for driving
;
smoke_pos_h	EQU	       1500H
smoke_pos_v	EQU	       1600H
stand_pos_v	EQU	       1700H
stand_pos_h	EQU	       1800H
stand_speed_h	EQU	       1900H
stand_speed_v	EQU	       1A00H
;---------------------------------------------------------------------------
RV_speed_table	EQU	       1000H   ;Rival	   3rank * 2byte
ZK_speed_table	EQU	       1006H   ;Zako  1&2  4rank * 2byte
RV_max_speed	EQU	       1026H   ;Rival 1~4  2rank * 2byte
Car_handle_table EQU	       1036H
RV_handle_table EQU	       1036H   ;Rival	   4	   2byte
ZK_handle_table EQU	       103EH   ;Zako  1~4  4	   2byte
;
car_pase_data	EQU	       104EH   ;round 5's pase(5byte)
priority_index	EQU	       1053H   ;char write priority index
disp_number	EQU	       1054H   ;on dislay car number(1byte)
;;;;number	EQU	       1055H   ;out display car number(1byte)
reset_rank	EQU	       1055H   ;reset car's ranking
nearcar_index	EQU	       1056H   ;most near car index
nearcar_sub	EQU	       1057H   ;old near car index
outcar_index	EQU	       1058H   ;four car index data 1byte*5
rival_free_speed EQU	       105DH   ;pactice rival speed data
rank_index	EQU	       105EH   ;mycar rank index(not calc rank)
;---------------------------------------------------------------------------
rival_flag	EQU	       1060H   ;TEST
late_pointa	EQU	       1061H
set_pointa	EQU	       1062H
stop_counter	 EQU	       1064H	 ;stop car's counter
crash_counter	 EQU	       1065H
crash_pase	 EQU	       1066H
speed_buffer	 EQU	       1067H	 ;2byte	 (wakidashi speed buffer)
folow_index	 EQU	       1069H	 ;the car which is most close to mycar
reset_index	 EQU	       106AH	 ;old reset car's index
reset_area	 EQU	       106BH	 ;mycar area when old reset occer
;
jump_checker	 EQU	       106CH	 ;1byte	 (jump status check)
jump_startx	 EQU	       106DH	 ;2byte	 (jump start locatex)
jump_starty	 EQU	       106FH	 ;2byte	 (jump start locatey)
jump_endx	 EQU	       1071H	 ;2byte	 (jump end   locatex)
jump_endy	 EQU	       1073H	 ;2byte	 (jump end   locatey)
check_jump_address EQU	       1075H	 ;1byte	 (jump sub address)
distance_check	 EQU	       1076H	 ;2byte*4pat   Set distance data
special_EX_flag	 EQU	       107EH	 ;special fall sequence flag
plus_V_data	 EQU	       107FH	 ;	  :    V pos data
;
Mark_point_x	 EQU		1080H	 ;car0~5   Mokuhyo point pos
Foward_x	 EQU		1080H
Behind_x	 EQU		108CH
;
Mark_point_y	 EQU		108EH	 ;  ;		 2 byte
Foward_y	 EQU		108EH
Behind_y	 EQU		109AH
;
Area_line	EQU	       109CH	;  ; Genzai no AREA  line
Foward_line	 EQU		109CH
Behind_line	 EQU		10A8H
;
Area_angle	EQU	       109DH	;  ;	   ;	     angle
Foward_angle	   EQU		  109DH
Behind_angle	   EQU		  10A9H
;
Area_status	EQU	       10AAH	;  ;	   ;	     status
Foward_status	EQU	       10AAH
Behind_status	EQU	       10B6H
;
Area_drive	EQU	       10ABH	;  ;	   ;	     drive
;------------------------------------------------------------------------
;foward_pointa	  EQU		 10BEH
;behind_pointa	  EQU		 10BFH
zako_pointa	  EQU		 10BEH
;
enemy_counter	EQU	       10D0H	; five	enemy car counter
;
back_index	EQU	       10D1H	; 1byte	 back check car's index
back_pos_x	EQU	       10D2H	; 2byte	 back position x
;
car_stack	EQU	       10D4H	; 1byte	 car stack pointa
late_number	EQU	       10D5H	; 1byte	 late car number
;
branch_area	EQU	       10D6H	;1byte branch start area
branch_addr	EQU	       10D8H	;1byte RAM branch data address
branch_flag	EQU	       10DAH	;Branch exist ? zero=no !!
top_priority	EQU	       10DCH	;Most big car number
;
EX_counter	EQU	       10DEH	;Explosion sequence count
EX_timer	EQU	       10DFH	;One sequence timer
;
calc_rank	EQU		10E0H
rival_late	EQU		10E1H
rival_late_data EQU		10E2H	;rival car stack
;
mycar_pose	EQU		10F0H	;mycar pose status
old_pos_v	EQU		10F1H	;1 frame before	   position v
old_wing	EQU		10F2H	;	:	   car_wing
old_canopy	EQU		10F3H	;	:	   car_canopy
minicar_counter EQU		10F4H	;minicar counter
;
stack_data1	EQU		1100H	;1byte*18H
stack_data2	EQU		1118H	;    :	   thru 1130H
;-----------------------------------------------------------------------------
car_course	EQU	       1130H	;b7=main or branch
					;b0&b1=0~2 line select
car_selecta	EQU	       1131H	;car select number
					;00=blue 01=green 02=gold 03=red
car_number     EQU	      1131H    ;zako number(for ZAKO)
zako_number	EQU	       1131H	;zako number(for ZAKO)
;
old_pointer	EQU	       1140H	;car_pose angle 1byte
center_angle	EQU		1141H	;1byte car pose (center terget)
handle_history	EQU		1141H	;handle data history
;
car_ranking	EQU	       1150H	;2byte*6     car ranking pointa
;
rank_priority	EQU	       1160H	;2byte Low=car area  High=round
;
rotate_x	EQU		1170H	;2byte kaiten shita shuusei ichi
rotate_y	EQU		1180H	; ;		;
;
point_angle	EQU		1190H	;terget point direction angle
car_pose_angle	EQU		1191H	;car display angle
;
car_distance	EQU		11A0H	;from mycar to enemycar distance
old_disp_h	EQU		11B0H	;2byte 1frame maeno disp pos
old_disp_v	EQU		11C0H	;	     :
block_num	EQU		11D0H	;1byte	car block no kosuu
;		EQU		11D1H	;not use
;-------------------------------------------------------------------------
parts_number	EQU		11E0H	   ;wing,conopy&body OAM number
oam_number	EQU		11E3H	   ;bank,slide&jump  OAM number
wing_set_addr	EQU		11E6H	   ;2byte
body_set_addr	EQU		11E8H	   ;2byte
wing_data_addr	EQU		11EAH	   ;1byte*7patern
canopy_data_addr  EQU		11F1H	   ;	 :	     Next F8
;
;-------------------mycar OAM data buffer---------------------------------
mycar_OAM	EQU		1C00H
bank_OAM	EQU		1C00H
slide_OAM	EQU		1C80H
jump_OAM	EQU		1CC0H
wing_OAM	EQU		1CE0H
canopy_OAM	EQU		1D50H
body_OAM	EQU		1D6CH
;-----------------------------------------------------------------------------------
area_speed	EQU		1E00H		;2byte area's speed
