/***************************************************************
	      Slit data compress  version 1.00
		    Programmed by Y.Nishida
					[ May.28, 1990 ]
 ***************************************************************/

#include	<stdio.h>
#include	<strings.h>

#define		unchar		unsigned char


main( argc,argv )
int	argc ;
char	**argv ;
{
	FILE	*rp,*wp ;
	char	rname[129],wname[129] ;
	void	press_slit() ;

	puts( "\nSlit data compress  version 1.00" ) ;
	if ( argc < 2 ) {
		puts( "Usage: slitpress <slit name> [ <slit name> .... ]" ) ;
		exit( 1 ) ;
	}
	while ( --argc ) {
		strcpy( rname,*++argv ) ;
		strcpy( wname,  *argv ) ;
		strcat( rname,".SLT"  ) ;
		strcat( wname,".SLT2" ) ;

		if (( rp = fopen( rname,"r" )) == NULL ) {
			perror( rname ) ;
		}
		else if (( wp = fopen( wname,"w" )) == NULL ) {
			perror( wname ) ;
			fclose( rp ) ;
		}
		else {
			printf( "Compress \"%s\" to \"%s\"\n", rname,wname ) ;
			press_slit( rp,wp ) ;
			fclose( rp ) ;
			fclose( wp ) ;
		}
	}
	exit( 0 ) ;
}

/****************************************************************/
/*	convert character data					*/
/****************************************************************/

void press_slit( rp,wp )
FILE	*rp,*wp ;
{
	int	datbuf[9] ;
	int	fg=1,nn,i,fgetw() ;
	void	fputw() ;

	while ( fg ) {
		for ( i=0 ; i<9 ; i++ ) {
			if (( datbuf[i] = fgetw( rp )) == EOF ) {
				fg = 0 ;
				break ;
			}
		}
		for ( i=0 ; i<8 ; i++ ) {
			nn = ( datbuf[i] << 2 ) | ( datbuf[8] & 0x0003 ) ;
			fputw( nn,wp ) ;
			datbuf[8] >>= 2 ;
		}

	}
}

/******* file put word *****************************************/

void fputw( data,fp )
int	data ;
FILE	*fp ;
{
	fputc( data % 256,fp ) ;
	fputc( data / 256,fp ) ;
}

/******* file get word *****************************************/

int fgetw( fp )
FILE	*fp ;
{
	register int	dh,dl ;

	if (( dl = fgetc( fp )) == EOF ) return ( EOF ) ;
	if (( dh = fgetc( fp )) == EOF ) return ( EOF ) ;
	return ( dh*256+dl ) ;
}
