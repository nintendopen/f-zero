;************************************************************************
;*	 SOUND		   -- sound control module --			*
;************************************************************************
;
		INCLUDE	  RP5A22:
		INCLUDE	  RP5C77:
		INCLUDE	  VARIABLE
		INCLUDE	  BUFFER
;
;=============== Cross reference =======================================
;
		EXT	  top_priority
		EXT	  Accele_datptr,Turbo_data
;
;=============== Cross definition ======================================
;
		GLB	  Boot_APU,Start_sound,Set_sound,Transfer_music
		GLB	  Engine_sound,Start_enemy_snd,Select_sound,Select_sound2
;
;=============== Define local constant =================================
;
APU_port0	EQU	  02140H	      ; APU I/O port 0
APU_port1	EQU	  02141H	      ; APU I/O port 1
APU_port2	EQU	  02142H	      ; APU I/O port 2
APU_port3	EQU	  02143H	      ; APU I/O port 3
;
;=============== Define local variable =================================
;
work0		EQU	  0000H
address		EQU	  0000H		      ; 3 byte	:Sound data address
;
;=======================================================================
;
		PROG
		EXTEND
;
;************************************************************************
;*		 Music data transfer					*
;************************************************************************
;
		MEM8
		IDX8
Transfer_music
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDX	  !course_code
		LDA	  >Music_number,X
		ASL	  A
		ASL	  A
		ASL	  A
		ADC	  >Music_number,X    ; Acc = Music_number * 9
		STA	  !address+0
		LDA	  #4BH
		STA	  !address+1
;-----------------------------------------------------------------------
;;;;		LDA	  #7BH		      ; for EMULATOR
		LDA	  #02H		      ; for REAL
		STA	  <address+2
;-----------------------------------------------------------------------
		LDA	  #0FFH
		STA	  !APU_port0
;-----------------------------------------------------------------------
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		BRA	  Boot_APU2

;=============== Music data address table ==============================
;
		DATA
;			  0  1	2  3  4	 5  6  7  8  9	A  B  C	 D  E
Music_number	BYTE	  1H,3H,2H,4H,9H,5H,8H,0H,6H,4H,9H,5H,0H,0H,7H
;
;************************************************************************
;*		 Sound boot						*
;************************************************************************
;
		PROG
		MEM16
		IDX16
Boot_APU
		PHP
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		STZ	  <address+0
		LDA	  #0100H	      ; for REAL
;;;;		LDA	  #7A00H	      ; for EMULATOR
		STA	  <address+1
;
;=======================================================================
Boot_APU2
		LDY	  #8000H	      ; IY = data pointer
		LDA	  #0BBAAH
Boot_initial	CMP	  !APU_port0	      ; repeat
		BNE	  Boot_initial	      ; until port0 = BBAAH
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #0CCH
		BRA	  Boot_entry1
;
;=============== Boot repeat ===========================================
Boot_repeat
		JSR	  Boot_get_data	      ; get sound data
		XBA			      ; B_reg = sound_data
		LDA	  #00H		      ; A_reg = boot_status
		BRA	  Boot_entry2
;
;=============== Boot loop =============================================
Boot_loop
		XBA
		JSR	  Boot_get_data	      ; get sound data
		XBA
;-----------------------------------------------------------------------
Boot_wait1	CMP	  !APU_port0	      ; repeat
		BNE	  Boot_wait1	      ; until port0.b == boot_status
		INC	  A		      ; increment boot_status
;
;=============== Boot entry 2 ==========================================
Boot_entry2
		REP	  #00100000B	      ; Memory 16 bit mode
		STA	  !APU_port0	      ; Write data
		SEP	  #00100000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
		DEX			      ; data end ?
		BNE	  Boot_loop	      ; no.
;-----------------------------------------------------------------------
Boot_wait2	CMP	  !APU_port0	      ; repeat
		BNE	  Boot_wait2	      ; until port0.b == boot_status
;-----------------------------------------------------------------------
Boot_zero	ADC	  #03H
		BEQ	  Boot_zero
;
;=============== Boot entry 1 ==========================================
Boot_entry1
		PHA
		JSR	  Boot_get_data	      ; Get data length low
		XBA
		JSR	  Boot_get_data	      ; Get data length high
		XBA
		TAX			      ; IX = data length
;-----------------------------------------------------------------------
		JSR	  Boot_get_data	      ; Get data length high
		XBA
		JSR	  Boot_get_data	      ; Get data length high
		XBA
		REP	  #00100000B	      ; Memory 16 bit mode
		STA	  !APU_port2	      ; Set destination address
		SEP	  #00100000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
		CPX	  #0001H
		LDA	  #00H
		ROL	  A		      ; if ( IX ) Acc = 01H
		STA	  !APU_port1	      ; Set status
		ADC	  #07FH
		PLA
		STA	  !APU_port0
;-----------------------------------------------------------------------
Boot_wait3	CMP	  !APU_port0
		BNE	  Boot_wait3
		BVS	  Boot_repeat
		PLP
		RTS
;
;=============== Boot get data =========================================
Boot_get_data
		LDA	  [<address],Y
		INY
		BNE	  Boot_get_data2
		LDY	  #8000H
		INC	  <address+2	      ; Increment bank
Boot_get_data2	RTS
;
;************************************************************************
;*		 Start sound						*
;************************************************************************
;
		MEM8
		IDX8
Start_sound
		JSR	  Select_sound100
		LDA	  <sound_control
		BMI	  Start_sound200
;
;=============== Sound normal ==========================================
Start_sound100
		LDA	  <sound_status_0
		STA	  !APU_port0
		AND	  #10001111B
		STA	  <sound_status_0
;-----------------------------------------------------------------------
		LDA	  <sound_status_1
		STA	  !APU_port1
		STZ	  <sound_status_1
;-----------------------------------------------------------------------
		LDA	  <sound_status_2
		STA	  !APU_port2
		STZ	  <sound_status_2
;-----------------------------------------------------------------------
		LDA	  <sound_status_3
		STA	  !APU_port3
		STZ	  <sound_status_3
;-----------------------------------------------------------------------
		RTS
;
;=============== Sound exception =======================================
Start_sound200
		LDA	  <sound_status_0
		AND	  #10001111B
		STA	  <sound_status_0
		STA	  !APU_port0
;-----------------------------------------------------------------------
		STZ	  <sound_status_1
		STZ	  !APU_port1
;-----------------------------------------------------------------------
		LDA	  <sound_status_2
		AND	  #10000000B
		STA	  !APU_port2
		STA	  <sound_status_2
;-----------------------------------------------------------------------
		STZ	  <sound_status_3
		STZ	  !APU_port3
;-----------------------------------------------------------------------
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Sound flag set routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Set sound status					*
;************************************************************************
;
		MEM16
		IDX8
Set_sound
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		JSR	  Mycar_engine
		JSR	  Set_alarm_sound
		JSR	  Set_wind_sound
		JSR	  Set_crash_sound
		JSR	  Set_jump_end
		JSR	  Free_lap_sound
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 My car engine sound					*
;************************************************************************
;
		MEM16
		IDX8
Mycar_engine
		REP	  #00100000B	      ; Memory 16 bit mode
		LDY	  !anime_car_index
		BNE	  Mycar_engine150
		LDA	  !car_speed,Y
		CMP	  #0900H
		BCS	  Mycar_engine120
		CMP	  #0200H
		BCS	  Mycar_engine110
;-----------------------------------------------------------------------
Mycar_engine100 ASL	  A
		ASL	  A
		XBA
		TAX
		BNE	  Mycar_engine140
		LDX	  #01H
		BRA	  Mycar_engine140
;-----------------------------------------------------------------------
Mycar_engine110 SEC
		SBC	  #0100H
		ASL	  A
		ASL	  A
		ASL	  A
		XBA
		TAX
		BNE	  Mycar_engine140
		LDX	  #01H
		BRA	  Mycar_engine140
;-----------------------------------------------------------------------
Mycar_engine120 LDX	  #00111111B
		MEM8
Mycar_engine140 SEP	  #00100000B	      ; Memory 8 bit mode
		TXA
		TSB	  <sound_status_1     ; Set engine sound level
		LDA	  <turbo_status
		BEQ	  Mycar_engine150
		LDA	  #10000000B
		TSB	  <sound_status_1
Mycar_engine150 SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Set alarm sound					*
;************************************************************************
;
		MEM8
		IDX8
Set_alarm_sound
		LDA	  !alarm_counter
		BMI	  Set_alarm300
;-----------------------------------------------------------------------
		LDA	  #00100000B
		BIT	  !alarm_priority
		BMI	  Set_alarm120	      ; if ( 1 UP )
		BVS	  Set_alarm130	      ; if ( score display )
		BNE	  Set_alarm110	      ; if ( LAP display )
		LDA	  !alarm_priority
		BEQ	  Set_alarm210
;------------------------------------------------------------ default --
Set_alarm100	LDA	  #00010000B
		BRA	  Set_alarm200
;-------------------------------------------------------- LAP display --
Set_alarm110	LDA	  #00100000B
		BRA	  Set_alarm200
;--------------------------------------------------------------- 1 UP --
Set_alarm120	LDA	  !alarm_counter
		BMI	  Set_alarm210
		LDA	  #10000000B
		STA	  !alarm_counter
		BRA	  Set_alarm210
;------------------------------------------------------ score display --
Set_alarm130	LDA	  !alarm_counter
		CMP	  #00001000B	      ; alarm counter == 8 ?
		BNE	  Set_alarm140	      ; no.
		STZ	  !alarm_counter      ; Reset alarm counter
		BRA	  Set_alarm210
Set_alarm140	INC	  A
		STA	  !alarm_counter
		CMP	  #00000100B	      ; 4
		BCS	  Set_alarm210
		LDA	  #00110000B
;=======================================================================
Set_alarm200	TSB	  <sound_status_0
		STZ	  !alarm_priority
		LDA	  #00010000B
		TRB	  !crash_priority     ; Reset road spark sound
Set_alarm210	RTS
;
;=============== 1 UP sound ============================================
Set_alarm300
		CMP	  #10111101B	      ; counter == 3DH (61) ?
		BNE	  Set_alarm310
		STZ	  !alarm_counter
		RTS
;-----------------------------------------------------------------------
Set_alarm310	INC	  !alarm_counter
		LDA	  #00110000B
		BRA	  Set_alarm200
;
;************************************************************************
;*		 Set crash sound					*
;************************************************************************
;
		MEM8
		IDX8
Set_crash_sound
		LDA	  #00100000B
		BIT	  !crash_priority
		BMI	  Set_crash110	      ; if ( crash enemy )
		BVS	  Set_crash120	      ; if ( out of course )
		BNE	  Set_crash130	      ; if ( on wall )
		LDA	  !crash_priority
		BEQ	  Set_crash210
;--------------------------------------------------------- road spark --
Set_crash100	LDA	  #00000001B
		BRA	  Set_crash200
;-------------------------------------------------------- crash enemy --
Set_crash110	LDA	  #00000100B
		BRA	  Set_crash200
;------------------------------------------------------ out of course --
Set_crash120	LDA	  #00000011B
		BRA	  Set_crash200
;-------------------------------------------------------------on wall --
Set_crash130	LDA	  #00000010B
;=======================================================================
Set_crash200	TSB	  <sound_status_2
		STZ	  !crash_priority
Set_crash210	RTS
;
;************************************************************************
;*		 Set wind sound						*
;************************************************************************
;
		MEM8
		IDX8
Set_wind_sound
		LDA	  #00100000B
		BIT	  !wind_priority
		BMI	  Set_wind120	      ; if ( hyper dash )
		BVS	  Set_wind110	      ; if ( jump | slip big )
		BEQ	  Set_wind210
;--------------------------------------------------------- slip small --
Set_wind100	LDA	  #00010000B
		BRA	  Set_wind200
;---------------------------------------------------- jump & slip big --
Set_wind110	LDA	  #00010000B
		TRB	  !crash_priority     ; Reset road spark sound
		LDA	  #00100000B
		BRA	  Set_wind200
;--------------------------------------------------------- hyper dash --
Set_wind120	LDA	  #00010000B
		TRB	  !crash_priority     ; Reset road spark sound
		LDA	  #00110000B
;=======================================================================
Set_wind200	TSB	  <sound_status_2
		STZ	  !wind_priority
Set_wind210	RTS
;
;************************************************************************
;*		 Set jump end sound					*
;************************************************************************
;
		MEM8
		IDX8
Set_jump_end
		LDA	  !jump_end_sound
		BEQ	  Set_jump_end_2
;-----------------------------------------------------------------------
		LDY	  !special_demo
		BNE	  Set_jump_end_3
		DEC	  A
		STA	  !jump_end_sound
		LDA	  #01000000B
		TSB	  <sound_status_2
Set_jump_end_2	RTS
;-----------------------------------------------------------------------
Set_jump_end_3	STZ	  !jump_end_sound
		LDA	  #00001000B
		TSB	  <sound_status_2     ; Set SOUND
		RTS
;
;************************************************************************
;*		 Select sound						*
;************************************************************************
;
		MEM8
Select_sound
		PHP
		SEP	  #00100000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
		PHA
		LDA	  #01000000B
		TSB	  !sound_status_1
		PLA
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Select sound						*
;************************************************************************
;
		MEM8
Select_sound2
		PHP
		SEP	  #00100000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
		PHA
		LDA	  !selsnd_status
		BEQ	  Select_sound20
		LDA	  #0BH			; 9
		BRA	  Select_sound30
Select_sound20	LDA	  #09H			; 7
Select_sound30	STA	  !selsnd_status
;-----------------------------------------------------------------------
		PLA
		PLP
		RTS
;
;=============== Set sound status 0 ====================================
Select_sound100
		LDA	  !selsnd_status
		BEQ	  Select_sound120
		CMP	  #06H			; 3
		BCC	  Select_sound110
		CMP	  #0AH			; 8
		BCS	  Select_sound110
		LDA	  #00110000B
		TSB	  <sound_status_0
Select_sound110 DEC	  !selsnd_status
Select_sound120 RTS
;
;************************************************************************
;*		 Free lap clear sound					*
;************************************************************************
;
		MEM8
		IDX8
Free_lap_sound
		LDA	  !lap_sound_cnt
		BEQ	  Free_lap_sound2
		DEC	  !lap_sound_cnt
		LDA	  #01000000B
		TSB	  <sound_status_1
Free_lap_sound2 RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ My car engine sound effect \\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 My car engine sound ( for effect )			*
;************************************************************************
;
		MEM8
		IDX8
Engine_sound
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		AND	  #11000000B
		CMP	  #10000000B	      ; accele on ?
		BNE	  Engine_sound200     ; no.
;
;=============== Engine power up =======================================
Engine_sound100
		LDX	  <mycar_number
		LDA	  !car_speed+0
		ASL	  A
		LDA	  !car_speed+1
		ROL	  A
		ADC	  !Accele_datptr,X
		TAX
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
		LDA	  >Turbo_data,X
		AND	  #00FFH
		ADC	  !car_speed
		CMP	  #07FFH	      ; engine power < max power ?
		BCC	  Engine_sound300     ; yes.
		LDA	  #07FFH
		BRA	  Engine_sound300
;
;=============== Engine power down =====================================
Engine_sound200
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
		LDA	  !car_speed
		ADC	  #0FFD8H	      ; engine power down
		BPL	  Engine_sound300
		LDA	  #0000H
;
;=============== Set engine sound ======================================
Engine_sound300
		STA	  !car_speed
		ASL	  A
		ASL	  A
		ASL	  A
		XBA
		TAX
		BNE	  Engine_sound310
		INX
Engine_sound310 STX	  !sound_status_1
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Start enemy engine sound				*
;************************************************************************
;
		MEM8
		IDX8
Start_enemy_snd
		LDA	  <play_mode	      ; Battle mode ?
		BEQ	  Start_enemy110      ; yes.
		LDA	  !rival_number	      ; Rival car nothing ?
		BMI	  Start_enemy120      ; yes.
;-----------------------------------------------------------------------
Start_enemy100	LDA	  #096H
		STA	  <sound_status_3
		RTS
;-----------------------------------------------------------------------
Start_enemy110	LDA	  #0F6H
		STA	  <sound_status_3
Start_enemy120	RTS
;
		END
