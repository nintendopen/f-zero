/***************************************************************
  Enemy car character data archiver for CAR RACE  version 1.00
		    Programmed by Y.Nishida
					[ Dec.14, 1989 ]
 ***************************************************************/

#include	<stdio.h>
#include	<memory.h>
#include	<strings.h>
#include	<ctype.h>

#define		unchar		unsigned char
#define		FSIZE		1024*32
#define		DSIZE		4096
#define		ERROR		-1
#define		COMPLETE	0

struct	patndata {
		short	size ;
		short	*data ;
	} pat[10] ;

short	patn00[24] = {
		0x00,0x01,0x02,0x03,0x04,0x05,0x20,0x21,0x22,0x23,0x24,0x25,
		0x10,0x11,0x12,0x13,0x14,0x15,0x30,0x31,0x32,0x33,0x34,0x35
	} ;
short	patn01[15] = {
		0x00,0x01,0x02,0x03,0x04,0x20,0x21,0x22,
		0x10,0x11,0x12,0x13,0x14,0x23,0x24
	} ;
short	patn02[12] = {
		0x00,0x01,0x02,0x03,0x20,0x21,
		0x10,0x11,0x12,0x13,0x22,0x23
	} ;
short	patn03[6] = {
		0x00,0x01,0x02,
		0x10,0x11,0x12
	} ;
short	patn04[4] = {
		0x00,0x01,
		0x10,0x11
	} ;
short	patn05[1] = { 0x00 } ;

short	patn06[12] = {
		0x00,0x01,0x02,0x20,0x21,0x22,
		0x10,0x11,0x12,0x30,0x31,0x32
	} ;
short	patn07[6] = {
		0x00,0x01,0x20,
		0x10,0x11,0x21
	} ;
short	patn08[2] = { 0x00,0x10 } ;
short	patn09[9] = {
		0x00,0x01,0x02,0x20,0x22,
		0x10,0x11,0x12,0x21
	} ;

unchar	chrbuf[DSIZE][32] ;		/* character data buffer	*/
int	chrcnt = 0 ;			/* character data counter	*/
int	total  = 0 ;			/* total size			*/


main( argc,argv )
int	argc ;
char	**argv ;
{
	FILE	*tp,*wp ;
	char	chr_name[65] ;
	char	tbl_name[65] ;
	char	strbuf[136] ;

	puts( "\nEnemy car character data archiver for CAR RACE  version 1.00" ) ;
	if ( argc != 2 ) {
		puts( "usage: mkenemy <data name>" ) ;
		exit( 1 ) ;
	}
	strcpy( tbl_name,*(++argv)) ;
	strcat( tbl_name,".TBL" ) ;
	if (( tp = fopen( tbl_name,"r" )) == NULL ) {
		perror( tbl_name ) ;
		exit( 1 ) ;
	}
	strcpy( chr_name,*argv ) ;
	strcat( chr_name,".CHR" ) ;
	if (( wp = fopen( chr_name,"w" )) == NULL ) {
		perror( chr_name ) ;
		exit( 1 ) ;
	}

	init_ptr() ;
	while ( fgets( strbuf,135,tp ) != NULL ) {
		if ( strbuf[0] == '$' ) {
			set_cname( strbuf+1 ) ;
			readchar( strbuf+1 ) ;
		}
		else if ( 'A' <= strbuf[0] && strbuf[0] <= 'L' ) {
			archive( wp,strbuf ) ;
		}
	}
	printf( "Total %d characters (%d bytes) write.\n",total,total*24 ) ;
	fclose( tp ) ;
	fclose( wp ) ;
}

/******* initialize pointer *************************************/

init_ptr()
{
	pat[ 0].size = 24 ;
	pat[ 0].data = patn00 ;
	pat[ 1].size = 15 ;
	pat[ 1].data = patn01 ;
	pat[ 2].size = 12 ;
	pat[ 2].data = patn02 ;
	pat[ 3].size = 6 ;
	pat[ 3].data = patn03 ;
	pat[ 4].size = 4 ;
	pat[ 4].data = patn04 ;
	pat[ 5].size = 1 ;
	pat[ 5].data = patn05 ;
	pat[ 6].size = 12 ;
	pat[ 6].data = patn06 ;
	pat[ 7].size = 6 ;
	pat[ 7].data = patn07 ;
	pat[ 8].size = 2 ;
	pat[ 8].data = patn08 ;
	pat[ 9].size = 9 ;
	pat[ 9].data = patn09 ;
}

/******* set character file name *********************************/

set_cname( ss )
char    *ss ;
{
	while ((*ss != '\0') && (*ss != '\n')) ss++ ;
	strcpy( ss,".CGX" ) ;
}

/****************************************************************/
/*	archiver main						*/
/****************************************************************/

archive( wp,dat )
FILE	*wp ;
char	*dat ;
{
	short	*pnt ;
	int	num,len,code,nn,i,j ;

	num    = *dat++ - 'A' ;
	len    = pat[num].size ;
	pnt    = pat[num].data ;
	code   = getnum( dat ) ;
	total += len ;

	for ( i=0 ; i<len ; i++ ) {
		nn = code + *pnt++ ;
		fwrite( chrbuf[nn],sizeof(char),16,wp ) ;
		for ( j=16 ; j<32 ; j+=2 ) {
			fputc( chrbuf[nn][j],wp ) ;
		}
	}
}

/*******  get number from table file  *****************************/

getnum( dat )
char	*dat ;
{
	int	cc,nn = 0 ;

	while ( *dat == ' ' || *dat == '\t' ) dat++ ;

	while ( isxdigit(*dat) ) {
		cc  = *dat++ ;
		nn *= 16 ;

		if      ( cc <= '9' )  nn += cc - '0' ;
		else if ( cc <= 'F' )  nn += cc - 'A' + 10 ;
		else		       nn += cc - 'a' + 10 ;
	}
	return ( nn ) ;
}

/****************************************************************/
/*	read character data					*/
/****************************************************************/

readchar( fname )
char	*fname ;
{
	FILE	*fp ;

	if ( chrcnt < DSIZE ) {
		if (( fp = fopen( fname,"r" )) == NULL ) {
			perror( fname ) ;
			return ( ERROR ) ;
		}
		printf( "read file \"%s\"\n", fname ) ;
		if ( fread( chrbuf[chrcnt],sizeof(char),FSIZE,fp ) != FSIZE ) {
			printf( "%s: not character file\n",fname ) ;
			fclose( fp ) ;
			return ( ERROR ) ;
		}
		fclose( fp ) ;
		chrcnt += 1024 ;
	}
	return ( COMPLETE ) ;
}
