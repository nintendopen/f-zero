;************************************************************************
;*	 CAR_CTRL	   -- car control module --			*
;************************************************************************
;
		INCLUDE	  RP5A22:
		INCLUDE	  RP5C77:
		INCLUDE	  VARIABLE
		INCLUDE	  BUFFER
;
;=============== Cross reference =======================================
;
		EXT	  Enemy_jump,Wall_damage,Out_damage,Bomb_damage
		EXT	  Set_crater,Display_shield,Calculate_score
		EXT	  Set_rank_window,Goal_in_rank,Disp_rank
		EXT	  Turbo_data,Brake_data
		EXT	  Slip_vector,Slip_vecspd1,Slip_vecspd2,Grip_vecspd
		EXT	  Maximum_speed,Slip_speed,Grip_limit
		EXT	  Friction_data,Reduce_data,Damage_time
		EXT	  Damage_speed,Power_down_sens,Copy_time_rank
		EXT	  Shadow_window_1,Shadow_window_2,Shadow_window_3
;
;=============== Cross Definition ======================================
;
		GLB	  Car_move,Camera_move,Enemy_bomb_ctrl
		GLB	  Set_scrl_count,Reset_location,Set_scrl_reg
		GLB	  Set_disp_pos,You_lost
;
;=============== Define local variable =================================
;
work0		EQU	  0000H		      ; 2 byte	:Work
max_speed	EQU	  0002H		      ; 2 byte	:Car max speed
angle_velocity	EQU	  0004H		      ; 2 byte	:Car angle velocity
move_speed_x	EQU	  0006H		      ; 4 byte	:Car move speed x
move_speed_y	EQU	  000AH		      ; 4 byte	:Car move speed y
move_speed	EQU	  000EH		      ; 2 byte	:Car mode speed
move_angle	EQU	  0010H		      ; 2 byte	:Car move angle
disp_angle	EQU	  0012H		      ; 2 byte	:Car display angle
gravity		EQU	  0014H		      ; 2 byte	:Gravity
accele_flag	EQU	  0016H		      ; 1 byte	:Car accele flag
slip_velocity	EQU	  0017H		      ; 2 byte	:Car slip velocity
fric_velocity	EQU	  0019H		      ; 2 byte	:Car friction velocity
redu_velocity	EQU	  001BH		      ; 2 byte	:Car Reduce velocity
drift_angle	EQU	  001DH		      ; 1 byte	:Car drift angle
damage_value	EQU	  001EH		      ; 1 byte	:Car damage value
;-----------------------------------------------------------------------
magnet_speed_x	EQU	  0020H		      ; 4 byte	:Magnet speed x
magnet_speed_y	EQU	  0024H		      ; 4 byte	:Magnet speed y
hight_level	EQU	  0028H		      ; 1 byte	:car hight
ground_level	EQU	  0029H		      ; 2 byte	:ground hight
crater_index	EQU	  002DH		      ; 1 byte	:crater index
spin_brake	EQU	  002EH		      ; 2 byte	:Car spin brake
;-----------------------------------------------------------------------
;
;=======================================================================
;
		PROG
		EXTEND
;
;************************************************************************
;*		 Car move entry						*
;************************************************************************
;
		MEM8
		IDX8
Car_move
		PHP
		SEP	  #00110000B	      ; memory,Index 8 bit mode
;
;=============== Initial temprary work =================================
Car_move100
		LDA	  #0FFH
		STA	  <crater_index
		LDA	  #04H		      ; Acc = friction of enemy
		STA	  <fric_velocity+0    ; Set friction velocity low
		STZ	  <fric_velocity+1    ; Set friction velocity high
		LDA	  #18H		      ; Acc = reduce of enemy
		STA	  <redu_velocity+0    ; Set reduce velocity low
		STZ	  <redu_velocity+1    ; Set reduce velocity high
;;;;		LDA	  #18H
		STA	  <gravity+0	      ; Set gravity low
		STZ	  <gravity+1	      ; Set gravity high
;
;=============== Car move main =========================================
Car_move200
		LDX	  #0AH
Car_move210	LDA	  !car_flag,X	      ; car work avirable ?
		BPL	  Car_move230	      ; no.
		BIT	  #00010000B	      ; Enemy jump exception ?
		BEQ	  Car_move220	      ; no.
;-----------------------------------------------------------------------
		JSR	  Jump_exception      ; Enemy jump exception
		BRA	  Car_move230
;-----------------------------------------------------------------------
Car_move220	JSR	  Set_drift_angle     ; Set drift angle
		JSR	  Rebound_control     ; Rebound control
		JSR	  Dash_control	      ; Hyper dash control
		JSR	  Check_max_speed     ; Check max speed
		JSR	  Check_slip	      ; Check slip
		JSR	  Check_spin	      ; Check spin
;-----------------------------------------------------------------------
		JSR	  Bomb_control	      ; Bomb control routines
		JSR	  Hight_control	      ; Hight control routines
		JSR	  Speed_control	      ; Speed control routines
		JSR	  Spin_control	      ; Spin control routines
		JSR	  Angle_control	      ; Angle control routines
		JSR	  Drift_control	      ; Drift control routines
		JSR	  Set_vector	      ; Set move_speed
		JSR	  Magnet_bar	      ; magnet bar
		JSR	  Check_vector	      ; Check vector max
		JSR	  Set_location	      ; Set car location
		JSR	  Check_line	      ; Check start line
;-----------------------------------------------------------------------
		LDA	  !car_unctrl_flag,X  ; car uncontrol ?
		BEQ	  Car_move230	      ; no.
		DEC	  !car_unctrl_flag,X  ; decrement counter
;-----------------------------------------------------------------------
Car_move230	DEX
		DEX
		BPL	  Car_move210
;
;=============== Other process =========================================
Car_move300
		JSR	  Camera_chase	      ; Camera chase ( free mode goal in )
		JSR	  Set_scrl_count      ; Set scroll counter
		JSR	  Set_scrn_angle      ; Set screen angle
		LDX	  <crater_index	      ; crater write ?
		BMI	  Car_move310	      ; no.
		JSR	  Set_crater	      ; Write crater
;-----------------------------------------------------------------------
Car_move310	PLP
		RTS
;
;************************************************************************
;*		 Check max speed					*
;************************************************************************
;
		MEM8
		IDX8
Check_max_speed
		LDA	  !car_unctrl_flag,X  ; car uncontrol ?
		BEQ	  Check_maxspd100     ; no.
;-----------------------------------------------------------------------
		LDA	  !car_control,X
		AND	  #01011100B
		STA	  !car_control,X      ; Reset accele and handle
;
;=============== Set friction and reduce velocity ======================
Check_maxspd100
		CPX	  #00H		      ; My car ?
		BNE	  Check_maxspd110     ; no.
;-----------------------------------------------------------------------
		LDY	  <mycar_number
		LDA	  !Friction_data,Y    ; Acc = friction of my car
		STA	  <fric_velocity+0    ; Set friction velocity low
		LDA	  !Reduce_data,Y      ; Acc = reduce of my car
		STA	  <redu_velocity+0    ; Set reduce velocity low
;-----------------------------------------------------------------------
		LDA	  !car_control
		AND	  #00001100B
		LSR	  A
		LSR	  A
		TAY
		LDA	  !Mycar_gravity,Y
		STA	  <gravity+0	      ; Set gravity low
;-----------------------------------------------------------------------
Check_maxspd110 STZ	  <max_speed+0	      ; Set max speed low
		LDA	  !car_control,X
		ASL	  A		      ; super dash ?
		BMI	  Check_maxspd400     ; yes.
		LDA	  !control_status,X   ; Jumpping ?
		BMI	  Check_maxspd250     ; yes.
		LDA	  !road_status_1,X    ; dirt zone ?
		BMI	  Check_maxspd300     ; yes.
		CPX	  #00H		      ; My car ?
		BNE	  Check_maxspd250     ; no.
;-----------------------------------------------------------------------
		LDA	  !control_status
		BIT	  #01000000B	      ; spin now ?
		BNE	  Check_maxspd500     ; yes.
		BIT	  #00001000B	      ; after burner on ?
		BNE	  Check_maxspd250     ; yes.
;
;=============== Set normal max speed ==================================
Check_maxspd200
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDY	  <mycar_pointer
		LDA	  <mycar_damage
		CMP	  #0200H	      ; mycar_damage >= 200H ?
		BCC	  Check_maxspd210     ; no.
		LDA	  !Maximum_speed,Y
		BRA	  Check_maxspd220
;-----------------------------------------------------------------------
Check_maxspd210 LDA	  #0200H
		SEC
		SBC	  <mycar_damage
		LSR	  A
		LSR	  A
		LSR	  A
		TAX
		STX	  !Multiplicand
		LDX	  !Power_down_sens,Y
		STX	  !Multiplier
		NOP
		SEC			      ; 2 cycle
		LDA	  !Maximum_speed,Y    ; 5 cycle
		SBC	  !Multiply
		LDX	  #00H
;-----------------------------------------------------------------------
Check_maxspd220 STA	  <max_speed
		SEP	  #00100000B	      ; Memory 8 bit mode
		MEM8
		RTS
;
;=============== Set jump or enemy max speed ===========================
Check_maxspd250
		LDA	  #09H
		STA	  <max_speed+1	      ; Set jump speed high
		RTS
;
;=============== Set dirt zone max speed ===============================
Check_maxspd300
		CPX	  #00H		      ; My car ?
		BNE	  Check_maxspd310     ; no.
		LDA	  #00001000B
		BIT	  !control_status     ; after burner on ?
		BNE	  Check_maxspd320     ; yes.
;-----------------------------------------------------------------------
Check_maxspd310 LDA	  #03H
		BRA	  Check_maxspd330
Check_maxspd320 LDA	  #06H
Check_maxspd330 STA	  <max_speed+1	      ; Set dirt zone speed high
		RTS
;
;=============== Set super dash max speed ==============================
Check_maxspd400
		LDA	  #0EH
		STA	  <max_speed+1	      ; Set super dash speed high
		RTS
;
;=============== Set spin max speed ====================================
Check_maxspd500
		LDA	  #07H
		STA	  <max_speed+1	      ; Set jump speed high
		LDA	  #80H
		STA	  <max_speed+0
		RTS
;
;=======================================================================
Mycar_gravity	BYTE	  20H,18H,28H,28H
;
;************************************************************************
;*		 Check slip performance					*
;************************************************************************
;
		MEM8
		IDX8
;
Check_slip
		CPX	  #00H		      ; My car ?
		BEQ	  Check_slip000
		JMP	  Check_slip500	      ; no. ( enemy velocity )
;
;=============== Set sound flag ========================================
Check_slip000
		LDY	  <mycar_number
		LDA	  <drift_angle	      ; drift_angle == 0 ?
		BEQ	  Check_slip110	      ; yes.
		LDA	  !car_speed+1
		CMP	  #05H		      ; my car speed >= 500H ?
		BCC	  Check_slip100	      ; no.
;-----------------------------------------------------------------------
		LDA	  <drift_angle
		CMP	  !Slip_vector,Y      ; slip ?
		BCS	  Check_slip010	      ; yes.
		LDA	  #00100000B
		BRA	  Check_slip020
;-----------------------------------------------------------------------
Check_slip010	LDA	  #01000000B
Check_slip020	TSB	  !wind_priority      ; Set SOUND
;
;=============== Check turn slip =======================================
Check_slip100
		LDA	  <drift_angle
		CMP	  #40		      ; drift_angle >= 40 ?
		BCS	  Check_slip700	      ; yes. ( slip limit )
		BRA	  Check_slip120
;-----------------------------------------------------------------------
Check_slip110	STZ	  <drift_status	      ; Clear drift status
Check_slip120	CMP	  !Slip_vector,Y      ; slip ?
		BCS	  Check_slip130	      ; yes.
		LDA	  !car_control	      ; Accele on ?
		BPL	  Check_slip600	      ; no. ( non slip )
;-----------------------------------------------------------------------
Check_slip130	LDA	  <drift_status	      ; restore slip ?
		LSR	  A
		BCS	  Check_slip700	      ; yes. ( restore slip )
;-----------------------------------------------------------------------
		LDA	  !control_status     ; Jumpping ?
		BMI	  Check_slip350	      ; yes.
		LDA	  <road_status_2      ; Slip zone ?
		BPL	  Check_slip200	      ; no.
		LDA	  <drift_angle
		CMP	  #08H
		BCS	  Check_slip300
;
;=============== Set slip velocity =====================================
Check_slip200
		TYA
		ASL	  A
		TAY
		LDA	  !car_speed+1
		CMP	  !Slip_speed+1,Y     ; car_speed >= slip_start_speed ?
		BCC	  Check_slip500	      ; no. ( non slip )
		BNE	  Check_slip210
		LDA	  !car_speed+0
		CMP	  !Slip_speed+0,Y     ; car_speed >= slip_start_speed ?
		BCC	  Check_slip500	      ; no. ( non slip )
;-----------------------------------------------------------------------
Check_slip210	LDA	  <drift_angle
		LDY	  <mycar_number
		CMP	  !Slip_vector,Y      ; slip ?
		BCS	  Check_slip230	      ; yes.
Check_slip220
		LDA	  !Slip_vecspd1,Y     ; Acc = 1/8 slip velocity
		BRA	  Check_slip240
Check_slip230
		LDA	  !Slip_vecspd2,Y
Check_slip240
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		AND	  #00FFH
		ASL	  A
		ASL	  A
		ASL	  A		      ; Acc *= 8
		STA	  <slip_velocity      ; Set slip velocity
		SEP	  #00100000B	      ; Memory 8 bit mode
		MEM8
		RTS
;
;=============== Set slip velocity ( slip zone ) =======================
Check_slip300
		LDA	  #020H
		STA	  <slip_velocity+0    ; Set slip velocity high ( slip zone )
		STZ	  <slip_velocity+1    ; Set slip velocity low
		RTS
;
;=============== Set slip velocity ( jumpping ) ========================
Check_slip350
		LDA	  #0A0H
		STA	  <slip_velocity+0    ; Set slip velocity high ( slip zone )
		STZ	  <slip_velocity+1    ; Set slip velocity low
		RTS
;
;=============== Set slip velocity ( non slip ) ========================
Check_slip500
		LDA	  #02H
		STA	  <slip_velocity+1    ; Set slip velocity high
		LDA	  #80H
		STA	  <slip_velocity+0    ; Set slip velocity low
		RTS
;
;=============== Set slip velocity ( accele off ) ======================
Check_slip600
		LDA	  #01H
		STA	  <slip_velocity+1    ; Set slip velocity high
		LDA	  #80H
		STA	  <slip_velocity+0    ; Set slip velocity low
		RTS
;
;=============== Set slip velocity ( restore slip ) ====================
Check_slip700
		LDY	  <mycar_number
		LDA	  !Grip_vecspd,Y
		STZ	  <slip_velocity+0    ; Set slip velocity low
		STA	  <slip_velocity+1    ; Set slip velocity high
		RTS
;
;************************************************************************
;*		 Check spin						*
;************************************************************************
;
		MEM8
		IDX8
Check_spin
		LDA	  !control_status,X
		ASL	  A		      ; Car spin now ?
		BPL	  Check_spin310	      ; no.  ( no ececute )
;
;=============== Check end of spin =====================================
Check_spin100
		LDA	  !car_speed+1,X      ; Car speed < 1 dot ?
		BEQ	  Check_spin300	      ; yes. ( end of spin )
		LDA	  !car_spin_velo+1,X  ; Car spin velocity high == 0 ?
		BNE	  Check_spin200
		LDA	  !car_spin_velo+0,X  ; Car spin velocity == 0 ?
		BEQ	  Check_spin300	      ; yes. ( end of spin )
		BPL	  Check_spin400	      ; if ( velocity < 80H )
;
;=============== spin pre-process ======================================
Check_spin200
		STZ	  <spin_brake+1	      ; Set spin brake
		STZ	  <slip_velocity+0
		STZ	  <slip_velocity+1    ; Set slip velocity
;-----------------------------------------------------------------------
		LDA	  #03H
		CPX	  #00H		      ; my car ?
		BNE	  Check_spin245	      ; no.
		LDA	  #10000000B
		TSB	  !wind_priority      ; Set slip sound
;-----------------------------------------------------------------------
Check_spin205	LDA	  #02H
		STA	  <spin_brake+0
		LDA	  !car_spin_velo+1,X  ; Spin left ?
		BPL	  Check_spin220	      ; no.
;------------------------------------------------------- Left spin -----
Check_spin210	LDA	  #00000001B
		BIT	  !car_control,X      ; Right turn ?
		BEQ	  Check_spin250	      ; no.
		BRA	  Check_spin240
;------------------------------------------------------- Right spin ----
Check_spin220	LDA	  #00000010B
		BIT	  !car_control,X      ; Left turn ?
		BEQ	  Check_spin250	      ; no.
		BRA	  Check_spin240
;-----------------------------------------------------------------------
Check_spin240	LDA	  #08H
Check_spin245	STA	  <spin_brake+0	      ; Set spin brake
;-----------------------------------------------------------------------
Check_spin250	LDA	  !car_control,X      ; Reset control status
		AND	  #01111100B
		STA	  !car_control,X      ; Reset control status
		RTS
;
;=============== no spin pre-process ===================================
Check_spin300
		LDA	  !control_status,X
		AND	  #10111111B
		STA	  !control_status,X
;-----------------------------------------------------------------------
Check_spin310	LDA	  #20H
		STA	  <spin_brake+0
		STZ	  <spin_brake+1
		RTS
;
;=======================================================================
Check_spin400
		STZ	  <spin_brake+1	      ; Set spin brake
		STZ	  <slip_velocity+0
		STZ	  <slip_velocity+1    ; Set slip velocity
		LDA	  #02H
		STA	  <spin_brake+0
		RTS
;
;************************************************************************
;*		 Set drift angle					*
;************************************************************************
;
		MEM8
		IDX8
Set_drift_angle
		LDA	  !car_angle+1,X
		SEC
		SBC	  !car_scrl_angle+1,X
		BCS	  Set_drift_ang10
		ADC	  #192
Set_drift_ang10
		CMP	  #96
		BCC	  Set_drift_ang20
		EOR	  #0FFH
		ADC	  #192		      ; ( CY = 1 )
Set_drift_ang20
		STA	  <drift_angle
		RTS
;
;************************************************************************
;*		 Speed control						*
;************************************************************************
;
		MEM8
		IDX8
Speed_control
		LDA	  !car_BG_flag,X
		AND	  #11100000B	      ; Out of course ?
		BNE	  Speed_ctrl100	      ; yes.
;-----------------------------------------------------------------------
		LDA	  !car_control,X
		STA	  <accele_flag
		ASL	  A		      ; Accele on ?
		BCS	  Speed_ctrl300	      ; yes.
		ASL	  A		      ; Hyper dash ?
		BCS	  Speed_ctrl300	      ; yes.
		ASL	  A		      ; brake on ?
		BCS	  Speed_ctrl500	      ; yes.
		ASL	  A		      ; friction ?
		BCC	  Speed_ctrl200	      ; yes.
		JMP	  Speed_ctrl600	      ; car stop exception
;
;=============== Speed hold ( out of course ) ==========================
Speed_ctrl100
		MEM16
		REP	  #00100000B	      ; memory 16 bit mode
		LDA	  <max_speed
		CMP	  !car_speed,X	      ; car_speed > max_speed ?
		BCC	  Speed_ctrl400	      ; yes. ( reduce )
;-----------------------------------------------------------------------
		LDA	  !car_speed,X	      ; speed hold
		JMP	  Speed_ctrl900
;
;=============== friction ( accele off ) ===============================
Speed_ctrl200
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  <max_speed
		CMP	  !car_speed,X	      ; car_speed > max_speed ?
		BCC	  Speed_ctrl400	      ; yes. ( reduce )
;-----------------------------------------------------------------------
Speed_ctrl210	LDA	  !car_speed,X
		SEC
		SBC	  <fric_velocity      ; Subtruct friction
		BCS	  Speed_ctrl230
Speed_ctrl220	LDA	  #0000H	      ; if ( speed < 0 ) speed = 0
Speed_ctrl230	JMP	  Speed_ctrl900
;
;=============== Accele on =============================================
Speed_ctrl300
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  <max_speed
		CMP	  !car_speed,X
		BCC	  Speed_ctrl400	      ; if ( car_speed	> max_speed )
;-----------------------------------------------------------------------
		JSR	  Accele_control
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		CLC
		ADC	  !car_speed_small,X
		STA	  !car_speed_small,X
		TYA
		XBA
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		ADC	  !car_speed,X
		BPL	  Speed_ctrl900
;-----------------------------------------------------------------------
		LDA	  !car_angle,X
		STA	  !car_scrl_angle,X
		STZ	  !car_speed_small,X
		LDA	  #0000H
		BRA	  Speed_ctrl900
;
;=============== Reduce ( car_speed > max_speed ) ======================
Speed_ctrl400
		MEM16
		LDA	  !car_speed,X
		SEC
		SBC	  <redu_velocity
		BCS	  Speed_ctrl900
		LDA	  #0000H
		BRA	  Speed_ctrl900
;
;=============== brake on ==============================================
Speed_ctrl500
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !car_speed,X
		ASL	  A		      ; ( CY=0 )
		XBA
		TXY
		TAX
		LDA	  >Brake_data,X
		TYX
		AND	  #00FFH
		STA	  <work0
;-----------------------------------------------------------------------
		LDA	  !car_speed,X
		SEC
		SBC	  <work0	      ; Subtruct brake
		BCS	  Speed_ctrl900
		LDA	  #0000H
		BRA	  Speed_ctrl900
;
;=============== stop ==================================================
Speed_ctrl600
		MEM16
		REP	  #00100000B	      ; memory 16 bit mode
		LDA	  !car_speed,X
		SEC
		SBC	  #0020H	      ; Subtruct brake
		BCS	  Speed_ctrl610
		LDA	  #0000H
;-----------------------------------------------------------------------
Speed_ctrl610	CPX	  #00H		      ; my car ?
		BNE	  Speed_ctrl900	      ; no.
		LDY	  <exception_flag     ; exception ?
		BEQ	  Speed_ctrl900	      ; no.
;-----------------------------------------------------------------------
		STA	  !car_speed,X	      ; Set car_speed
		STA	  <move_speed	      ; Set move speed
		LDA	  #0000H	      ; Acc = angle velocity
		BRA	  Speed_ctrl990
;
;=============== Set angle velocity ====================================
Speed_ctrl900
		MEM16
		STA	  !car_speed,X
		STA	  <move_speed
		CMP	  #0000H
		BEQ	  Speed_ctrl990
;-----------------------------------------------------------------------
		CPX	  #00H
		BNE	  Speed_ctrl980
		LDA	  !control_status
		AND	  #00C0H
		BNE	  Speed_ctrl980
		LDY	  !road_status_1
		BPL	  Speed_ctrl980
;-----------------------------------------------------------------------
		LDA	  !car_handle,X
		LSR	  A
		BRA	  Speed_ctrl990
Speed_ctrl980
		LDA	  !car_handle,X
Speed_ctrl990
		STA	  <angle_velocity
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Accele control						*
;************************************************************************
;
		MEM16
		IDX8
Accele_control
		CPX	  #00H		      ; My car ?
		BNE	  Accele_ctrl250      ; no.
		LDY	  !control_status+0   ; My car jumpping ?
		BPL	  Accele_ctrl200      ; no.
;
;=============== My car jump accele ====================================
Accele_ctrl100
		LDY	  <jump_count
		CPY	  #04H		      ; jump_count < 04H ?
		BCS	  Accele_ctrl120      ; no.
;-----------------------------------------------------------------------
Accele_ctrl110	INY
		STY	  <jump_count	      ; Count up jump_count
		LDA	  #0000H
		TAY			      ; Accele = 0
		RTS
;-----------------------------------------------------------------------
Accele_ctrl120	CPY	  #14H		      ; jump_count < 14H ?
		BCS	  Accele_ctrl300      ; no.
		INY
		STY	  <jump_count	      ; Count up jump_count
		LDA	  #0000H	      ; Set dummy speed
		BRA	  Accele_ctrl310      ; ( CY=0 )
;
;=============== Check my car super dash & handle ======================
Accele_ctrl200
		LDA	  <accele_flag-1
		ASL	  A		      ; Super dash ?
		BMI	  Accele_ctrl290      ; yes.
		LDA	  !control_status
		AND	  #0000000000001000B  ; After burner ?
		BNE	  Accele_ctrl290      ; yes.
;-----------------------------------------------------------------------
		LDA	  !car_control+0
		AND	  #0000000000000011B  ; Handle on ?
		BEQ	  Accele_ctrl300      ; no.
		LDA	  !car_speed+0
		CMP	  #0600H	      ; Speed >= 6 dot ?
		BCC	  Accele_ctrl300      ; no.
		LDY	  <drift_angle
		CPY	  #05H
		BCC	  Accele_ctrl300
;-----------------------------------------------------------------------
		LDA	  #0FF00H
		LDY	  #0FFH		      ; Accele = FFFF00H
		RTS
;
;=============== Check enemy super dash ================================
Accele_ctrl250
		LDA	  <accele_flag-1
		ASL	  A		      ; Super dash ?
		BPL	  Accele_ctrl300      ; no.
;-----------------------------------------------------------------------
Accele_ctrl290	LDA	  #0000H
		LDY	  #01H		      ; Accele = 010000H
		RTS
;
;=============== Set accele value ======================================
Accele_ctrl300
		LDA	  !car_speed,X
		ASL	  A		      ; ( CY=0 )
		XBA
;--------------------------------------------------- Set accele value --
		MEM8
Accele_ctrl310	SEP	  #00100000B	      ; Memory 8 bit mode
		ADC	  !accele_perform,X
		TXY
		TAX
		LDA	  >Turbo_data,X
		STA	  !Multiplicand
;-------------------------------------------------- Check drift angle --
		LDX	  <drift_angle
		LDA	  >COS_sign,X
		BEQ	  Accele_ctrl330
;--------------------------------------------- Set accele ( COS < 0 ) --
Accele_ctrl320	LDA	  #16
		STA	  !Multiplicand
		JSR	  Accele_ctrl330
		MEM16
		EOR	  #0FFFFH
		INC	  A		      ; Negate A
		DEY			      ; sign = minus
		RTS
;-------------------------------------------- Set accele ( COS >= 0 ) --
		MEM8
Accele_ctrl330	LDA	  >COS_absolute,X    ; Acc = COS( acc_angle )
		STA	  !Multiplier
		NOP
		NOP
		TYX
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !Multiply
		LSR	  A
		LDY	  #00H		      ; sign = plus
		RTS
;
;************************************************************************
;*		 Angle control						*
;************************************************************************
;
		MEM8
		IDX8
Angle_control
		LDA	  !car_control,X
		LSR	  A
		BCS	  Angle_ctrl200	      ; right turn
		LSR	  A
		BCS	  Angle_ctrl100	      ; left turn
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; memory 16 bit mode
		LDA	  !car_angle,X
		BRA	  Angle_ctrl300
;
;=============== Turn left =============================================
Angle_ctrl100
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !car_angle,X
		SEC
		SBC	  <angle_velocity
		BCS	  Angle_ctrl300
		ADC	  #192*256
		BRA	  Angle_ctrl300
;
;=============== Turn right ============================================
Angle_ctrl200
		MEM16
		REP	  #00100000B	      ; Memory 16bit mode
		LDA	  !car_angle,X
		CLC
		ADC	  <angle_velocity
		CMP	  #192*256
		BCC	  Angle_ctrl300
		SBC	  #192*256
;
;=============== Set screen,scroll angle ===============================
Angle_ctrl300
		MEM16
		STA	  !car_angle,X	      ; Set car_angle
		STA	  <disp_angle
		SEP	  #00100000B	      ; memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Spin control						*
;************************************************************************
;
		MEM16
		IDX8
Spin_control
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !car_spin_velo,X
		BEQ	  Spin_ctrl300
		BMI	  Spin_ctrl200
;
;=============== Spin right ============================================
Spin_ctrl100
		CLC
		ADC	  !car_angle,X
		CMP	  #192*256
		BCC	  Spin_ctrl110
		SBC	  #192*256
Spin_ctrl110
		STA	  !car_angle,X
;-----------------------------------------------------------------------
		LDA	  !car_spin_velo,X
		SEC
		SBC	  <spin_brake
		BCS	  Spin_ctrl120
		LDA	  #0000H
Spin_ctrl120
		STA	  !car_spin_velo,X
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; memory 8 bit mode
		RTS
;
;=============== Spin left =============================================
Spin_ctrl200
		AND	  #07FFFH
		STA	  <work0
		LDA	  !car_angle,X
		SEC
		SBC	  <work0
		BCS	  Spin_ctrl210
		ADC	  #192*256
Spin_ctrl210
		STA	  !car_angle,X
;-----------------------------------------------------------------------
		LDA	  <work0
		SEC
		SBC	  <spin_brake
		ORA	  #8000H
		BCS	  Spin_ctrl220
		LDA	  #0000H
Spin_ctrl220
		STA	  !car_spin_velo,X
;-----------------------------------------------------------------------
Spin_ctrl300
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Drift control						*
;************************************************************************
;
		MEM16
		IDX8
Drift_control
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !car_scrl_angle,X
		STA	  <move_angle	      ; move_angle = scroll_angle
;-----------------------------------------------------------------------
		LDA	  <move_speed	      ; move_speed == 0 ?
		BEQ	  Drift_ctrl400	      ; yes. ( reset drift )
		LDA	  !car_control,X
		AND	  #11000011B	      ; car accele ?
		BEQ	  Drift_ctrl510	      ; no. ( no restore )
		LDA	  !car_unctrl_flag,X  ; car uncontrol ?
		BNE	  Drift_ctrl510	      ; yes.
;-----------------------------------------------------------------------
		JSR	  Drift_ctrl900	      ; Check drift
		BCS	  Drift_ctrl300	      ; if ( drift left )
;
;=============== Drift right ===========================================
Drift_ctrl200
		LDA	  <move_angle
		CLC
		ADC	  <slip_velocity
		CMP	  #192*256
		BCC	  Drift_ctrl210
		SBC	  #192*256
;-----------------------------------------------------------------------
Drift_ctrl210	STA	  <move_angle
		JSR	  Drift_ctrl900	      ; Check drift
		BCC	  Drift_ctrl500	      ; if ( drift right )
		BRA	  Drift_ctrl400	      ; if ( drift left	 ) clear
;
;=============== Drift left ============================================
Drift_ctrl300
		LDA	  <move_angle
		SEC
		SBC	  <slip_velocity
		BCS	  Drift_ctrl310
		ADC	  #192*256
;-----------------------------------------------------------------------
Drift_ctrl310	STA	  <move_angle
		JSR	  Drift_ctrl900	      ; Check drift
		BCS	  Drift_ctrl500	      ; if ( drift left	 )
;
;=============== Clear drift ===========================================
Drift_ctrl400
		LDA	  <disp_angle
		STA	  <move_angle
;
;=============== Set scroll angle ======================================
Drift_ctrl500
		LDA	  <move_angle
		STA	  !car_scrl_angle,X
Drift_ctrl510	SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Check drift angle ( left or right ) ===================
Drift_ctrl900
		LDA	  <disp_angle
		SEC
		SBC	  <move_angle
		BCS	  Drift_ctrl910
		ADC	  #192*256	      ; RETURN
Drift_ctrl910	CMP	  #96*256	      ;	 CY=0 : Right side
		RTS			      ;	 CY=1 : Left side
;
;************************************************************************
;*		 Set speed vector					*
;************************************************************************
;
		MEM8
		IDX8
Set_vector
		PHX
		LDX	  <move_angle+1	      ; IX = move angle
;
;=============== Set car speed x =======================================
Set_vector100
		LDA	  >SIN_absolute,X    ; Acc = SIN( acc_angle )
		STA	  !Multiplicand
		LDA	  <move_speed+0
		STA	  !Multiplier
		NOP
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		STZ	  <move_speed_x+2
		LDA	  !Multiply
		STA	  <move_speed_x+0
;-----------------------------------------------------------------------
		LDY	  <move_speed+1
		STY	  !Multiplier
		NOP
		NOP
		LDA	  <move_speed_x+1
		CLC
		ADC	  !Multiply
		STA	  <move_speed_x+1
;-----------------------------------------------------------------------
		ASL	  <move_speed_x+0
		ROL	  <move_speed_x+2
;-----------------------------------------------------------------------
		LDA	  >SIN_sign,X
		AND	  #00FFH
		BEQ	  Set_vector200
;-----------------------------------------------------------------------
		SEC
		LDA	  #0000H
		SBC	  <move_speed_x+0
		STA	  <move_speed_x+0
		LDA	  #0000H
		SBC	  <move_speed_x+2
		STA	  <move_speed_x+2
;
;=============== Set car speed y =======================================
Set_vector200
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  >COS_absolute,X    ; Acc = COS( acc_angle )
		STA	  !Multiplicand
		LDA	  <move_speed+0
		STA	  !Multiplier
		NOP
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		STZ	  <move_speed_y+2
		LDA	  !Multiply
		STA	  <move_speed_y+0
;-----------------------------------------------------------------------
		LDY	  <move_speed+1
		STY	  !Multiplier
		NOP
		NOP
		LDA	  <move_speed_y+1
		CLC
		ADC	  !Multiply
		STA	  <move_speed_y+1
;-----------------------------------------------------------------------
		ASL	  <move_speed_y+0
		ROL	  <move_speed_y+2
;-----------------------------------------------------------------------
		LDA	  >COS_sign,X
		AND	  #00FFH
		BNE	  Set_vector300
;-----------------------------------------------------------------------
		SEC
		LDA	  #0000H
		SBC	  <move_speed_y+0
		STA	  <move_speed_y+0
		LDA	  #0000H
		SBC	  <move_speed_y+2
		STA	  <move_speed_y+2
;-----------------------------------------------------------------------
Set_vector300
		PLX
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Magnet bar						*
;************************************************************************
;
		MEM8
		IDX8
Magnet_bar
		CPX	  #00H		      ; my car ?
		BNE	  Magnet_bar10	      ; no.
		LDA	  <exception_flag     ; exception ?
		BNE	  Magnet_bar10	      ; yes.
		LDA	  !car_BG_flag+0      ; hit wall ?
		BNE	  Magnet_bar10	      ; yes.
		LDA	  !control_status
		AND	  #00100000B	      ; Super dash ?
		BEQ	  Magnet_bar100	      ; no.
Magnet_bar10	RTS
;
;=============== Check road status =====================================
Magnet_bar100
		LDA	  <road_status_2
		AND	  #01100000B	      ; magnet zone ?
		BEQ	  Magnet_bar10	      ; no.
;-----------------------------------------------------------------------
		PHX
		AND	  #01000000B	      ; magnet right ?
		BEQ	  Magnet_left	      ; no.
;
;=============== Magnet right ==========================================
Magnet_right
		LDA	  <road_angle
		CLC
		ADC	  #48+4
		CMP	  #192
		BCC	  Magnet_bar200
		SBC	  #192
		BRA	  Magnet_bar200
;
;=============== Magnet left ===========================================
Magnet_left
		LDA	  <road_angle
		SEC
		SBC	  #48-4
		BCS	  Magnet_bar200
		ADC	  #192
;
;=============== Set magnet speed ======================================
Magnet_bar200
		AND	  #11111000B
		LSR	  A
		LSR	  A
		TAX
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		STZ	  <magnet_speed_x+2
		LDA	  >Magnet_powx,X
		BPL	  Magnet_bar210
		DEC	  <magnet_speed_x+2
Magnet_bar210
		ASL	  A
		ASL	  A
		ROL	  <magnet_speed_x+2
		ASL	  A
		ROL	  <magnet_speed_x+2
		STA	  <magnet_speed_x+0
;-----------------------------------------------------------------------
		STZ	  <magnet_speed_y+2
		LDA	  >Magnet_powy,X
		BPL	  Magnet_bar220
		DEC	  <magnet_speed_y+2
Magnet_bar220
		ASL	  A
		ASL	  A
		ROL	  <magnet_speed_y+2
		ASL	  A
		ROL	  <magnet_speed_y+2
		STA	  <magnet_speed_y+0
;
;=============== Add magnet speed ======================================
Magnet_bar300
		LDA	  <move_speed_x+0
		ADC	  <magnet_speed_x+0
		STA	  <move_speed_x+0
		LDA	  <move_speed_x+2
		ADC	  <magnet_speed_x+2
		STA	  <move_speed_x+2
;-----------------------------------------------------------------------
		CLC
		LDA	  <move_speed_y+0
		ADC	  <magnet_speed_y+0
		STA	  <move_speed_y+0
		LDA	  <move_speed_y+2
		ADC	  <magnet_speed_y+2
		STA	  <move_speed_y+2
;-----------------------------------------------------------------------
		PLX
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;=======================================================================
Magnet_powx	EQU	  09EF92H
Magnet_powy	EQU	  09EFC2H
;
;************************************************************************
;*		 Check vector						*
;************************************************************************
;
		MEM16
		IDX8
Check_vector
		REP	  #00100000B	      ; Memory 16 bit mode
		CPX	  #00H		      ; My car ?
		BNE	  Check_vector300     ; no.
;-----------------------------------------------------------------------
		LDA	  !control_status
		AND	  #0000000000101000B  ; Super dash or after burner ?
		BNE	  Check_vector300     ; yes.
;
;=============== Check vector x ========================================
Check_vector100
		LDA	  <move_speed_x+2
		BMI	  Check_vector150
;-----------------------------------------------------------------------
Check_vector110
		CMP	  #0009H
		BCC	  Check_vector200
		LDA	  #0009H
		STA	  <move_speed_x+2
		STZ	  <move_speed_x+0
		BRA	  Check_vector200
;-----------------------------------------------------------------------
Check_vector150
		CMP	  #0FFF7H
		BCS	  Check_vector200
		LDA	  #0FFF7H
		STA	  <move_speed_x+2
		STZ	  <move_speed_x+0
;
;=============== Check vector y ========================================
Check_vector200
		LDA	  <move_speed_y+2
		BMI	  Check_vector250
;-----------------------------------------------------------------------
Check_vector210
		CMP	  #0009H
		BCC	  Check_vector300
		LDA	  #0009H
		STA	  <move_speed_y+2
		STZ	  <move_speed_y+0
		BRA	  Check_vector300
;-----------------------------------------------------------------------
Check_vector250
		CMP	  #0FFF7H
		BCS	  Check_vector300
		LDA	  #0FFF7H
		STA	  <move_speed_y+2
		STZ	  <move_speed_y+0
;
;=============== Set car speed x,y =====================================
Check_vector300
		LDA	  <move_speed_x+0
		STA	  !car_speedx_l,X
		LDA	  <move_speed_x+2
		STA	  !car_speedx_h,X
;
		LDA	  <move_speed_y+0
		STA	  !car_speedy_l,X
		LDA	  <move_speed_y+2
		STA	  !car_speedy_h,X
;
;=======================================================================
Check_vector400
		SEP	  #00100000B
		RTS
;
;************************************************************************
;*		 Set car location					*
;************************************************************************
;
		MEM16
		IDX8
Set_location
		REP	  #00100001B	      ; Memory 16 bit mode
		LDA	  !car_locatex_l,X
		ADC	  <move_speed_x+0
		STA	  !car_locatex_l,X
		LDA	  !car_locatex_h,X
		ADC	  <move_speed_x+2
		AND	  #1FFFH
		STA	  !car_locatex_h,X
;-----------------------------------------------------------------------
		CLC
		LDA	  !car_locatey_l,X
		ADC	  <move_speed_y+0
		STA	  !car_locatey_l,X
		LDA	  !car_locatey_h,X
		ADC	  <move_speed_y+2
		AND	  #0FFFH
		STA	  !car_locatey_h,X
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Reset car location					*
;************************************************************************
;
		MEM16
		IDX8
Reset_location
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		SEC
		LDA	  !car_locatex_l,X
		SBC	  !car_speedx_l,X
		STA	  !car_locatex_l,X
		LDA	  !car_locatex_h,X
		SBC	  !car_speedx_h,X
		AND	  #1FFFH
		STA	  !car_locatex_h,X
;-----------------------------------------------------------------------
		SEC
		LDA	  !car_locatey_l,X
		SBC	  !car_speedy_l,X
		STA	  !car_locatey_l,X
		LDA	  !car_locatey_h,X
		SBC	  !car_speedy_h,X
		AND	  #0FFFH
		STA	  !car_locatey_h,X
;-----------------------------------------------------------------------
		STZ	  !car_speedx_l,X
		STZ	  !car_speedx_h,X
		STZ	  !car_speedy_l,X
		STZ	  !car_speedy_h,X
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Set scroll counter					*
;************************************************************************
;
		MEM16
Set_scrl_count
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDA	  !car_locatex_l+0
		STA	  <scroll_count_x+0
;
		LDA	  !car_locatex_h+0
		SEC
		SBC	  #0200H
		AND	  #1FFFH
		STA	  <scroll_count_x+2
;-----------------------------------------------------------------------
		LDA	  !car_locatey_l+0
		STA	  <scroll_count_y+0
;
		LDA	  !car_locatey_h+0
		SEC
		SBC	  #0200H
		AND	  #0FFFH
		STA	  <scroll_count_y+2
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Set scroll & center register				*
;************************************************************************
;
		MEM16
Set_scrl_reg
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
		LDY	  <scroll_selector
;-----------------------------------------------------------------------
		LDA	  <scroll_count_x+2
		CLC
		ADC	  #0180H
		AND	  #03FFH
		STA	  !scroll_table_H2,Y
		CLC
		ADC	  #0080H
		STA	  <center_x	      ; Set rotation center X
;-----------------------------------------------------------------------
		LDA	  <scroll_count_y+2
		CLC
		ADC	  #0150H
		AND	  #03FFH
		STA	  !scroll_table_V2,Y
		CLC
		ADC	  #00B0H
		STA	  <center_y	      ; Set rotation center Y
;-----------------------------------------------------------------------
		PLP			      ; Restore mode
		RTS
;
;************************************************************************
;*		 Set screen angle					*
;************************************************************************
;
		MEM8
		IDX8
Set_scrn_angle
		LDA	  !car_angle+1
		BEQ	  Set_scrn_angle2
		LDA	  #192
		SEC
		SBC	  !car_angle+1
Set_scrn_angle2 STA	  <screen_angle	      ; Set screen_angle
		RTS
;
;************************************************************************
;*		 Hyper dash control entry				*
;************************************************************************
;
		MEM8
		IDX8
Dash_control
		LDA	  #00010000B
		BIT	  !control_status,X   ; Hyper dash accele ?
		BNE	  Dash_control200     ; yes. ( dash accele )
;-----------------------------------------------------------------------
		BIT	  !road_status_1,X    ; Hyper dash zone ?
		BEQ	  Dash_control020     ; no.
		SEC
		LDA	  !car_angle+1,X
		SBC	  <road_angle
		BCS	  Dash_control010
		ADC	  #192
Dash_control010 CMP	  #18H		      ; angle < 18H ?
		BCC	  Dash_control100     ; yes.
		CMP	  #0A9H		      ; angle > A9H ?
		BCS	  Dash_control100     ; yes.
;-----------------------------------------------------------------------
Dash_control020 LDA	  #00100000B
		BIT	  !control_status,X   ; Hyper dash brake ?
		BNE	  Dash_control300     ; yes. ( check end of dash )
		RTS
;
;=============== Start Hyper dash ======================================
Dash_control100
		LDA	  <road_angle
		STA	  !car_scrl_angle+1,X ; Set car scroll angle high
		STZ	  !car_scrl_angle+0,X ; Set car scroll angle low
;-----------------------------------------------------------------------
		LDA	  !control_status,X
		ORA	  #00110000B
		STA	  !control_status,X   ; Set hyper dash flag
;
;=============== Hyper dash accele =====================================
Dash_control200
		CPX	  #00H
		BNE	  Dash_control205
		LDA	  #10000000B
		TSB	  !wind_priority
;-----------------------------------------------------------------------
Dash_control205 LDA	  !car_speed+1,X
		CMP	  #0DH		      ; car_speed >= 0D00H ?
		BCS	  Dash_control220     ; yes.
;-----------------------------------------------------------------------
Dash_control210 LDA	  !car_control,X
		AND	  #00001111B
		ORA	  #11000000B
		STA	  !car_control,X      ; Hyper accele
		RTS
;-----------------------------------------------------------------------
Dash_control220 LDA	  !control_status,X
		AND	  #11101111B
		STA	  !control_status,X   ; Reset hyper accele flag
		LDA	  #0EH
		STA	  !car_speed+1,X
		STZ	  !car_speed+0,X
		STZ	  !car_speed_small,X  ; Set max speed
		BRA	  Dash_control320
;
;=============== Hyper dash brake ======================================
Dash_control300
		CPX	  #00H
		BNE	  Dash_control305
		LDA	  #10000000B
		TSB	  !wind_priority
;-----------------------------------------------------------------------
Dash_control305 LDA	  !car_speed+1,X
		CMP	  #09H		      ; car_speed >= 900H ?
		BCS	  Dash_control320     ; yes.
;-----------------------------------------------------------------------
Dash_control310 LDA	  !control_status,X
		AND	  #11011111B
		STA	  !control_status,X   ; Reset hyper dash flag
		RTS
;-----------------------------------------------------------------------
Dash_control320 LDA	  !car_control,X
		AND	  #00111111B
		STA	  !car_control,X      ; Reset accele
		RTS
;
;************************************************************************
;*		 Rebound control entry					*
;************************************************************************
;
		MEM8
		IDX8
Rebound_control
		LDA	  !control_status,X
		BPL	  Rebound_ctrl010
		LDA	  !car_BG_flag,X
		BRA	  Rebound_ctrl020
;-----------------------------------------------------------------------
Rebound_ctrl010 LDA	  !road_status_1,X
		AND	  #00001100B
		ORA	  !car_BG_flag,X
Rebound_ctrl020 STA	  <work0
;-----------------------------------------------------------------------
		CPX	  #00H		      ; My car ?
		BEQ	  Mycar_rebound	      ; yes.
;-----------------------------------------------------------------------
		AND	  #11111100B	      ; Hit laser wall ?
		BEQ	  Rebound_ctrl290     ; no.
;
;=============== Enemy car frashing ====================================
Rebound_ctrl100
		LDA	  !carcol_change,X
		CMP	  #05H		      ; enemy color change OK ?
		BCS	  Rebound_ctrl200     ; no.
		LDA	  #09H
		STA	  !carcol_change,X    ; Enemy color change
;
;=============== Check out of course ===================================
Rebound_ctrl200
		LDA	  <work0
		AND	  #00011100B	      ; On the laser wall ?
		BNE	  Rebound_ctrl210     ; yes.
;-----------------------------------------------------------------------
		LDA	  #04H
		STA	  !car_unctrl_flag+0,X	; Set uncontrol flag
		STZ	  !car_unctrl_flag+1,X
;-----------------------------------------------------------------------
		MEM16
Rebound_ctrl210 REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !car_speed,X
		CMP	  #0500H	      ; car_speed >= damage_speed ?
		BCC	  Rebound_ctrl220     ; no.
		SBC	  #0010H
		STA	  !car_speed,X	      ; Set car speed
;-----------------------------------------------------------------------
Rebound_ctrl220 SEP	  #00100000B	      ; Memory 8 bit mode
Rebound_ctrl290 RTS
;
;************************************************************************
;*		 Set mycar damage					*
;************************************************************************
;
		MEM8
		IDX8
Mycar_rebound
		LDY	  <exception_flag
		BNE	  Rebound_ctrl290
;-----------------------------------------------------------------------
		AND	  #11111100B	      ; Hit laser wall ?
		BEQ	  Rebound_ctrl290     ; no.
		BIT	  #00010000B	      ; My car on wall ?
		BNE	  Mycar_damage200     ; yes.
		BIT	  #00001100B	      ; My car on magnet bar ?
		BNE	  Mycar_damage210     ; yes.
;
;=============== My car rebound process ================================
Mycar_bound100
		LDA	  <road_col_count     ; road_color_counter == 0 ?
		BNE	  Mycar_bound110      ; no.
		LDA	  #09H
		STA	  <road_col_count     ; Set road_color_counter
;------------------------------------------------- Set uncontrol time --
Mycar_bound110	LDY	  <mycar_number
		LDA	  !Damage_time,Y
		STA	  !car_unctrl_flag+0  ; Set uncontrol flag
		STZ	  !car_unctrl_flag+1
;
;=============== Set damage ( out of course ) ==========================
Mycar_damage100
		LDA	  #01000000B
		TSB	  !crash_priority     ; Set SOUND
;-----------------------------------------------------------------------
		LDA	  <super_flag	      ; Invincible ?
		BNE	  Mycar_damage300     ; no.
		LDA	  #09H
		STA	  <super_flag	      ; Set invincible
		JSR	  Out_damage	      ; Set damage
		JSR	  Display_shield
		BRA	  Mycar_damage300
;
;=============== Set damage ( on laser wall ) ==========================
Mycar_damage200
		LDA	  <laser_counter      ; laser_counter == 0 ?
		BNE	  Mycar_damage210     ; no.
		LDA	  #08H
		STA	  <laser_counter      ; Set laser counter
;-----------------------------------------------------------------------
Mycar_damage210 LDA	  <super_flag	      ; Invincible ?
		BNE	  Mycar_damage300     ; yes.
;-----------------------------------------------------------------------
		LDA	  #02H
		STA	  <super_flag	      ; Set invincible
		JSR	  Wall_damage	      ; Set damage
		JSR	  Display_shield      ; Display shield
;
;=============== Set car speed =========================================
Mycar_damage300
		LDA	  <mycar_number
		ASL	  A
		TAY
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDA	  !car_speed
		CMP	  !Damage_speed,Y     ; car_speed >= damage_speed ?
		BCC	  Mycar_damage400     ; no.
		SBC	  #0010H
		STA	  !car_speed	      ; Set car speed
;
;=============== Start spark ===========================================
Mycar_damage400
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
		LDA	  #11100000B
		BIT	  <wing_count	      ; wing normal mode ?
		BNE	  Mycar_damage410     ; no.
		LDA	  #10001001B
		STA	  <wing_count	      ; Set On_wall mode
;-----------------------------------------------------------------------
Mycar_damage410 LDA	  <mycar_damage+1     ; my car broken ?
		BMI	  Mycar_damage420     ; yes.
		LDA	  <spark_counter      ; spark_counter active ?
		BMI	  Mycar_damage420     ; yes.
		LDA	  #8BH
		STA	  <spark_counter      ; Set spark counter
Mycar_damage420 RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\  Lap check routines  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Check start line					*
;************************************************************************
;
		MEM8
		IDX8
Check_line
		LDA	  !car_locatex_h+1,X
		CMP	  <start_room_x	      ; location_x == start_room_x ?
		BNE	  Check_line300	      ; no.
		LDA	  !car_locatey_h+1,X
		CMP	  <start_room_y	      ; location_y == start_room_y ?
		BEQ	  Check_line010	      ; tes.
		DEC	  A
		CMP	  <start_room_y
		BNE	  Check_line300	      ; no.
;-----------------------------------------------------------------------
Check_line010	LDA	  !car_locatex_h+0,X  ; before start line ?
		CMP	  #80H		      ;
		BCS	  Check_line200	      ; no.
;
;=============== car before start line =================================
Check_line100
		LDA	  !round_counter+1,X
		BEQ	  Check_line110	      ; if first run on start room
		BMI	  Check_line120	      ; if round_status = BEFORE
;-----------------------------------------------------------------------
		CPX	  #00H		      ; my car ?
		BNE	  Check_line105	      ; no.
		LDA	  !round_counter+0
		CMP	  !clear_round
		BNE	  Check_line110
;-----------------------------------------------------------------------
Check_line105	DEC	  !round_counter+0,X  ; decrement round counter
Check_line110	LDA	  #10000000B
		STA	  !round_counter+1,X  ; round_status = BEFORE
Check_line120	RTS
;
;=============== car after start line ==================================
Check_line200
		LDA	  !round_counter+1,X
		BEQ	  Check_line220	      ; if first run on start room
		BPL	  Check_line230	      ; if round_status = AFTER
;-----------------------------------------------------------------------
		LDA	  !round_counter+0,X
		INC	  A		      ; increment round_counter
		CMP	  #07H		      ; round counter >= 7 ?
		BVS	  Check_line210	      ; yes.
		STA	  !round_counter+0,X  ; Set round_counter
		STA	  !round_counter2,X   ; Set round_counter 2
;-----------------------------------------------------------------------
Check_line210	JSR	  Set_lap_time
Check_line220	LDA	  #01000000B
		STA	  !round_counter+1,X  ; round_status = AFTER
Check_line230	RTS
;
;=============== car not start room ====================================
Check_line300
		STZ	  !round_counter+1,X  ; Clear round_status
		RTS
;
;************************************************************************
;*		 Set lap time						*
;************************************************************************
;
		MEM8
		IDX8
Set_lap_time
		CPX	  #00H		      ; My car ?
		BNE	  Set_lap_time290     ; no.
		LDA	  <exception_flag     ; exception ?
		BNE	  Set_lap_time290     ; yes.
;
;=============== Set lap time ==========================================
Set_lap_time100
		LDA	  !round_counter+0    ; round counter <= 0 ?
		BEQ	  Set_lap_time290     ; yes.
		BMI	  Set_lap_time290     ; yes.
;-----------------------------------------------------------------------
		DEC	  A
		ASL	  A		      ; ( CY = 0 )
		ADC	  !round_counter+0
		DEC	  A
		TAY
		LDA	  !time_buffer,Y
		CMP	  #0FFH
		BNE	  Set_lap_time290
;-----------------------------------------------------------------------
		LDA	  !round_counter+0
		STA	  !clear_round
;------------------------------------------------------- Set lap time --
		LDA	  !time_counter+0
		STA	  !time_buffer+0,Y
		LDA	  !time_counter+1
		STA	  !time_buffer+1,Y
		LDA	  !time_counter+2
		STA	  !time_buffer+2,Y
;
;=============== Lap clear process =====================================
Set_lap_time200
		STZ	  !rank_warnning
		LDA	  !round_counter+0
		CMP	  #05H		      ; Final lap ?
		BEQ	  Final_process	      ; yes.
		JMP	  Lap_process	      ; no.
Set_lap_time290 RTS
;
;************************************************************************
;*		 Final lap process					*
;************************************************************************
;
		MEM8
		IDX8
Final_process
		DEC	  !rank_warnning
		LDA	  #00100000B
		STA	  <exception_flag     ; Set game end flag
		STZ	  !course_clr_flag
;
;=============== Set data for clear demo ===============================
Final_pros100
		LDA	  #180
		STA	  <initial_timer      ; Set initial timer
		STZ	  <turbo_status	      ; Clear turbo status
		JSR	  Goal_in_rank
		LDX	  #00H
		LDA	  !mycar_rank
		STA	  !goal_in_rank	      ; Set my car goal in rank
		STA	  !rank_buffer+4      ; Set lap 5 rank
		STA	  !rank_buffer+5      ; Set total rank
		LDA	  #06H
		STA	  !rast_round
		LDA	  #11111111B
		STA	  !OAM_sub+0DH	      ; Clear beam
		STA	  !OAM_sub+0FH	      ; Clear my car turbo jet
;
;=============== Check goal in rank ====================================
Final_pros200
		LDA	  <play_mode	      ; battle mode ?
		BNE	  Final_pros220	      ; no.
		LDA	  !goal_in_rank
		CMP	  #04H		      ; goal in rank < 4 ?
		BCC	  Final_pros230	      ; yes.
;----------------------------------------------------------- Rank out --
Final_pros210	LDA	  #10000000B
		STA	  <exception_flag     ; Game over
		LDA	  #03H
		STA	  !over_rank
		INC	  A
		STA	  !over_status
		DEC	  !rast_round
		LDA	  #0AH
		STA	  !rank_disp_count
		JSR	  Disp_rank	      ; Dipslay rank
		BRA	  Final_pros300
;---------------------------------------------------------- Free mode --
Final_pros220	LDA	  #11100000B
		STA	  !spot_color_B	      ; Clear shadow
		LDA	  #00000010B
		TSB	  <exception_flag     ; Set camera chase flag
		LDA	  <game_scene
		CMP	  #6		      ; scene 7 ?
		BNE	  Final_pros300	      ; no.
		RTS
;------------------------------------------------------------ Rank in --
Final_pros230	DEC	  A		      ; rank 1 ?
		BEQ	  Final_pros240	      ; yes.
		LDA	  <game_scene
		CMP	  #04H		      ; bettle last course ?
		BEQ	  Final_pros240	      ; yes.
		LDA	  #00000010B
		TSB	  <exception_flag     ; Set camera chase flag
		INC	  !course_clr_flag
;-----------------------------------------------------------------------
Final_pros240	INC	  !course_clr_flag
		LDY	  !goal_in_rank
		DEY
		LDA	  !Goal_in_score_h,Y
		STA	  <param1
		LDA	  !Goal_in_score_l,Y
		STA	  <param0
		JSR	  Calculate_score     ; Add score
		JSR	  Set_rank_window     ; Set rank window table
		JSR	  Disp_rank	      ; Dipslay rank
		LDX	  #00H
;
;=============== Set course time =======================================
Final_pros300
		PHK
		JSR	  Copy_time_rank
		LDX	  #00H
Final_pros390	RTS
;
;************************************************************************
;*		 Lap process						*
;************************************************************************
;
		MEM8
		IDX8
Lap_process
		JSR	  Goal_in_rank	      ; Check rank
		LDX	  #00H
		LDY	  !round_counter+0
		LDA	  !mycar_rank
		STA	  !rank_buffer-1,Y    ; Set lap rank
		CMP	  !Lap_clear_rank-1,Y ; Lap clear ?
		BCC	  Lap_process100      ; yes.
;
;=============== Game over ( rank out ) ================================
Lap_process000
		LDA	  !Lap_clear_rank-1,Y
		STA	  !over_rank
		LDA	  #04H
		STA	  !over_status
		LDA	  #0AH
		STA	  !rank_disp_count
		DEC	  !rank_warnning
		JSR	  Disp_rank	      ; Dipslay rank
		JSR	  You_lost
		PHK
		JSR	  Copy_time_rank
		RTS
;
;=============== Set data for next lap =================================
Lap_process100
		LDA	  <demo_flag	      ; demo mode ?
		BNE	  Lap_process220      ; yes.
		LDA	  <play_mode	      ; battle mode ?
		BNE	  Lap_process150      ; no.
;-----------------------------------------------------------------------
		LDA	  !Safe_rank_data,Y
		STA	  !safe_rank_char+0   ; Set safe rank character
		LDA	  !Lap_clear_rank,Y
		STA	  !clear_rank	      ; Set clear rank of next lap
		JSR	  Disp_rank	      ; Dipslay rank
;-----------------------------------------------------------------------
		LDA	  #20H
		LDY	  !mycar_rank
		CPY	  #04H
		BCS	  Lap_process110
		DEY
		LDA	  !Lap_clear_score,Y
Lap_process110	STA	  <param0
		STZ	  <param1
		JSR	  Calculate_score     ; Add score
		BRA	  Lap_process200
;-----------------------------------------------------------------------
Lap_process150	LDA	  #22
		LDY	  !round_counter+0
		CPY	  #04H
		BNE	  Lap_process160
		LDA	  #108
Lap_process160	STA	  !lap_sound_cnt
;
;=============== Display message =======================================
Lap_process200
		LDA	  !burner_count
		CMP	  #03H
		BCS	  Lap_process205
		LDA	  #32
		STA	  !turbo_ok_count
		INC	  !burner_inc_flag    ; after burner increment
;-----------------------------------------------------------------------
Lap_process205	LDA	  #00010000B
		LDY	  !round_counter+0
		CPY	  #04H		      ; Lap 4 ?
		BNE	  Lap_process210      ; no.
		LSR	  A
Lap_process210	TSB	  <message_flag
Lap_process220	RTS
;
;=======================================================================
Lap_clear_rank	BYTE	  16,11,8,6,4
Lap_clear_score BYTE	  90H,60H,40H
Goal_in_score_h BYTE	  02H,01H,01H
Goal_in_score_l BYTE	  50H,50H,00H
Safe_rank_data	BYTE	  8AH,89H,88H,87H,86H
;
;************************************************************************
;*		 You lost request					*
;************************************************************************
;
		MEM8
		IDX8
You_lost
		PHP
		SEP	  #00110000B
;-----------------------------------------------------------------------
		LDA	  !clear_round
		STA	  !rast_round
		LDA	  #180
		STA	  <initial_timer      ; Set initial timer
		LDA	  #11111111B
		STA	  !OAM_sub+0DH	      ; Clear beam
		STA	  !OAM_sub+0FH	      ; Clear my car turbo jet
		LDA	  #10000000B
		STA	  <exception_flag     ; Game over
;-----------------------------------------------------------------------
		PLP
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Hight control routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 My car hight control					*
;************************************************************************
;
		MEM8
		IDX8
Hight_control
		CPX	  #00H		      ; My car ?
		BNE	  Hight_control1      ; no.
		LDA	  !short_cut_flag     ; short cut ?
		BEQ	  Mycar_hight	      ; no.
		RTS
;
;=============== My car hight control entry ============================
Mycar_hight
		LDX	  #00H
		LDA	  <turbo_status	      ; turbo on ?
		BEQ	  Mycar_hight200      ; no.
;-----------------------------------------------------------------------
		CMP	  #80H		      ; turbo trigger ?
		BNE	  Mycar_hight100      ; no.
		STZ	  <hover_counter      ; Clear hover counter
;
;=============== My car turbo ==========================================
Mycar_hight100
		LDY	  <hover_counter
		LDA	  !Turbo_hover_lev,Y
		CMP	  <hover_level	      ; Hover_level <= Turbo_hover ?
		BCS	  Mycar_hight110      ; yes.
;--------------------------------------------------------- Hover down --
		DEC	  <hover_level
		LDA	  <hover_level
		BRA	  Mycar_hight120
;---------------------------------------------------------- Set hover --
Mycar_hight110	STA	  <hover_level
Mycar_hight120	CPY	  #06H
		BEQ	  Hight_control2
		INC	  <hover_counter
		BRA	  Hight_control2
;-----------------------------------------------------------------------
Turbo_hover_lev BYTE	  01H,02H,03H,04H,04H,03H,02H
;
;=============== My car non turbo ======================================
Mycar_hight200
		LDA	  <hover_level	      ; hover level == 0 ?
		BEQ	  Hight_control2      ; yes.
;-----------------------------------------------------------------------
		DEC	  A
		STA	  <hover_level	      ; my car hight down
		BRA	  Hight_control2
;
;************************************************************************
;*		 Hight control main					*
;************************************************************************
;
		MEM8
		IDX8
Hight_control1	LDA	  #02H
Hight_control2	CLC
		ADC	  #04H
		STA	  <hight_level	      ; Set car hight level
		DEC	  A
		DEC	  A
		STZ	  <ground_level+0     ; Set ground level ( for jump )
		STA	  <ground_level+1
;
;=============== Jumpping check ========================================
Hight_ctrl100
		LDA	  !control_status,X   ; Jumpping ?
		BMI	  Hight_ctrl130	      ; yes.
;-------------------------------------------------- Check jump start --
Hight_ctrl110	LDA	  !road_status_1,X
		ASL	  A		      ; jump zone ?
		BPL	  Hight_ctrl200	      ; no.
		LDA	  !mycar_rank
		CMP	  #20		      ; rank out ?
		BCS	  Hight_ctrl200	      ; yes.
;------------------------------------------------------- Jump process --
Hight_ctrl120	JSR	  Jump_start
Hight_ctrl130	JSR	  Jump_control
		RTS
;
;=============== Check road ============================================
Hight_ctrl200
		LDA	  <hight_level
		STA	  !car_hight+1,X      ; Set car hight level
;-----------------------------------------------------------------------
Hight_ctrl210	LDA	  !road_status_1,X    ; dirt zone ?
		BPL	  Hight_ctrl220	      ; no.
		LDA	  !car_speed+1,X      ; speed >= 1 dot ?
		BNE	  Hight_ctrl300	      ; yes. ( dirt zone )
;-----------------------------------------------------------------------
Hight_ctrl220	CPX	  #00H		      ; My car ?
		BEQ	  Hight_ctrl400	      ; yes.
		BRA	  Hight_ctrl500	      ; no.
;
;=============== dirt zone =============================================
Hight_ctrl300
		LDA	  <frame_counter
		LSR	  A
		LSR	  A
		LDA	  !car_hight+1,X
		ADC	  #00H
		STA	  !car_hight+1,X      ; Set dirt zone offset
		RTS
;
;=============== My car turbo ==========================================
Hight_ctrl400
		LDA	  <turbo_status	      ; Check turbo_status
		BMI	  Hight_ctrl410	      ; if ( turbo trigger )
		BEQ	  Hight_ctrl410	      ; if ( turbo off	   )
;-----------------------------------------------------------------------
		LDA	  <frame_counter
		LSR	  A
		LDA	  !car_hight+1,X
		SBC	  #00H
		STA	  !car_hight+1,X      ; Set turbo offset
Hight_ctrl410	RTS
;
;=============== Enemy car crash =======================================
Hight_ctrl500
		LDA	  !carcol_change,X    ; enemy car crash ?
		BEQ	  Hight_ctrl510	      ; no.
;-----------------------------------------------------------------------
		EOR	  #00000001B
		AND	  #00000001B
		ASL	  A
		ADC	  !car_hight+1,X
		DEC	  A
		STA	  !car_hight+1,X
Hight_ctrl510	RTS
;
;************************************************************************
;*		 Jump start process					*
;************************************************************************
;
		MEM8
		IDX8
Jump_start
		LDA	  #10000000B
		ORA	  !control_status,X
		STA	  !control_status,X   ; Set jump flag
;
;=============== Set car jump speed ====================================
Jump_start100
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDA	  !car_speed,X
		LSR	  A
		CMP	  #0100H	      ; jump speed >= 100H ?
		BCS	  Jump_start105	      ; yes.
		LDA	  #0100H	      ; Set jump min speed
		BRA	  Jump_start110
Jump_start105	CMP	  #0481H	      ; jump speed < 480H ?
		BCC	  Jump_start110	      ; yes.
		LDA	  #0480H	      ; Set jump max speed
Jump_start110	STA	  !car_jump_speed,X   ; Set car jump speed
;-----------------------------------------------------------------------
		CPX	  #00H		      ; My car jump ?
		BNE	  Jump_start200	      ; no.
;-----------------------------------------------------------------------
		LDA	  !car_speed+0
		CMP	  #0200H	      ; speed >= 2 dot ?
		BCC	  Jump_start120	      ; no.
		LDA	  !car_speed+0
		SEC
		SBC	  #0080H
		STA	  !car_speed+0	      ; Speed down 0.5 dot
Jump_start120	LDY	  #00H
		STY	  <jump_count	      ; Clear jump counter
;
;=============== Set initial car hight =================================
Jump_start200
		LDA	  !control_status,X
		AND	  #1111111111111110B
		STA	  !control_status,X
		LDA	  <hight_level-1
		AND	  #0FF00H
		STA	  !car_hight,X	      ; Set car hight
		RTS
;
;************************************************************************
;*		 Jump control routine					*
;************************************************************************
;
		MEM16
		IDX8
Jump_control
		REP	  #00100000B	      ; Memory 16 bit mode
;
;=============== Car hight level control ===============================
Jump_control100
		LDA	  !car_hight,X
		CLC
		ADC	  !car_jump_speed,X
		STA	  !car_hight,X	      ; Set car hight
;-----------------------------------------------------------------------
		BMI	  Jump_control500     ; if ( hight < 0 ) jump end
		CMP	  <ground_level	      ; hight < ground ?
		BCC	  Jump_control500     ; yes. ( jump end )
;
;=============== Car jump speed control ================================
Jump_control200
		LDA	  !road_status_1,X
		AND	  #0000000000001000B  ; magnet down zone ?
		CLC
		BNE	  Jump_control210     ; yes.
;-----------------------------------------------------------------------
		LDA	  !car_jump_speed,X
		SBC	  <gravity	      ; subtruct gravity
		BRA	  Jump_control220
Jump_control210 LDA	  !car_jump_speed,X
		SBC	  #0060H	      ; subtruct magnet power
Jump_control220 STA	  !car_jump_speed,X   ; Set jump speed
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		CPX	  #00H		      ; My car ?
		BNE	  Jump_control290     ; no.
		LDA	  !car_control
		AND	  #00000100B
		BNE	  Jump_control230
		LDA	  #01000000B
		BRA	  Jump_control240
Jump_control230 LDA	  #10000000B
Jump_control240 TSB	  !wind_priority      ; Set jump sound
Jump_control290 SEC
		RTS
;
;=============== Jump end process ======================================
Jump_control500
		MEM16
		LDA	  !car_jump_speed,X   ; jump_speed >= 0 ?
		BPL	  Jump_control200     ; yes. ( jump up )
;-----------------------------------------------------------------------
		LDA	  <ground_level
		STA	  !car_hight,X	      ; car_hight = 0
		CPX	  #00H		      ; My car ?
		BNE	  Jump_control530     ; no. ( no bound )
;-----------------------------------------------------------------------
		LDY	  !car_BGdata+0
		LDA	  !control_status+0
		LSR	  A		      ; Normal jump ?
		BCS	  Jump_control504     ; no. ( skip out of course check )
		CPY	  #069H
		BEQ	  Jump_control505
		CPY	  #0A5H
		BEQ	  Jump_control505
Jump_control504 CPY	  #0D0H		      ; Out of course ?
		BCC	  Jump_control510     ; no.
;-----------------------------------------------------------------------
		MEM8
Jump_control505 SEP	  #00100000B	      ; debug
		LDA	  #01000000B
		TSB	  <exception_flag     ; Set game over exception
		LDA	  #06H
		STA	  <explosion_count
		LDA	  !round_counter
		STA	  !rast_round
		BRA	  Jump_control530
;-----------------------------------------------------------------------
		MEM16
Jump_control510 LDA	  #0000H
		SEC
		SBC	  !car_jump_speed,X   ; Acc = abs(jump_speed)
		CMP	  #0280H	      ; jump_speed >= 280H ?
		BCS	  Jump_control600     ; yes. ( bound )
;-----------------------------------------------------------------------
		MEM8
Jump_control520 SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #00000001B
		STA	  <drift_status	      ; Restore grip
;-----------------------------------------------------------------------
Jump_control530 SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  !control_status,X
		AND	  #01111111B
		STA	  !control_status,X   ; Reset jump_flag
;-----------------------------------------------------------------------
		CPX	  #00H		      ; my car ?
		BNE	  Jump_control550     ; no.
		LSR	  A		      ; normal jump ?
		BCS	  Jump_control540     ; no.
		LDA	  #0CH
		STA	  !jump_end_sound
Jump_control540 LDA	  !road_status_1
		AND	  #01000000B	      ; Jump zone ?
		BNE	  Jump_control700     ; yes.
;-----------------------------------------------------------------------
Jump_control550 SEC
		RTS
;
;=============== Bound process =========================================
Jump_control600
		MEM16
		LSR	  A
		LSR	  A		      ; jump_speed /= 4
		STA	  !car_jump_speed,X   ; Set jump_speed
;-----------------------------------------------------------------------
		LDA	  !car_control
		AND	  #0000000000000100B  ; push UP ?
		BNE	  Jump_control520     ; yes.
		LDA	  !road_status_1
		AND	  #0000000001000000B  ; Jump zone ?
		BNE	  Jump_control700     ; yes.
;-----------------------------------------------------------------------
		LDA	  !special_demo	      ; special demo ?
		BNE	  Jump_control610     ; yes.
		LDY	  <spark_counter
		BMI	  Jump_control610
		LDY	  #01000101B
		STY	  <spark_counter
;-----------------------------------------------------------------------
Jump_control610 LDA	  !car_speed+0
		CMP	  #0200H	      ; speed >= 2 dot ?
		BCC	  Jump_control620     ; no.
		SEC
		SBC	  #0100H	      ; speed down 1 dot
		STA	  !car_speed+0	      ; Set speed
		MEM8
		SEP	  #00100000B
		LDA	  #00001000B
		TSB	  <sound_status_2     ; Set SOUND
;-----------------------------------------------------------------------
Jump_control620 SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #00000001B
		TSB	  !control_status+0   ; Set course check skip mode
		SEC
		RTS
;
;=============== Jump more =============================================
Jump_control700
		MEM8
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		LDA	  !mycar_rank
		CMP	  #20		      ; rank out ?
		BCS	  Jump_control710     ; yes.
		JSR	  Jump_start
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
Jump_control710 SEC
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\  Set display position	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Set display position					*
;************************************************************************
;
		MEM16
		IDX8
Set_disp_pos
		REP	  #00100000B	      ; Memory 16 bit mode
;
;=============== Set display position H ================================
Set_disp_pos100
		LDA	  !car_position_h,X
		STA	  !car_display_h,X
		SEP	  #00100000B	      ; Memory 8 bit mode
		MEM8
;
;=============== Set display position V ( my car ) =====================
Set_disp_pos200
		CPX	  #00H		      ; My car ?
		BNE	  Set_disp_pos300     ; no.
		LDA	  !special_demo
		BNE	  Set_disp_pos210
;-----------------------------------------------------------------------
		LDA	  !car_position_v+0
		SEC
		SBC	  !car_hight+1
		STA	  !car_display_v+0
		RTS
;-----------------------------------------------------------------------
Set_disp_pos210 LDA	  !car_position_v+0
		STA	  !car_display_v+0
		CLC
		LDA	  !car_hight+1
		ADC	  #40H
		STA	  <screen_zoom+0
		RTS
;
;=============== Set display position V ( enemy ) ======================
Set_disp_pos300
		LDA	  !car_flag,X
		BIT	  #00010000B
		BNE	  Set_disp_pos310
;-----------------------------------------------------------------------
		PHK
		PER	  3
		JMP	  >Enemy_jump
		EOR	  #0FFH
		SEC
		ADC	  !car_position_v+0,X
		STA	  !car_display_v+0,X
Set_disp_pos310 RTS
;
;************************************************************************
;*		 Enemy jump exception					*
;************************************************************************
;
		MEM8
		IDX8
Jump_exception
		LDA	  !car_display_v+0,X
		CMP	  #0F0H
		BCS	  Jump_except200
		ADC	  #10H
		STA	  !car_display_v+0,X
		RTS
;
;=============== Reset jump flag =======================================
Jump_except200
		LDA	  !control_status,X
		AND	  #01111111B
		STA	  !control_status,X
		RTS
;
;************************************************************************
;*		 Bomb control						*
;************************************************************************
;
		MEM8
		IDX8
Bomb_control
		LDA	  !car_flag,X
		AND	  #10111110B
		CMP	  #10001000B	      ; car on screen ?
		BNE	  Bomb_ctrl90	      ; no.
;-----------------------------------------------------------------------
		LDA	  !road_status_1,X
		AND	  #00100000B	      ; on bomb ?
		BEQ	  Bomb_ctrl90	      ; no.
		LDA	  !control_status,X   ; jumpping ?
		BMI	  Bomb_ctrl90	      ; yes.
;-----------------------------------------------------------------------
		LDA	  !car_position_v,X
		CMP	  #40H
		BCS	  Bomb_ctrl100
Bomb_ctrl90	RTS
;
;=============== Set crater ============================================
Bomb_ctrl100
		LDA	  #32
		STA	  !car_unctrl_flag,X
		LDA	  #85H
		STA	  !carcol_change,X
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		STX	  <crater_index	      ; Set crater index
		LDA	  !car_locatex_h,X
		STA	  !crater_pos_x	      ; Set crater position x
		LDA	  !car_locatey_h,X
		STA	  !crater_pos_y	      ; Set crater position y
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		CPX	  #00H		      ; my car ?
		BNE	  Bomb_ctrl200	      ; no.
		JSR	  Bomb_damage	      ; Set damage
		JSR	  Display_shield      ; Display shield meter
		LDX	  #00H
;
;=============== Car jump up ===========================================
Bomb_ctrl200
		LDA	  !control_status,X
		ORA	  #10000001B
		STA	  !control_status,X   ; Set car jump flag
;-----------------------------------------------------------------------
;;		LDA	  <hight_level
;;		STA	  >car_hight+1,X      ; Set car hight high
;;		STZ	  >car_hight+0,X      ; Set car hight low
		LDA	  #01H
		STA	  !car_jump_speed+1,X ; Set car jump speed high
		STZ	  !car_jump_speed+0,X ; Set car jump speed low
;
;=============== Set spin velocity =====================================
Bomb_ctrl300
		LDA	  !car_scrl_angle+1,X
		SEC
		SBC	  #24
		BCS	  Bomb_ctrl310
		ADC	  #192
;-----------------------------------------------------------------------
Bomb_ctrl310	STA	  !Dividend+0
		STZ	  !Dividend+1
		LDA	  #12
		STA	  !Divisor
		LDA	  #08H
		STA	  !car_speed+1,X      ; Set car speed high
		STZ	  !car_speed+0,X      ; Set car speed low
		STZ	  !car_spin_velo+0,X  ; Set spin velocity low
		LDA	  !car_BGdata,X
		SEC
		SBC	  #0C8H
		STA	  <work0
		LDA	  !Divide+0
		AND	  #11111100B
		ORA	  <work0
		TAY
		LDA	  !Bomb_move_data,Y
		STA	  !car_spin_velo+1,X  ; Set spin velocity
		BPL	  Bomb_ctrl500
;
;=============== Move left =============================================
Bomb_ctrl400
		LDA	  !control_status+0,X
		ASL	  A		      ; car spin ?
		BPL	  Bomb_ctrl410
		DEC	  !car_spin_velo+1,X  ; Set spin velocity
;-----------------------------------------------------------------------
Bomb_ctrl410	LDA	  !car_scrl_angle+1,X
		SEC
		SBC	  #20
		BCS	  Bomb_ctrl420
		ADC	  #192
Bomb_ctrl420	STA	  !car_scrl_angle+1,X
		LDA	  #01000011B
		STA	  <wing_count
		RTS
;
;=============== Move right ============================================
Bomb_ctrl500
		LDA	  !control_status+0,X
		ASL	  A		      ; car spin ?
		BPL	  Bomb_ctrl410
		DEC	  !car_spin_velo+1,X  ; Set spin velocity
;-----------------------------------------------------------------------
		LDA	  !car_scrl_angle+1,X
		CLC
		ADC	  #20
		CMP	  #192
		BCC	  Bomb_ctrl520
		SBC	  #192
Bomb_ctrl520	STA	  !car_scrl_angle+1,X
		LDA	  #01000111B
		STA	  <wing_count
		RTS
;
;=======================================================================
;
Bomb_move_data	BYTE	  82H,82H,02H,02H
		BYTE	  02H,82H,02H,82H
		BYTE	  02H,02H,82H,82H
		BYTE	  82H,02H,82H,02H
;
;************************************************************************
;*		 Enemy bomb control					*
;************************************************************************
;
		MEM8
		IDX8
Enemy_bomb_ctrl
		PHA
		LDA	  #32
		STA	  !car_unctrl_flag+0  ; Set uncontrol flag
		LDA	  #85H
		STA	  !carcol_change+0    ; Set car color change
;-----------------------------------------------------------------------
		LDA	  #00001000B
		TSB	  <sound_status_2     ; Set SOUND
		JSR	  Bomb_damage	      ; Set damage
		JSR	  Display_shield      ; Display shield meter
;
;=============== Car jump up ===========================================
Enemy_bomb100
		LDA	  #10000001B
		TSB	  !control_status     ; Set car jump flag
;-----------------------------------------------------------------------
		LDA	  #01H
		LDX	  #00H
		STA	  !car_jump_speed+1,X ; Set car jump speed high
		STZ	  !car_jump_speed+0,X ; Set car jump speed low
;
;=============== Set car spin velocity =================================
Enemy_bomb200
		PLA
		LSR	  A
		LSR	  A
		ROR	  A
		AND	  #10000000B
		EOR	  #10000000B
		ORA	  #00000010B
		STZ	  !car_spin_velo+0    ; Set spin velocity low
		STA	  !car_spin_velo+1    ; Set spin velocity
		BPL	  Bomb_ctrl500
		BRA	  Bomb_ctrl400
;
;************************************************************************
;*		 Set camera location					*
;************************************************************************
;
		MEM16
		IDX8
Camera_move
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
		SEP	  #00010000B	      ; Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  !car_scrl_angle+0
		STA	  <move_angle	      ; Set move angle
		LDA	  !car_speed+0
		STA	  <move_speed	      ; Set move speed
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDX	  #00H
		JSR	  Set_vector	      ; Set speed vector
		JSR	  Set_location	      ; Set camera location
;-----------------------------------------------------------------------
		PLP			      ; Restore mode
		RTS
;
;************************************************************************
;*		 Camera chase my car					*
;************************************************************************
;
		MEM8
		IDX8
Camera_chase
		LDA	  <exception_flag
		AND	  #00000010B	      ; Camera chase exception ?
		BEQ	  Camera_chase190     ; no.
;
;=============== Set camera position ===================================
Camera_chase100
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDY	  !anime_car_index
		LDA	  !car_locatex_h,Y
		STA	  !car_locatex_h      ; Set camera location x
		LDA	  !car_locatey_h,Y
		STA	  !car_locatey_h      ; Set camera location x high
		SEP	  #00100000B	      ; Memory 8 bit mode
Camera_chase190 RTS
;
		END
