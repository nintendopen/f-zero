;************************************************************************
;*	 EN_INIT_4	   -- enemy car Initialize Routine ---		*
;************************************************************************
		INCLUDE	  BUFFER
		INCLUDE	  VARIABLE
		INCLUDE	  WORK
;-----------------------------------------------------------------------
		GLB	  Set_mycar_OAM
;----------------------------------------------------------------------------
tempolary	EQU		10H
mycar_palet	EQU		20H
mycar_type	EQU		22H
counter		EQU		24H
plus_data	EQU		26H
bank_patern	EQU		28H
;----------------------------------------------------------------------------
		PROG
		extend
		MEM8
		IDX8
Set_mycar_OAM
		SEP		#00110000B
;
		PEA		0002H
		PLB
;
		STZ		<mycar_palet+00H
		LDA		#00H
		LDX		<play_mode
		BNE		Set_OAM_data
		LDA		<mycar_number
		ASL		A
Set_OAM_data
		ORA		#00001000B
		STA		<mycar_palet+01H
		LDA		<mycar_number
		AND		#00000001B
		STA		<mycar_type+00H
		STZ		<mycar_type+01H
		TAY
		LDA		!Wing_set_addr,Y
		STA		!wing_set_addr+00H
		STZ		!wing_set_addr+01H
		LDA		!Body_set_addr,Y
		STA		!body_set_addr+00H
		STZ		!body_set_addr+01H
;
		LDX		#00H
		LDA		#00H
Set_data_addr
		STA		!canopy_data_addr,X
		PHA
		ASL		A
		LDY		<mycar_type
		BNE		Set_wing_addr
		ASL		A
Set_wing_addr
		STA		!wing_data_addr,X
		PLA
		CLC
		ADC		#04H
		INX
		CPX		#07H
		BNE		Set_data_addr
		LDX		#00H
		LDY		<mycar_type
Set_number_loop
		LDA		Parts_number,Y	;parts_number+00H
		STA		!parts_number,X ;	=wing OAM num
		INY				;	     +01H
		INY				;	=canopy OAM num
		INX				;	     +02H
		CPX		#03H		;	=body OAM num
		BNE		Set_number_loop
;
		LDA		#08H
		STA		!oam_number+02H ;    =jump  OAM number
		LDA		#06H
		STA		!oam_number+01H ;    =slide OAM number
		LDX		<mycar_number
		LDA		Bank_num_data,X
		STA		!oam_number+00H ;    =bank  OAM number
;Open_position
		LDA		Plus_v_data,X
		STA		<plus_data
		MEM16
		IDX16
		REP		#00110000B
		LDY		#0000H
Open_pos_loop
		LDA		!oam_number+00H
		STA		<counter+00H
		LDX		#0000H
		PHY
		JSR		Open_pos_data
		PLY
		TYA
		CLC
		ADC		#20H
		CMP		#80H
		BEQ		Open_slide_pos
		TAY
		BRA		Open_pos_loop
Open_slide_pos
		LDY		#0080H
Open_slide_loop
		LDX		#0000H
		LDA		#006H
		STA		<counter
		PHY
		JSR		Open_pos_data
		PLY
		TYA
		CLC
		ADC		#18H
		CMP		#0E0H
		BEQ		Open_parts
		TAY
		BRA		Open_slide_loop
Open_parts
		MEM16
		IDX16
		REP		#00110000B
		LDX		#00H
Set_parts_loop
		LDA		Address_data_2,X
		TAY
		PHX
		LDA		<mycar_type
		BEQ		Type_AC
		INX
Type_AC
		LDA		Count_data,X
		STA		<counter
		LDA		Address_data_1,X
		AND		#00FFH
		CLC
		ADC		#Mycar_OAM_data
		TAX
		JSR		Open_OAM_data
		PLX
		INX
		INX
		CPX		#08H
		BNE		Set_parts_loop
Slide_set	LDA		<mycar_number
		AND		#00FFH
		TAX
		LDA		Address_data_3,X
		AND		#00FFH
		CLC
		ADC		#Mycar_OAM_data
		TAX
		LDY		#0080H
		LDA		#0012
		STA		<counter
		JSR		Open_OAM_data
;----------------------------------------------------------------------------
		MEM16
		IDX16
		REP		#00110000B
		LDA		#0008H
		STA		<counter
		LDX		#Jump_data
		LDY		#00C0H
		JSR		Set_oam_data
;
		LDX		#Bank_rest_A
		MEM8
		SEP		#00100000B
		LDA		<mycar_number
		BIT		#00000001B
		BNE		No_rest_bank
		BIT		#00000010B
		BEQ		A_type_bank
		LDX		#Bank_rest_C
A_type_bank
		LDY		#0018H
Set_rest_loop
		MEM16
		REP		#00110000B
		PHY
		LDA		#02H
		STA		<counter
		JSR		Set_oam_data
		PLY
		TYA
		CLC
		ADC		#0020H
		TAY
		CMP		#0098H
		BNE		Set_rest_loop
No_rest_bank
		SEP		#00110000B
;
		PLB
;
		RTS
;-----------------------------------------------------------------------------------
		MEM16
		IDX16
Set_oam_data
		LDA		>020000H,X
		STA		!mycar_OAM+00H,Y
		LDA		>020002H,X
		ORA		<mycar_palet
		STA		!mycar_OAM+02H,Y
		INX
		INX
		INX
		INX
		INY
		INY
		INY
		INY
		DEC		<counter
		BNE		Set_oam_data
		RTS
;----------------------------------------------------------------------------
		MEM8
		IDX16
Open_OAM_data
		SEP		#00100000B
		STZ		<tempolary
		LDA		>020000H,X
		BIT		#00010000B
		BEQ		No_change_bank
		INC		<tempolary
No_change_bank
		BIT		#00000001B
		BEQ		No_change_flip
		AND		#11101110B
		STA		!mycar_OAM+02H,Y
		LDA		#01110000B
		BRA		Set_OAM_3
No_change_flip
		AND		#11101110B
		STA		!mycar_OAM+02H,Y
		LDA		#00110000B
Set_OAM_3
		ORA		<tempolary
		ORA		<mycar_palet+01H
		STA		!mycar_OAM+03H,Y
		INX
		INY
		INY
		INY
		INY
		DEC		<counter
		BNE		Open_OAM_data
Return_main
		REP		#00100000B
		RTS
;------------------------------------------------------------------------------
		MEM8
		IDX16
Open_pos_data
		SEP		#00100000B
		LDA		!Stand_h+00H,X
		STA		!mycar_OAM+00H,Y
		LDA		!Stand_v+00H,X
		CLC
		ADC		<plus_data
		STA		!mycar_OAM+01H,Y
		DEC		<counter
		BEQ		Return_main
		INX
		INY
		INY
		INY
		INY
		BRA		Open_pos_data
;---------------------------------------------------------------------------------
		ORG		2FC10H
;----------------------------------------------------------------------------------
;				TypeA(0&2)   TypeB(1&3)
Parts_number
Wing_number	BYTE		004H,002H
Canopy_number	BYTE		001H,000H
Body_number	BYTE		001H,004H
;				Car0 Car1 Car2 Car3
Slide_ch_addr	BYTE		000H,00CH,018H,00CH
Bank_num_data	BYTE		008H,006H,008H,006H
;
Wing_set_addr	BYTE		08H,00H
Body_set_addr	BYTE		04H,08H
Plus_wing_data	BYTE		20H,00H
;				Car0 Car1 Car2 Car3
Plus_v_data    BYTE		002H,001H,002H,001H
;-----------------------------------------------------------------------------
Stand_h		BYTE		0E8H,0E8H,0F8H,0F8H,008H,008H
;------------------------------------------------------------------------------
Stand_v		BYTE		0E0H,0F0H,0E0H,0F0H,0E0H,0F0H
;-----------------------------------------------------------------------------------
Count_data	BYTE		32,32		;bank
		BYTE		01,04		;body
		BYTE		28,14		;wing
		BYTE		07,07		;canopy
Address_data_1	BYTE		00H,20H		;bank
		BYTE		64H,65H		;body
		BYTE		70H,8CH		;wing
		BYTE		69H,69H		;canopy
Address_data_2	WORD		0000H		;bank
		WORD		016CH		;body
		WORD		00E0H		;wing
		WORD		0150H		;canopy
Address_data_3	BYTE		40H,4CH,58H,4CH ;slide
;----------------------------------------------------------------------------
Jump_data
		BYTE		0E8H,0E0H,080H,00111000B
		BYTE		0F0H,0E0H,081H,00111000B
		BYTE		000H,0E0H,081H,01111000B
		BYTE		008H,0E0H,080H,01111000B
		BYTE		0E8H,0F0H,083H,00111000B
		BYTE		0F0H,0F0H,084H,00111000B
		BYTE		000H,0F0H,084H,01111000B
		BYTE		008H,0F0H,083H,01111000B
;----------------------------------------------------------------------------------------
Mycar_OAM_data
Bank_AC		BYTE		02EH,08EH,04EH,010H,06EH,012H,000H,000H	   ;00H
		BYTE		0E6H,0ECH,0E8H,0EEH,0EAH,00EH,000H,000H
		BYTE		0EBH,00FH,0E9H,0EFH,0E7H,0EDH,000H,000H
		BYTE		06FH,013H,04FH,011H,02FH,08FH,000H,000H
Bank_BD		BYTE		0E0H,0E6H,0E2H,0E8H,0E4H,0EAH,000H,000H
		BYTE		0AAH,0CAH,0ACH,0CCH,0AEH,0CEH,000H,000H
		BYTE		0AFH,0CFH,0ADH,0CDH,0ABH,0CBH,000H,000H
		BYTE		0E5H,0EBH,0E3H,0E9H,0E1H,0E7H,000H,000H
;------------------------------------------------------------------------------------
Slide_A		BYTE		00CH,02CH,0E8H,0EEH,04CH,06CH	;0040H
		BYTE		04DH,06DH,0E9H,0EFH,00DH,02DH
Slide_BD	BYTE		0AAH,0CAH,0ECH,0EEH,0AEH,0CEH	;004CH
		BYTE		0AFH,0CFH,0EDH,0EFH,0ABH,0CBH
Slide_C		BYTE		00CH,02CH,08CH,0EEH,04CH,06CH	;0058H
		BYTE		04DH,06DH,08DH,0EFH,00DH,02DH
;----------------------------------------------------------------------------------
Body_A		BYTE		0AEH				;0064H
Body_B		BYTE		0A0H,0C0H,0A1H,0C1H		;0065H
;--------------------------------------------------------------------------
Canopy		BYTE		0E4H,0E2H,0E0H			;0069H
		BYTE		0CEH,0E1H,0E3H,0E5H
;------------------------------------------------------------------------
Wing_A		BYTE		0A6H,0C6H,0ACH,0CCH		;0070H
		BYTE		0A4H,0C4H,0AAH,0CAH
		BYTE		0A2H,0C2H,0A8H,0C8H
		BYTE		0A0H,0C0H,0A1H,0C1H
		BYTE		0A9H,0C9H,0A3H,0C3H
		BYTE		0ABH,0CBH,0A5H,0C5H
		BYTE		0ADH,0CDH,0A7H,0C7H
;-------------------------------------------------------------------------
Wing_B		BYTE		0A8H,0C8H	;kai 1 bit =attr 008CH
		BYTE		0A6H,0C6H	;0=Not hanten
		BYTE		0A4H,0C4H	;1=R-F hanten
		BYTE		0A2H,0C2H
		BYTE		0A5H,0C5H
		BYTE		0A7H,0C7H
		BYTE		0A9H,0C9H
;-------------------------------------------------------------------------
Bank_rest_A	;9AH
		BYTE		018H,0F4H,004H,031H,000H,0F0H,005H,031H
		BYTE		000H,0F0H,005H,031H,000H,0F0H,005H,031H
		BYTE		000H,0F0H,005H,071H,000H,0F0H,005H,071H
		BYTE		0E0H,0F4H,004H,071H,0E0H,0F0H,005H,031H
Bank_rest_C	;BAH
		BYTE		0F8H,0DAH,014H,031H,000H,0DAH,015H,031H
		BYTE		008H,0DAH,004H,031H,008H,0DAH,004H,031H
		BYTE		0F0H,0DAH,004H,071H,0F0H,0DAH,004H,071H
		BYTE		0F8H,0DAH,015H,071H,000H,0DAH,014H,071H
		end
