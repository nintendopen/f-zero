/***************************************************************
   Car select screen data compress for CAR RACE  version 1.00
		    Programmed by Y.Nishida
					[ Apr.05, 1990 ]
 ***************************************************************/

#include	<stdio.h>

#define		unchar		unsigned char
#define		unshort		unsigned short
#define		ERROR		-1
#define		COMPLETE	0


int	bincnt ;			/*  counter of total byte			*/
int	norcnt = 0 ;			/*  counter of non compress format counter	*/
int	chrcnt = 0 ;			/*  counter of character compress format data	*/
int	atrcnt = 0 ;			/*  counter of attribute compress format data	*/


main( argc,argv )
int	argc ;
char	**argv ;
{
	FILE	*rp,*wp ;
	void	make_data() ;

	puts( "\nCar select screen data compress for CAR RACE  version 1.00" ) ;
	if ( argc != 3 ) {
		puts( "Usage: mkselect <source file> <destination file>" ) ;
		exit( 1 ) ;
	}
	if (( rp = fopen( *++argv,"r" )) == NULL ) {
		perror( *argv ) ;
		exit( 1 ) ;
	}
	if (( wp = fopen( *++argv,"w" )) == NULL ) {
		perror( *argv ) ;
		exit( 1 ) ;
	}
	make_data( rp,wp ) ;
	fclose( rp ) ;
	fclose( wp ) ;

	bincnt = norcnt + chrcnt + atrcnt + 1 ;
	printf( "\nnon compress format data         %4d bytes", norcnt ) ;
	printf( "\ncharacter compress format data   %4d bytes", chrcnt ) ;
	printf( "\nattribute compress format data   %4d bytes", atrcnt ) ;
	printf( "\n\nTotal %5d ( %04X ) bytes write\n", bincnt,bincnt ) ;
	exit( 0 ) ;
}

/****************************************************************/
/*	make data						*/
/****************************************************************/

void make_data( rp,wp )
FILE	*rp,*wp ;
{
	unchar	chrbuf[32] ;
	int	red_char,arc_char ;
	int	red_attr,arc_attr ;
	int	chrlen=0,atrlen=0 ;

	arc_char = fgetc( rp ) ;
	arc_attr = fgetc( rp ) & 0xdf ;
	fprintf( wp,"%c%c",arc_attr,arc_char ) ;
	norcnt += 2 ;

	while (( red_char = fgetc( rp )) != EOF ) {
		red_attr = fgetc( rp ) & 0xdf ;

		if (( arc_char == red_char ) && ( arc_attr == red_attr )) {
			if (   atrlen       )  atrlen = write_attr( wp,atrlen,chrbuf ) ;
			if ( ++chrlen == 32 )  chrlen = write_char( wp,chrlen ) ;
			continue ;
		}
		if ( chrlen )  chrlen = write_char( wp,chrlen ) ;

		if ( arc_attr == red_attr ) {
			chrbuf[atrlen] = red_char ;
			if ( ++atrlen == 32 )  atrlen = write_attr( wp,atrlen,chrbuf ) ;
			continue ;
		}
		if ( atrlen )  atrlen = write_attr( wp,atrlen,chrbuf ) ;

		arc_char = red_char ;
		arc_attr = red_attr ;
		fprintf( wp,"%c%c",arc_attr,arc_char ) ;
		norcnt += 2 ;
	}
	if ( chrlen )  write_char( wp,chrlen ) ;
	if ( atrlen )  write_attr( wp,atrlen,chrbuf ) ;
	fputc( 0x60,wp ) ;
}

/******* write character compless data ******************/

int write_char( wp,len )
FILE	*wp ;
int	len ;
{
	fputc( 0x20+len-1,wp ) ;
	chrcnt += 1 ;
	return ( 0 ) ;
}

/******* write attribute compress data ******************/

int write_attr( wp,len,buf )
FILE	*wp ;
int	len ;
unchar	*buf ;
{
	fputc( 0xa0+len-1,wp ) ;
	fwrite( buf,sizeof(unchar),len,wp ) ;
	atrcnt += len+1 ;
	return ( 0 ) ;
}
