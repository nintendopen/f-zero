;************************************************************************
;*	 PLAY_MAIN	   -- game play main module --			*
;************************************************************************
;
		INCLUDE	  RP5A22:
		INCLUDE	  RP5C77:
		INCLUDE	  VARIABLE
		INCLUDE	  BUFFER
		INCLUDE	  ALPHA
;
;=============== Cross reference =======================================
;
		EXT	  Clear_OAM,Set_demo_stick,Engine_sound,Start_enemy_snd
		EXT	  Transfer_object,Select_sound,Transfer_music
		EXT	  Init_gamescrn,Set_course_data
		EXT	  Init_demopers,Set_demopers
		EXT	  Init_gamepers,Set_gamepers
		EXT	  Init_backscrn,Init_forescrn,Set_mycar_style
		EXT	  Meter_initial1,Meter_initial2,Meter_control
		EXT	  Player_initial,Player_control,Player_display
		EXT	  Enemy_initial,Enemy_control,Turbo_ok_disp
		EXT	  BG_scroll,Car_BGcheck,Move_backscrn,Change_color
		EXT	  Initial_OBJchar,Setting_OBJ,Print_OBJmsg
		EXT	  Effect_initial,Laser_spark,Laser_frash
		EXT	  Display_course1,Display_course2,Display_retry,Disp_rank
		EXT	  Display_message,Enemy_shadow,Demo_shadow,Mycar_shadow
		EXT	  Display_READY,Display_GO,Clear_GO,Set_rank_color
		EXT	  Car_move,Set_scrl_reg,Set_scrl_count,Set_turbo_jet
		EXT	  Set_sound,Mycar_short,Crash_effect,Wing_spark
		EXT	  Set_disp_pos,Reset_location,Disp_repair_cnt
		EXT	  Check_pause,Game_pause,You_lost,Check_start
;
;=============== Cross Definition ======================================
;
		GLB	  Game_entry
		GLB	  Set_OAM_address,Map_char_offset,Scene_init
;
;=============== Define local variable =================================
;
work0		EQU	  0000H		      ; 2 byte	:Work
data_address	EQU	  0002H		      ; 3 byte	:Transfer source address
buff_address	EQU	  0005H		      ; 3 byte	:Transfer destination address
hit_status	EQU	  0010H		      ; 2 byte	:
hit_pointer	EQU	  0012H		      ; 2 byte	:
;
;=======================================================================
;
		PROG
		EXTEND
;
;************************************************************************
;*		 Set Object character					*
;************************************************************************
;
		MEM16
		IDX16
Set_OBJ_char
		PHP
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		LDA	  !course_code
		AND	  #00FFH
		TAX
		LDA	  >Map_disp_ofsH,X
		AND	  #00FFH
		STA	  !map_offset_h
		LDA	  >Map_disp_ofsV,X
		AND	  #00FFH
		STA	  !map_offset_v
		TXA
		ASL	  A
		TAX
		LDA	  >Map_char_offset,X
		TAY
		LDX	  #5000H
		PHK
		JSR	  Transfer_object     ; Transfer map character
;-----------------------------------------------------------------------
		LDX	  #5000H
		LDY	  #0000H
		PHK
		JSR	  Transfer_object     ; Transfer Object character
;-----------------------------------------------------------------------
		LDX	  #4000H
		LDY	  #2D94H
		PHK
		JSR	  Transfer_object     ; Transfer "READY GO!!"
;-----------------------------------------------------------------------
		JSR	  Set_mycar_style
		PLP
		RTS
;
;=======================================================================
Map_char_offset EQU	  0FC25CH
Map_disp_ofsH	EQU	  0FC27AH
Map_disp_ofsV	EQU	  0FC289H
;
;************************************************************************
;*		 Game play entry					*
;************************************************************************
;
		MEM8
		IDX8
Game_entry
;		LDA	  <stick_status+1     ; DEBUG
;		BIT	  #00001000B
;		BNE	  Game_entry_2	      ;
;		LDA	  <stick_trigger+1    ; DEBUG
;		BIT	  #00000100B	      ;
;		BNE	  Game_entry_2	      ;
;		RTS			      ;
;
Game_entry_2	LDA	  <game_process
		ASL	  A
		TAX
		JMP	  (!Game_vector,X)
;-----------------------------------------------------------------------
Game_vector	WORD	  Game_setscrn	      ; game screen set
		WORD	  Scene_select	      ; scene select
		WORD	  Game_start	      ; game start
		WORD	  Game_play	      ; game play
		WORD	  Game_exit	      ; game exit
		WORD	  Game_pause	      ; game pause
		WORD	  Exit_check	      ; game exit check
		WORD	  Game_lost	      ; YOU LOST entry
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Game screen set routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Game screen set entry					*
;************************************************************************
;
		MEM8
		IDX8
Game_setscrn
		LDA	  <game_status
		ASL	  A
		TAX
		JMP	  (!Setscrn_vector,X)
;-----------------------------------------------------------------------
Setscrn_vector	WORD	  Init_screen	      ; Screen initialize
		WORD	  Zoom_screen	      ; Screen zoom
		WORD	  Open_screen	      ; Screen open

;************************************************************************
;*		 Game screen initialize					*
;************************************************************************
;
		MEM8
		IDX8
Init_screen
		JSR	  Initial_screen
		JSR	  Set_scrl_reg	      ; Set scroll register
		JSR	  Set_BG1_scroll
		JSR	  Display_course1     ; Display course number
;-----------------------------------------------------------------------
		LDA	  <play_mode	      ; battle mode ?
		BNE	  Init_screen20	      ; no.
		JSR	  Display_retry
		JSR	  Disp_repair_cnt     ; Display repair count
;-----------------------------------------------------------------------
Init_screen20	LDA	  #01H
		STA	  <colwte_flag	      ; Set color write mode
		LDA	  #02H
		STA	  <pers_flag	      ; Set rotation mode
;-----------------------------------------------------------------------
		LDA	  #02H
		STZ	  <screen_zoom+0
		STA	  <screen_zoom+1      ; Set screen zoom
		JSR	  Init_demopers
		PHK
		JSR	  Set_demopers
		JSR	  Transfer_music
;-----------------------------------------------------------------------
		LDA	  #01H
		STA	  <fade_flag	      ; Set fade_in mode
		INC	  <game_status
		RTS
;
;************************************************************************
;*		 Initialize player work					*
;************************************************************************
;
		MEM8
		IDX8
System_initial
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		STZ	  <param5	      ; for DEBUG
;-----------------------------------------------------------------------
		STZ	  <OAM_counter
		STZ	  <time_counter+0
		STZ	  <time_counter+1
		STZ	  <time_counter+2     ; Set "00:00:00"
		STZ	  !time_over_flag
		STZ	  <goal_in_count
		STZ	  !alarm_counter
		STZ	  !lap_sound_cnt
;-----------------------------------------------------------------------
;;		STZ	  <rank_disp_count
;;		LDA	  #00001111B
;;		STA	  <rank_display_sw
		LDA	  #10
		STA	  !best_time_renew
		STZ	  !best_lap_renew
;-----------------------------------------------------------------------
		LDA	  #80H
		STA	  <screen_zoom+0
		STZ	  <screen_zoom+1      ; Set screen zoom
		LDA	  #11100000B
		STA	  !spot_color_B	      ; Set spot color
		STZ	  <window_color_sw    ; window sub on/color add mode
		LDA	  #01H
		STA	  !shield_meter+0
		STZ	  !shield_meter+1
		STZ	  !top_prio_sub
		STZ	  !jump_pose_flag
;-----------------------------------------------------------------------
		STZ	  <message_flag+0
		STZ	  <message_flag+1
		STZ	  <message_status
		STZ	  <exception_flag
		STZ	  <pers_selector
		STZ	  <scroll_selector
		STZ	  <laser_counter      ; Wall laser counter
		STZ	  !clear_round
;-----------------------------------------------------------------------
		LDA	  !car_angle+1
		BEQ	  System_init110
		LDA	  #192
		SEC
		SBC	  !car_angle+1
System_init110
		STA	  <screen_angle	      ; Set screen angle
;-----------------------------------------------------------------------
		JSR	  Set_scrl_count      ; Set scroll counter
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDX	  #0EH
System_init200
		STZ	  !car_spin_velo,X
		STZ	  !car_BG_flag,X
		STZ	  !car_unctrl_flag,X
		STZ	  !car_crash_flag,X
		STZ	  !car_hight,X
		STZ	  !car_speed_small,X
		STZ	  !road_status_1,X
		LDA	  #80FFH
		STA	  !round_counter,X
		STA	  !round_counter2,X
		LDA	  #0FFFFH
		STA	  !time_buffer,X
		STZ	  !car_display_h,X
		LDA	  #00F0H
		STA	  !car_display_v,X
		DEX
		DEX
		BPL	  System_init200
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Set BG1 scroll register				*
;************************************************************************
;
		MEM16
		IDX8
Set_BG1_scroll
		PHP
		REP	  #00100000B
;-----------------------------------------------------------------------
		LDY	  <scroll_selector
		LDA	  !scroll_table_H2,Y
		STA	  <BG1_scroll_h
		LDA	  !scroll_table_V2,Y
		STA	  <BG1_scroll_v
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Initial screen						*
;************************************************************************
;
		MEM8
		IDX8
Initial_screen
		LDX	  #0C0H
Initial_screen2 STZ	  <00H,X
		INX
		CPX	  #0F8H
		BNE	  Initial_screen2
;-----------------------------------------------------------------------
		JSR	  Set_course_data     ; Set course number
		JSR	  Enemy_initial	      ; Initialize enemy work
		JSR	  Player_initial      ; Initialize player work
		JSR	  System_initial      ; Initialize system work
;-----------------------------------------------------------------------
		JSR	  Init_gamescrn	      ; Initialize game screen
		JSR	  Init_forescrn	      ; Initialize fore screen
		JSR	  Init_backscrn	      ; Initialize back screen
		JSR	  Move_backscrn	      ; Set back screen
		JSR	  Meter_initial1      ; Initialize meter part 1
		JSR	  Effect_initial      ; Initialize super effect
;-----------------------------------------------------------------------
		JSR	  Clear_OAM	      ; Clear OAM
		JSR	  Initial_OBJchar     ; Initialize object
		JSR	  Set_OBJ_char	      ; DEBUG
		RTS
;
;************************************************************************
;*		 Game screen zoom					*
;************************************************************************
;
		MEM8
		IDX8
;
;=============== feed in screen ========================================
Zoom_screen
		LDA	  #02H
		STA	  <sound_status_0     ; Start ZOOM sound
		LDA	  <screen_zoom+0
		AND	  #00000100B
		BNE	  Zoom_screen100
;-----------------------------------------------------------------------
		LDA	  !const_color_R
		BEQ	  Zoom_screen100
		DEC	  A
		STA	  !const_color_R
		STA	  !const_color_G
		STA	  !const_color_B
;
;=============== Zoom decrement ========================================
Zoom_screen100
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  <screen_zoom
		SEC
		SBC	  #0004H
		CMP	  #0074H	      ; Zoom >= 74H ?
		BCS	  Zoom_screen120      ; yes.
;-----------------------------------------------------------------------
		LDA	  #0073H	      ; Zoom = 73H
		LDX	  #00010000B
		STX	  <window_color_sw    ; window sub on/color add mode
		LDX	  #02H
		STX	  <game_status	      ; Set pers start mode
;-----------------------------------------------------------------------
Zoom_screen120
		STA	  <screen_zoom	      ; Set screen zoom
		PHK
		JSR	  Set_demopers
		JSR	  Change_color	      ; Change color entry
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Game screen open					*
;************************************************************************
;
		MEM8
		IDX8
Open_screen
		JSR	  Change_color	      ; Change color entry
		LDA	  <pers_angle
		CMP	  #15		      ; pers_angle == 15 ?
		BEQ	  Open_screen200      ; yes.
		CMP	  #16		      ; pers_angle == 16 ?
		BEQ	  Open_screen300      ; yes.
;
;=============== Change pers angle =====================================
Open_screen100
		SEC
		SBC	  #11
		BCC	  Open_screen110
;-----------------------------------------------------------------------
		PHA
		JSR	  Move_backscrn	      ; Set back scroll counter
		JSR	  Set_scrl_reg	      ; Set scroll register
		LDY	  <scroll_selector
		PLX
		LDA	  !Change_raster,X
		STA	  !scroll_table_L1,Y
		LDA	  !scroll_table_V1,Y
		CLC
		ADC	  !Scroll_offset,X
		STA	  !scroll_table_V1,Y
		LDA	  <BG2_scroll_v+0
		CLC
		ADC	  !Scroll_offset,X
		STA	  <BG2_scroll_v+0     ; Set BG2 scroll V
		LDA	  #02H
		STA	  <part_flag	      ; Set mode_change 2
;-----------------------------------------------------------------------
Open_screen110
		LDA	  #01H
		STA	  <pers_flag	      ; Set pers mode
		PHK
		JSR	  Set_demopers
		INC	  <pers_angle
Open_screen190
		RTS
;
;=============== Start color add DMA ===================================
Open_screen200
		JSR	  Move_backscrn	      ; Set back scroll counter
		JSR	  Init_gamepers
		PHK
		JSR	  Set_gamepers
		LDY	  <scroll_selector
		LDA	  #2FH
		STA	  !scroll_table_L1,Y
;-----------------------------------------------------------------------
		INC	  <pers_angle
		LDA	  #01H
		STA	  <part_flag	      ; Set mode_change 1
		STA	  <window_flag	      ; Set window flag
		RTS
;
;=============== End of open mode ======================================
Open_screen300
		JSR	  Move_backscrn	      ; Set back scroll counter
		JSR	  Init_gamepers
		PHK
		JSR	  Set_gamepers
		LDY	  <scroll_selector
		LDA	  #2FH
		STA	  !scroll_table_L1,Y
;-----------------------------------------------------------------------
		INC	  <game_process
		INC	  <game_process
		STZ	  <game_status
		RTS
;
;=============== Open anime data =======================================
;
Change_raster	BYTE	  07H,13H,1EH,28H     ; mode change raster
Scroll_offset	BYTE	  28H,1CH,11H,07H     ; BG2 scroll offset
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Scene select routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Scene select entry					*
;************************************************************************
;
		MEM8
		IDX8
Scene_select
		LDA	  <game_status
		ASL	  A
		TAX
		JMP	  (!Select_vector,X)
;-----------------------------------------------------------------------
Select_vector	WORD	  Scene_init	      ; game screen set
		WORD	  Scene_main
		WORD	  Scene_next
;
;************************************************************************
;*		 Scene initialize					*
;************************************************************************
;
		MEM8
		IDX8
Scene_init
		JSR	  Initial_screen
		JSR	  Set_scrl_reg	      ; Set scroll register
		JSR	  Display_course2     ; Display course number
;-----------------------------------------------------------------------
		LDA	  #01H
		STA	  <colwte_flag	      ; Set color write mode
		STA	  <pers_flag	      ; Set rotation mode
		STA	  <part_flag	      ; Set mode_change 1
;-----------------------------------------------------------------------
		JSR	  Init_demopers
		JSR	  Init_gamepers
		PHK
		JSR	  Set_gamepers
		JSR	  Init_gamepers
		PHK
		JSR	  Set_gamepers
;-----------------------------------------------------------------------
		LDA	  #2FH
		STA	  !scroll_table_L1+00H
		STA	  !scroll_table_L1+10H
		LDA	  #11100000B
		STA	  !const_color_R
		STA	  !const_color_G
		STA	  !const_color_B
		STA	  !meter_color_R
		STA	  !meter_color_G
		STA	  !meter_color_B
;-----------------------------------------------------------------------
		LDA	  #01H
		STA	  <fade_step
		STA	  <fade_flag	      ; Set fade_in mode
		INC	  <game_status
		RTS
;
;************************************************************************
;*		 Scene select main routine				*
;************************************************************************
;
		MEM8
		IDX8
Scene_main
		LDA	  !special_demo	      ; special demo ?
		BNE	  Scene_main120
		LDA	  <demo_flag	      ; title demo ?
		BNE	  Scene_main110	      ; yes.
;-----------------------------------------------------------------------
		STZ	  <sound_status_0
		JSR	  Scene_main500	      ; Blink message
		LDA	  <stick_trigger+0    ; Push A ?
		BMI	  Scene_main100
		LDA	  <stick_trigger+1    ; Push B ?
		BMI	  Scene_main100	      ; yes.
		ASL	  A
		ASL	  A		      ; Push SELECT ?
		BMI	  Scene_main200	      ; yes.
		ASL	  A		      ; Push START ?
		BMI	  Scene_main100	      ; yes.
		ASL	  A		      ; Push UP ?
		BMI	  Scene_main210	      ; yes.
		ASL	  A		      ; Push DOWN ?
		BMI	  Scene_main200	      ; yes.
		RTS
;
;=============== Exit select mode ======================================
Scene_main100
		JSR	  Select_sound
		JSR	  Transfer_music
;-----------------------------------------------------------------------
Scene_main110	LDA	  #00010000B
		STA	  <window_color_sw    ; window sub on/color add mode
		LDA	  #01H
		STA	  <window_flag	      ; Set window flag
Scene_main120	STZ	  <game_status
		INC	  <game_process
		RTS
;
;=============== Select scene ==========================================
Scene_main200
		INC	  <game_scene
		LDA	  <game_scene
		CMP	  #07H
		BNE	  Scene_main220
		STZ	  <game_scene
		BRA	  Scene_main220
;-----------------------------------------------------------------------
Scene_main210	DEC	  <game_scene
		BPL	  Scene_main220
		LDA	  #06H
		STA	  <game_scene
;-----------------------------------------------------------------------
Scene_main220	LDA	  #01H
		STA	  <fade_step	      ; Set fade_out step
		INC	  A
		STA	  <fade_flag	      ; Set fade_out mode
		INC	  <game_status
		JSR	  Select_sound
		RTS
;
;=============== Blink message =========================================
Scene_main500
		LDA	  <frame_counter
		AND	  #00111111B
		CMP	  #24
		BCS	  Scene_main520
;-----------------------------------------------------------------------
Scene_main510	LDA	  #11111111B
		STA	  !OAM_sub+00H
		STA	  !OAM_sub+01H
		STA	  !OAM_sub+02H
		STA	  !OAM_sub+03H
		STA	  !OAM_sub+04H
		STA	  !OAM_sub+05H
		RTS
;-----------------------------------------------------------------------
Scene_main520	STZ	  !OAM_sub+00H
		STZ	  !OAM_sub+01H
		STZ	  !OAM_sub+02H
		STZ	  !OAM_sub+03H
		STZ	  !OAM_sub+04H
		STZ	  !OAM_sub+05H
		RTS

;************************************************************************
;*		 Next scene display					*
;************************************************************************
;
		MEM8
		IDX8
Scene_next
		STZ	  <part_flag	      ; Clear pert mode change flag
		STZ	  <pers_flag	      ; Clear pers flag
		STZ	  <window_flag	      ; Clear window flag
		STZ	  <colwte_flag	      ; Clear color write flag
		STZ	  <game_status	      ; Clear game status
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\\ Game start routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Game start entry					*
;************************************************************************
;
		MEM8
		IDX8
Game_start
		LDA	  <game_status
		ASL	  A
		TAX
		JMP	  (!Start_vector,X)
;-----------------------------------------------------------------------
Start_vector	WORD	  Game_initial
		WORD	  Setting_car
		WORD	  Start_engine
;
;************************************************************************
;*		 Game initialize entry					*
;************************************************************************
;
		MEM8
		IDX8
Game_initial
		JSR	  Clear_OAM	      ; Clear OAM
		JSR	  Meter_initial2      ; Initialize meter part 2
		JSR	  Effect_initial      ; Special effect initialize
		JSR	  Game_initial300     ; Transfer object color ( for demo )
		LDA	  #00000100B
		STA	  <meter_switch	      ; Set meter switch
		JSR	  Setting_OBJ	      ; for position set
		LDA	  #00000001B
		TSB	  <message_flag+0     ; Message OAM request
;
;=============== Set initial hight =====================================
Game_initial100
		LDX	  #06H
Game_initial110 LDA	  !car_position_v,X
		BMI	  Game_initial120
		LDA	  #80H
Game_initial120 INC	  A
		STA	  !car_hight+1,X      ; Set car_hight high
		LDA	  #00H
		STA	  !car_hight+0,X      ; Set car_hight low
		DEX
		DEX
		BPL	  Game_initial110
;-----------------------------------------------------------------------
		JSR	  Setting_OBJ	      ; for OAM initialize
		JSR	  Set_OAM_address
;
;=============== Set startup timer and counter =========================
Game_initial200
		LDA	  #68
		STA	  <startup_timer
		LDA	  #54H
		STA	  <startup_count
		LDA	  #01H
		STA	  <objwte_flag
;-----------------------------------------------------------------------
		LDA	  <game_world
		ASL	  A
		ASL	  A
		ADC	  <game_world
		STA	  <course_select      ; for ending
;-----------------------------------------------------------------------
		DEC	  <retry_counter
		JSR	  Display_retry
		STZ	  <end_status
		INC	  <game_status
		RTS
;
;=============== Transfer object color ( for demo ) ====================
Game_initial300
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		LDX	  #0CD00H
		LDY	  #color_buffer+100H
		LDA	  #007FH
		MVN	  #0FH,#00H	      ; Transfer object color
		SEP	  #00110000B
		RTS
;
;************************************************************************
;*		 Setting car to start line				*
;************************************************************************
;
		MEM8
		IDX8
Setting_car
		JSR	  Start_enemy_snd
		LDA	  <startup_count      ; Setting already ?
		BMI	  Setting_car300      ; yes.
;-----------------------------------------------------------------------
		LDA	  <startup_timer
		CMP	  #60		      ; Setting start ?
		BCS	  Setting_car300      ; no.
;
;=============== Set display position ==================================
Setting_car100
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDX	  #06H
Setting_car110	LDA	  <startup_count
		AND	  #00FFH
		CLC
		ADC	  !car_position_v,X   ; display_v = position_v + offset_v
		CMP	  #0100H	      ; display_v >= 100H ?
		BCS	  Setting_car120      ; yes.
;-----------------------------------------------------------------------
		LDA	  <startup_count
		EOR	  #00FFH
		INC	  A
		AND	  #00FFH
		XBA
		STA	  !car_hight,X	      ; Set offset_v
;-----------------------------------------------------------------------
		LDA	  <demo_flag
		AND	  #00FFH	      ; demo mode ?
		BEQ	  Setting_car120      ; no.
;-----------------------------------------------------------------------
		LDA	  #0400H
		STA	  !car_speed,X	      ; Set speed
		CLC
		LDA	  !car_hight,X
		ADC	  #0700H
		STA	  !car_hight,X
;-----------------------------------------------------------------------
Setting_car120	DEX
		DEX
		BPL	  Setting_car110
;
;=============== Set my car shadow =====================================
Setting_car200
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
Setting_car205	LDA	  #11100101B
		STA	  !spot_color_B	      ; Set spot color
		LDA	  <startup_count
		CLC
		ADC	  #30H
		BPL	  Setting_car210
		LDA	  #7FH
Setting_car210	STA	  !window_table+0CH   ; Set my car shadow pos V
;
;=============== Decrement startup counter =============================
Setting_car250
		LDA	  <demo_flag
		BNE	  Setting_car260
;-----------------------------------------------------------------------
		DEC	  <startup_count
		LDA	  <startup_count
		CMP	  #09H
		BCC	  Setting_car300
		DEC	  <startup_count
		BRA	  Setting_car300
;-----------------------------------------------------------------------
Setting_car260	LDA	  <startup_count
		SEC
		SBC	  #06H
		STA	  <startup_count
;
;=============== Set Object ============================================
Setting_car300
		LDA	  #00000011B
		STA	  <sound_status_1
		JSR	  Setting_OBJ
		JSR	  Set_OAM_address
		JSR	  Demo_shadow	      ; Set enemy shadow
		JSR	  Change_color	      ; Change color entry
;-----------------------------------------------------------------------
		LDA	  <demo_flag	      ; demo mode ?
		BEQ	  Setting_car400      ; no.
		LDA	  <startup_count      ; Setting already ?
		BMI	  Setting_car500      ; yes.
;
;=============== Engine start check ====================================
Setting_car400
		DEC	  <startup_timer
		BNE	  Setting_car410
;-----------------------------------------------------------------------
		JSR	  Display_READY
		LDA	  #01H
		STA	  <sound_status_0     ; Start READY sound
		INC	  <game_status
Setting_car410	RTS
;
;=============== Demo running start ====================================
Setting_car500
		LDA	  #04H
		STA	  !car_hight+1	      ; Set my car hight
		LDA	  #6CH
		STA	  <startup_count      ; for ClearGO
		STZ	  <startup_timer
		STZ	  <game_status
		INC	  <game_process
		RTS
;
;************************************************************************
;*		 Start engine						*
;************************************************************************
;
		MEM8
		IDX8
Start_engine
		JSR	  Start_enemy_snd
		LDA	  !car_hight+1
		CMP	  #04H		      ; car hight == 4 ?
		BEQ	  Start_engine300     ; yes.
;
;=============== Set car hight =========================================
Start_engine100
		LDA	  <startup_timer
		AND	  #00001111B
		BNE	  Start_engine300
;-----------------------------------------------------------------------
		LDA	  !car_hight+1
		INC	  A
		STA	  !car_hight+1	      ; Set my car hight
		STA	  !car_hight+3	      ; Set rival hight
		STA	  !car_hight+5	      ; Set rival hight
		STA	  !car_hight+7	      ; Set rival hight
;
;=============== Set display position ==================================
Start_engine300
		LDA	  <stick_status+1
		JSR	  Engine_sound
		LDA	  <stick_status+1
		JSR	  Engine_sound
		JSR	  Set_position
		JSR	  Setting_OBJ
		JSR	  Set_OAM_address
		JSR	  Enemy_shadow	      ; Set eneemy shadow
		JSR	  Change_color	      ; Change color entry
		JSR	  Display_message     ; Display message
		DEC	  <startup_timer
		BNE	  Start_engine900
;
;=============== Game start ============================================
Start_engine400
		JSR	  Set_start_speed     ; Set start speed
		JSR	  Display_GO
		STZ	  <game_status
		INC	  <game_process
Start_engine900 RTS
;
;************************************************************************
;*		 Set display position all car				*
;************************************************************************
;
		MEM8
		IDX8
Set_position
		LDX	  #0EH
Set_position10
		JSR	  Set_disp_pos
		DEX
		DEX
		BPL	  Set_position10
		RTS
;
;************************************************************************
;*		 Set car start speed					*
;************************************************************************
;
		MEM8
		IDX8
Set_start_speed
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Set my car start speed ================================
Start_speed100
		LDA	  !car_speed+1
		CMP	  #02H		      ; engine power < 2 ?
		BCC	  Start_speed120      ; yes.
		CMP	  #07H		      ; engine power high == 07H ?
		BNE	  Start_speed110      ; no.
		INC	  !car_speed+0	      ; engine power low == FFH ?
		BNE	  Start_speed110      ; no.
		LDA	  <stick_status+1     ; Accele on ?
		BPL	  Start_speed110      ; no.
;-----------------------------------------------------------------------
		LDA	  #00000010B
		TSB	  !control_status     ; Set roket start flag
		LDA	  #07H		      ; Acc = start speed
		BRA	  Start_speed150
Start_speed110	LDA	  #02H		      ; Acc = start speed
		BRA	  Start_speed150
Start_speed120	LDA	  #00H		      ; Acc = start speed
;-----------------------------------------------------------------------
Start_speed150	STA	  !car_speed+1
		STZ	  !car_speed+0
;
;=============== Set lival car start speed =============================
Start_speed200
		LDA	  #02H
		STA	  !car_speed+3
		STA	  !car_speed+5
		STA	  !car_speed+7
;-----------------------------------------------------------------------
		PLP
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Game play routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Game play entry					*
;************************************************************************
;
		MEM8
		IDX8
Game_play
		LDA	  <demo_flag
		BEQ	  Game_play_0
		PHK
		PER	  3
		JMP	  >Set_demo_stick
		LDA	  #00000001B
		STA	  <message_flag+1
;-----------------------------------------------------------------------
Game_play_0	JSR	  Check_pause	      ; Check pause mode
		BCC	  Game_play_1
		RTS
;-------------------------------------------------------------- Still --
Game_play_1
;;;;		LDA	  <still_flag	      ; for DEBUG
;;;;		BEQ	  Game_play_2
;;;;		LDA	  <stick_status+0     ; push X ? ( level )
;;;;		BMI	  Game_play_2	      ; yes.
;;;;		LDA	  <stick_trigger+0
;;;;		ASL	  A		      ; push Y ? ( trigger )
;;;;		BMI	  Game_play_2	      ; yes.
;;;;		DEC	  <frame_counter      ; frame counter no count
;;;;		JMP	  Game_play99
;-----------------------------------------------------------------------
Game_play_2	JSR	  Clear_GO
		JSR	  Car_BGcheck	      ; BG check under the car
		JSR	  Check_character     ; Check BG character
;-----------------------------------------------------------------------
		JSR	  Enemy_control	      ; Enemy control entry
		JSR	  Player_control      ; Player control entry
		JSR	  Car_move	      ; Car move entry
		JSR	  Enemy_check
;-----------------------------------------------------------------------
		PHK
		JSR	  BG_scroll	      ; BG scroll entry
		PHK
		JSR	  Set_gamepers
		JSR	  Player_display      ; Player animation
		JSR	  Setting_OBJ	      ; Set Object Attribute Memory
		JSR	  Set_OAM_address
;-----------------------------------------------------------------------
		LDA	  <explosion_count
		BNE	  Game_play_4
		LDA	  <exception_flag
		BNE	  Game_play_3
		JSR	  Mycar_shadow
		JSR	  Set_turbo_jet
Game_play_3
		JSR	  Mycar_short
		JSR	  Wing_spark
		JSR	  Laser_spark
		JSR	  Enemy_shadow	      ; Set enemy shadow
Game_play_4
		JSR	  Laser_frash
		JSR	  Crash_effect
		JSR	  Display_message
		JSR	  Change_color	      ; Change color entry
		JSR	  Meter_control	      ; Meter control entry
		JSR	  Time_count
		JSR	  Check_rank
		JSR	  Turbo_ok_disp
;-----------------------------------------------------------------------
		JSR	  Move_backscrn	      ; Set back screen scroll
		JSR	  Set_scrl_reg	      ; Set ground screen scroll
		LDA	  <pers_flag
		CMP	  #02H
		BNE	  Game_play_5
		JSR	  Set_BG1_scroll      ; Set scroll register
Game_play_5	JSR	  Set_sound
;-----------------------------------------------------------------------
		LDA	  <super_flag
		BEQ	  Game_play10
		DEC	  <super_flag
;------------------------------------------------------ Process meter --
Game_play10
;;;;		LDA	  <param5
;;;;		BEQ	  Game_play20
;;;;		LDA	  #00001100B
;;;;		STA	  >PPU_control
;;;;		STA	  <super_flag	      ; for DEBUG
;
;=============== Check game over =======================================
Game_play20
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  <end_status
		BEQ	  Game_play99
;-----------------------------------------------------------------------
		LDA	  #04H
		STA	  <game_process	      ; Clear game process counter
		STZ	  <game_status	      ; Clear game status
;-----------------------------------------------------------------------
Game_play99	RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Charater check routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Check charater entry					*
;************************************************************************
;
		MEM8
		IDX8
Check_character
		STZ	  <road_status_2
		LDX	  #0AH
;-----------------------------------------------------------------------
		LDA	  <exception_flag
		AND	  #11000000B	      ; Player explode ?
		BEQ	  Check_char100	      ; no.
		STZ	  !car_BG_flag+0AH    ; Clear BG check flag
		STZ	  !car_BG_flag+0BH    ; Clear BG check flag
		STZ	  !road_status_1+0AH
		STZ	  !road_branch+0AH
		LDX	  #08H
;
;=============== Check enemy car =======================================
Check_char100
		LDA	  !car_flag,X
		BPL	  Check_char110	      ; no.
		JSR	  Check_wall
		LDA	  !control_status,X
		AND	  #10000001B
		CMP	  #10000000B	      ; normal jumpping ?
		BNE	  Check_char110	      ; no.
		STZ	  !car_BG_flag+0,X    ; Clear BG check flag
		STZ	  !car_BG_flag+1,X    ; Clear BG check flag
Check_char110	DEX
		DEX
		BNE	  Check_char100
;
;=============== Check my car ==========================================
Check_char200
		LDA	  !short_cut_flag
		BNE	  Check_char300
;-----------------------------------------------------------------------
		JSR	  Check_wall
		LDA	  !control_status
		AND	  #10000001B
		CMP	  #10000000B	      ; normal jumpping ?
		BNE	  Check_char210	      ; no.
		STZ	  !car_BG_flag+0,X    ; Clear BG check flag
		STZ	  !car_BG_flag+1,X    ; Clear BG check flag
Check_char210	RTS
;
;=============== My car exception ======================================
Check_char300
		STZ	  !car_BG_flag+00H    ; Clear BG check flag
		STZ	  !car_BG_flag+01H    ; Clear BG check flag
		STZ	  !road_status_1+00H
		STZ	  !road_branch+00H
		RTS
;
;************************************************************************
;*		 Check laser wall					*
;************************************************************************
;
		MEM8
		IDX8
Check_wall
		STZ	  !road_status_1,X
		STZ	  !road_branch,X
;-----------------------------------------------------------------------
		LDY	  !car_BGdata+0,X
		CPY	  #82H		      ; Hit laser wall ?
		BCS	  Check_wall400	      ; no.
		CPY	  #69H		      ; Hit null character ?
		BCC	  Check_wall300	      ; no.
		BNE	  Check_wall200
;-----------------------------------------------------------------------
Check_wall010	LDA	  #10000000B
		STA	  !road_branch,X
		BRA	  Check_wall200
;-----------------------------------------------------------------------
Check_wall020	LDA	  #01000000B
		STA	  !road_branch,X
;
;=============== Hit laser wall ========================================
Check_wall200
		LDY	  !car_BG_flag+1,X    ; forced return ?
		BMI	  Check_wall550	      ; if ( out of course )
		BEQ	  Check_wall220	      ; if ( first time )
		CPY	  #03H		      ; exception ?
		BEQ	  Check_wall250	      ; yes.
		CPY	  #02H		      ; hit 2 time already ?
		BEQ	  Check_wall230	      ; yes.
;------------------------------------------------------- second time ---
Check_wall210	INC	  !car_BG_flag+1,X    ; rebound_counter = 2
		RTS
;------------------------------------------------------- first time ----
		MEM16
Check_wall220	REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !car_scrl_angle,X
		STA	  !car_old_angle,X    ; Set old_angle
		LDA	  #0120H
		STA	  !car_BG_flag,X      ; set 'rebound' mode
		BRA	  Check_wall260
;------------------------------------------------------- third time ----
Check_wall230	REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  #0340H
		STA	  !car_BG_flag,X      ; Set 'exception' mode
;------------------------------------------------------- other time ----
Check_wall250	REP	  #00100000B	      ; Memory 16 bit mode
Check_wall260	LDA	  !car_speed,X
		CMP	  #0080H
		BCS	  Check_wall290
		LDA	  #0080H
		STA	  !car_speed,X
;-----------------------------------------------------------------------
Check_wall290	SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== On laser wall =========================================
Check_wall300
		MEM8
		LDA	  #010H
		STA	  !car_BG_flag+0,X    ; Set 'on laser wall'
		STZ	  !car_BG_flag+1,X    ; Clear rebound_counter
		RTS
;
;=============== On road ===============================================
Check_wall400
		CPY	  #0A5H		      ; Hit null character 2 ?
		BEQ	  Check_wall020	      ; yes.
		CPY	  #0D0H		      ; Out of course ?
		BCS	  Check_wall500	      ; yes.
;-----------------------------------------------------------------------
		STZ	  !car_BG_flag+0,X    ; Clear BG check flag
		STZ	  !car_BG_flag+1,X    ; Clear BG check flag
		PHK
		PER	  3
		JMP	  >Check_road	     ; Check road
		RTS
;
;=============== Out of course =========================================
Check_wall500
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  #0FF80H
		STA	  !car_BG_flag,X      ; Set 'out of course'
;-----------------------------------------------------------------------
Check_wall550	REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !car_speed,X
		CMP	  #0100H	      ; car_speed >= 1 dot ?
		BCS	  Check_wall560	      ; yes.
		LDA	  #0100H
		STA	  !car_speed,X	      ; car_speed = 1 dot
;-----------------------------------------------------------------------
Check_wall560	SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Check road surface					*
;************************************************************************
;
		DATA
		MEM8
		IDX8
Check_road
		CPY	  #0C0H
		BCS	  Check_road_30
		CPY	  #0B0H
		BCS	  Check_road_20
;
;=============== Check 82H - AFH =======================================
Check_road_10
		CPY	  #0A7H
		BCC	  Check_road_11
		LDA	  #10000000B	      ; dirt zone
		BRA	  Check_road_70
;-----------------------------------------------------------------------
Check_road_11	CPY	  #0A6H
		BNE	  Check_road_12
		LDA	  #00001000B	      ; magnet down
		BRA	  Check_road_70
;-----------------------------------------------------------------------
Check_road_12	CPY	  #09EH
		BEQ	  Check_road_13
		CPY	  #0A1H
		BEQ	  Check_road_13
		CPY	  #0A2H
		BNE	  Check_road_14
Check_road_13	LDA	  #00000100B	      ; on magnet bar
		BRA	  Check_road_70
;-----------------------------------------------------------------------
Check_road_14	CPX	  #00H		      ; My car ?
		BNE	  Check_road_90	      ; no.
;-----------------------------------------------------------------------
		CPY	  #0A0H
		BNE	  Check_road_15
		LDA	  #01000000B	      ; Right magnet
		BRA	  Check_road_80
;-----------------------------------------------------------------------
Check_road_15	CPY	  #9CH
		BNE	  Check_road_90
		LDA	  #00100000B	      ; Left magnet
		BRA	  Check_road_80
;
;=============== Check B0H - BFH =======================================
Check_road_20
		CPY	  #0BAH
		BCC	  Check_road_21
		LDA	  #01000000B	      ; Jump zone
		BRA	  Check_road_70
;-----------------------------------------------------------------------
Check_road_21	CPX	  #00H		      ; My car ?
		BNE	  Check_road_90	      ; no.
		CPY	  #0B9H
		BNE	  Check_road_22
		LDA	  #00010000B	      ; pit zone
		BRA	  Check_road_80
;-----------------------------------------------------------------------
Check_road_22	LDA	  #10000000B	      ; slip zone
		BRA	  Check_road_80
;
;=============== Check C0H - CFH =======================================
Check_road_30
		CPY	  #0C3H		      ; Branch character ?
		BCC	  Check_road_40	      ; yes.
;-----------------------------------------------------------------------
		CPY	  #0C8H
		BCS	  Check_road_31
		LDA	  #00010000B	      ; Hyper dash
		BRA	  Check_road_70
;-----------------------------------------------------------------------
Check_road_31	CPY	  #0CCH
		BCC	  Check_road_32
		LDA	  #10000000B	      ; dirt road
		BRA	  Check_road_70	      ;
;-----------------------------------------------------------------------
Check_road_32	LDA	  #00100000B	      ; bomb
		BRA	  Check_road_70
;
;=============== Check road branch =====================================
Check_road_40
		LDA	  #10000000B
		CPY	  #0C0H		      ; Course branch 0	 ?
		BEQ	  Check_road_60	      ; yes.
;-----------------------------------------------------------------------
		LSR	  A
		CPY	  #0C1H		      ; Course branch 1
		BEQ	  Check_road_60	      ; yes.
;-----------------------------------------------------------------------
		LDA	  #00000000B
;
;=============== End of check ==========================================
Check_road_60
		STA	  !road_branch,X      ; Set road_branch flag
		RTL
;-----------------------------------------------------------------------
Check_road_70	STA	  !road_status_1,X    ; Set road_status_1
		RTL
;-----------------------------------------------------------------------
Check_road_80	STA	  <road_status_2      ; Set road_status_2
Check_road_90	RTL
;
;************************************************************************
;*		 Time count						*
;************************************************************************
;
		PROG
		MEM8
		IDX8
Time_count
		LDA	  <exception_flag
		AND	  #11111000B	      ; Goal in ?
		BNE	  Time_count230	      ; yes.
		LDA	  !time_over_flag     ; Time over ?
		BNE	  Time_count210	      ; yes.
;
;=============== Increment time counter ================================
Time_count100
		LDA	  <frame_counter
		LSR	  A		      ; CY = flame counter LSB
		SED			      ; BCD mode
		LDA	  <time_counter+2
		ADC	  #01H
		STA	  <time_counter+2
;
		LDA	  <time_counter+1
		ADC	  #00H
		STA	  <time_counter+1
		CMP	  #60H
		BNE	  Time_count220
;
		STZ	  <time_counter+1
		LDA	  <time_counter+0
		CLC
		ADC	  #01H
		STA	  <time_counter+0
;-----------------------------------------------------------------------
		CMP	  #10H
		BNE	  Time_count220
;
;=============== Player time out =======================================
Time_count200
		DEC	  !time_over_flag
;-----------------------------------------------------------------------
Time_count210	LDA	  #09H
		STA	  <time_counter+0
		LDA	  #59H
		STA	  <time_counter+1
		LDA	  #99H
		STA	  <time_counter+2
		LDA	  !control_status     ; jumpping ?
		BMI	  Time_count220	      ; yes.
;-----------------------------------------------------------------------
		LDA	  #06H
		STA	  !over_status
		JSR	  You_lost
;-----------------------------------------------------------------------
Time_count220	CLD			      ; Binary mode
Time_count230	RTS
;
;************************************************************************
;*		 Check my car rank					*
;************************************************************************
;
		MEM8
		IDX8
Check_rank
		LDA	  <exception_flag     ; Exception ?
		BNE	  Check_rank10	      ; yes.
		LDA	  !control_status     ; my car jumpping ?
		BMI	  Check_rank10	      ; yes.
		LDA	  !short_cut_flag
		BNE	  Check_rank10
;-----------------------------------------------------------------------
		LDA	  !mycar_rank
		CMP	  #20
		BCC	  Check_rank10
;-----------------------------------------------------------------------
		LDA	  #20
		STA	  !over_rank
		LDA	  #04H
		STA	  !over_status
		JSR	  You_lost
;-----------------------------------------------------------------------
		LDA	  #0AH
		STA	  !rank_disp_count
		JSR	  Disp_rank	      ; Display rank
Check_rank10	RTS
;
;************************************************************************
;*		 Set OAM address and length				*
;************************************************************************
;
		MEM16
		IDX8
Set_OAM_address
		REP	  #00100000B	      ; Memory 16 bit mode
		LDY	  <objwte_flag
		BEQ	  Set_OAM_addr290
		LDY	  #0AH
;
;=============== Set Clear OAM data ====================================
Set_OAM_addr100
		CPY	  <OAM_counter
		BCC	  Set_OAM_addr200
;
		LDA	  #OAM_dummy
		STA	  !OAM_address,Y
		LDA	  #0FFFFH
		STA	  !OAM_sub+11H,Y
		DEY
		DEY
		BPL	  Set_OAM_addr100
;-----------------------------------------------------------------------
Set_OAM_addr290 SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Set OAM data ==========================================
Set_OAM_addr200
		LDX	  !OAM_priority,Y
		LDA	  !Car_OAM_addr,X
		STA	  !OAM_address,Y
		LDA	  !car_OAM_sub,X
		STA	  !OAM_sub+11H,Y
		DEY
		DEY
		BPL	  Set_OAM_addr200
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Car OAM address data and length data ==================
;
Car_OAM_addr	WORD	  0300H,0320H,0340H,0360H,0380H,03A0H,OAM_dummy,OAM_dummy
;
OAM_dummy	BYTE	  80H,80H,00H,00H
		BYTE	  80H,80H,00H,00H
		BYTE	  80H,80H,00H,00H
		BYTE	  80H,80H,00H,00H
		BYTE	  80H,80H,00H,00H
		BYTE	  80H,80H,00H,00H
		BYTE	  80H,80H,00H,00H
		BYTE	  80H,80H,00H,00H
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Game exit routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Game exit entry					*
;************************************************************************
;
		MEM8
		IDX8
Game_exit
		LDA	  <demo_flag
		BNE	  Game_exit05
		LDA	  #01111111B
		TRB	  <sound_status_0     ; Music stop
Game_exit05	STZ	  <sound_status_1     ; Music stop
		STZ	  <sound_status_2     ; Music stop
		STZ	  <sound_status_3     ; Music stop
		STZ	  <sound_control      ; Music stop
;-----------------------------------------------------------------------
		LDA	  #11100000B
		STA	  !spot_color_B	      ; Clear spot color
		LDA	  <exception_flag
		AND	  #00001000B
		BEQ	  Game_exit10
		JSR	  Set_rank_color      ; Set rank window color
;-----------------------------------------------------------------------
Game_exit10	STZ	  !shield_meter+1     ; Shield meter window off
;;;;		LDA	  #10000100B
;;;;		TSB	  <meter_switch	      ; Clear meter switch
		STZ	  <objwte_flag	      ; OAM buffer normal
		JSR	  Clear_OAM	      ; Clear OAM
;-----------------------------------------------------------------------
		LDA	  <end_status
		DEC	  A
		STA	  <game_process	      ; Set game process
;-----------------------------------------------------------------------
		LDA	  #03H
		STA	  <game_mode	      ; Game over mode
		STZ	  <game_status	      ; Clear game status
		RTS
;
;************************************************************************
;*		 Game exit check					*
;************************************************************************
;
		MEM8
		IDX8
Exit_check
		JSR	  Check_start	      ; Push START ?
		BCC	  Exit_check20	      ; no.
;-----------------------------------------------------------------------
		LDA	  <play_mode
		ASL	  A
		INC	  A
		STA	  <end_status	      ; Set game end status
;-----------------------------------------------------------------------
		LDA	  #04H
		STA	  <game_process	      ; Clear game process counter
		STZ	  <game_status	      ; Clear game status
;-----------------------------------------------------------------------
		LDA	  <pers_flag
		BEQ	  Exit_check20
		LDA	  #02H
		STA	  <fade_flag
		LDA	  #10000000B
		TSB	  <sound_status_0
;-----------------------------------------------------------------------
Exit_check20	LDA	  <exception_flag
		BMI	  Exit_check30
		JSR	  Setting_OBJ	      ; Setting Object
		JSR	  Change_color	      ; Change color
Exit_check30	RTS
;
;************************************************************************
;*		 YOU LOST						*
;************************************************************************
;
		MEM8
		IDX8
Game_lost
		LDA	  #00010000B
		STA	  !Through_screen     ; Display OBJ only
;---------------------------------------------------- Display message --
		JSR	  Clear_OAM
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  #Lost_message
		JSR	  Print_OBJmsg	      ; Display "YOU LOST"
;-----------------------------------------------------------------------
		JSR	  Disp_repair_cnt     ; Display repair count
		JSR	  Display_retry
;----------------------------------------------------- Screen fade in --
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #06H
		STA	  <game_process	      ; Set exit check mode
		STZ	  <game_status
		LDA	  #01H
		STA	  <fade_flag	      ; Set fade in mode
		RTS
;=======================================================================
Lost_message	BYTE	  060H,053H,00110011B
		BYTE	  _Y,_O,_U,SP,_L,_O,_S,_T,0
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Check enemy hit \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Enemy hit check entry					*
;************************************************************************
;
		MEM8
		IDX8
Enemy_check
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Check main ============================================
Enemy_check100
		LDX	  #02H		      ; IX = main pointer
Enemy_check110	LDA	  !car_flag,X
		AND	  #10001000B
		CMP	  #10001000B	      ; main enemy car active ?
		BNE	  Enemy_check120      ; no.
;-----------------------------------------------------------------------
		LDA	  !car_hight+1,X
		CMP	  #10H		      ; main enemy car jumpping ?
		BCS	  Enemy_check120      ; yes.
;-----------------------------------------------------------------------
		LDA	  !car_position_v,X
		CMP	  #40H
		BCC	  Enemy_check120
		JSR	  Enemy_check200      ; Check sub
;-----------------------------------------------------------------------
Enemy_check120	INX
		INX
		CPX	  #0AH		      ; last enemy car ?
		BNE	  Enemy_check110      ; no.
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== Check sub =============================================
Enemy_check200
		TXY			      ; IY = sub pointer
Enemy_check210	INY
		INY
		LDA	  !car_flag,Y
		AND	  #10001000B
		CMP	  #10001000B	      ; sub enemy car active ?
		BNE	  Enemy_check220      ; no.
;-----------------------------------------------------------------------
		LDA	  !car_hight+1,X
		CMP	  #10H		      ; sub enemy car jumpping ?
		BCS	  Enemy_check220      ; yes.
		JSR	  Enemy_check300
;-----------------------------------------------------------------------
Enemy_check220	CPY	  #0AH
		BNE	  Enemy_check210
		RTS
;
;=============== Check position v ======================================
Enemy_check300
		LDA	  !car_flag+1
		LSR	  A		      ; My car reverse ?
		BCS	  Enemy_check500      ; yes.
;-----------------------------------------------------------------------
		LDA	  !car_position_v,Y
		CMP	  #40H
		BCC	  Enemy_check320
;-----------------------------------------------------------------------
		STY	  <hit_pointer
		SEC
		SBC	  !car_position_v,X   ; Car_Y - Car_X >= 0 ?
		BCS	  Enemy_check310      ; yes.
		STX	  <hit_pointer
		EOR	  #0FFH
		INC	  A
;-----------------------------------------------------------------------
Enemy_check310	CMP	  #11H		      ; distance_v < 11H ?
		BCC	  Enemy_check400      ; yes. ( hit )
;-----------------------------------------------------------------------
Enemy_check320	RTS
;
;=============== Check position h ======================================
Enemy_check400
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		STZ	  <hit_status
		LDA	  !car_position_h,Y
		SEC
		SBC	  !car_position_h,X   ; Car_Y - CAR_X >= 0 ?
		BPL	  Enemy_check410      ; yes.
		EOR	  #0FFFFH
		INC	  A
		INC	  <hit_status	      ; hit left
;-----------------------------------------------------------------------
Enemy_check410	CMP	  #0008H	      ; distance_x < 10H
		BCC	  Enemy_crash	      ; yes. ( crash )
		CMP	  #0018H	      ; distance_x < 20H
		BCC	  Enemy_graze	      ; yes. ( graze )
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Check position v ======================================
Enemy_check500
		MEM8
		LDA	  !car_position_v,Y
		CMP	  #40H
		BCC	  Enemy_check520
;-----------------------------------------------------------------------
		STX	  <hit_pointer
		SEC
		SBC	  !car_position_v,X   ; Car_Y - Car_X >= 0 ?
		BCS	  Enemy_check510      ; yes.
		STY	  <hit_pointer
		EOR	  #0FFH
		INC	  A
;-----------------------------------------------------------------------
Enemy_check510	CMP	  #11H		      ; distance_v < 11H ?
		BCC	  Enemy_check600      ; yes. ( hit )
;-----------------------------------------------------------------------
Enemy_check520	RTS
;
;=============== Check position h ======================================
Enemy_check600
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDA	  #00000001B
		STA	  <hit_status
		LDA	  !car_position_h,Y
		SEC
		SBC	  !car_position_h,X   ; Car_Y - CAR_X >= 0 ?
		BPL	  Enemy_check610      ; yes.
		EOR	  #0FFFFH
		INC	  A
		STZ	  <hit_status	      ; hit right
;-----------------------------------------------------------------------
Enemy_check610	CMP	  #0008H	      ; distance_x < 10H
		BCC	  Enemy_crash	      ; yes. ( crash )
		CMP	  #0018H	      ; distance_x < 20H
		BCC	  Enemy_graze	      ; yes. ( graze )
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Enemy crash						*
;************************************************************************
;
		MEM16
		IDX8
Enemy_crash
		PHX
		LDX	  <hit_pointer
;-----------------------------------------------------------------------
		LDA	  !car_speed,X
		SEC
		SBC	  #0040H
		BCS	  Enemy_crash110
		LDA	  #0000H
Enemy_crash110	STA	  !car_speed,X
		PLX
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS

;************************************************************************
;*		 Enemy graze						*
;************************************************************************
;
		MEM8
		IDX8
Enemy_graze
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  <hit_status
		BNE	  Enemy_graze200
;
;=============== Graze right ===========================================
Enemy_graze100
		LDA	  !car_scrl_angle+1,X
		SEC
		SBC	  #04H
		BCS	  Enemy_graze110
		ADC	  #192
Enemy_graze110
		STA	  !car_scrl_angle+1,X
;-----------------------------------------------------------------------
		LDA	  !car_scrl_angle+1,Y
		CLC
		ADC	  #04H
		CMP	  #192
		BCC	  Enemy_graze120
		SBC	  #192
Enemy_graze120
		STA	  !car_scrl_angle+1,Y
		RTS
;
;=============== Graze left ============================================
Enemy_graze200
		LDA	  !car_scrl_angle+1,Y
		SEC
		SBC	  #04H
		BCS	  Enemy_graze210
		ADC	  #192
Enemy_graze210
		STA	  !car_scrl_angle+1,Y
;-----------------------------------------------------------------------
		LDA	  !car_scrl_angle+1,X
		CLC
		ADC	  #04H
		CMP	  #192
		BCC	  Enemy_graze220
		SBC	  #192
Enemy_graze220
		STA	  !car_scrl_angle+1,X
		RTS
;
		END
