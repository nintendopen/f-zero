;************************************************************************
;*	 EFFECT		   -- special effect module --			*
;************************************************************************
;
		INCLUDE	  RP5A22:
		INCLUDE	  RP5C77:
		INCLUDE	  VARIABLE
		INCLUDE	  BUFFER
;
;=============== Cross reference =======================================
;
		EXT	  zako_number,car_selecta
		EXT	  Shadow_window_1,Shadow_window_2,Shadow_window_3
		EXT	  Mycar1_jet_pos,Mycar2_jet_pos,Mycar3_jet_pos,Mycar4_jet_pos
;
;=============== Cross Definition ======================================
;
		GLB	  Effect_initial,Laser_spark,Set_laser_color,Laser_frash
		GLB	  Clear_carcolor,Reset_carcolor,Turbo_ok_disp
		GLB	  Clear_roadcolor,Reset_roadcolor
		GLB	  Change_color,Crash_effect,Wing_spark
		GLB	  Mycar_shadow,Enemy_shadow,Demo_shadow,Set_turbo_jet,Mycar_short
		GLB	  Pull_beam_start,Pull_beam_end,Pull_beam_move,Display_UFO
;
;=============== Define constant =======================================
;
object_color	EQU	  0FCD00H
;
;=============== Define local variable =================================
;
work0		EQU	  0000H		      ; 2 byte	:Work 0
work1		EQU	  0002H		      ; 2 byte	:Work 1
work2		EQU	  0004H		      ; 2 byte	:Work 2
shadow_ofs_h	EQU	  0010H		      ; 2 byte	:Shadow offset h
shadow_pos_v	EQU	  0012H		      ; 1 byte	:Shadow position v
shadow_main	EQU	  0013H		      ; 1 byte	:Shadow display OAM main pointer
shadow_sub	EQU	  0014H		      ; 1 byte	:Shadow display OAM sub pointer
OAM_sub_data	EQU	  0015H		      ; 1 byte	:Shadow OAM sub data
shadow_ofs_v	EQU	  0016H		      ; 1 byte	:Shadow offset v
spark_data_addr EQU	  0017H		      ; 3 byte	:Spark OAM data address
;-----------------------------------------------------------------------
mycar_position	EQU	  0020H		      ; 2 byte	:My car display position
;
;=======================================================================
;
		PROG
		EXTEND
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Super effect initialize routines \\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Super effect initialize main				*
;************************************************************************
;
		MEM16
		IDX8
Effect_initial
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
		SEP	  #00010000B	      ; Index 8 bit mode
;
;=============== Set road color buffer =================================
Effect_init200
		LDY	  #00H
		LDA	  !color_buffer+032H
		JSR	  Effect_init900
		LDA	  !color_buffer+034H
		JSR	  Effect_init900
		LDA	  !color_buffer+04EH
		JSR	  Effect_init900
;
;=============== Set laser beam color ( RGB ) ==========================
Effect_init300
		LDX	  #00H
		LDY	  #24H
Effect_init310	LDA	  !color_buffer,Y
		JSR	  Init_laser_col
		INY
		INY
		TXA
		CLC
		ADC	  #0010H
		TAX
		CPY	  #2EH
		BNE	  Effect_init310
;
;=============== My car turbo jet initial ==============================
Effect_init400
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		STZ	  <turbo_status
		LDA	  <mycar_number
		ASL	  A
		TAY
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !Turbo_jump_tbl,Y
		STA	  <turbo_jump_ptr
;
;=============== Color effect work initialize ==========================
Effect_init500
		MEM8
		SEP	  #00100000B
		STZ	  <color_counter
		PLP			      ; Restore memory
		RTS
;
;=============== Set road color buffer =================================
Effect_init900
		MEM16
		STA	  !road_color_init,Y  ; Set road initial color
		LSR	  A
		AND	  #0011110111101111B
		STA	  !road_color_half,Y  ; Set road half color
		INY
		INY
		RTS
;
;=======================================================================
Turbo_jump_tbl	WORD	  Mycar1_turbo
		WORD	  Mycar2_turbo
		WORD	  Mycar3_turbo
		WORD	  Mycar4_turbo
;
;************************************************************************
;*		 Set laser color					*
;************************************************************************
;
		MEM16
		IDX8
Init_laser_col
		STA	  !laser_color+00H,X  ; Buffer+00H = color +0
		LDA	  #0111111111111111B
		STA	  !laser_color+08H,X  ; Buffer+0AH = white
		STA	  !laser_color+0AH,X  ; Buffer+0AH = white
		STA	  !laser_color+0CH,X  ; Buffer+0CH = white
;-----------------------------------------------------------------------
		SEC
		SBC	  !laser_color+00H,X
		LSR	  A
		AND	  #0011110111101111B
		STA	  <work1
		LSR	  A
		AND	  #0001110011100111B
		STA	  <work2
;-----------------------------------------------------------------------
		CLC
		LDA	  !laser_color+00H,X
		ADC	  <work1
		STA	  !laser_color+0EH,X  ; Buffer+0EH = color +1/2
		STA	  !laser_color+04H,X  ; Buffer+06H = color +1/2
		ADC	  <work2
		STA	  !laser_color+06H,X  ; Buffer+08H = color +3/4
		LDA	  !laser_color+00H,X
		ADC	  <work2
		STA	  !laser_color+02H,X  ; Buffer+04H = color +1/4
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Wing spark routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Wing spark routine					*
;************************************************************************
;
		MEM8
		IDX8
Wing_spark
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <exception_flag     ; Exception ?
		BNE	  Wing_spark110	      ; yes.
		LDA	  !control_status     ; jumpping ?
		BMI	  Wing_spark100	      ; yes.
		LDA	  !car_speed+1
		CMP	  #04H		      ; mycar speed < 400H ?
		BCC	  Wing_spark100	      ; yes.
		LDA	  <wing_spark_cnt     ; spark now ?
		BNE	  Wing_spark200	      ; yes.
;
;=============== Clear Spark ===========================================
Wing_spark100
		LDA	  #0F0H
		STA	  !OAM_main+0BDH      ; Clear object
		STZ	  <wing_spark_cnt
		BRA	  Wing_spark190
;-----------------------------------------------------------------------
Wing_spark110	BIT	  #00010000B	      ; Ending ?
		BEQ	  Wing_spark100	      ; no.
Wing_spark190	PLP
		RTS
;
;=============== Check spark timming ===================================
Wing_spark200
		LDY	  #00000000B	      ; IY = data pointer
		DEC	  A
		BPL	  Wing_spark210
		LDY	  #00010000B
		AND	  #01111111B
Wing_spark210	STY	  <work0
;-----------------------------------------------------------------------
		BIT	  #00001000B	      ; BIG spark ?
		BEQ	  Wing_spark400	      ; no.
;
;=============== Big spark =============================================
Wing_spark300
		AND	  #00000001B
		ASL	  A
		ASL	  A
		ORA	  <work0
		TAX
		LDA	  #10000000B
		STA	  !OAM_sub+0BH	      ; Set OAM sub
		LDA	  #00010000B
		TSB	  !crash_priority     ; Set SOUND
		BRA	  Wing_spark500
;
;=============== Small spark ===========================================
Wing_spark400
		AND	  #00000010B
		ASL	  A
		ORA	  #00001000B
		ORA	  <work0
		TAX
		STZ	  !OAM_sub+0BH	      ; Set OAM sub
;
;=============== Set OAM brock =========================================
Wing_spark500
		LDA	  >Wing_spark_data+3,X
		STA	  !OAM_main+0BFH      ; Set character attribute
		LDA	  >Wing_spark_data+2,X
		STA	  !OAM_main+0BEH      ; Set character number
		LDA	  >Wing_spark_data+1,X
		CLC
		ADC	  !car_display_v
		STA	  !OAM_main+0BDH      ; Set character number
		LDA	  >Wing_spark_data+0,X
		CLC
		ADC	  !car_display_h
		STA	  !OAM_main+0BCH      ; Set character number
;-----------------------------------------------------------------------
Wing_spark510	LDA	  <wing_spark_cnt     ; spark now ?
		INC	  A
		ASL	  A
		PHP
		CMP	  #00010110B
		BNE	  Wing_spark520
		LDA	  #00000010B
Wing_spark520	PLP
		ROR	  A
		STA	  <wing_spark_cnt
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=======================================================================
Wing_spark_data EQU	  0FC200H
;					 VHPPCCC
;ing_spark_data BYTE	  0E0H,0FCH,04CH,00110111B	; BIG
;		BYTE	  0E0H,0FCH,04CH,11110111B
;		BYTE	  0E8H,0FCH,04DH,00110111B	; SMALL
;		BYTE	  0E8H,0FCH,05CH,10110111B
;-----------------------------------------------------------------------
;		BYTE	  010H,0FCH,04CH,01110111B	; BIG
;		BYTE	  010H,0FCH,04CH,10110111B
;		BYTE	  010H,0FCH,04DH,01110111B	; SMALL
;		BYTE	  010H,0FCH,05CH,11110111B
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Laser spark routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Laser wall color frash					*
;************************************************************************
;
		MEM8
		IDX8
Laser_frash
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Check laser wall frashing =============================
Laser_frash100
		LDA	  <laser_counter      ; Laser frash ?
		BEQ	  Laser_frash200      ; no.
		DEC	  A		      ; --counter == 0 ?
		STA	  <laser_counter
		JSR	  Set_laser_color     ; Set laser wall color
;
;=============== Check road color clear ================================
Laser_frash200
		LDA	  <road_col_count
		BEQ	  Laser_frash220
		DEC	  A
		STA	  <road_col_count
		BEQ	  Laser_frash210
;-----------------------------------------------------------------------
		CMP	  #08H
		BNE	  Laser_frash220
		JSR	  Clear_roadcolor     ; Clear road color
		BRA	  Laser_frash220
;-----------------------------------------------------------------------
Laser_frash210	JSR	  Reset_roadcolor     ; Reset road color
Laser_frash220	PLP
		RTS
;
;************************************************************************
;*		 Laser spark entry					*
;************************************************************************
;
		MEM8
		IDX8
Laser_spark
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <spark_counter      ; Spark ?
		BEQ	  Laser_spark190      ; no.
		DEC	  A		      ; --counter
		STA	  <spark_counter
		BIT	  #00001111B	      ; check counter
		BEQ	  Laser_spark100      ; if ( counter == 0    )
		ASL	  A
		BCS	  Laser_spark200      ; if ( spark wall beam )
		BMI	  Laser_spark300      ; if ( spark jump down )
		BRA	  Laser_spark190      ; else
;
;=============== End of spark ==========================================
Laser_spark100
		LDA	  #0F0H
		STA	  !OAM_main+1C1H
		STA	  !OAM_main+1C5H
		STA	  !OAM_main+1C9H      ; Clear spark position
		STZ	  <spark_counter      ; Reset spark flag
;-----------------------------------------------------------------------
Laser_spark190	PLP
		RTS
;
;=============== Display spark of wall beam ============================
Laser_spark200
		LDA	  <spark_counter
		CMP	  #8AH
		BNE	  Laser_spark205
		LDA	  #00100000B
		TSB	  !crash_priority     ; Set SOUND
;-----------------------------------------------------------------------
Laser_spark205	LDA	  <spark_counter
		AND	  #00001111B
		DEC	  A
		TAY
		LDX	  #00H
		LDA	  !Spark_color_flg,Y
		BEQ	  Laser_spark220      ; if ( not color change )
		DEC	  A
		BEQ	  Laser_spark210      ; if ( color clear )
;-----------------------------------------------------------------------
		JSR	  Reset_carcolor      ; Reset my car color
		BRA	  Laser_spark220
Laser_spark210	JSR	  Clear_carcolor      ; Clear my car color
;-----------------------------------------------------------------------
Laser_spark220	LDA	  <spark_counter
		AND	  #00001111B
		DEC	  A
		ASL	  A
		BRA	  Laser_spark500
;
;=============== Display spark of jump down ============================
Laser_spark300
		LDX	  #00H
		LDA	  <spark_counter
		AND	  #00001111B
;-----------------------------------------------------------------------
		CMP	  #04H
		BNE	  Laser_spark310
		JSR	  Clear_carcolor      ; Clear my car color
		BRA	  Laser_spark320
;-----------------------------------------------------------------------
Laser_spark310	CMP	  #02H
		BNE	  Laser_spark320
		JSR	  Reset_carcolor      ; Clear my car color
;-----------------------------------------------------------------------
Laser_spark320	LDA	  <spark_counter
		AND	  #00001111B
		ASL	  A
		ADC	  #12H
;
;=============== Display spark =========================================
Laser_spark500
		TAY
		LDA	  !Spark_OAM_table+0,Y
		STA	  <spark_data_addr+0
		LDA	  !Spark_OAM_table+1,Y
		STA	  <spark_data_addr+1
		LDY	  #0BH
		STY	  <spark_data_addr+2
;-----------------------------------------------------------------------
Laser_spark510	LDA	  [<spark_data_addr],Y
		BEQ	  Laser_spark520
		STA	  !OAM_main+1C0H,Y    ; Set character attribute
		DEY
		LDA	  [<spark_data_addr],Y
		STA	  !OAM_main+1C0H,Y    ; Set character number
		DEY
		LDA	  [<spark_data_addr],Y
		CLC
		ADC	  !car_display_v
		STA	  !OAM_main+1C0H,Y    ; Set character number
		DEY
		LDA	  [<spark_data_addr],Y
		CLC
		ADC	  !car_display_h
		STA	  !OAM_main+1C0H,Y    ; Set character number
		DEY
		BPL	  Laser_spark510
;-----------------------------------------------------------------------
		PLP
		RTS
;-----------------------------------------------------------------------
Laser_spark520	DEY
		DEY
		LDA	  #0F0H
		STA	  !OAM_main+1C0H,Y
		DEY
		DEY
		BPL	  Laser_spark510
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== Spark OAM data ========================================
;
Spark_color_flg BYTE	  2,1,0,2,0,1,0,2,0,1
;
Spark_OAM_table WORD	  Wall_spark_01&0FFFFH	      ; 00
		WORD	  Wall_spark_02&0FFFFH	      ; 02
		WORD	  Wall_spark_03&0FFFFH	      ; 04
		WORD	  Wall_spark_04&0FFFFH	      ; 06
		WORD	  Wall_spark_05&0FFFFH	      ; 08
		WORD	  Wall_spark_06&0FFFFH	      ; 0A
		WORD	  Wall_spark_07&0FFFFH	      ; 0C
		WORD	  Wall_spark_08&0FFFFH	      ; 0E
		WORD	  Wall_spark_09&0FFFFH	      ; 10
		WORD	  Wall_spark_10&0FFFFH	      ; 12
		WORD	  Jump_spark_01&0FFFFH	      ; 14
		WORD	  Jump_spark_02&0FFFFH	      ; 16
		WORD	  Jump_spark_03&0FFFFH	      ; 18
		WORD	  Jump_spark_04&0FFFFH	      ; 1A
;-----------------------------------------------------------------------
Wall_spark_10	EQU	  0BEDC0H
Wall_spark_09	EQU	  0BEDCCH
Wall_spark_08	EQU	  0BEDD8H
Wall_spark_07	EQU	  0BEDE4H
Wall_spark_06	EQU	  0BEDF0H
Wall_spark_05	EQU	  0BEDFCH
Wall_spark_04	EQU	  0BEE08H
Wall_spark_03	EQU	  0BEE14H
Wall_spark_02	EQU	  0BEE20H
Wall_spark_01	EQU	  0BEE2CH
Jump_spark_04	EQU	  0BEE38H
Jump_spark_03	EQU	  0BEE44H
Jump_spark_02	EQU	  0BEE50H
Jump_spark_01	EQU	  0BEE5CH
;
;************************************************************************
;*		 Set laser wall color					*
;************************************************************************
;
		MEM8
		IDX8
Set_laser_color
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		ASL	  A
		TAX
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !laser_color+00H,X
		STA	  !color_buffer+24H   ; beam color 1
		LDA	  !laser_color+10H,X
		STA	  !color_buffer+26H   ; beam color 2
		LDA	  !laser_color+20H,X
		STA	  !color_buffer+28H   ; beam color 3
		LDA	  !laser_color+30H,X
		STA	  !color_buffer+2AH   ; beam color 4
		LDA	  !laser_color+40H,X
		STA	  !color_buffer+2CH   ; beam color 5
;-----------------------------------------------------------------------
		PLP			      ; Restore mode
		RTS
;
;************************************************************************
;*		 Clear road color					*
;************************************************************************
;
		MEM16
Clear_roadcolor
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDA	  !road_color_half+00H
		STA	  !color_buffer+032H
		LDA	  !road_color_half+02H
		STA	  !color_buffer+034H
		LDA	  !road_color_half+04H
		STA	  !color_buffer+04EH
;-----------------------------------------------------------------------
		PLP			      ; Restore mode
		RTS
;
;************************************************************************
;*		 Reset road color					*
;************************************************************************
;
		MEM16
Reset_roadcolor
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDA	  !road_color_init+00H
		STA	  !color_buffer+032H
		LDA	  !road_color_init+02H
		STA	  !color_buffer+034H
		LDA	  !road_color_init+04H
		STA	  !color_buffer+04EH
;-----------------------------------------------------------------------
		PLP			      ; Restore mode
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Car crash effect routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Car crash effect					*
;************************************************************************
;
		MEM8
		IDX8
Crash_effect
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Check color change car main ===========================
Crash_effect100
		LDX	  #0AH
Crash_effect110 PHX
		LDA	  !carcol_change,X    ; car changed ?
		BEQ	  Crash_effect120     ; no.
		BMI	  Crash_effect300
		BRA	  Crash_effect200
;-----------------------------------------------------------------------
Crash_effect120 PLX
		DEX
		DEX
		BPL	  Crash_effect110
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== Change color ( car crash ) ============================
Crash_effect200
		LDA	  !car_flag,X
		AND	  #10001100B
		CMP	  #10001000B	      ; enemy car active ?
		BNE	  Crash_effect500     ; no.
;-----------------------------------------------------------------------
		DEC	  !carcol_change,X    ; Decrement counter
		LDA	  !carcol_change,X
		CMP	  #06H		      ; counter == 6 ?
		BEQ	  Crash_effect510     ; yes.
		CMP	  #08H		      ; counter == 8 ?
		BEQ	  Crash_effect520     ; yes.
		BRA	  Crash_effect120
;
;=============== Change color ( car bomb ) =============================
Crash_effect300
		LDA	  !car_flag,X
		AND	  #10001100B
		CMP	  #10001000B	      ; enemy car active ?
		BNE	  Crash_effect500     ; no.
;-----------------------------------------------------------------------
		DEC	  !carcol_change,X    ; Decrement counter
		LDA	  !carcol_change,X
		AND	  #01111111B	      ; counter == 0 ?
		BEQ	  Crash_effect500     ; yes.
		CMP	  #04H		      ; counter == 4 ?
		BEQ	  Crash_effect520     ; yes.
		BRA	  Crash_effect120
;
;=============== Reset car color =======================================
;
Crash_effect500 STZ	  !carcol_change,X
Crash_effect510 JSR	  Reset_carcolor
		BRA	  Crash_effect120     ; Reset car color
;-----------------------------------------------------------------------
Crash_effect520 JSR	  Clear_carcolor      ; Clear car color
		BRA	  Crash_effect120
;
;************************************************************************
;*		 Clear car color					*
;************************************************************************
;
		MEM8
		IDX8
Clear_carcolor
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		JSR	  Calc_carcol_idx
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
Clear_carcol10	LDA	  !car_clear_buff,X
		STA	  !color_buffer+100H,X
		INX
		INX
		DEY
		BNE	  Clear_carcol10
;-----------------------------------------------------------------------
Clear_carcol20	PLP
		RTS
;
;************************************************************************
;*		 Reset car color					*
;************************************************************************
;
		MEM8
		IDX8
Reset_carcolor
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		JSR	  Calc_carcol_idx
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
Reset_carcol10	LDA	  !car_color_buff,X
		STA	  !color_buffer+100H,X
		INX
		INX
		DEY
		BNE	  Reset_carcol10
;-----------------------------------------------------------------------
Reset_carcol20	PLP
		RTS
;
;************************************************************************
;*		 Calculate car color index				*
;************************************************************************
;
		MEM8
		IDX8
Calc_carcol_idx
		LDA	  !car_char_num+1,X
		LSR	  A
		AND	  #00000111B
		CMP	  #03H
		BEQ	  Calc_carcol_ix2
		TAX
		LDA	  !Car_clrcol_ptr,X
		LDY	  !Car_clrcol_len,X
		TAX
		RTS
;-----------------------------------------------------------------------
Calc_carcol_ix2 PLA
		PLA				; pull return address
		PLP
		RTS
;
;=======================================================================
;
Car_clrcol_ptr	BYTE	  000H,020H,040H,060H,080H,0A0H,0C0H,0E0H
Car_clrcol_len	BYTE	  008H,008H,008H,008H,010H,010H,010H,010H
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Change color palet routines \\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Change color palet  entry				*
;************************************************************************
;
		MEM8
		IDX8
Change_color
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		JSR	  Course_color	      ; Change course color
		JSR	  Enemy_jet_color     ; Set enemy jet color
		JSR	  Mycar_jet_color     ; Set my car jet color
		JSR	  Bomb_enemy_col      ; Change bomb enemy color
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Bomb enemy color change				*
;************************************************************************
;
		MEM8
		IDX8
Bomb_enemy_col
		LDA	  <frame_counter
		BIT	  #00000011B	      ; Change timming ?
		BNE	  Bomb_emy_col130     ; no.
		BIT	  #00000100B	      ; Clear timming ?
		BEQ	  Bomb_emy_col110     ; no.
		MEM16
		IDX16
;-----------------------------------------------------------------------
Bomb_emy_col100 REP	  #00110000B	      ; Memory,Index 16 bit mode
		LDX	  #car_clear_buff+60H ; IX = clear color address
		BRA	  Bomb_emy_col120
;-----------------------------------------------------------------------
Bomb_emy_col110 REP	  #00110000B	      ; Memory,Index 16 bit mode
		LDX	  #car_color_buff+60H ; IX = car color address
Bomb_emy_col120 LDY	  #color_buffer+160H
		LDA	  #000FH
		MVN	  #00H,#00H
;-----------------------------------------------------------------------
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
Bomb_emy_col130 RTS
;
;************************************************************************
;*		 Course color change					*
;************************************************************************
;
		MEM8
		IDX16
Course_color
		REP	  #00010000B	      ; Index 16 bit mode
;
;=============== Change course common color 1 ==========================
Course_color100
		INC	  <color_counter
		LDA	  <color_counter
		AND	  #00000011B	      ; color change timming 1 ?
		BEQ	  Course_color120     ; yes.
		DEC	  A		      ; color change timming 2 ?
		BEQ	  Course_color130     ; yes.
		LDA	  <demo_flag	      ; demo mode ?
		BNE	  Course_color200     ; yes.
;-----------------------------------------------------------------------
		LDX	  #0000H	      ; IX = write color
		LDA	  !round_counter+0    ; my car final lap ?
		BMI	  Course_color110     ; no.
		CMP	  #04H
		BCC	  Course_color110     ; no.
		LDA	  !course_half
		BEQ	  Course_color110     ; no.
;-----------------------------------------------------------------------
		LDA	  <color_counter
		AND	  #00000100B
		BEQ	  Course_color110
		LDX	  #03E0H	      ; IX = write color
Course_color110 STX	  !color_buffer+5EH
		BRA	  Course_color200
;-----------------------------------------------------------------------
Course_color120 LDX	  !color_buffer+54H
		LDY	  !color_buffer+56H
		STX	  !color_buffer+56H
		STY	  !color_buffer+54H
		BRA	  Course_color200
;-----------------------------------------------------------------------
Course_color130 LDX	  !color_buffer+58H
		LDY	  !color_buffer+5AH
		STX	  !color_buffer+5AH
		STY	  !color_buffer+58H
;
;=============== Change course common color 2 ==========================
Course_color200
		IDX8
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		SEP	  #00010000B	      ; Index 8 bit mode
		LDX	  !course_number
		CPX	  #01H
		BEQ	  Course_color330
		CPX	  #05H
		BEQ	  Course_color330
		CPX	  #08H
		BEQ	  Course_color400
;-----------------------------------------------------------------------
		LDA	  !color_buffer+06EH
		PHA
		LDA	  !color_buffer+08EH
		PHA
		LDA	  !color_buffer+0AEH
		PHA
;-----------------------------------------------------------------------
		LDX	  #0CH
Course_color210 LDA	  !color_buffer+060H,X
		STA	  !color_buffer+062H,X
		LDA	  !color_buffer+080H,X
		STA	  !color_buffer+082H,X
		LDA	  !color_buffer+0A0H,X
		STA	  !color_buffer+0A2H,X
		DEX
		DEX
		BPL	  Course_color210
;-----------------------------------------------------------------------
		PLA
		STA	  !color_buffer+0A0H
		PLA
		STA	  !color_buffer+080H
		PLA
		STA	  !color_buffer+060H
;
;=============== Change course 1,5 color ===============================
Course_color300
		LDX	  !course_number
		BEQ	  Course_color310
		CPX	  #04H
		BNE	  Course_color330
;-----------------------------------------------------------------------
Course_color310 LDA	  !color_buffer+09EH
		PHA
;-----------------------------------------------------------------------
		LDX	  #0CH
Course_color320 LDA	  !color_buffer+090H,X
		STA	  !color_buffer+092H,X
		DEX
		DEX
		BPL	  Course_color320
;-----------------------------------------------------------------------
		PLA
		STA	  !color_buffer+090H
;-----------------------------------------------------------------------
Course_color330 SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Change course 9 color =================================
Course_color400
		LDA	  !color_buffer+06EH
		PHA
;-----------------------------------------------------------------------
		LDX	  #0CH
Course_color410 LDA	  !color_buffer+060H,X
		STA	  !color_buffer+062H,X
		DEX
		DEX
		BPL	  Course_color410
;-----------------------------------------------------------------------
		PLA
		STA	  !color_buffer+060H
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Set enemy car jet color				*
;************************************************************************
;
		MEM8
		IDX8
Enemy_jet_color
		LDA	  <frame_counter
		AND	  #00000011B
;
;=============== Set enemy jet color ===================================
Enemy_jetcol100
		PHA
		JSR	  Enemy_jetcol500     ; IY = color pointer
		BCS	  Enemy_jetcol200     ; if ( do not use work )
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !car_speed,X
		CMP	  #08FFH
		BCS	  Enemy_jetcol110     ; set max color
		CMP	  #0500H
		BCC	  Enemy_jetcol120     ; set min color
;-----------------------------------------------------------------------
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		XBA
		SEC
		SBC	  #50
		BRA	  Enemy_jetcol130
;-----------------------------------------------------------------------
Enemy_jetcol110 LDA	  #93		      ; Acc = max color level
		BRA	  Enemy_jetcol130
Enemy_jetcol120 LDA	  #30		      ; Acc = min color level
Enemy_jetcol130 JSR	  Set_jet_color
		STA	  !color_buffer+10EH,Y
;
;=============== Clear enemy jet color =================================
Enemy_jetcol200
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
;-----------------------------------------------------------------------
		PLA
		EOR	  #00000010B
		JSR	  Enemy_jetcol500     ; IY = color pointer
		BCS	  Enemy_jetcol290     ; if ( do not use work )
;-----------------------------------------------------------------------
		LDA	  #00H
		STA	  !color_buffer+10EH,Y
		STA	  !color_buffer+10FH,Y
Enemy_jetcol290 RTS

;=============== Set work pointer ======================================
Enemy_jetcol500
		INC	  A
		ASL	  A
		TAX			      ; IX = car work pointer
;-----------------------------------------------------------------------
		LDA	  !car_flag,X
		AND	  #10001000B
		CMP	  #10001000B	      ; Car avairable ?
		BEQ	  Enemy_jetcol510     ; yes.
		SEC
		RTS			      ; return ( true )
;-----------------------------------------------------------------------
Enemy_jetcol510 LDA	  !car_char_num+1,X
		AND	  #00001110B
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		TAY			      ; IY = Color pointer
		CLC
		RTS			      ; return ( false )
;
;************************************************************************
;*		 Set my car jet color					*
;************************************************************************
;
		MEM8
		IDX8
Mycar_jet_color
		LDA	  !car_char_num+1
		AND	  #00000110B
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		TAX			      ; IX = Color pointer
;
;=============== Set my car jet color ==================================
Mycar_jetcol100
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !car_speed
		CMP	  #07FFH	      ; speed < 7FFH ?
		BCC	  Mycar_jetcol110     ; yes.
		LDA	  #00FFH	      ; Acc = speed / 8
		BRA	  Mycar_jetcol120
;-----------------------------------------------------------------------
Mycar_jetcol110 LSR	  A
		LSR	  A
		LSR	  A		      ; Acc = speed / 8
;-----------------------------------------------------------------------
		MEM8
Mycar_jetcol120 SEP	  #00100000B	      ; Memory 8 bit mode
		STA	  !Multiplicand
		LDY	  <mycar_jet_count
		LDA	  !Mycar_jet_level,Y
		STA	  !Multiplier
		NOP
		NOP
		NOP
		NOP
;-----------------------------------------------------------------------
		LDA	  !Multiply+1
		JSR	  Set_jet_color
		STA	  !color_buffer+18EH,X
		XBA
		STA	  !color_buffer+18FH,X
;-----------------------------------------------------------------------
		TYA
		INC	  A
		AND	  #00000011B
		STA	  <mycar_jet_count
		RTS
;
;=======================================================================
Mycar_jet_level BYTE	  00,32,63,94
;
;************************************************************************
;*		 My car shadow display					*
;************************************************************************
;
		MEM16
		IDX8
Mycar_shadow
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDY	  !car_hight+1
		CPY	  #10		      ; my_car_hight >= 10 ?
		BCC	  Mycar_shadow130     ; no.
		CPY	  #20		      ; my_car_hight >= 20 ?
		BCC	  Mycar_shadow120     ; no.
;-----------------------------------------------------------------------
Mycar_shadow110 LDA	  #Shadow_window_3    ; Shadow small
		BRA	  Mycar_shadow140
Mycar_shadow120 LDA	  #Shadow_window_2    ; Shadow middle
		BRA	  Mycar_shadow140
Mycar_shadow130 LDA	  #Shadow_window_1    ; Shadow normal
Mycar_shadow140 STA	  !shadow_address     ; Set shadow data address
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Enemy shadow for start demo				*
;************************************************************************
;
		MEM16
		IDX8
Demo_shadow
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
;
;=============== Set vertical position for demo ========================
Demo_shadow200
		LDX	  #06H
Demo_shadow110	LDA	  !car_position_v,X
		PHA
		LDA	  !car_display_v,X
		CMP	  #00FCH
		BCC	  Demo_shadow120
		LDA	  #00FCH
Demo_shadow120	STA	  !car_position_v,X
		DEX
		DEX
		BNE	  Demo_shadow110
;
;=============== Set enemy shadow ======================================
Demo_shadow300
		JSR	  Enemy_shadow
		PLA
		STA	  !car_position_v+2
		PLA
		STA	  !car_position_v+4
		PLA
		STA	  !car_position_v+6
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Enemy shadow						*
;************************************************************************
;
		MEM8
		IDX8
Enemy_shadow
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		PEA	  000BH
		PLB			      ; Set data bank register
;-----------------------------------------------------------------------
		STZ	  <shadow_main
		STZ	  <shadow_sub
;-----------------------------------------------------------------------
		LDA	  <frame_counter
		LSR	  A		      ; frame counter == even ?
		BCS	  Enemy_shadow200     ; no.
;
;=============== Set shadow main ( frame even ) ========================
Enemy_shadow100
		LDX	  #02H
		JSR	  Enemy_shadow300
		LDX	  #06H
		JSR	  Enemy_shadow300
		LDX	  #0AH
		JSR	  Enemy_shadow300
		BRA	  Enemy_shadow210
;
;=============== Set shadow main ( frame odd ) =========================
Enemy_shadow200
		LDX	  #04H
		JSR	  Enemy_shadow300
		LDX	  #08H
		JSR	  Enemy_shadow300
		LDX	  #0CH
		JSR	  Enemy_shadow300
;-----------------------------------------------------------------------
Enemy_shadow210 PLB			      ; Restore data bank register
		PLP
		RTS
;
;=============== Set pointer ===========================================
Enemy_shadow300
		LDA	  !car_flag,X
		AND	  #10001100B
		CMP	  #10001000B	      ; car work avilable ?
		BNE	  Enemy_shadow500     ; no. ( clear OAM )
;-----------------------------------------------------------------------
		LDY	  !car_size,X
		CPY	  #08H		      ; Car size >= 7 ?
		BCS	  Enemy_shadow500     ; yes.
		LDA	  !Shadow_offset_v,Y
		STA	  <shadow_ofs_v	      ; Set shadow V offset
		LDA	  !car_position_h+0,X
		STA	  <shadow_ofs_h+0     ; Set shadow H offset low
		LDA	  !car_position_h+1,X
		STA	  <shadow_ofs_h+1     ; Set shadow H offset high
;-----------------------------------------------------------------------
		SEC
		LDA	  !car_position_v,X
		SBC	  <shadow_ofs_v
		STA	  <shadow_pos_v	      ; Set shadow V position
;-----------------------------------------------------------------------
		TYA
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		TAY			      ; IY = brock pointer
;
;=============== Set brock data into OAM main ==========================
Enemy_shadow400
		LDA	  #04H
		STA	  <work0	      ; work0.b = loop counter
		LDX	  <shadow_main	      ; IX = OAM main pointer
;-----------------------------------------------------------------------
Enemy_shadow410
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  !Shadow_brock+00H,Y ; Acc = offset H & attribute
		BEQ	  Enemy_shadow510     ; if ( null data )
		CLC
		ADC	  <shadow_ofs_h
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		STA	  !OAM_main+1D0H,X    ; Set H position
;-----------------------------------------------------------------------
		XBA
		LSR	  A
		ROR	  <OAM_sub_data	      ; Set H position MSB
;-----------------------------------------------------------------------
		LDA	  <shadow_pos_v
		STA	  !OAM_main+1D1H,X    ; Set V position
;-----------------------------------------------------------------------
		LDA	  !Shadow_brock+02H,Y
		STA	  !OAM_main+1D2H,X    ; Set character
		LDA	  !Shadow_brock+03H,Y
		LSR	  A
		ROR	  <OAM_sub_data	      ; Set OAM size bit
		STA	  !OAM_main+1D3H,X    ; Set attribute
;-----------------------------------------------------------------------
		INX
		INX
		INX
		INX			      ; Increment OAM pointer
		INY
		INY
		INY			      ; Increment block pointer
		INY
		DEC	  <work0
		BNE	  Enemy_shadow410
		BRA	  Enemy_shadow600
;
;=============== Set clear data into OAM main ==========================
Enemy_shadow500
		LDA	  #04H
		STA	  <work0	      ; work0.b = loop counter
		LDX	  <shadow_main	      ; IX = OAM main pointer
Enemy_shadow510
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
Enemy_shadow520
		LDA	  #80H
		STA	  !OAM_main+1D0H,X    ; Set H position
		STA	  !OAM_main+1D1H,X    ; Set V position
		SEC
		ROR	  <OAM_sub_data
		SEC
		ROR	  <OAM_sub_data
;-----------------------------------------------------------------------
		INX
		INX
		INX
		INX
		DEC	  <work0
		BNE	  Enemy_shadow520
;
;=============== Set OAM sub ===========================================
Enemy_shadow600
		STX	  <shadow_main
		LDX	  <shadow_sub
		LDA	  <OAM_sub_data
		STA	  !OAM_sub+1DH,X
		INX
		STX	  <shadow_sub
		RTS
;
;=============== Enemy shadow brock data ===============================
Shadow_offset_v EQU	  0ECC2H
Shadow_brock	EQU	  0ECD0H
;
;************************************************************************
;*		 Pull beam start					*
;************************************************************************
;
		MEM8
		IDX8
Pull_beam_start
		STZ	  <pull_counter_A
		LDA	  #50H
		STA	  <pull_counter_B
		LDA	  #10101010B
		STA	  !OAM_sub+0DH	      ; Set OAM sub
		STA	  !OAM_sub+0EH	      ; Set OAM sub
;-----------------------------------------------------------------------
		LDA	  !short_cut_flag
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory 16 bit mode
		BEQ	  Pul_beam_start1     ; if ( shield repair )
		LDX	  #Pull_beam_data&0FFFFH
		BRA	  Pul_beam_start2
Pul_beam_start1 LDX	  #(Pull_beam_data&0FFFFH)+20H
Pul_beam_start2 LDA	  #001FH
		LDY	  #OAM_main+0D0H
		MVN	  #0BH,#00H
;-----------------------------------------------------------------------
		SEP	  #00110000B	      ; Memory 8 bit mode
		RTS
;
;=============== Pull beam OAM data ====================================
Pull_beam_data	EQU	  0BED50H
;
;************************************************************************
;*		 Pull beam end						*
;************************************************************************
;
		MEM8
		IDX8
Pull_beam_end
		LDA	  #11111111B
		STA	  !OAM_sub+0DH	      ; Clear OAM sub
		STA	  !OAM_sub+0EH	      ; Clear OAM sub
;-----------------------------------------------------------------------
		LDX	  #1CH
		LDA	  #80H
Pull_end110
		STA	  !OAM_main+0D0H,X    ; Set position H
		DEX
		DEX
		DEX
		DEX
		BPL	  Pull_end110
		RTS
;
;************************************************************************
;*		 Pull beam move						*
;************************************************************************
;
		MEM8
		IDX8
Pull_beam_move
		LDA	  <pull_counter_A
		CLC
		ADC	  #010H		      ; Add pull counter A
		CMP	  #0B0H		      ; counter == B0H ?
		BNE	  Pull_move110	      ; no.
		LDA	  #000H
;-----------------------------------------------------------------------
Pull_move110	STA	  <pull_counter_A
		STA	  !OAM_main+0D1H
		STA	  !OAM_main+0D5H
		STA	  !OAM_main+0D9H
		STA	  !OAM_main+0DDH
;-----------------------------------------------------------------------
		LDA	  <pull_counter_B
		CLC
		ADC	  #010H		      ; Add pull counter B
		CMP	  #0B0H		      ; counter == B0H ?
		BNE	  Pull_move120	      ; no.
		LDA	  #000H
;-----------------------------------------------------------------------
Pull_move120	STA	  <pull_counter_B
		STA	  !OAM_main+0E1H
		STA	  !OAM_main+0E5H
		STA	  !OAM_main+0E9H
		STA	  !OAM_main+0EDH
;-----------------------------------------------------------------------
		LDA	  #10101010B
		STA	  !OAM_sub+0DH	      ; Set OAM sub
		RTS
;
;************************************************************************
;*		 Display UFO						*
;************************************************************************
;
		MEM8
		IDX8
Display_UFO
		LDA	  <repair_counter
		DEC	  A		      ; first display ?
		BEQ	  Display_UFO300      ; yes.
;
;=============== UFO move vertical =====================================
Display_UFO100
		TAX
		LSR	  A
		ADC	  #0F0H		      ; ( CY = 0 )
		STA	  !OAM_main+11H
		STA	  !OAM_main+15H
		STA	  !OAM_main+19H
		STA	  !OAM_main+1DH
		STA	  !OAM_main+21H
		STA	  !OAM_main+25H
		STA	  !OAM_main+29H
		STA	  !OAM_main+2DH
;-----------------------------------------------------------------------
		STA	  !OAM_main+01H
		STA	  !OAM_main+05H
		CLC
		ADC	  #05H
		STA	  !OAM_main+09H
		STA	  !OAM_main+0DH
		TXA
;
;=============== UFO move horizontal ===================================
Display_UFO200
		ASL	  A
		EOR	  #0FFH
		SEC
		ADC	  #088H
		STA	  !OAM_main+10H
		CLC
		ADC	  #08H
		STA	  !OAM_main+14H
		ADC	  #02H
		STA	  !OAM_main+08H
		ADC	  #02H
		STA	  !OAM_main+00H
		ADC	  #0CH
		STA	  !OAM_main+18H
		ADC	  #10H
		STA	  !OAM_main+1CH
		ADC	  #10H
		STA	  !OAM_main+20H
		ADC	  #10H
		STA	  !OAM_main+24H
		ADC	  #10H
		STA	  !OAM_main+28H
		ADC	  #04H
		STA	  !OAM_main+04H
		ADC	  #02H
		STA	  !OAM_main+0CH
		ADC	  #0AH
		STA	  !OAM_main+2CH
		RTS
;
;=============== Display UFO ===========================================
Display_UFO300
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDX	  #11111111B
		LDA	  #0010101010101000B
		STX	  !OAM_sub+000H
		STA	  !OAM_sub+001H	      ; Set OAM sub
;-----------------------------------------------------------------------
		IDX16
		REP	  #00010000B	      ; Index 16 bit mode
		LDA	  #002FH
		LDX	  #UFO_OAM_data&0FFFFH
		LDY	  #OAM_main
		MVN	  #0BH,#00H
;-----------------------------------------------------------------------
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		RTS
;=======================================================================
UFO_OAM_data	EQU	  0BED90H
;
;************************************************************************
;*		 Set my car turbo jet					*
;************************************************************************
;
		MEM8
		IDX8
Set_turbo_jet
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  !special_demo	      ; special demo ?
		BNE	  Set_turbo_jet10     ; yes.
		LDA	  <turbo_status	      ; turbo on ?
		BMI	  Turbo_jet100	      ; yes. ( trigger )
		BNE	  Turbo_jet200	      ; no.
;-----------------------------------------------------------------------
Set_turbo_jet10 JSR	  Clear_turbo_jet     ; clear turbo jet
		PLP			      ; Restore mode
		RTS
;
;=============== Turbo trigger =========================================
Turbo_jet100
		AND	  #01111111B	      ; first time ?
		BEQ	  Turbo_jet130	      ; yes.
		DEC	  A		      ; second time ?
		BEQ	  Turbo_jet120	      ; yes.
		DEC	  A		      ; third time ?
		BEQ	  Turbo_jet110	      ; yes.
;-----------------------------------------------------------------------
		DEC	  <turbo_timer	      ; --timer == 0 ?
		BNE	  Turbo_jet150	      ; no.
		LDA	  #04H
		STA	  <turbo_status	      ; turbo_status = 04H
		LDA	  #01H
		STA	  <turbo_timer	      ; turbo_timer  = 01H
		STZ	  <turbo_count	      ; turbo_count  = 00H
		STZ	  <turbo_level	      ; turbo_level  = 00H
		BRA	  Turbo_jet150
;------------------------------------------------------- third time ----
Turbo_jet110	DEC	  <turbo_timer	      ; --timer == 0 ?
		BNE	  Turbo_jet150	      ; no.
		INC	  <turbo_status	      ; turbo_status = 84H
		LDA	  #00000001B
		STA	  <break_counter      ; Reset break counter
		STZ	  <mycar_jet_count    ; Reset my car jet counter
		LDA	  #02H
		STA	  <turbo_timer	      ; turbo_timer = 02H
		LDX	  #00H
		BRA	  Turbo_jet140
;------------------------------------------------------- second time ---
Turbo_jet120	INC	  <turbo_status	      ; turbo_status = 82H
		LDA	  #02H
		STA	  <turbo_timer	      ; turbo_timer = 02H
		LDX	  #01H
		BRA	  Turbo_jet140
;------------------------------------------------------- first time ----
Turbo_jet130	INC	  <turbo_status	      ; turbo_status = 81H
		LDA	  #11111111B
		STA	  !color_buffer+138H  ; Set color low  = white
		STA	  !color_buffer+139H  ; Set color high = white
		LDX	  #02H
;-----------------------------------------------------------------------
Turbo_jet140	LDA	  !Turbo_jet_char,X
		STA	  !OAM_main+0F2H      ; Set display character
		STA	  !OAM_main+0F6H
		STA	  !OAM_main+0FAH
		STA	  !OAM_main+0FEH
		LDA	  #00110011B
		STA	  !OAM_main+0F3H
		STA	  !OAM_main+0F7H
		STA	  !OAM_main+0FBH
		STA	  !OAM_main+0FFH
;-----------------------------------------------------------------------
		LDA	  !Vertical_offset,X
		STA	  <turbo_offset_v     ; Set jet display offset v
;-----------------------------------------------------------------------
Turbo_jet150	JSR	  Disp_turbo_jet      ; Set display position
		PLP			      ; restore mode
		RTS
;
;=============== Decrement turbo timer =================================
Turbo_jet200
		DEC	  <turbo_timer	      ; --turbo_timer == 0 ?
		BEQ	  Turbo_jet300	      ; yes.
;-----------------------------------------------------------------------
		LDA	  <turbo_count
		CMP	  #03H		      ; jet displaying ?
		BEQ	  Turbo_jet210	      ; no.
		JSR	  Disp_turbo_jet      ; Set display position
		BRA	  Turbo_jet220
Turbo_jet210	JSR	  Clear_turbo_jet     ; Clear turbo jet
;-----------------------------------------------------------------------
Turbo_jet220	PLP			      ; Restore mode
		RTS
;
;=============== decrement turbo count =================================
Turbo_jet300
		DEC	  <turbo_count	      ; Clear timming ?
		BPL	  Turbo_jet400	      ; no.
;-----------------------------------------------------------------------
		LDA	  #03H
		STA	  <turbo_count	      ; Reset turbo_count
		LDA	  <turbo_status
		DEC	  A		      ; --turbo_status == 0 ?
		BNE	  Turbo_jet310	      ; no.
		INC	  A		      ; turbo_status = 1
Turbo_jet310
		STA	  <turbo_status	      ; Set turbo_status
		STA	  <turbo_timer	      ; Reset timer
		JSR	  Clear_turbo_jet     ; Clear turbo jet
;-----------------------------------------------------------------------
		PLP			      ; Restore mode
		RTS
;
;=============== Display turbo jet =====================================
Turbo_jet400
		LDA	  <turbo_status
		STA	  <turbo_timer	      ; Reset timer
;-----------------------------------------------------------------------
		LDX	  <turbo_count
		LDA	  !Turbo_jet_char,X
		STA	  !OAM_main+0F2H      ; Set display character
		STA	  !OAM_main+0F6H
		STA	  !OAM_main+0FAH
		STA	  !OAM_main+0FEH
		LDA	  #00110011B
		STA	  !OAM_main+0F3H
		STA	  !OAM_main+0F7H
		STA	  !OAM_main+0FBH
		STA	  !OAM_main+0FFH
;-----------------------------------------------------------------------
		LDA	  !Vertical_offset,X
		STA	  <turbo_offset_v     ; Set jet display offset v
;-----------------------------------------------------------------------
		JSR	  Disp_turbo_jet
		JSR	  Set_turbo_color
		PLP
		RTS
;
;************************************************************************
;*		 Set turbo color					*
;************************************************************************
;
		MEM16
		IDX8
Set_turbo_color
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;
;=============== Set turbo level =======================================
Turbo_color100
		LDA	  !car_speed
		CMP	  #07FFH	      ; speed < 800H ?
		BCC	  Turbo_color110      ; yes.
		LDA	  #00FFH	      ; Acc = speed / 8
		BRA	  Turbo_color120
;-----------------------------------------------------------------------
Turbo_color110	LSR	  A
		LSR	  A
		LSR	  A		      ; Acc = speed / 8
;-----------------------------------------------------------------------
		MEM8
Turbo_color120
		SEP	  #00100000B	      ; Memory 8 bit mode
		STA	  !Multiplicand
		LDA	  #94
		STA	  !Multiplier
		NOP
		NOP
		NOP
;-----------------------------------------------------------------------
		LDA	  <turbo_level
		CMP	  !Multiply+1
		BCS	  Turbo_color130
		INC	  A
		BRA	  Turbo_color140
Turbo_color130
		LDA	  !Multiply+1
Turbo_color140
		STA	  !turbo_level
		JSR	  Set_jet_color
		STA	  !color_buffer+138H  ; Set color low
		XBA
		STA	  !color_buffer+139H  ; Set color high
		RTS
;
;************************************************************************
;*		 Set jet color code					*
;************************************************************************
;
		MEM8
		IDX8
Set_jet_color
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		CMP	  #63
		BCS	  Set_jet_color30
		CMP	  #32
		BCS	  Set_jet_color20
;------------------------------------------------------- change red ----
		MEM16
Set_jet_color10 REP	  #00100000B	      ; Memory 16 bit mode
		AND	  #0000000000011111B
		BRA	  Set_jet_color90
;------------------------------------------------------- change green --
		MEM8
Set_jet_color20 SEC
		SBC	  #31
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		AND	  #0000000000011111B
		XBA
		LSR	  A
		LSR	  A
		LSR	  A
		ORA	  #0000000000011111B
		BRA	  Set_jet_color90
;------------------------------------------------------- change blue ---
		MEM8
Set_jet_color30 SEC
		SBC	  #62
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		AND	  #0000000000011111B
		XBA
		ASL	  A
		ASL	  A
		ORA	  #0000001111111111B
;-----------------------------------------------------------------------
Set_jet_color90 PLP
		RTS
;
;************************************************************************
;*		 Display turbo jet					*
;************************************************************************
;
		MEM8
		IDX8
Disp_turbo_jet
		LDA	  <repair_counter     ; Shield repair now ?
		BEQ	  Disp_turbo_jet1     ; no.
		LDA	  !OAM_main+0F2H
		STA	  !OAM_main+002H
		STA	  !OAM_main+006H
		STA	  !OAM_main+00AH
		STA	  !OAM_main+00EH
		STZ	  !OAM_sub+000H
;-----------------------------------------------------------------------
Disp_turbo_jet1 SEC
		LDA	  !car_display_h
		SBC	  #24
		STA	  <mycar_position+0   ; Set my car home position h
		LDA	  !car_display_v
		SBC	  #32
		CLC
		ADC	  <turbo_offset_v
		STA	  <mycar_position+1   ; Set my car home position v
;-----------------------------------------------------------------------
		LDA	  <car_canopy	      ; jump up ?
		BPL	  Disp_turbo_jet2     ; no.
		INC	  <mycar_position+1   ; Increment home position v
		INC	  <mycar_position+1   ; Increment home position v
;-----------------------------------------------------------------------
Disp_turbo_jet2 LDA	  <car_wing	      ; car wing exception ?
		BPL	  Disp_turbo_jet3     ; no.
		AND	  #01111111B
		CLC
		ADC	  #0BH
;-----------------------------------------------------------------------
Disp_turbo_jet3 TAY
		JMP	  (!turbo_jump_ptr)
;
;=============== My car 1 turbo jet ====================================
Mycar1_turbo
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDX	  !Pose_offset_1,Y
		LDA	  >Mycar1_jet_pos+0,X
		ADC	  <mycar_position
		STA	  !OAM_main+0F0H
		LDA	  >Mycar1_jet_pos+2,X
		ADC	  <mycar_position
		STA	  !OAM_main+0F4H
		LDA	  >Mycar1_jet_pos+4,X
		ADC	  <mycar_position
		STA	  !OAM_main+0F8H
		LDA	  >Mycar1_jet_pos+6,X
		ADC	  <mycar_position
		STA	  !OAM_main+0FCH
;-----------------------------------------------------------------------
		LDX	  #00000000B
		STX	  !OAM_sub+0FH
		RTS
;
;=============== My car 2 turbo jet ====================================
Mycar2_turbo
		MEM8
		LDX	  !Pose_offset_2,Y
		LDA	  !Car2_offset,Y
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
		AND	  #00FFH
		ADC	  >Mycar2_jet_pos+0,X
		ADC	  <mycar_position
		STA	  !OAM_main+0F0H
		LDA	  >Mycar2_jet_pos+2,X
		ADC	  <mycar_position
		STA	  !OAM_main+0F4H
		LDA	  >Mycar2_jet_pos+4,X
		ADC	  <mycar_position
		STA	  !OAM_main+0F8H
;-----------------------------------------------------------------------
		LDX	  #11000000B
		STX	  !OAM_sub+0FH
		RTS
;
;=============== My car 3 turbo jet ====================================
Mycar3_turbo
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDX	  !Pose_offset_1,Y
		LDA	  >Mycar3_jet_pos+0,X
		ADC	  <mycar_position
		STA	  !OAM_main+0F0H
		LDA	  >Mycar3_jet_pos+2,X
		ADC	  <mycar_position
		STA	  !OAM_main+0F4H
		LDA	  >Mycar3_jet_pos+4,X
		ADC	  <mycar_position
		STA	  !OAM_main+0F8H
		LDA	  >Mycar3_jet_pos+6,X
		ADC	  <mycar_position
		STA	  !OAM_main+0FCH
;-----------------------------------------------------------------------
		LDX	  #00000000B
		STX	  !OAM_sub+0FH
		RTS
;
;=============== My car 4 turbo jet ====================================
Mycar4_turbo
		MEM8
		LDA	  !Pose_offset_1,Y
		LSR	  A
		TAX
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
		LDA	  >Mycar4_jet_pos+0,X
		ADC	  <mycar_position
		STA	  !OAM_main+0F0H
		LDA	  >Mycar4_jet_pos+2,X
		ADC	  <mycar_position
		STA	  !OAM_main+0F4H
;-----------------------------------------------------------------------
		LDX	  #11110000B
		STX	  !OAM_sub+0FH
		RTS
;
;************************************************************************
;*		 Clear turbo jet					*
;************************************************************************
;
		MEM16
		IDX8
Clear_turbo_jet
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDA	  #8080H
		STA	  !OAM_main+0F0H      ; Clear display position
		STA	  !OAM_main+0F4H
		STA	  !OAM_main+0F8H
		STA	  !OAM_main+0FCH
		LDX	  #11111111B
		STX	  !OAM_sub+00FH	      ; Reset OAM sub
;-----------------------------------------------------------------------
		LDY	  <repair_counter     ; Shield repair now ?
		BEQ	  Clear_turbo10	      ; no.
		STX	  !OAM_sub+000H	      ; Reset OAM sub
;-----------------------------------------------------------------------
Clear_turbo10	SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;************************************************************************
;*		 Turbo jet data						*
;************************************************************************
;
;			  00 01 02 03 04 05 06 07 08 09 0A 80 81
Pose_offset_1	BYTE	  00,08,16,16,16,16,16,16,16,24,32,08,24
Pose_offset_2	BYTE	  00,06,12,12,12,12,12,12,12,18,24,30,36
Car2_offset	BYTE	  00,00,00,01,02,03,04,05,06,00,00,00,00
Vertical_offset BYTE	  03H,01H,00H
Turbo_jet_char	BYTE	  72H,63H,62H
;
;************************************************************************
;*		 TURBO OK display					*
;************************************************************************
;
		PROG
		MEM8
		IDX8
Turbo_ok_disp
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  !turbo_ok_count     ; TURBO OK display ?
		BEQ	  Turbo_ok_dsp110     ; no.
		LDY	  <exception_flag     ; Exception ?
		BNE	  Turbo_ok_dsp100     ; yes.
		CMP	  #01H		      ; display end ?
		BEQ	  Turbo_ok_dsp100     ; yes.
		CMP	  #05H
		BCS	  Turbo_ok_dsp200
;-----------------------------------------------------------------------
		DEC	  !turbo_ok_count
		JMP	  Turbo_ok_dsp510
;
;=============== TURBO OK display end ==================================
Turbo_ok_dsp100
		STZ	  !turbo_ok_count
		STZ	  !OAM_sub+04H
		LDA	  #0F0H
		STA	  !OAM_main+49H
		STA	  !OAM_main+4DH
;-----------------------------------------------------------------------
Turbo_ok_dsp110 PLP
		RTS
;
;=======================================================================
Turbo_ok_dsp200
		DEC	  A
		STA	  !turbo_ok_count
		BIT	  #00000010B
		BEQ	  Turbo_ok_dsp510
;-----------------------------------------------------------------------
		LDX	  <mycar_number
		LSR	  A
		BCS	  Turbo_ok_dsp210
		LDY	  !Turbo_body_col1,X
		BRA	  Turbo_ok_dsp300
Turbo_ok_dsp210 LDY	  !Turbo_body_col2,X
;
;=============== Color change ==========================================
Turbo_ok_dsp300
		MEM16
		IDX16
		REP	  #00110001B
;-----------------------------------------------------------------------
		TYA
		ADC	  #(object_color&0FFFFH)+80H
		TAX			      ; IX = source address
;-----------------------------------------------------------------------
		LDA	  !car_char_num+1
		AND	  #0000000000000110B
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		ADC	  #color_buffer+180H
		TAY			      ; IY = destination address
		LDA	  #001FH
		MVN	  #0FH,#00H
;-----------------------------------------------------------------------
		MEM8
		IDX8
		SEP	  #00110000B
		LDA	  !turbo_ok_count
		CMP	  #1FH		      ; display start ?
		BNE	  Turbo_ok_dsp510     ; no.
;
;=============== Display TURBO OK ======================================
Turbo_ok_dsp500
		LDA	  #070H
		STA	  !OAM_main+48H
		LDA	  #080H
		STA	  !OAM_main+4CH
;-----------------------------------------------------------------------
		LDA	  #06CH
		STA	  !OAM_main+4AH
		LDA	  #0A6H
		STA	  !OAM_main+4EH
		LDA	  #00110001B
		STA	  !OAM_main+4BH
		STA	  !OAM_main+4FH
;-----------------------------------------------------------------------
		LDA	  #10100000B
		STA	  !OAM_sub+04H
;-----------------------------------------------------------------------
Turbo_ok_dsp510 LDA	  #84H
;;;;		SEC
;;;;		LDA	  >car_display_v
;;;;		SBC	  #30H
		STA	  !OAM_main+49H
		STA	  !OAM_main+4DH
		PLP
		RTS
;
;=======================================================================
Turbo_body_col1 BYTE	  00H,20H,40H,60H     ; normal color
Turbo_body_col2 BYTE	  40H,40H,60H,00H     ; effect color
;
;************************************************************************
;*		 My car shield down					*
;************************************************************************
;
		MEM8
		IDX8
Mycar_short
		PHP
		SEP	  #00110000B	      ; Memory,index 8 bit mode
;
;=============== Check my car damage ===================================
Mycar_short100
		LDA	  !non_turbo_cnt
		BEQ	  Mycar_short105
		DEC	  !non_turbo_cnt
		BRA	  Mycar_short110
;-----------------------------------------------------------------------
Mycar_short105	LDA	  <mycar_damage+1     ; damage < 100H ?
		BMI	  Mycar_short110      ; yes.
		BEQ	  Mycar_short110      ; yes.
		CMP	  #02H		      ; damage < 200H ?
		BCS	  Mycar_short300      ; no.
;-----------------------------------------------------------------------
		LDA	  #00011111B
		BRA	  Mycar_short120
Mycar_short110	LDA	  #00001111B
Mycar_short120	STA	  <work0	      ; work0 = counter mask
;
;=============== My car damage animation ===============================
Mycar_short200
		LDA	  <spark_counter      ; Laser spark ?
		BNE	  Mycar_short220      ; yes.
		LDA	  #00010011B
		BIT	  <break_counter      ; no display cycle ?
		BNE	  Mycar_short210      ; yes.
;-----------------------------------------------------------------------
		LDA	  #0FFH
		JSR	  Mycar_short400      ; Spark my car color
		LDA	  <exception_flag
		AND	  #11111000B	      ; Goal in or game over ?
		BNE	  Mycar_short220      ; yes.
;-----------------------------------------------------------------------
		LDA	  #01000000B
		TSB	  <sound_status_0     ; Set SOUND
		LDA	  !OAM_sub+0FH
		CMP	  #11111111B	      ; OAM used ?
		BNE	  Mycar_short220      ; no.
		JSR	  Mycar_short500
		BRA	  Mycar_short220
;-----------------------------------------------------------------------
Mycar_short210	LDA	  <break_counter
		AND	  #00010011B
		DEC	  A
		BNE	  Mycar_short220
		LDA	  #00H
		JSR	  Mycar_short400      ; Reset my car color
;-----------------------------------------------------------------------
Mycar_short220	LDA	  <break_counter
		INC	  A
		AND	  <work0
		STA	  <break_counter
;-----------------------------------------------------------------------
		PLP
		RTS
;
;=============== Check shield repair ===================================
Mycar_short300
		LDA	  <break_counter      ; My car short ?
		BEQ	  Mycar_short310      ; no.
;-----------------------------------------------------------------------
		STZ	  <break_counter      ; reset break counter
		LDA	  #00H
		JSR	  Mycar_short400      ; Reset my car color
;-----------------------------------------------------------------------
Mycar_short310	PLP
		RTS
;
;=============== Calculate my car color index ==========================
Mycar_short400
		XBA
		LDA	  !car_char_num+1
		AND	  #00001110B
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		TAX
		XBA
		STA	  !color_buffer+102H,X
		STA	  !color_buffer+103H,X
		RTS
;
;=============== Display my car spark ==================================
Mycar_short500
		STZ	  !OAM_sub+0FH	      ; Set OAM sub
		LDA	  !break_counter
		AND	  #00001100B
		TAX
;-----------------------------------------------------------------------
		SEC
		LDA	  !car_display_h
		SBC	  #24
		STA	  <mycar_position+0
		LDA	  !car_display_v
		SBC	  #32
		STA	  <mycar_position+1
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
		LDA	  !Mycar_spark_pos+0
		ADC	  <mycar_position
		STA	  !OAM_main+0F0H      ; Set position 1
		LDA	  !Mycar_spark_pos+2
		ADC	  <mycar_position
		STA	  !OAM_main+0F4H      ; Set position 2
		LDA	  !Mycar_spark_pos+4
		ADC	  <mycar_position
		STA	  !OAM_main+0F8H      ; Set position 3
		LDA	  !Mycar_spark_pos+6
		ADC	  <mycar_position
		STA	  !OAM_main+0FCH      ; Set position 4
;-----------------------------------------------------------------------
		LDA	  >Mycar_spark_chr+0,X
		STA	  !OAM_main+0F2H      ; Set character 1
		EOR	  #0100000000000000B
		STA	  !OAM_main+0F6H      ; Set character 2
		LDA	  >Mycar_spark_chr+2,X
		STA	  !OAM_main+0FAH      ; Set character 3
		EOR	  #0100000000000000B
		STA	  !OAM_main+0FEH      ; Set character 4
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=======================================================================
;			  posH posV
Mycar_spark_pos BYTE	  007H,009H,021H,009H,010H,006H,018H,006H
Mycar_spark_chr EQU	  0FC24CH
;
		END
