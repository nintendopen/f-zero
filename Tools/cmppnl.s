*************************************************************************
*	int cmppnl( pnlbuf,panel,pnlcnt )				*
*	char	*pnlbuf,*panel ;					*
*	int	pnlcnt ;						*
*************************************************************************
*	Programmed by Y.Nishida			[ Apr.10, 1990 ]	*

		.globl		_cmppnl
		.set		pnlbuf,8
		.set		panel,12
		.set		pnlcnt,16
		.set		autosize,0

		.text
_cmppnl:
		link		fp,#autosize
		movea.l		(panel,fp),a0
		clr.l		d0			/*  d0.l = buffer offset	*/
		move.l		(pnlcnt,fp),d1		/*  d1.l = panel counter	*/
		move.l		(a0),d2			/*  d2.l = panel data		*/
		movea.l		(pnlbuf,fp),a0		/*  a0.l = panel buffer		*/
repeat:
		cmp.l		(a0,d0.l*4),d2
		beq.w		exit_func
		addq.l		#1,d0
		cmp.l		d1,d0
		blt.w		repeat
exit_func:
		unlk		fp
		rts
