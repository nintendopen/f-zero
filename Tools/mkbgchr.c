/****************************************************************
	Convert 8bit mode file into screen mode 7 file
		progurammed by Y.Nishida
					[ Jun.06, 1989 ]
 ****************************************************************/

#include	<stdio.h>
#include	<strings.h>
#include	<memory.h>

#define		ERROR		-1
#define		COMPLETE	0
#define		DATSIZE		0x10000
#define		BUFSIZE		0x8400
#define		CHRSIZE		0x400
#define		unchar		unsigned char

unchar	chrdat[DATSIZE] ;
unchar	chrbuf[BUFSIZE] ;

main( argc,argv )
int	argc ;
char	**argv ;
{
	char	fname[65] ;

	puts( "Make BG screen character file for CAR RACE." ) ;
	if ( argc != 2  ) {
		puts( "usage: mkbgchr <data name>" ) ;
		exit( 1 ) ;
	}
	strcpy( fname,*(++argv)) ;
	strcat( fname,".CGX" ) ;
	if ( chread( fname,chrdat,DATSIZE ) == ERROR ) {
		printf( "%s: No such file or not 8bit mode.\n",fname ) ;
		exit( 1 ) ;
	}
	convert( chrdat,CHRSIZE ) ;
	archive( chrdat,chrbuf,CHRSIZE ) ;

	strcpy( fname,*argv ) ;
	strcat( fname,".CHR" ) ;
	if ( chwrite( fname,chrbuf,BUFSIZE ) == ERROR ) {
		perror( *argv ) ;
		exit( 1 ) ;
	}
	exit( 0 ) ;
}

/******* read character data from file **************************/

chread( fname,buff,size )
char	*fname,*buff ;
int	size ;
{
	FILE	*fp ;

	if (( fp = fopen( fname,"r" )) == NULL )	  return ( ERROR ) ;
	if ( fread( buff,sizeof(char),size,fp ) != size ) return ( ERROR ) ;
	fclose( fp ) ;
	return ( COMPLETE ) ;
}

/******* write character data into file *************************/

chwrite( fname,buff,size )
char	*fname,*buff ;
int	size ;
{
	FILE	*fp ;

	if (( fp = fopen( fname,"w" )) == NULL )	   return ( ERROR ) ;
	if ( fwrite( buff,sizeof(char),size,fp ) != size ) return ( ERROR ) ;
	fclose( fp ) ;
	return ( COMPLETE ) ;
}

/******* character converter ************************************/

convert( data,size )
unchar	*data ;
int	size ;
{
	unchar	buff[0x40], *dt,*bf,*p ;
	int	ct,ix,iy,ii,nn ;

	for ( ct=0 ; ct<size ; ct++ ) {
		bf = buff ;
		dt = data ;
		for ( iy=0 ; iy<8 ; iy++ ) {
			for ( ix=0 ; ix<8 ; ix++ ) {
				for ( p=dt,nn=ii=0 ; ii<4 ; p+=0x10,ii++ ) {
					nn >>= 1 ;
					nn  |= ( *(p+0) & 0x80 ) ;
					nn >>= 1 ;
					nn  |= ( *(p+1) & 0x80 ) ;

					*(p+0) <<= 1 ;
					*(p+1) <<= 1 ;
				}
				*bf++ = nn ;
			}
			dt += 2 ;
		}
		memcpy( data,buff,0x40 ) ;
		data += 0x40 ;
	}
}

/******* character archiver ************************************/

archive( data,buff,size )
unchar	*data,*buff ;
int	size ;
{
	int	ct,i,nn ;

	for ( ct=0 ; ct<size ; ct++ ) {
		*buff++ = getbnk( data ) << 4 ;
		for ( i=0 ; i<32 ; i++ ) {
			nn      = ( *data++ & 0x0f ) << 4 ;
			nn     |= ( *data++ & 0x0f ) ;
			*buff++ = nn ;
		}
	}
}

/******* get color bank ****************************************/

getbnk( data )
unchar	*data ;
{
	register int	*p,i,n,m ;
	int	bnk[16] ;

	for ( p=bnk,i=0 ; i<16 ; *p++=0,i++ ) ;

	for ( i=0 ; i<64 ; i++ ) {
		n       = ( *data++ & 0xf0 ) >> 4 ;
		bnk[n] += 1 ;
	}
	for ( n=m=i=0 ; i<16 ; i++ ) {
		if ( m < bnk[i] ) {
			m = bnk[i] ;
			n = i ;
		}
	}
	return ( n ) ;
}
