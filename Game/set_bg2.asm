;************************************************************************
;*	 SET_BG2	   -- game screen set module 2 --		*
;************************************************************************
;
		INCLUDE	  RP5A22:
		INCLUDE	  RP5C77:
		INCLUDE	  VARIABLE
		INCLUDE	  BUFFER
;
;=============== Cross reference =======================================
;
		EXT	  Write_screen
;
;=============== Cross definition ======================================
;
		GLB	  INIT_COURSE,BG_SCROLL
		GLB	  SET_GAMEPERS,SET_DEMOPERS
		GLB	  TRANSFER_ENEMY,TRANSFER_OBJECT,TRANSFER_PERS
		GLB	  EXPAND_BACK,EXPAND_SELECT,TRANS_SEL_CHAR,TRANS_MINICAR
		GLB	  TRANS_TITLE_CHR,TITLE_OBJECT,TITLE_SUB

;=============== Define constant =======================================
;
panel_bank	EQU	  000004H
write_offset_x	EQU	  0AED00H
;
;=============== Define local variable =================================
;
work0		EQU	  00H		      ; 2 byte	:Work 0
work1		EQU	  02H		      ; 2 byte	:Work 1
;-----------------------------------------------------------------------
chr_address	EQU	  04H		      ; 3 byte	:character address
col_address	EQU	  04H		      ; 3 byte	:color address
char_bank	EQU	  07H		      ; 1 byte	:character bank
;-----------------------------------------------------------------------
loop_counter	EQU	  04H		      ; 2 byte	:loop counter
slit_pointer	EQU	  09H		      ; 3 byte	:Slit pointer
panel_pointer	EQU	  0CH		      ; 2 byte	:Panel pointer
room_offset	EQU	  0EH		      ; 2 byte	:room offset ( panel_y )
slit_offset	EQU	  10H		      ; 2 byte	:slit offset ( panel_x )
;-----------------------------------------------------------------------
home_locate_x	EQU	  12H		      ; 2 byte	:Screen home location x
home_locate_y	EQU	  14H		      ; 2 byte	:Screen home location y
room_count_x	EQU	  16H		      ; 2 byte	:Room counter x
room_count_y	EQU	  18H		      ; 2 byte	:Room counter y
room_counter	EQU	  18H		      ; 2 byte	:Room counter yx
;-----------------------------------------------------------------------
scrl_position_x EQU	  20H		      ; 2 byte
scrl_position_y EQU	  22H		      ; 2 byte
scrl_distance_x EQU	  24H		      ; 2 byte
scrl_distance_y EQU	  26H		      ; 2 byte
check_offset_x	EQU	  28H		      ; 2 byte
check_offset_y	EQU	  2AH		      ; 2 byte
;-----------------------------------------------------------------------
matrix_temp	EQU	  04H		      ; 4 byte	:Rotation matrix tempolary
;-----------------------------------------------------------------------
data_address	EQU	  04H		      ; 3 byte	:Transfer source address
buff_address	EQU	  07H		      ; 3 byte	:Transfer destination address
expand_char	EQU	  0AH		      ; 2 byte	:Expand chanarter number
;
;=======================================================================
;
		DATA
		EXTEND
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ BG screen set routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Set initial course					*
;************************************************************************
;
		MEM16
		IDX8
INIT_COURSE
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
		SEP	  #00010000B	      ; Index 8 bit mode
;
;=============== Initialize work =======================================
Init_course100
		JSR	  Set_scrlpos
		LDA	  <scrl_position_x
		STA	  <scrl_backct_x      ; Set scroll back counter x
		LDA	  <scrl_position_y
		STA	  <scrl_backct_y      ; Set scroll back counter y
;-----------------------------------------------------------------------
		LDA	  #0FFFFH
		STA	  <scrl_distance_y    ; Set dummy scroll distance
;
;=============== Set BG screen =========================================
Init_course200
		LDX	  #40H		      ; IX = roop counter
Init_course210	PHX
		JSR	  Set_Hwteparm	      ; Set Hwrite parameter
		JSR	  Set_Hwtebuff	      ; Set Hwrite buffer
		LDY	  #01H
		STY	  <srnwte_status      ; Set Hwrite mode
		PHK
		JSR	  Init_course900      ; Write buffer to VRAM
;-----------------------------------------------------------------------
		LDA	  <scrl_position_y
		CLC
		ADC	  #0010H
		STA	  <scrl_position_y    ; Set next position y
		PLX
		DEX
		BNE	  Init_course210
;-----------------------------------------------------------------------
		PLP			      ; Restore mode
		RTL
;
;=======================================================================
Init_course900	JMP	  >Write_screen
;
;************************************************************************
;*		 BG scroll entry					*
;************************************************************************
;
		MEM16
		IDX8
BG_SCROLL
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
		SEP	  #00010000B	      ; Index 8 bit mode
		JSR	  Set_scrlpos	      ; Set scroll position
		JSR	  Correct_difover     ; Correction difference over
;
;=============== Horizontal write entry ================================
BG_scroll100
		JSR	  Set_Hwteparm	      ; Set Hwrite parameter
		JSR	  Set_Hwtebuff	      ; Set Hwrite buffer
		LDA	  <scrl_position_x
		STA	  <scrl_backct_x
;
;=============== Vertical write entry ==================================
BG_scroll200
		JSR	  Set_Vwteparm	      ; Set Vwrite parameter
		JSR	  Set_Vwtebuff	      ; Set Vwrite buffer
		LDA	  <scrl_position_y
		STA	  <scrl_backct_y
;
;=============== Set screen write status ===============================
BG_scroll300
		LDX	  #02H
		STX	  <srnwte_status      ; Set Vwrite mode
		PLP			      ; Restore mode
		RTL
;
;************************************************************************
;*		 Set scroll offset ( for rotate screen )		*
;************************************************************************
;
		MEM16
		IDX16
Set_scrlpos
		PHP
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;
;=============== Set scroll position x =================================
Set_scrlpos100
		LDA	  !car_angle+1
		AND	  #00FFH
		TAY
;-----------------------------------------------------------------------
		ASL	  A
		TAX
		LDA	  <scroll_count_x+2
		CLC
		ADC	  >write_offset_x,X
		AND	  #1FFFH
		STA	  <scrl_position_x    ; Set scroll position x
;-----------------------------------------------------------------------
		SEC
		SBC	  <scrl_backct_x
		AND	  #1FFFH
		STA	  <scrl_distance_x    ; Set scroll difference x
;
;=============== Set scroll position y =================================
Set_scrlpos200
		TYA
		SEC
		SBC	  #0030H	      ; Subtruct 90(deg)
		BCS	  Set_scrlpos210
		ADC	  #00C0H
;-----------------------------------------------------------------------
Set_scrlpos210	ASL	  A
		TAX
		LDA	  <scroll_count_y+2
		CLC
		ADC	  >write_offset_x,X
		AND	  #0FFFH
		STA	  <scrl_position_y    ; Set scroll position y
;-----------------------------------------------------------------------
		SEC
		SBC	  <scrl_backct_y
		AND	  #0FFFH
		STA	  <scrl_distance_y    ; Set scroll difference y
;-----------------------------------------------------------------------
Set_scrlpos900	PLP
		RTS
;
;************************************************************************
;*		 Correction scroll difference over			*
;************************************************************************
;
		MEM16
Correct_difover
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
;
;=============== Correct over x ========================================
Correct_over100
		LDA	  <scrl_distance_x
		CMP	  #0011H	      ; distance <= +10H ?
		BCC	  Correct_over200     ; yes.
		CMP	  #1FF0H	      ; distance >= -10H ?
		BCS	  Correct_over200     ; yes.
;
		CMP	  #1000H	      ; distance < 0 ?
		BCS	  Correct_over150     ; yes.
;-----------------------------------------------------------------------
Correct_over110
		EOR	  #0FFFFH
		SEC
		ADC	  #0010H	      ; Acc = 10H - |distance|
		CLC
		ADC	  <scrl_position_x
		AND	  #1FFFH
		STA	  <scrl_position_x    ; Set scroll positon x
		LDA	  #0010H
		STA	  <scrl_distance_x    ; Set scroll difference x
		BRA	  Correct_over200
;-----------------------------------------------------------------------
Correct_over150
		EOR	  #0FFFFH
		INC	  A
		SEC
		SBC	  #0010H	      ; Acc = |distance| - 10H
		CLC
		ADC	  <scrl_position_x
		AND	  #1FFFH
		STA	  <scrl_position_x    ; Set scroll position x
		LDA	  #1FF0H
		STA	  <scrl_distance_x    ; Set scroll difference x
;
;=============== Correct over y ========================================
Correct_over200
		LDA	  <scrl_distance_y
		CMP	  #0011H	      ; distance <= +10H ?
		BCC	  Correct_over300     ; yes.
		CMP	  #0FF0H	      ; distance >= -10H ?
		BCS	  Correct_over300     ; yes.
;
		CMP	  #0800H	      ; distance < 0 ?
		BCS	  Correct_over250     ; yes.
;-----------------------------------------------------------------------
Correct_over210
		EOR	  #0FFFFH
		SEC
		ADC	  #0010H	      ; Acc = 10H - |distance|
		CLC
		ADC	  <scrl_position_y
		AND	  #0FFFH
		STA	  <scrl_position_y    ; Set scroll position x
		LDA	  #0010H
		STA	  <scrl_distance_y    ; Set scroll difference y
		BRA	  Correct_over300
;-----------------------------------------------------------------------
Correct_over250
		EOR	  #0FFFFH
		INC	  A
		SEC
		SBC	  #0010H	      ; Acc = |distance| - 10H
		CLC
		ADC	  <scrl_position_y
		AND	  #0FFFH
		STA	  <scrl_position_y    ; Set scroll position y
		LDA	  #0FF0H
		STA	  <scrl_distance_y    ; Set scroll difference y
;
;=======================================================================
Correct_over300
		PLP
		RTS
;
;************************************************************************
;*		 Set parameter for Hwrite				*
;************************************************************************
;
		MEM16
Set_Hwteparm
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
;
;=============== Set home location x ===================================
Set_Hwteparm100
		LDA	  <scrl_position_x
		STA	  <home_locate_x      ; Set home location x
;
;=============== Set home location y ===================================
Set_Hwteparm200
		LDA	  <scrl_distance_y
		AND	  #0F00H	      ; Scroll distance y < 0 ?
		BNE	  Set_Hwteparm210     ; yes.
;-----------------------------------------------------------------------
		LDA	  <scrl_position_y
		CLC
		ADC	  #03F0H
		AND	  #0FFFH
		STA	  <home_locate_y      ; Set home location y
		BRA	  Set_Hwteparm300
;-----------------------------------------------------------------------
Set_Hwteparm210
		LDA	  <scrl_position_y
		STA	  <home_locate_y      ; Set home location y
;
;=============== Set VRAM write address ================================
Set_Hwteparm300
		AND	  #03F0H	      ; Acc = home location y
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		STA	  <Hwrite_address     ; Set Hwrite address
;
;=============== Set room counter ======================================
Set_Hwteparm400
		LDA	  <home_locate_x+1
		AND	  #001FH
		STA	  <room_count_x	      ; Set room counter x
;
		LDA	  <home_locate_y
		AND	  #0F00H
		LSR	  A
		LSR	  A
		LSR	  A
		STA	  <room_count_y	      ; Set room counter y
;
;=======================================================================
Set_Hwteparm500
		PLP			      ; Restore mode
		RTS
;
;************************************************************************
;*		 Set parameter for Vwrite				*
;************************************************************************
;
		MEM16
Set_Vwteparm
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
;
;=============== Set home location y ===================================
Set_Vwteparm100
		LDA	  <scrl_position_y
		STA	  <home_locate_y      ; Set home location y
;
;=============== Set home location x ===================================
Set_Vwteparm200
		LDA	  <scrl_distance_x
		AND	  #1F00H	      ; Scroll distance x < 0 ?
		BNE	  Set_Vwteparm210     ; yes.
;-----------------------------------------------------------------------
		LDA	  <scrl_position_x
		CLC
		ADC	  #03F0H
		AND	  #1FFFH
		STA	  <home_locate_x      ; Set home location x
		BRA	  Set_Vwteparm300
;-----------------------------------------------------------------------
Set_Vwteparm210
		LDA	  <scrl_position_x
		STA	  <home_locate_x      ; Set home location x
;
;=============== Set VRAM write address ================================
Set_Vwteparm300
		AND	  #03F0H	      ; Acc = home location x
		LSR	  A
		LSR	  A
		LSR	  A
		STA	  <Vwrite_address     ; Set Vwrite address
;
;=============== Set room counter ======================================
Set_Vwteparm400
		LDA	  <home_locate_x+1
		AND	  #001FH
		STA	  <room_count_x
;
		LDA	  <home_locate_y
		AND	  #0F00H
		LSR	  A
		LSR	  A
		LSR	  A
		ORA	  <room_count_x
		STA	  <room_counter	      ; Set room counter
;
;=======================================================================
Set_Vwteparm500
		PLP			      ; Restore mode
		RTS
;
;************************************************************************
;*		 Set horizontal write buffer				*
;************************************************************************
;
		MEM16
		IDX16
Set_Hwtebuff
		PHP
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		PEA	  007FH
		PLB			      ; Data Bank Register = 7FH
;
;=============== Set pointer ===========================================
Set_Hwtebuff100
		LDA	  <home_locate_y
		AND	  #0000000011110000B
		LSR	  A
		LSR	  A
		LSR	  A
		STA	  <room_offset	      ; Set room offset ( panel_y * 2 )
;-----------------------------------------------------------------------
		LDA	  <home_locate_x
		AND	  #0000001111110000B
		LSR	  A
		LSR	  A
		LSR	  A
		TAX			      ; IX = buffer write pointer
;-----------------------------------------------------------------------
		LDA	  #0040H
		STA	  <loop_counter	      ; Set loop counter
;
;=============== Set BG data to write buffer ===========================
Set_Hwtebuff200
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		LDA	  <room_count_x
		ORA	  <room_count_y
		TAY
		LDA	  (<world_pointer),Y
		AND	  #00FFH	      ; Acc.w = room number
;
		XBA
		LSR	  A
		LSR	  A
		LSR	  A		      ; ( CY = 0 )
		ADC	  <room_offset
;
		TAY
		LDA	  !Map_room_data&0FFFFH,Y    ; Acc.w = slit address
		STA	  <slit_pointer	      ; Set slit pointer
;-----------------------------------------------------------------------
		MEM8
		IDX8
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Hwrite loop 2 =========================================
Set_Hwtebuff300
		TXA
		AND	  #00011110B	      ; Acc.b = slit offset ( panel_x * 2 )
Set_Hwtebuff310
		TAY
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		LDA	  (<slit_pointer),Y    ; Acc.w = panel address
		STA	  <panel_pointer      ; Set panel pointer
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDY	  #00H
		LDA	  (<panel_pointer),Y
		STA	  !Hwrite_buffer1&0FFFFH,X   ; Set character 0
		INY
		LDA	  (<panel_pointer),Y
		STA	  !Hwrite_buffer2&0FFFFH,X   ; Set character 1
		INY
		INX
		LDA	  (<panel_pointer),Y
		STA	  !Hwrite_buffer1&0FFFFH,X   ; Set character 2
		INY
		LDA	  (<panel_pointer),Y
		STA	  !Hwrite_buffer2&0FFFFH,X   ; Set character 3
		INX
;-----------------------------------------------------------------------
		DEC	  <loop_counter+0     ; --counter == 0
		BEQ	  Set_Hwtebuff900     ; yes.
;-----------------------------------------------------------------------
		TXA
		AND	  #00011110B	      ; over room ?
		BNE	  Set_Hwtebuff310     ; no.
;
;=============== Increment room x ======================================
Set_Hwtebuff400
		LDA	  <room_count_x+0
		INC	  A
		AND	  #00011111B
		STA	  <room_count_x+0     ; Set next room counter x
;-----------------------------------------------------------------------
		TXA
		AND	  #01111110B
		TAX
		BRA	  Set_Hwtebuff200
;
;=============== End of Hwrite =========================================
Set_Hwtebuff900
		PLB			      ; Set data bank register
		PLP			      ; Restore mode
		RTS
;
;************************************************************************
;*		 Set vertical write buffer				*
;************************************************************************
;
		MEM16
		IDX16
Set_Vwtebuff
		PHP
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		PEA	  007FH
		PLB			      ; Data Bank Register = 7FH
;
;=============== Set pointer ===========================================
Set_Vwtebuff100
		LDA	  <home_locate_x
		AND	  #0000000011110000B
		LSR	  A
		LSR	  A
		LSR	  A
		STA	  <slit_offset	      ; Set slit offset ( panel_x * 2 )
;-----------------------------------------------------------------------
		LDA	  <home_locate_y
		AND	  #0000001111110000B
		LSR	  A
		LSR	  A
		LSR	  A
		TAX			      ; IX = buffer write pointer
;-----------------------------------------------------------------------
		LDA	  #0040H
		STA	  <loop_counter	      ; Set loop counter
;
;=============== Set BG data to write buffer ===========================
Set_Vwtebuff200
		LDY	  <room_counter
		LDA	  (<world_pointer),Y
		AND	  #00FFH	      ; Acc.w = room number
;
		XBA
		LSR	  A
		LSR	  A
		LSR	  A
		STA	  <room_offset	      ; Set room offset
;
;=============== Vwrite loop 2 =========================================
Set_Vwtebuff300
		TXA
		AND	  #0000000000011110B  ; Acc.w = slit offset ( panel_y * 2 )
		CLC
Set_Vwtebuff310
		ADC	  <room_offset
		TAY
		LDA	  !Map_room_data&0FFFFH,Y    ; Acc.w = ribbon address
		STA	  <slit_pointer	      ; Set slit pointer
;-----------------------------------------------------------------------
		IDX8
		SEP	  #00010000B	      ; Index 8 bit mode
		LDY	  <slit_offset+0
		LDA	  (<slit_pointer),Y    ; Acc.b = panel address
		STA	  <panel_pointer      ; Set panel pointer
;-----------------------------------------------------------------------
		LDY	  #00H
		LDA	  (<panel_pointer),Y
		STA	  !Vwrite_buffer1&0FFFFH,X   ; Set character 0,1
		LDY	  #02H
		LDA	  (<panel_pointer),Y
		STA	  !Vwrite_buffer2&0FFFFH,X   ; Set character 2,3
		INX
		INX
;-----------------------------------------------------------------------
		DEC	  <loop_counter	      ; if ( --counter == 0 )
		BEQ	  Set_Vwtebuff900
;-----------------------------------------------------------------------
		IDX16
		REP	  #00010001B	      ; Index 16 bit mode
		TXA
		AND	  #0000000000011110B  ; over room ?
		BNE	  Set_Vwtebuff310     ; no.
;
;=============== Increment room y ======================================
Set_Vwtebuff400
		LDA	  <room_counter
		CLC
		ADC	  #0000000000100000B
		AND	  #0000000111111111B
		STA	  <room_counter	      ; Set next room counter y
;-----------------------------------------------------------------------
		TXA
		AND	  #0000000001111111B
		TAX
		BRA	  Set_Vwtebuff200
;
;=============== End of Vwrite =========================================
Set_Vwtebuff900
		PLB			      ; Set data bank register
		PLP			      ; Restore mode
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Perspective control routines \\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Set demo perspective control				*
;************************************************************************
;
		MEM8
		IDX8
SET_DEMOPERS
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <pers_flag
		CMP	  #02H		      ; pers mode ?
		BCC	  Set_demopers100     ; yes.
		JMP	  !Set_rotation
;
;=============== Set perspective entry =================================
Set_demopers100
		LDA	  <pers_selector
		EOR	  #00010000B
		STA	  <pers_selector
		TAY			      ; IY = buffer selector
;-----------------------------------------------------------------------
		LDA	  <pers_angle
		ASL	  A
		TAX
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
		LDA	  >Demopers_addr+0,X
		STA	  !pers_tableB1+1,Y
		ADC	  #00E0H
		STA	  !pers_tableB1+4,Y
;-----------------------------------------------------------------------
		PLP
		RTL
;
;=======================================================================
Demopers_addr	WORD	  0C360H,0C520H,0C6E0H,0C8A0H
		WORD	  0CA60H,0CC20H,0CDE0H,0CFA0H
		WORD	  0D160H,0D320H,0D4E0H,0D6A0H
		WORD	  0D860H,0DA20H,0DBE0H
;
;************************************************************************
;*		 Set game perspective control				*
;************************************************************************
;
		MEM8
		IDX8
SET_GAMEPERS
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <pers_flag
		CMP	  #02H		      ; pers mode ?
		BCC	  Set_gamepers100     ; yes.
		JMP	  !Set_rotation
;
;=============== Set petspective entry =================================
Set_gamepers100
		LDA	  <pers_selector
		EOR	  #00010000B
		STA	  <pers_selector
		TAY			      ; IY = buffer selector
;-----------------------------------------------------------------------
		LDA	  <screen_angle
		SEC
		SBC	  #48
		BCC	  Set_gamepers400	  ; area 0
		SBC	  #48
		BCC	  Set_gamepers500	  ; area 1
		SBC	  #48
		BCC	  Set_gamepers600	  ; area 2
		JMP	  !Set_gamepers700	  ; area 3
;
;=============== area 0 ================================================
;
		MEM8
Set_gamepers400
		ADC	  #48
		STA	  <work0+0
		STZ	  <work0+1
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B
		JSR	  COS_pers
		STA	  !pers_tableA1+4,Y   ; matrix A = +COS
		CLC
		ADC	  #224
		STA	  !pers_tableA1+7,Y
;
		JSR	  SIN_pers
		STA	  !pers_tableB1+4,Y   ; matrix B = +SIN
		CLC
		ADC	  #224
		STA	  !pers_tableB1+7,Y
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B
		LDA	  #0EH
		STA	  <pers_databank_A
		STA	  <pers_databank_D
		STA	  <pers_databank_B
		LDA	  #7EH
		STA	  <pers_databank_C
;-----------------------------------------------------------------------
		PLP
		RTL
;
;=============== area 1 ================================================
Set_gamepers500
		ADC	  #48
		STA	  <work0+0
		STZ	  <work0+1
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B
		JSR	  SIN_pers
		STA	  !pers_tableA1+4,Y   ; matrix A = -SIN
		CLC
		ADC	  #224
		STA	  !pers_tableA1+7,Y
;
		JSR	  COS_pers
		STA	  !pers_tableB1+4,Y   ; matrix B = +COS
		CLC
		ADC	  #224
		STA	  !pers_tableB1+7,Y
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B
		LDA	  #7EH
		STA	  <pers_databank_A
		STA	  <pers_databank_D
		STA	  <pers_databank_C
		LDA	  #0EH
		STA	  <pers_databank_B
;-----------------------------------------------------------------------
		PLP
		RTL
;
;=============== area 2 ================================================
Set_gamepers600
		ADC	  #48
		STA	  <work0+0
		STZ	  <work0+1
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B
		JSR	  COS_pers
		STA	  !pers_tableA1+4,Y   ; matrix A = -COS
		CLC
		ADC	  #224
		STA	  !pers_tableA1+7,Y
		JSR	  SIN_pers
;
		STA	  !pers_tableB1+4,Y   ; mairix B = -SIN
		CLC
		ADC	  #224
		STA	  !pers_tableB1+7,Y
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B
		LDA	  #7EH
		STA	  <pers_databank_A
		STA	  <pers_databank_D
		STA	  <pers_databank_B
		LDA	  #0EH
		STA	  <pers_databank_C
;-----------------------------------------------------------------------
		PLP
		RTL
;
;=============== area 3 ================================================
Set_gamepers700
		STA	  <work0+0
		STZ	  <work0+1
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B
		JSR	  SIN_pers
		STA	  !pers_tableA1+4,Y   ; matrix A = +SIN
		CLC
		ADC	  #224
		STA	  !pers_tableA1+7,Y
;
		JSR	  COS_pers
		STA	  !pers_tableB1+4,Y   ; matrix B = -COS
		CLC
		ADC	  #224
		STA	  !pers_tableB1+7,Y
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B
		LDA	  #0EH
		STA	  <pers_databank_A
		STA	  <pers_databank_D
		STA	  <pers_databank_C
		LDA	  #7EH
		STA	  <pers_databank_B
;-----------------------------------------------------------------------
		PLP
		RTL
;
;************************************************************************
;*		 Calcurate perspective data address			*
;************************************************************************
;
		MEM16
COS_pers
		LDA	  #48
		SEC
		SBC	  <work0
		BRA	  SIN_pers110
;
;=======================================================================
SIN_pers
		LDA	  <work0
SIN_pers110
		STA	  <work1
		ASL	  A
		ASL	  A
		ADC	  <work1	      ; Acc = work * 5
		ASL	  A		      ; Acc = work * 10
		ADC	  <work1	      ; Acc = work * 11
;
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A		      ; Acc = work * 352
		ORA	  #8000H
		RTS
;
;************************************************************************
;*		 Set rotation matrix					*
;************************************************************************
;
		MEM8
		IDX8
;
;=============== Calculate zoom * cos( A ) =============================
Set_rotation
		LDX	  !car_angle+1
		LDA	  >COS_absolute,X    ; Acc = COS( car_angle )
		STA	  !Multiplicand
		LDA	  <screen_zoom+0
		STA	  !Multiplier
		NOP
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		STZ	  <matrix_temp+2
		LDA	  !Multiply
		STA	  <matrix_temp+0
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  >COS_absolute,X    ; Acc = COS( car_angle )
		STA	  !Multiplicand
		LDA	  <screen_zoom+1
		STA	  !Multiplier
		NOP
		NOP
		NOP
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
		LDA	  !Multiply
		ADC	  <matrix_temp+1
		STA	  <matrix_temp+1
;-----------------------------------------------------------------------
		ASL	  <matrix_temp+0
		ROL	  <matrix_temp+2
;
;=============== Set matrix_A ==========================================
Set_rotation150
		LDA	  >COS_sign,X
		AND	  #00FFH
		BEQ	  Set_rotation160
;-----------------------------------------------------------------------
		SEC
		LDA	  #0000H
		SBC	  <matrix_temp+1
		STA	  <matrix_A
		BRA	  Set_rotation200
;-----------------------------------------------------------------------
Set_rotation160
		LDA	  <matrix_temp+1
		STA	  <matrix_A
;
;=============== Calculate zoom * sin( A ) =============================
Set_rotation200
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  >SIN_absolute,X    ; Acc = SIN( car_angle )
		STA	  !Multiplicand
		LDA	  <screen_zoom+0
		STA	  !Multiplier
		NOP
		NOP
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		STZ	  <matrix_temp+2
		LDA	  !Multiply
		STA	  <matrix_temp+0
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  >SIN_absolute,X    ; Acc = SIN( car_angle )
		STA	  !Multiplicand
		LDA	  <screen_zoom+1
		STA	  !Multiplier
		NOP
		NOP
		NOP
		MEM16
		REP	  #00100001B	      ; Memory 16 bit mode
		LDA	  !Multiply
		ADC	  <matrix_temp+1
		STA	  <matrix_temp+1
;-----------------------------------------------------------------------
		ASL	  <matrix_temp+0
		ROL	  <matrix_temp+2
;
;=============== Set matrix B and C ====================================
Set_rotation250
		LDA	  >SIN_sign,X
		AND	  #00FFH
		BNE	  Set_rotation270
;-----------------------------------------------------------------------
Set_rotation260
		SEC
		LDA	  #0000H
		SBC	  <matrix_temp+1
		STA	  <matrix_B
		LDA	  <matrix_temp+1
		STA	  <matrix_C
		PLP
		RTL
;
;-----------------------------------------------------------------------
Set_rotation270
		SEC
		LDA	  #0000H
		SBC	  <matrix_temp+1
		STA	  <matrix_C
		LDA	  <matrix_temp+1
		STA	  <matrix_B
		PLP
		RTL
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Transfer data to work RAM \\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Transfer Enemy character data				*
;************************************************************************
;
		MEM16
		IDX8
TRANSFER_ENEMY
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
		SEP	  #00010000B	      ; Index 8 bit mode
		LDX	  #00H		      ; IX = address data pointer
;
;=============== Set destination address ===============================
Trans_enemy100
		LDA	  >Enemy_data,X	     ; get data length
		BEQ	  Trans_enemy300      ; if ( no data )
		TAY			      ; IY = character counter
		XBA
		STA	  <buff_address+0     ; Set destination address low
		INX
		INX
		LDA	  >Enemy_data,X
		STA	  <buff_address+1     ; Set destination address high
		INX
		INX
		LDA	  >Enemy_data,X
		STA	  <data_address+0     ; Set sourse address low
		INX
		LDA	  >Enemy_data,X
		STA	  <data_address+1     ; Set sourse address high
		INX
		INX
		PHX
;
;=============== Transfer data =========================================
Trans_enemy200
		PHY
		LDY	  #00H
		LDX	  #8
Trans_enemy210	LDA	  [<data_address],Y
		STA	  [<buff_address],Y
		INC	  <data_address
		INC	  <data_address
		INC	  <buff_address
		INC	  <buff_address
		DEX
		BNE	  Trans_enemy210
;-----------------------------------------------------------------------
		LDY	  #00H
		LDX	  #8
Trans_enemy220	LDA	  [<data_address],Y
		AND	  #00FFH
		STA	  [<buff_address],Y
		INC	  <data_address
		INC	  <buff_address
		INC	  <buff_address
		DEX
		BNE	  Trans_enemy220
;-----------------------------------------------------------------------
		PLY
		DEY
		BNE	  Trans_enemy200
;-----------------------------------------------------------------------
		PLX
		BRA	  Trans_enemy100
;-----------------------------------------------------------------------
Trans_enemy300	PLP
		RTL
;
;=============== destination pointer ===================================
;
;			  LEN DAL DAH DBK SAL  SAH  SBK
Enemy_data	BYTE	  252,00H,20H,7EH,000H,0D1H,0DH
		BYTE	  249,00H,40H,7EH,0A0H,0E8H,0DH
		BYTE	  189,00H,60H,7EH,000H,0CEH,02H	 ; for REAL
;;;;		BYTE	  189,00H,60H,7EH,000H,0CEH,79H	 ; for EMULATOR
		WORD	  0000H
;-----------------------------------------------------------------------
Minicar_char	EQU	  02CB80H	      ; for REAL
;;;;Minicar_char    EQU	      79CB80H		  ; for EMULATOR
;
;************************************************************************
;*		 Transfer mini car ( special demo )			*
;************************************************************************
;
		MEM8
TRANS_MINICAR
		PHP
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #10000000B
		STA	  !Screen_step	      ; Set screen write step
;-----------------------------------------------------------------------
		MEM16
		IDX16
		REP	  #00110000B	      ; Memory,Index 16 bit mode
		LDA	  #05000H
		STA	  !Screen_address     ; Set write address
;-----------------------------------------------------------------------
		LDY	  #0140H
		LDX	  #0000H
Trans_minicar10 LDA	  >Minicar_char,X
		STA	  !Screen_write
		INX
		INX
		DEY
		BNE	  Trans_minicar10
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Transfer OBJ character into VRAM			*
;************************************************************************
;
		MEM8
		IDX16
TRANSFER_OBJECT
		PHP
		SEP	  #00100000B	      ; Memory 8 bit mode
		REP	  #00010000B	      ; Index 16 bit mode
;-----------------------------------------------------------------------
		PEA	  000FH
		PLB			      ; Data bank register = 0FH
		LDA	  #10000000B
		STA	  !Screen_step	      ; Set screen write step
		STX	  <param1	      ; param1.w = VRAM top address
;
;=============== Check transfer mode ===================================
Trans_object100
		LDA	  !8000H,Y	      ; get data
		BEQ	  Trans_object190     ; if ( end of data )
;-----------------------------------------------------------------------
		AND	  #00111111B
		STA	  <param0	      ; param0 = data length
		LDA	  !8000H,Y	      ; get data
		ASL	  A
		ROL	  A
		ROL	  A
		ASL	  A
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		AND	  #0000000000000110B
		TAX			      ; IX = process index
;-----------------------------------------------------------------------
		LDA	  !8001H,Y	      ; get character number
		AND	  #00FFH
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		ADC	  <param1	      ; Add VRAM top address
		STA	  !Screen_address     ; Set screen write address
		INY			      ; Increment get pointer
		INY
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
Trans_object110 JSR	  (!Trans_vector,X)
		DEC	  <param0
		BNE	  Trans_object110
		BRA	  Trans_object100
;-----------------------------------------------------------------------
Trans_object190 PLB			      ; Restore data bank
		PLP
		RTL
;
;=============== Transfer routine pointer ==============================
;
Trans_vector	WORD	  Trans_object200     ; Transfer 4 bit data
		WORD	  Trans_object300     ; Transfer 1 bit data
		WORD	  Trans_object400     ; Transfer 2 bit data
		WORD	  Trans_object500     ; Transfer 3 bit data
;
;=============== Transfer 4 bit data ===================================
Trans_object200
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		PHX
		LDX	  #0010H	      ; IX = loop counter
Trans_object210 LDA	  !8000H,Y	      ; Get data from ROM
		STA	  !Screen_write	      ; Put data into VRAM
		INY			      ; Increment get pointer
		INY
		DEX			      ; decrement counter
		BNE	  Trans_object210
		PLX
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Transfer 1 bit data ===================================
Trans_object300
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		PHX
		LDX	  #0008H	      ; IX = loop counter
Trans_object310 STZ	  !Screen_write	      ; Put null data into VRAM
		DEX			      ; Decrement counter
		BNE	  Trans_object310
;-----------------------------------------------------------------------
		LDX	  #0008H	      ; IX = loop counter
Trans_object320 LDA	  !7FFFH,Y	      ; Get data from ROM
		AND	  #0FF00H
		STA	  !Screen_write	      ; Put data into VRAM
		INY			      ; Increment get pointer
		DEX			      ; Decrement counter
		BNE	  Trans_object320
		PLX
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Transfer 2 bit data ===================================
Trans_object400
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		PHX
		LDX	  #0008H	      ; IX = loop counter
Trans_object410 LDA	  !8000H,Y	      ; Get data from ROM
		STA	  !Screen_write	      ; Put data into VRAM
		INY			      ; Increment get pointer
		INY
		DEX			      ; Decrement counter
		BNE	  Trans_object410
;-----------------------------------------------------------------------
		LDX	  #0008H	      ; IX = loop counter
Trans_object420 STZ	  !Screen_write	      ; Put null data into VRAM
		DEX			      ; Decrement counter
		BNE	  Trans_object420
		PLX
;-----------------------------------------------------------------------
		SEP	  #00100000B	      ; Memory 8 bit mode
		RTS
;
;=============== Transfer 3 bit data ===================================
Trans_object500
		MEM8
		PHX
		LDX	  #0007H	      ; IX = buffer pointer
Trans_object510 LDA	  !8000H,Y	      ; Get data from ROM
		STA	  !Screen_write+0     ; Put data into VRAM low
		STA	  !string_buffer,X
		INY			      ; Increment get pointer
		LDA	  !8000H,Y	      ; Get data from ROM
		STA	  !Screen_write+1     ; Put data into VRAM high
		ORA	  !string_buffer,X
		STA	  !string_buffer,X
		INY			      ; Increment get pointer
		DEX			      ; decrement buffer pointer
		BPL	  Trans_object510
;-----------------------------------------------------------------------
		LDX	  #0007H	      ; IX = buffer pointer
Trans_object520 LDA	  !8000H,Y	      ; Get data from ROM
		STA	  !Screen_write+0     ; Put data into VRAM low
		ORA	  !string_buffer,X
		STA	  !Screen_write+1     ; Put data into VRAM high
		INY			      ; Increment get pointer
		DEX			      ; Decrement buffer pointer
		BPL	  Trans_object520
;-----------------------------------------------------------------------
		PLX
		RTS
;
;************************************************************************
;*		 Transfer demo pers data				*
;************************************************************************
;
		MEM16
		IDX16
TRANSFER_PERS
		PHP
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		PEA	  000EH
		PLB			      ; Data Bank Register = 0EH
;
;=============== Transfer main =========================================
Trans_pers100
		LDX	  #5D9EH
Trans_pers110	LDA	  !8000H,X
		EOR	  #0FFFFH
		INC	  A
		STA	  >7E8000H,X
		DEX
		DEX
		BPL	  Trans_pers110
;-----------------------------------------------------------------------
		PLB			      ; Data Bank Register = 00H
		PLP
		RTL
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Expand BG screen data \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Expand back screen					*
;************************************************************************
;
		MEM8
		IDX16
EXPAND_BACK
		PHP
		SEP	  #00100000B	      ; Memory 8 bit mode
		REP	  #00010000B	      ; Index 16 bit mode
;-----------------------------------------------------------------------
		PEA	  000FH
		PLB			      ; Set data bank register
		LDX	  #3000H	      ; IX = destination address
;
;=============== Check transfer mode ===================================
Expand_back100
		LDA	  !8000H,Y
		CMP	  #11111100B	      ; End of data ?
		BEQ	  Expand_back110      ; yes.
;-----------------------------------------------------------------------
		AND	  #00000011B	      ; non compress ?
		BEQ	  Expand_back200      ; yes.
		DEC	  A		      ; 2 byte compress ?
		BEQ	  Expand_back300      ; yes.
		DEC	  A		      ; 3 byte compress ?
		BEQ	  Expand_back400      ; yes.
		BRA	  Expand_back500      ; null char compress
;-----------------------------------------------------------------------
Expand_back110	PLB			      ; Reset data bank
		PLP
		RTL
;
;=============== Non compress character ================================
Expand_back200
		LDA	  !8000H,Y
		ORA	  #00011000B
		STA	  >7F0001H,X	     ; Set character attribute
		LDA	  !8001H,Y
		STA	  >7F0000H,X	     ; Set character number
		INY
		INY
		INX
		INX
		BRA	  Expand_back100
;
;=============== 2 byte compress character =============================
Expand_back300
		MEM8
		JSR	  Expand_back900      ; Set character code
;-----------------------------------------------------------------------
		LDA	  !7FFEH,Y
		LSR	  A
		LSR	  A
		LSR	  A
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		AND	  #0007H	      ; Acc = loop counter
		BRA	  Expand_back600

;=============== 3 byte compress character =============================
Expand_back400
		MEM8
		JSR	  Expand_back900      ; Set character code
;-----------------------------------------------------------------------
		LDA	  !8000H,Y
		INY
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		AND	  #00FFH	      ; Acc = loop counter
		BRA	  Expand_back600
;
;=============== Null character compress ===============================
Expand_back500
		MEM8
		LDA	  #00011101B
		STA	  <expand_char+1      ; Set null character attribute
		LDA	  #80H
		STA	  <expand_char+0      ; Set null character number
;-----------------------------------------------------------------------
		LDA	  !8000H,Y
		INY
		LSR	  A
		LSR	  A
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		AND	  #003FH	      ; Acc = loop counter
;
;=============== Set character code ====================================
Expand_back600
		PHY
		TAY			      ; IY = loop counter
		LDA	  <expand_char
;-----------------------------------------------------------------------
Expand_back610	STA	  >7F0000H,X	     ; Set character code
		INX
		INX
		DEY
		BPL	  Expand_back610
;-----------------------------------------------------------------------
		PLY
		SEP	  #00100000B	      ; Memory 8 bit mode
		BRA	  Expand_back100
;
;=============== Set character code ====================================
Expand_back900
		MEM8
		LDA	  !8000H,Y	      ; Get attribute
		AND	  #11000100B
		ORA	  #00011000B	      ; Set color palette number
		STA	  <expand_char+1      ; Set character attribute
		LDA	  !8001H,Y
		STA	  <expand_char+0      ; Set character number
		INY
		INY
		RTS
;
;************************************************************************
;*		 Expand select screen					*
;************************************************************************
;
		MEM16
		IDX16
EXPAND_SELECT
		PHP
		REP	  #00110000B	      ; Memory,Index 16 bit mode
;-----------------------------------------------------------------------
		PHA
		PLB			      ; Set data bank register
;
;=============== Check transfer mode ===================================
Expand_sel100
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #10000000B
		STA	  !Screen_step	      ; Set VRAM inc. mode
		STY	  !Screen_address     ; Set VRAM address
;-----------------------------------------------------------------------
Expand_sel110	LDA	  #00100000B
		BIT	  !8000H,X
		BEQ	  Expand_sel200	      ; if ( non compress )
		BMI	  Expand_sel400	      ; if ( compress attribute )
		BVC	  Expand_sel300	      ; if ( compress character )
;-----------------------------------------------------------------------
		PLB			      ; Reset data bank
		PLP
		RTL
;
;=============== Non compress ==========================================
Expand_sel200
		LDA	  !8001H,X
		STA	  <expand_char+0
		STA	  !Screen_write+0     ; Write screen low
		LDA	  !8000H,X
		STA	  <expand_char+1
		STA	  !Screen_write+1     ; Write screen high
		INX
		INX
		BRA	  Expand_sel110
;
;=============== Compress character ====================================
Expand_sel300
		LDA	  !8000H,X
		INX
		AND	  #00011111B	      ; Acc = loop counter
		LDY	  <expand_char	      ; IX  = character code
Expand_sel310	STY	  !Screen_write
		DEC	  A
		BPL	  Expand_sel310
		BRA	  Expand_sel110
;
;=============== Compress attribute ====================================
Expand_sel400
		LDA	  #00000000B
		XBA
		LDA	  !8000H,X
		INX
		AND	  #00011111B
		TAY			      ; IY = loop counter
Expand_sel410	LDA	  !8000H,X
		STA	  !Screen_write+0     ; Write character number
		LDA	  <expand_char+1
		STA	  !Screen_write+1     ; Write attribute
		INX
		DEY
		BPL	  Expand_sel410
		BRA	  Expand_sel110
;
;************************************************************************
;*		 Transfer Car select character				*
;************************************************************************
;
		MEM8
		IDX16
TRANS_SEL_CHAR
		PHP
		SEP	  #00100000B	      ; Memory 8 bit mode
		REP	  #00010000B	      ; Index 16 bit mode
;-----------------------------------------------------------------------
		PEA	  000EH
		PLB			      ; Data bank register = 0DH
		LDA	  #10000000B
		STA	  !Screen_step	      ; Set screen write step
		STX	  !Screen_address     ; Set screen write address
		LDX	  #0000H	      ; IX = data pointer
;
;=============== Transfer data =========================================
Trans_selchr200
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
Trans_selchr210 LDY	  #0007H
Trans_selchr220 LDA	  !0E800H,X	      ; Get data from ROM
		STA	  !Screen_write	      ; Put data into VRAM
		INX
		INX			      ; Increment get pointer
		DEY
		BPL	  Trans_selchr220
;-----------------------------------------------------------------------
		LDY	  #0007H
Trans_selchr230 LDA	  !0E800H,X	      ; Get data from ROM
		AND	  #00FFH
		STA	  !Screen_write	      ; Put data into VRAM
		INX			      ; Increment get pointer
		DEY
		BPL	  Trans_selchr230
;-----------------------------------------------------------------------
		CPX	  #1800H
		BNE	  Trans_selchr210
;-----------------------------------------------------------------------
		PLB			      ; Data bank register = 0
		PLP
		RTL
;
;************************************************************************
;*		 Transfer title character				*
;************************************************************************
;
		MEM16
		IDX8
TRANS_TITLE_CHR
		PHP
		REP	  #00100000B	      ; Memory 16 bit mode
		SEP	  #00010000B	      ; Index 8 bit mode
;-----------------------------------------------------------------------
		LDY	  #10000000B
		STY	  !Screen_step	      ; Set screen write step
		LDA	  #1801H
		STA	  !DMA_0+0	      ; Set DMA ctrl & B address
		LDY	  #0CH
		STY	  !DMA_0+4	      ; Set DMA A bank
		LDY	  #00000001B
		LDX	  #5AH		      ; IX = table pointer
;-----------------------------------------------------------------------
Trans_title_ch2 LDA	  >Title_char_addr+0,X
		STA	  !Screen_address     ; Set screen write address
		LDA	  >Title_char_addr+2,X
		STA	  !DMA_0+2	      ; Set DMA A address
		LDA	  >Title_char_addr+4,X
		STA	  !DMA_0+5	      ; Set DMA data length
		STY	  !DMA_burst	      ; Start DMA channel #0
		TXA
		SEC
		SBC	  #06H
		TAX
		BCS	  Trans_title_ch2
;-----------------------------------------------------------------------
		PLP
		RTL
;
;=======================================================================
;			  VRAM	ROM    LEN
Title_char_addr WORD	  4000H,0EC00H,0180H
		WORD	  4100H,0EE00H,0180H
		WORD	  4200H,0F000H,0180H
		WORD	  4300H,0F200H,0180H
		WORD	  4400H,0F400H,0180H
		WORD	  4500H,0F600H,0180H
		WORD	  4600H,0ED80H,0080H
		WORD	  4700H,0EF80H,0080H
		WORD	  4640H,0F180H,0080H
		WORD	  4740H,0F380H,0080H
		WORD	  4680H,0F580H,0080H
		WORD	  4780H,0F780H,0080H
		WORD	  5200H,0F800H,0540H
		WORD	  5500H,0FE00H,0140H
		WORD	  5060H,0FD60H,00A0H
		WORD	  50B0H,0FF60H,00A0H
;
;************************************************************************
;*		 Title object data					*
;************************************************************************
;
TITLE_OBJECT	EQU	  0BEE94H
TITLE_SUB	EQU	  0BEF5CH
;
		END
