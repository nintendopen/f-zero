.SUFFIXES:	.rel .asm

.asm.rel:
		as65c -l $<

MAP = fzero_pal.map
SYM = fzero_pal.sym
HEX = fzero_pal.hex
REL = fzero_main.rel title_main_usa.rel calculate_usa.rel play_main_pal.rel car_control.rel \
       set_bg1.rel set_bg2_usa.rel set_back.rel player.rel effect.rel game_over_usa.rel \
       en-init-1.rel en-init-3.rel en-init-4.rel en-check.rel en-drive.rel set-obj.rel \
       sound.rel control_data.rel en-data-1.rel en-data-2.rel data-5.rel data-6.rel

$(HEX):		link.out
		load -o $(HEX)
		insp -f -s $(SYM) link.out

link.out:	$(REL)
		link $(REL) -r PROG=8000,DATA=38000 -ls $(MAP)

clean:
		'rm'	$(REL) $(HEX) $(SYM)
