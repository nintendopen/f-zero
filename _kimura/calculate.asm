;************************************************************************
;*	 CALCULATE	   -- data calculate module --			*
;************************************************************************
;
		INCLUDE	  RP5A22:
		INCLUDE	  VARIABLE
		INCLUDE	  BUFFER
;
;=============== Cross refference ======================================
;
		EXT	  Damage_data_1,Damage_data_2,Damage_data_3,Damage_data_4,Damage_data_5
		EXT	  Clear_roadcolor,Set_laser_color,Scan_emulate
		EXT	  Map_data_number,Demo_stick_wld1,Demo_stick_wld2,Demo_stick_wld3
;
;=============== Cross define ==========================================
;
		GLB	  Calc_lap_time,CALC_LAP_TIME,Calculate_score,Set_backup_bank
		GLB	  Crash_damage,Graze_damage,Wall_damage,Out_damage,Bomb_damage
		GLB	  Set_demo_stick,Enemy_jump,Set_unctrl_time
		GLB	  Set_backup_buff,Set_best_time,ERASE_RECORD
		GLB	  Backup_pointer,Copy_time_rank,Change_BCD,CHANGE_BCD
;
;=============== Define constant =======================================
;
Backup_RAM	EQU	  700000H	      ; Backup RAM address
Demo_stick_data EQU	  0BEF70H	      ; Demo data
;
;=======================================================================
;
		PROG
		EXTEND
;
;************************************************************************
;*		 Bank 3 interface					*
;************************************************************************
;
Set_backup_bank JMP	  >SET_BACKUP_BANK
Set_backup_buff JMP	  >SET_BACKUP_BUFF
Set_best_time	JMP	  >SET_BEST_TIME
Calc_lap_time	JMP	  >CALC_LAP_TIME
Copy_time_rank	JMP	  >COPY_TIME_RANK
Change_BCD	JMP	  >CHANGE_BCD
;
;************************************************************************
;*		 Calculate score					*
;************************************************************************
;
		MEM16
		IDX8
Calculate_score
		PHP
		REP	  #00100001B	      ; Memory 16 bit mode
		SEP	  #00011000B	      ; Index 8 bit,BCD mode
;-----------------------------------------------------------------------
		LDA	  !score_buffer+0
		ADC	  <param0
		STA	  !score_buffer+0
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Set uncontrol time					*
;************************************************************************
;
		MEM8
		IDX8
Set_unctrl_time
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		CMP	  !car_unctrl_flag+0,X
		BCC	  Set_unctrl_tm10
		STA	  !car_unctrl_flag+0,X
		LDA	  #00H
		STA	  !car_unctrl_flag+1,X
;-----------------------------------------------------------------------
Set_unctrl_tm10 PLP
		RTS
;
;************************************************************************
;*		 Calculate damage ( my car crash enemy )		*
;************************************************************************
;
		MEM8
		IDX8
Crash_damage
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Calculate car speed difference ========================
Crash_damage100
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
;-----------------------------------------------------------------------
		LDA	  !car_speed+0
		ASL	  A
		ASL	  A
		ASL	  A		      ; Acc = my car speed * 8
		STA	  <param0	      ; param1 = 0****.***B
		LDA	  !car_speed,X
		LSR	  A
		LSR	  A
		LSR	  A
		LSR	  A		      ; Acc = enemy car speed / 16
		MEM8
		SEP	  #00100001B	      ; Memory 8 bit mode ( CY=1 )
		STA	  !Multiplicand	      ; Multiplicand = ****.****B
;-----------------------------------------------------------------------
		LDA	  !car_scrl_angle,X
		SBC	  !car_scrl_angle     ; Caclulate angle difference
		BCS	  Crash_damage110
		ADC	  #192
;-----------------------------------------------------------------------
Crash_damage110 TXY
		TAX
		LDA	  >COS_absolute,X
		STA	  !Multiplier	      ; Set COS( angle_deff. )
		LDA	  >COS_sign,X	     ; COS( angle ) < 0 ?
		BNE	  Crash_damage120     ; yes.
;-----------------------------------------------------------------------
		SEC
		LDA	  <param1
		SBC	  !Multiply+1	      ; my_car_speed - enemy_speed
		BCS	  Crash_damage200     ; if ( plus )
		EOR	  #11111111B
		INC	  A		      ; ( = NOT	  A )
		BRA	  Crash_damage200
;-----------------------------------------------------------------------
Crash_damage120 CLC
		LDA	  <param1
		ADC	  !Multiply+1	      ; my_car_speed + enemy_speed
;
;=============== Calculate damage value ================================
Crash_damage200
		TYX
		CMP	  #7FH		      ; car_speed < 7FH ?
		BCC	  Crash_damage210     ; yes.
		LDA	  #7FH		      ; car_speed = 7FH
Crash_damage210 STA	  !Multiplicand	      ; Set speed difference * 16
;-----------------------------------------------------------------------
		LDY	  <mycar_number
		LDA	  !Damage_data_1,Y
		STA	  !Multiplier	      ; Set max damage
;-----------------------------------------------------------------------
		LDA	  #10000000B
		TSB	  !crash_priority
		LDA	  #00000011B
		TSB	  <sound_status_2     ; Set SOUND
;-----------------------------------------------------------------------
		LDA	  !Multiply+1
		BRA	  Set_damage
;
;************************************************************************
;*		 Calculate damage ( my car graze enemy )		*
;************************************************************************
;
		MEM8
		IDX8
Graze_damage
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  #10000000B
		TSB	  !crash_priority
		LDA	  #00000001B
		TSB	  <sound_status_2     ; Set SOUND
;-----------------------------------------------------------------------
		LDY	  <mycar_number
		LDA	  !Damage_data_2,Y
		BRA	  Set_damage
;
;************************************************************************
;*		 Set damage ( my car on wall )				*
;************************************************************************
;
		MEM8
		IDX8
Wall_damage
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDY	  <mycar_number
		LDA	  !Damage_data_3,Y
		BRA	  Set_damage
;
;************************************************************************
;*		 Set damage ( my car out of course )			*
;************************************************************************
;
		MEM8
		IDX8
Out_damage
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDY	  <mycar_number
		LDA	  !Damage_data_4,Y
		BRA	  Set_damage
;
;************************************************************************
;*		 Set damage ( my car bomb )				*
;************************************************************************
;
		MEM8
		IDX8
Bomb_damage
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDY	  <mycar_number
		LDA	  !Damage_data_5,Y
;
;=============== Set damage ============================================
Set_damage
		LDY	  <mycar_damage+1     ; shield down ?
		BMI	  Mycar_broken	      ; yes.
;-----------------------------------------------------------------------
		MEM16
		REP	  #00100000B	      ; Memory 16 bit mode
		AND	  #000FFH
		EOR	  #0FFFFH
		SEC			      ; ( = NOT	   A )
		ADC	  <mycar_damage
		STA	  <mycar_damage	      ; Set damage
;-----------------------------------------------------------------------
		CPY	  #02H
		BCC	  Set_damage110
		CMP	  #0200H
		BCS	  Set_damage110
;-----------------------------------------------------------------------
		MEM8
		SEP	  #00100000B	      ; Memory 8 bit mode
		LDA	  #01000000B
		TSB	  <message_flag	      ; Display "POWER DOWN"
;-----------------------------------------------------------------------
Set_damage110	PLP
		RTS
;
;=============== my car broken =========================================
Mycar_broken
		LDA	  #01000000B
		TSB	  <exception_flag     ; Set game over exception
		LDA	  #06H
		STA	  <explosion_count
		LDA	  !round_counter
		STA	  !rast_round
;-----------------------------------------------------------------------
		PLP			      ; Restore mode
		RTS
;
;************************************************************************
;*		 Jump							*
;************************************************************************
;
		DATA
		MEM8
		IDX8
Enemy_jump
		LDA	  #255
		SEC
		SBC	  !car_position_v+0,X
		TXY
		TAX
		LDA	  >Jump_pers_data,X
		STA	  !Multiplicand
		TYX
;-----------------------------------------------------------------------
		LDA	  !car_hight+1,X
		BMI	  Enemy_jump190
		ASL	  A
		STA	  !Multiplier
		NOP
		NOP
		NOP
		NOP
		LDA	  !Multiply+1
Enemy_jump190	RTL
;
;************************************************************************
;*		 Set demo stick status					*
;************************************************************************
;
		MEM8
		IDX8
Set_demo_stick
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Set stick_backst ======================================
Demo_stick100
		LDA	  !demo_back_data
		JSR	  Demo_stick500	      ; Set stick_backst
		AND	  #11101111B
		STA	  <stick_backst+1
		STZ	  <stick_backst+0
;-----------------------------------------------------------------------
		LDY	  !special_demo	      ; special demo ?
		BEQ	  Demo_stick200	      ; no.
		STZ	  <stick_status+1     ; Clear START
;
;=============== Set stick_status ======================================
Demo_stick200
		LDA	  <mycar_damage+1
		CMP	  #08H		      ; my car damage ?
		BNE	  Demo_stick210	      ; yes. ( demo abort )
;-----------------------------------------------------------------------
		LDA	  !demo_status
		BIT	  #00011111B	      ; repeat counter == 0 ?
		BNE	  Demo_stick250	      ; no.
;--------------------------------------------------- Set data address --
		LDA	  !special_demo
		ASL	  A
		ADC	  !special_demo
		TAX
		LDA	  >Demo_stick_ptr+0,X
		STA	  <param0
		LDA	  >Demo_stick_ptr+1,X
		STA	  <param1
		LDA	  >Demo_stick_ptr+2,X
		STA	  <param2
;------------------------------------------------------- Get new data --
		LDY	  !demo_data_point
		LDA	  [<param0],Y	       ; Get new data
		BEQ	  Demo_stick210
		INC	  !demo_data_point    ; Increment pointer
		BRA	  Demo_stick250
;----------------------------------------------------------- Deta end --
Demo_stick210	LDA	  #00010000B
		STA	  <stick_status+1
		LDA	  #00000001B
;-----------------------------------------------------------------------
Demo_stick250	DEC	  A
		STA	  !demo_status	      ; Set next status
		JSR	  Demo_stick500
		STA	  <stick_status+1
		STZ	  <stick_status+0
;
;=============== Controller scan emulate ===============================
Demo_stick300
		PHK
		PER	  3
		JMP	  >Scan_emulate
		LDA	  !demo_status
		STA	  !demo_back_data
;-----------------------------------------------------------------------
		PLP
		RTL
;
;=============== Make stick status =====================================
Demo_stick500
		PHA
		AND	  #10000000B
		STA	  <param0	      ; Set Accele
		PLA
		ASL	  A
		ASL	  A
		ROL	  A
		ROL	  A
		AND	  #00000011B
		TSB	  <param0	      ; Set Left,Right
;-----------------------------------------------------------------------
		LDA	  <stick_status+0     ; push A ?
		BMI	  Demo_stick510	      ; yes.
		LDA	  <stick_status+1
		AND	  #10010000B	      ; push START or B ?
		BEQ	  Demo_stick520	      ; no.
Demo_stick510	LDA	  #00010000B
		TSB	  <param0	      ; Set START
;-----------------------------------------------------------------------
Demo_stick520	LDA	  !special_demo
		CMP	  #02H		      ; Queen ?
		BNE	  Demo_stick530	      ; no.
		LDA	  #00000100B
		ORA	  <param0
		RTS
;-----------------------------------------------------------------------
Demo_stick530	LDA	  <param0
		RTS
;
;=======================================================================
;
Demo_stick_ptr	BYTE	  LOW  Demo_stick_data
		BYTE	  HIGH Demo_stick_data
		BYTE	  BANK Demo_stick_data
;-----------------------------------------------------------------------
		BYTE	  LOW  Demo_stick_wld1
		BYTE	  HIGH Demo_stick_wld1
		BYTE	  BANK Demo_stick_wld1
;-----------------------------------------------------------------------
		BYTE	  LOW  Demo_stick_wld2
		BYTE	  HIGH Demo_stick_wld2
		BYTE	  BANK Demo_stick_wld2
;-----------------------------------------------------------------------
		BYTE	  LOW  Demo_stick_wld3
		BYTE	  HIGH Demo_stick_wld3
		BYTE	  BANK Demo_stick_wld3
;
;************************************************************************
;*		 Change binary to BCD					*
;************************************************************************
;
		MEM8
		IDX8
CHANGE_BCD
		PHP
		SEP	  #00110001B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDX	  #0FFH
Change_BCD110	INX
		SBC	  #10
		BCS	  Change_BCD110
		ADC	  #10
		STA	  !param0
;-----------------------------------------------------------------------
		TXA
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		ORA	  !param0
;-----------------------------------------------------------------------
		PLP
		RTL
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Lap time calculate routines \\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Copy course time & rank				*
;************************************************************************
;
		MEM8
		IDX8
COPY_TIME_RANK
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Copy time data ========================================
Cp_time_rank100
		LDA	  <game_scene
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		ADC	  #0EH
		TAY
		LDX	  #0EH
;-----------------------------------------------------------------------
Cp_time_rank110 LDA	  !time_buffer,X
		STA	  !course_time_1,Y
		DEY
		DEX
		BPL	  Cp_time_rank110
;
;=============== Copy rank data ========================================
Cp_time_rank200
		LDA	  <game_scene
		ASL	  A
		ASL	  A
		ASL	  A
		ADC	  #05H
		TAY
		LDX	  #05H
;-----------------------------------------------------------------------
Cp_time_rank210 LDA	  !rank_buffer,X
		STA	  !course_rank_1,Y
		DEY
		DEX
		BPL	  Cp_time_rank210
;-----------------------------------------------------------------------
		PLP
		RTL
;
;************************************************************************
;*		 Set backup bank					*
;************************************************************************
;
		ORG	  02C300H
		MEM8
		IDX8
SET_BACKUP_BANK
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		LDA	  <play_mode	      ; free mode ?
		BNE	  Set_back_bnk200     ; no.
;
;=============== battle mode ===========================================
Set_back_bnk100
		LDA	  <game_world
		STA	  !backup_bank
		LDA	  <game_scene
		STA	  !backup_number
		BRA	  Set_back_bnk300
;
;=============== free mode =============================================
Set_back_bnk200
		LDA	  !map_data_code
		LDX	  #14
Set_back_bnk210 CMP	  >Map_data_number,X
		BEQ	  Set_back_bnk220
		DEX
		BRA	  Set_back_bnk210
;-----------------------------------------------------------------------
Set_back_bnk220 TXA
		LDX	  #0FFH
		SEC
Set_back_bnk230 INX
		SBC	  #5
		BCS	  Set_back_bnk230
;-----------------------------------------------------------------------
		ADC	  #5
		STA	  !backup_number
		STX	  !backup_bank
;
;=============== Set backup data pointer ===============================
Set_back_bnk300
		REP	  #00010000B	      ; Index 16 bit mode
		JSR	  Calc_backup_ptr
		STX	  !backup_pointer
		PLP
		RTL
;
;************************************************************************
;*		 Calculate lap time					*
;************************************************************************
;
		MEM8
		IDX8
CALC_LAP_TIME
		PHP
		SEP	  #01100000B	      ; Memory,Index 8 bit mode
;-----------------------------------------------------------------------
		LDA	  <game_scene
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		TAX
;----------------------------------------------------- Set lap 1 time --
		LDA	  !course_time+00H,X
		STA	  !lap_time_buffer+00H
		LDA	  !course_time+01H,X
		STA	  !lap_time_buffer+01H
		LDA	  !course_time+02H,X
		STA	  !lap_time_buffer+02H
;----------------------------------------------------- Set total time --
		LDA	  !course_time+0CH,X
		STA	  !lap_time_buffer+0FH
		LDA	  !course_time+0DH,X
		STA	  !lap_time_buffer+10H
		LDA	  !course_time+0EH,X
		STA	  !lap_time_buffer+11H
;------------------------------------------------------ Calc lap time --
		LDY	  #03H
		SED			      ; Set BCD mode
;-----------------------------------------------------------------------
Calc_lap_time10 SEC
		LDA	  !course_time+05H,X
		SBC	  !course_time+02H,X
		STA	  !lap_time_buffer+02H,Y
		LDA	  !course_time+04H,X
		SBC	  !course_time+01H,X
		STA	  !lap_time_buffer+01H,Y
		PHP
		LDA	  !course_time+03H,X
		SBC	  !course_time+00H,X
		STA	  !lap_time_buffer+00H,Y
		INX
		INX
		INX
		PLP
		BCS	  Calc_lap_time20
		LDA	  !lap_time_buffer+01H,Y
		SEC
		SBC	  #40H
		STA	  !lap_time_buffer+01H,Y
Calc_lap_time20 INY
		INY
		INY
		CPY	  #0FH
		BNE	  Calc_lap_time10
;-----------------------------------------------------------------------
		PLP
		RTL
;
;************************************************************************
;*		 Compare time ( IY - IX )				*
;************************************************************************
;
		MEM8
		IDX16
Compare_time
		SEP	  #00100000B	      ; Memory 8 bit mode
		REP	  #00010000B	      ; Index 16 bit mode
;-----------------------------------------------------------------------
		LDA	  >backup_buffer+0,X
		AND	  #00001111B
		STA	  <param0
		LDA	  !lap_time_buffer+0,Y
		CMP	  <param0
		BNE	  Compare_time10
		LDA	  !lap_time_buffer+1,Y
		CMP	  >backup_buffer+1,X
		BNE	  Compare_time10
		LDA	  !lap_time_buffer+2,Y
		CMP	  >backup_buffer+2,X
Compare_time10	RTS			      ; ( not long )
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Best time set routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Erase best time record					*
;************************************************************************
;
ERASE_RECORD	JSR	  Init_best_time
		JSR	  Write_SRAM
		RTL

;************************************************************************
;*		 Set best time main					*
;************************************************************************
;
		MEM8
		IDX8
SET_BEST_TIME
		PHP
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
;
;=============== Calculate pointer =====================================
Set_best_tm100
		MEM16
		IDX16
		REP	  #00010000B	      ; Index 16 bit mode
		JSR	  Calc_backup_ptr
;-----------------------------------------------------------------------
		REP	  #00100001B	      ; Memory 16 bit mode
		TXA
		ADC	  #001EH
		STA	  <param2	      ; param2 = firstest lap pointer
		SEP	  #00100000B	      ; Memory 8 bit mode
		MEM8
;
;=============== Check best time =======================================
Set_best_tm200
		LDY	  #000FH	      ; IY = total time pointer
		STZ	  !best_time_renew    ; Reset best time renew rank
;-----------------------------------------------------------------------
Set_best_tm210	JSR	  Compare_time	      ; IY - IX ( now - best )
		BCC	  Set_best_tm220      ; if ( in best 10 )
		INC	  !best_time_renew
		INX
		INX
		INX
		CPX	  <param2	      ; data end ?
		BNE	  Set_best_tm210      ; no.
		BRA	  Set_best_tm300
;-----------------------------------------------------------------------
Set_best_tm220	JSR	  Write_best_time     ; Write best time to SRAM
;
;=============== Check best lap ========================================
Set_best_tm300
		LDX	  !param2	      ; IX = firstest lap time
		LDY	  #0000H	      ; IY = my car lap time
		STZ	  !best_lap_renew     ; Reset best lap renew flag
		STZ	  <param4	      ; Clear lap counter
;-----------------------------------------------------------------------
Set_best_tm310	JSR	  Compare_time	      ; IY - IX ( now - best )
		BCS	  Set_best_tm320      ; if ( out best 10 )
		JSR	  Write_lap_time      ; Write best lap to SRAM
Set_best_tm320	INY
		INY
		INY
		INC	  <param4
		CPY	  #000FH
		BNE	  Set_best_tm310
;
;=============== Write SRAM ============================================
Set_best_tm400
		LDA	  !best_time_renew
		CMP	  #10		      ; Best time renew ?
		BCC	  Set_best_tm410      ; yes.
;-----------------------------------------------------------------------
		LDA	  !best_lap_renew     ; Best lap renew ?
		BEQ	  Set_best_tm420      ; no.
Set_best_tm410	JSR	  Write_SRAM	      ; Write SRAM
		JSR	  Set_renew_flag
;-----------------------------------------------------------------------
Set_best_tm420	PLP
		RTL
;
;************************************************************************
;*		 Write best total time					*
;************************************************************************
;
		MEM8
		IDX16
Write_best_time
		STX	  <param0
		LDA	  !best_time_renew
		CMP	  #9		      ; rank 10 ?
		BEQ	  Write_best200	      ; yes.
;
;=============== Brock move ============================================
Write_best100
		LDX	  <param2
		DEX
		DEX
		DEX
Write_best110	DEX
		LDA	  >backup_buffer+0,X
		STA	  >backup_buffer+3,X
		CPX	  <param0
		BNE	  Write_best110
;
;=============== Set new time ==========================================
Write_best200
		JSR	  Set_info_flag
		ORA	  !lap_time_buffer+0FH
		STA	  >backup_buffer+00H,X
		LDA	  !lap_time_buffer+10H
		STA	  >backup_buffer+01H,X
		LDA	  !lap_time_buffer+11H
		STA	  >backup_buffer+02H,X
		RTS
;
;************************************************************************
;*		 Write firstest lap time				*
;************************************************************************
;
		MEM8
		IDX16
Write_lap_time
		LDA	  <param4
		INC	  A
		STA	  !best_lap_renew
;-----------------------------------------------------------------------
		JSR	  Set_info_flag
		ORA	  !lap_time_buffer+00H,Y
		STA	  >backup_buffer+00H,X
		LDA	  !lap_time_buffer+01H,Y
		STA	  >backup_buffer+01H,X
		LDA	  !lap_time_buffer+02H,Y
		STA	  >backup_buffer+02H,X
		RTS
;
;************************************************************************
;*		 Set renew flag						*
;************************************************************************
;
		MEM8
		IDX8
Set_renew_flag
		SEP	  #00110000B	      ; Memory,Index 8 bit mode
		LDA	  <play_mode	      ; battle mode ?
		BNE	  Set_renew_flag3     ; no.
;-----------------------------------------------------------------------
		LDA	  <game_world
		ASL	  A
		ASL	  A
		ADC	  <game_world
		ADC	  <game_scene
		TAX
;-----------------------------------------------------------------------
		LDA	  !best_time_renew
		CMP	  #10		      ; Best time renew ?
		BCS	  Set_renew_flag2     ; no.
		STA	  !best_time_rank,X
;-----------------------------------------------------------------------
Set_renew_flag2 LDA	  !best_lap_renew     ; Best lap renew ?
		BEQ	  Set_renew_flag3     ; no.
		STZ	  !best_lap_flag,X
Set_renew_flag3 RTS

;************************************************************************
;*		 Set infromation flag					*
;************************************************************************
;
		MEM8
Set_info_flag
		LDA	  <mycar_number
		ASL	  A
		ASL	  A
		ASL	  A
		ASL	  A
		ORA	  #10000000B
		STA	  <param0
;-----------------------------------------------------------------------
		LDA	  <play_mode
		LSR	  A
		ROR	  A
		ROR	  A
		ORA	  <param0
		RTS
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Backup buffer initialize routines \\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Set backup RAM buffer					*
;************************************************************************
;
		MEM8
		IDX16
SET_BACKUP_BUFF
		PHP
		SEP	  #00100000B	      ; Memory 8 bit mode
		REP	  #00010000B	      ; Index 16 bit mode
;
;=============== Check backup RAM ======================================
Set_back_buf100
		JSR	  Check_pass_word     ; Check pass word
		CMP	  #00H		      ; data available ?
		BNE	  Set_back_buf110     ; yes.
;-----------------------------------------------------------------------
		JSR	  Initial_backup
		BRA	  Set_back_buf200
Set_back_buf110 JSR	  Check_check_sum     ; Check check_sum data
		JSR	  Check_master	      ; Check master mode flag
;
;=============== Transfer backup data ==================================
Set_back_buf200
		LDX	  #01FFH
Set_back_buf210 LDA	  >Backup_RAM,X
		STA	  >backup_buffer,X
		DEX
		BPL	  Set_back_buf210
;-----------------------------------------------------------------------
		PLP
		RTL
;
;************************************************************************
;*		 Check pass word					*
;************************************************************************
;
		MEM8
		IDX16
Check_pass_word
		STZ	  <param0
		LDX	  #0004H
		JSR	  Check_passwd200
		LDX	  #01FFH
		JSR	  Check_passwd200
		LDA	  <param0
		RTS
;
;=============== Check pass word =======================================
Check_passwd200
		LDY	  #0004H
Check_passwd210 LDA	  !Pass_word_data,Y
		CMP	  >Backup_RAM,X
		BNE	  Check_passwd300     ; different pass word
		DEX
		DEY
		BPL	  Check_passwd210
;-----------------------------------------------------------------------
		INC	  <param0
		RTS
;
;=============== Copy pass word ========================================
Check_passwd300
Check_passwd310 LDA	  !Pass_word_data,Y
		STA	  >Backup_RAM,X
		DEX
		DEY
		BPL	  Check_passwd310
		RTS
;
;************************************************************************
;*		 Check check_sum data					*
;************************************************************************
;
		MEM8
		IDX16
Check_check_sum
		LDX	  #0005H
		JSR	  Check_chksum200
		LDX	  #00ACH
		JSR	  Check_chksum200
		LDX	  #0153H
		JSR	  Check_chksum200
		RTS
;
;=============== Check check_sum =======================================
Check_chksum200
		PHX
		LDY	  #165
		LDA	  #00H
		STA	  <param0
;-----------------------------------------------------------------------
Check_chksum210 CLC
		ADC	  >Backup_RAM,X
		BCC	  Check_chksum220
		INC	  <param0
Check_chksum220 INX
		DEY
		BNE	  Check_chksum210
;-----------------------------------------------------------------------
		CMP	  >Backup_RAM+0,X
		BNE	  Check_chksum300
		LDA	  <param0
		CMP	  >Backup_RAM+1,X
		BNE	  Check_chksum300
		PLX
		RTS
;
;=============== Initial time ==========================================
Check_chksum300
		PLX
Check_chksum310 LDY	  #55
;-----------------------------------------------------------------------
Check_chksum320 LDA	  #09H
		STA	  >Backup_RAM,X	     ; Initial minute
		INX
		LDA	  #59H
		STA	  >Backup_RAM,X	     ; Initial second
		INX
		LDA	  #99H
		STA	  >Backup_RAM,X	     ; Initial 1/100 second
		INX
		DEY
		BNE	  Check_chksum320
;-----------------------------------------------------------------------
		LDA	  #0EDH
		STA	  >Backup_RAM+0,X    ; Set check sum low
		LDA	  #035H
		STA	  >Backup_RAM+1,X    ; Set check sum high
		RTS
;
;************************************************************************
;*		 Check master flag					*
;************************************************************************
;
		MEM8
Check_master
		LDA	  >Backup_RAM+1FAH
		AND	  #0FH
		STA	  <param0
		LDA	  >Backup_RAM+1FAH
		LSR	  A
		LSR	  A
		LSR	  A
		LSR	  A
		CMP	  <param0
		BNE	  Check_master10
		AND	  #00001000B
		BEQ	  Check_master20
;-----------------------------------------------------------------------
Check_master10	LDA	  #00H
		STA	  >Backup_RAM+1FAH
Check_master20	RTS
;
;************************************************************************
;*		 Initialize backup data					*
;************************************************************************
;
		MEM8
		IDX16
Initial_backup
		LDX	  #0005H
		JSR	  Check_chksum310
		LDX	  #00ACH
		JSR	  Check_chksum310
		LDX	  #0153H
		JSR	  Check_chksum310
		BRA	  Check_master10
;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;\\\\\\\\\\\\\\\ Best time set routines \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;************************************************************************
;*		 Initialize best time					*
;************************************************************************
;
		MEM8
		IDX16
Init_best_time
		PHP
		SEP	  #00100000B	      ; Memory 8 bit mode
		REP	  #00010000B	      ; Index 16 bit mode
;
;=============== Initial backup buffer =================================
Init_best_tm100
		JSR	  Calc_backup_ptr
		LDY	  #11
;-----------------------------------------------------------------------
Init_best_tm110 LDA	  #09H
		STA	  >backup_buffer,X   ; Initial minute
		INX
		LDA	  #59H
		STA	  >backup_buffer,X   ; Initial second
		INX
		LDA	  #99H
		STA	  >backup_buffer,X   ; Initial 1/100 second
		INX
		DEY
		BNE	  Init_best_tm110
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Write data to SRAM					*
;************************************************************************
;
		MEM8
		IDX16
Write_SRAM
		PHP
		SEP	  #00100000B	      ; Memory 8 bit mode
		REP	  #00010000B	      ; Index 16 bit mode
;
;=============== Transfer time data ====================================
Write_SRAM100
		JSR	  Calc_backup_ptr
		LDY	  #33
Write_SRAM110	LDA	  >backup_buffer,X
		STA	  >Backup_RAM,X
		INX
		DEY
		BNE	  Write_SRAM110
;
;=============== Calculate check sum ===================================
Write_SRAM200
		JSR	  Calc_backup_bnk
		LDY	  #165
		LDA	  #00H
		STA	  <param0
;-----------------------------------------------------------------------
Write_SRAM210	CLC
		ADC	  >Backup_RAM,X
		BCC	  Write_SRAM220
		INC	  <param0
Write_SRAM220	INX
		DEY
		BNE	  Write_SRAM210
;-----------------------------------------------------------------------
		STA	  >backup_buffer+0,X
		STA	  >Backup_RAM+0,X
		LDA	  <param0
		STA	  >backup_buffer+1,X
		STA	  >Backup_RAM+1,X
;-----------------------------------------------------------------------
		PLP
		RTS
;
;************************************************************************
;*		 Calculate backup data pointer				*
;************************************************************************
;
		MEM8
		IDX16
;
;=============== Calculate backup_pointer ==============================
Calc_backup_ptr
		LDA	  #00H
		XBA
		LDA	  !backup_bank
		ASL	  A
		ASL	  A
		ADC	  !backup_bank
		ADC	  !backup_number
		ASL	  A
		TAY
		LDX	  !Backup_pointer,Y
		RTS
;
;=============== Calculate backup_bank_pointer =========================
Calc_backup_bnk
		LDA	  #00H
		XBA
		LDA	  !backup_bank
		ASL	  A
		TAY
		LDX	  !Backup_bankptr,Y
		RTS
;
;************************************************************************
;*		 Backup RAM initial data				*
;************************************************************************
;
		PROG
;			   F   Z   E   R   O
Pass_word_data	BYTE	  46H,5AH,45H,52H,4FH
Backup_bankptr	WORD	  005H,0ACH,153H
Backup_pointer	WORD	  005H,026H,047H,068H,089H
		WORD	  0ACH,0CDH,0EEH,10FH,130H
		WORD	  153H,174H,195H,1B6H,1D7H
;
		END
