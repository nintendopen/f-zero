*************************************************************************
*	int schpnl( pnlofs,pnlbuf,panel,pnlcnt )			*
*	char	*pnlofs,*pnlbuf,*panel ;				*
*	int	pnlcnt ;						*
*************************************************************************
*	Programmed by Y.Nishida			[ Apr.10, 1990 ]	*

		.globl		_schpnl
		.set		pnlofs,8
		.set		pnlbuf,12
		.set		panel,16
		.set		pnlcnt,20
		.set		autosize,0

		.text
_schpnl:
		link		fp,#autosize
		movea.l		(panel,fp),a0
		clr.l		d0			/*  d0.l = panel number		*/
		move.l		(pnlcnt,fp),d1		/*  d1.l = panel counter	*/
		move.l		(a0),d2			/*  d2.l = panel data		*/
		movea.l		(pnlbuf,fp),a0		/*  a0.l = panel buffer		*/
		movea.l		(pnlofs,fp),a1		/*  a1.l = panel offset buff.	*/
repeat:
		move.w		(a1,d0.l*2),d3		/*  d3.w = buffer offset	*/
		cmp.l		(a0,d3.w),d2
		beq.w		exit_func
		addq.l		#1,d0
		cmp.l		d1,d0
		blt.w		repeat
exit_func:
		unlk		fp
		rts
