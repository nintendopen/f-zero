;************************************************************************
;*	 EN_DRIVE	   -- enemy car move module --			*
;************************************************************************
		INCLUDE	  BUFFER
		INCLUDE	  VARIABLE
		INCLUDE	  WORK
;-----------------------------------------------------------------------
		EXT	  OAM_PTR_07
		EXT	  OAM_PTR_00
;-------------------------------------------------------------------------
;		ENEMY DATA
;=========================================================================
		EXT		Accele_datptr
		EXT		Check_jump
		EXT		Calc_distance
		EXT		Set_enemy_engine
;-------------------------------------------------------------------------
		GLB		Enemy_control
		GLB		Change_point
		GLB		Set_mycar_buffer
		GLB		zako_number
		GLB		Phase_check
		GLB		Angle_CALC
		GLB		mark_point_x,mark_point_y,view_point_x,view_point_y
		GLB		line_data,angle_data,status_data
		GLB		calc_angle
		GLB		car_selecta
		GLB		Goal_in_rank
		GLB		BIT_CHECK_DATA
;***********************************************************************
;*		Define local variable				       *
;***********************************************************************
work0		EQU	  0000H		      ; 2 byte	:work
work1		EQU	  0002H		      ; 2 byte	:work
work2		EQU	  0004H		      ; 2 byte	:work
;
counter		EQU	  0008H
area_change_flag  EQU	  000AH		       ;car area koushin check
;-----------------------------------------------------------
;		COMN
;-----------------------------------------------------------
abs_dif_x	EQU		10H		;2 byte
abs_dif_y	EQU		12H		;  :
abs_dif_xy	EQU		14H
phase_pointer	EQU		16H		;  :
angle_pointer	EQU		18H		;  :
smaller		EQU		1AH		;  :
;
calc_angle	EQU		1CH		;  :
stand_angle	EQU		1DH
scrl_angle	EQU		1EH
pose_angle	EQU		1FH
bound_angle	EQU		20H
bound_check	EQU		21H
;
course_selecter EQU		22H
;
mark_point_x	EQU		24H		;2 byte
mark_point_y	EQU		26H		;  :
view_point_x	EQU		28H		;  :
view_point_y	EQU		2AH		;  :
;
line_data	EQU		2CH		;  :
angle_data	EQU		2EH
;
round_data	EQU		30H		; 1byte
status_data	EQU		31H		; 1byte
drive_status	EQU		32H		; 1byte
CP_flag		EQU		33H		; 2byte
;
lane_x		EQU		36H		;2byte
lane_y		EQU		38H
;
shift_x		EQU		3AH		;2byte
shift_y		EQU		3CH		;  :
;
rank_counter	EQU		30H		;1byte rank check counter
late_flag	EQU		32H		;shuukai okure koka?
shift_point	EQU		34H		;area betu set point shift
;;;;;;;;;;	  EQU		36H
myrank_check	EQU		38H		;check rank
zako_plus	EQU		3AH		;zako speed flag
minus_area	EQU		3CH
plus_area	EQU		3CH
distance_comp	EQU		3EH		;2byte
;-----------------------------------------------------------
tempolary	EQU		00H
;==============================================================================
		PROG
		extend
;===========================================================================
;		Enemy control Main Routin
;===========================================================================
		MEM8
		IDX8
Enemy_control
		PHP
		SEP		#00110000B
		STZ		<area_change_flag
		LDX		#00H
		LDA		!car_flag+00H
		BPL		Enemy_area_check
		LDA		!short_cut_flag
		BNE		Enemy_area_check
		LDA		#00100000B
		BIT		!road_branch
		BMI		Check_course_00
		BVS		Check_course_11
		BRA		Go_area_check
Check_course_00
		LDA		!car_course
		BPL		Go_area_check
		BRA		Go_to_change_point
Check_course_11
		LDA		!car_course
		BMI		Go_area_check
Go_to_change_point
		EOR		#10000000B
		STA		!car_course
		LDY		!car_area
		JSR		Set_mycar_buffer
Go_area_check
		LDA		!car_scrl_angle+1
		SEC
		SBC		!Area_angle
		BCS		MCC_001
		EOR		#0FFH
		INC		A
MCC_001
		CMP		#96
		BCC		MCC_002
		STA		<tempolary
		LDA		#192
		SBC		<tempolary
MCC_002
		PHA
		LDA		!car_flag+1
		BIT		#00000001B
		BNE		Mycar_opposite
		PLA
		CMP		#72
		BCC		Go_to_check
		LDA		#00000001B
		TSB		!car_flag+1
		BRA		Go_to_check
Mycar_opposite
		PLA
		CMP		#24
		BCS		Go_to_check
Go_to_foward_check
		LDA		#00000001B
		TRB		!car_flag+1
Go_to_check
		LDA		!round_counter+01H
		BNE		Special_check
		LDA		<frame_counter
		BIT		#00000001B
		BNE		Go_foward_check
		BRA		Go_behind_check
Special_check
		LDA		!car_flag+1
		BIT		#00000001B
		BNE		Go_behind_check
Go_foward_check
		JSR		Foward_check
		BRA		Enemy_area_check
Go_behind_check
		JSR		Behind_check
;-----------------------------------------------------------------------
Enemy_area_check
		MEM8
		IDX8
		SEP		#00110000B
		LDX		#02H
Rebound_loop
		SEP		#00110000B
		LDA		!car_flag+0,X
		BPL		Not_area_check
		CPX		!set_pointa
		BEQ		Not_area_check
		LDA		!road_branch,X
		BEQ		No_branch_check
		BPL		Check_course_1
Check_course_0
		LDA		!car_course,X
		BPL		No_branch_check
		AND		#01111111B
		STA		!car_course,X
		BRA		Go_change_point
Check_course_1
		LDA		!car_course,X
		BMI		No_branch_check
		ORA		#10000000B
		STA		!car_course,X
Go_change_point
		LDA		!car_area,X
		TAY
		JSR		Change_point
No_branch_check
		JSR		Area_Check
		JSR		Controler
Not_area_check
		INX
		INX
		CPX		#0CH
		BCC		Rebound_loop
		LDA		<exception_flag
		BIT		#01000000B
		BNE		Game_over
		BIT		#10111000B
		BNE		Mycar_goll_in
		LDA		!short_cut_flag
		BNE		Short_cut_penalty
		LDA		<area_change_flag
		BEQ		No_change_area
		JSR		Check_rank
No_change_area
		JSR		Set_marker
		JSR		Car_manager
		JSR		Check_jump
Short_cut_penalty
I_am_not_exist
Mycar_goll_in
		JSR		Set_enemy_engine
Game_over
		PLP
		RTS
;-------------------------------------------------------------------------
Set_mycar_buffer
		MEM8
		IDX8
		SEP		#00110000B
		LDX		#00H
;						Entry Y=terget area
		PHY
		JSR		Change_point
		LDA		<mark_point_x+00H
		STA		!Foward_x+00H
		LDA		<mark_point_x+01H
		STA		!Foward_x+01H
		LDA		<mark_point_y+00H
		STA		!Foward_y+00H
		LDA		<mark_point_y+01H
		STA		!Foward_y+01H
		LDA		<angle_data
		STA		!Foward_angle
		LDA		<status_data
		STA		!Foward_status
;
		PLY
		DEY
		CPY		#0FFH
		BNE		No_minus_over
		LDY		<area_number
No_minus_over
		JSR		Change_point
		LDA		<mark_point_x+00H
		STA		!Behind_x+00H
		LDA		<mark_point_x+01H
		STA		!Behind_x+01H
		LDA		<mark_point_y+00H
		STA		!Behind_y+00H
		LDA		<mark_point_y+01H
		STA		!Behind_y+01H
		LDA		<status_data
		STA		!Behind_status
		LDA		<angle_data
		STA		!Behind_angle
		RTS
;====================================================================================
Controler
		MEM8
		IDX8
		SEP		#00110000B
		LDA		!car_flag+0,X
		BIT		#00100000B
		BEQ		Enemy_cont
Car_stopping
		LDA		#00110000B
		STA		!car_control,X
		RTS
;------------------------------------------------------------------
Enemy_cont
		LDA		!car_control,X
		AND		#11111100B
		STA		!car_control,X
;
		LDA		!point_angle,X
		SEC
		SBC		!car_angle+1,X
		BEQ		Enemy_speed
;
		BCC		EN_handle_001
		CMP		#002H
		BCC		Enemy_speed
		CMP		#0BEH
		BCS		Enemy_speed
		CMP		#060H
		BCS		EN_handle_002
EN_handle_000
		LDA		!car_control,X
		ORA		#00000001B
		STA		!car_control,X
		BRA		Enemy_speed
EN_handle_001
		CMP		#0FEH
		BCS		Enemy_speed
		CMP		#042H
		BCC		Enemy_speed
		CMP		#0A0H
		BCC		EN_handle_000
EN_handle_002					     ;Left handle
		LDA		!car_control,X
		ORA		#00000010B
		STA		!car_control,X
;=================================================================================
Enemy_speed
		LDA		!area_speed+01H,X
		CMP		!car_speed+01H,X
		BCC		Brake_point
		BEQ		Comp_speed_001
Accel_point
		LDA		!car_control,X
		AND		#11001111B
		ORA		#10000000B
		STA		!car_control,X
		RTS				;Return main  M&X=8
Comp_speed_001
		LDA		!area_speed+00H,X
		CMP		!car_speed+00H,X
		BCS		Accel_point
;----------------------------------------------------------------------------
Brake_point
		LDA		!car_control,X
		AND		#01111111B
		ORA		#00100000B
		STA		!car_control,X
		RTS				;Return main  M&X=8
;====================================================================================
Check_rank_data BYTE		013,008,005,003,002
;-------------------------------------------------------------------------
Car_manager
		MEM8
		IDX8
		SEP		#00110000B
		LDA		!disp_number
		CMP		#05H
		BNE		Go_on_car_manager
		RTS
Go_on_car_manager
		JSR		Rival_speed_control
;
		LDY		#00H
		LDA		!Area_status+00H
		BPL		Not_far_place
		LDY		#06H
		BRA		Set_dist_data
Not_far_place
		LDX		!round_counter+00H
		BMI		Set_dist_data
		LDA		!mycar_rank
		CMP		#06H
		BCC		Set_dist_data
		LDY		#02H
		CMP		Check_rank_data,X
		BCC		Set_dist_data
		INY
		INY
Set_dist_data
		LDA		!distance_check+00H,Y
		STA		<distance_comp+00H
		LDA		!distance_check+01H,Y
		STA		<distance_comp+01H
;
		LDX		!set_pointa
		BNE		Set_car_exist
;
		LDA		!control_status	     ;short cut jump da
		BPL		Not_jumpping	     ;area ga kuruu kara
		LDA		!car_speed+01H
		CMP		#05H
		BCS		On_jumpping
Not_jumpping
		STZ		!course_half
		LDA		<area_number	     ;course zanhan ka?
		LSR		A		     ;	     kouhan ?
		CMP		!car_area+0
		BCS		No_half_area
		DEC		!course_half
No_half_area
		LDA		<play_mode
		BEQ		Go_on_battle
		LDA		!rival_number
		BPL		Practice_manager
On_jumpping
		RTS
Practice_manager
		LDA		!rival_free_speed
		STA		!car_rank+02H
		JMP		Car_point_reset
;--------------------------------------------------------------------------
Go_on_battle
		MEM8
		LDX		!round_counter+00H
		BMI		Minus_round
		LDA		!late_number
		CMP		!Late_data,X
		BCS		No_late_car
		JMP		Inc_latecar
No_late_car
		TAX
		BEQ		Check_calc_rank
		LDA		<frame_counter
		BIT		#00000010B
		BNE		Check_calc_rank
		JMP		Late_car_control
Minus_round
		LDX		#00H
Check_calc_rank
		LDA		!calc_rank
		CMP		CSE_data,X
		BNE		Car_reset_CSE
;Car_stable
		JMP		Car_point_reset
Car_reset_CSE
		BCC		Rank_up
;Rank_down
		JMP		Car_goback
Rank_up
		JMP		Car_goahead
;----------------------------------------------------------------------
Set_car_exist
		LDA		!car_flag+01H,X
		BPL		No_resetcar_exist
		BIT		#00001000B
		BNE		Behind_not_set
		LDY		!reset_rank
		INY
		INY
		LDA		!car_ranking+00H,Y
		TAY
		LDA		!car_area,Y
		CMP		!car_area,X
		BNE		Behind_dist_check
		JMP		Go_to_move
No_resetcar_exist
		BIT		#00100000B
		BNE		Behind_sub
Foward_sub
		BIT		#00001000B
		BNE		Foward_not_set
		LDA		!Area_status,X
		BIT		#00001000B
		BNE		Dont_permit_area
Not_stop_crash_car
		LDA		!car_distance+01H,X
		CMP		#01BH
		BCC		Foward_not_set
		JMP		Go_to_move
Dont_permit_area
		LDA		!car_flag+00H,X
		AND		#00100001B
		CMP		#00100001B
		BEQ		Foward_not_set
		BRA		Not_stop_crash_car
Foward_not_set
		JMP		Reserve_INC
Behind_sub
		BIT		#00001000B
		BNE		Behind_not_set
Behind_dist_check
		JSR		Check_car_distance
		BCC		Behind_not_set
		JMP		Go_to_move
Behind_not_set
		JMP		Reserve_DEC
;-----------------------------------------------------------------------------
CSE_data
		BYTE		03H,03H,02H
Late_data
		BYTE		00,01H,01H,02H,02H
;------------------------------------------------------------------------
;		SUBROUTINS   (FOR CAR MANAGIMENT)
;------------------------------------------------------------------------------
Check_car_distance
;------------------------------------X=just reset car's index---------------
		MEM8
		LDA		!car_distance+01H,X
		CMP		<distance_comp+01H
		BEQ		Check_low_dist
		RTS
Check_low_dist
		LDA		!car_distance+00H,X
		CMP		<distance_comp+00H
Not_top_reset
		RTS
;				SEC:Over dist	    CLC:Inner dist
;----------------------------------------------------------------------------------
Car_point_reset
		LDA		!car_flag+01H
		BIT		#00000001B
		BNE		No_car_reset
		LDA		!Area_status+00H
		BMI		No_car_reset
		LDY		#00H
		LDA		!calc_rank
		ASL		A
		TAX
		STX		!reset_rank
		LDA		!car_ranking,X
		TAX
		CPX		!reset_index
		BNE		Not_same_001
		LDA		!car_flag+00H,X
		BPL		No_car_reset
		LDA		!car_area+00H
		CMP		!reset_area
		BNE		Not_same_002
No_car_reset
		RTS
Not_same_001
		LDA		!car_area+00H
Not_same_002
		CMP		!car_area+00H,X
		BEQ		No_car_reset
;
		LDA		!car_flag+01H,X
		BIT		#00001000B
		BNE		No_point_reset
		LDA		!car_distance+01H,X
		CMP		#0AH
		BCC		No_point_reset
;Go_point_reset
		LDA		!car_flag+01H,X
		ORA		#10000000B
		STA		!car_flag+01H,X	  ;reset bit
		LDA		!car_area+00H
		STA		!reset_area
		STX		!reset_index
		LDY		#00H
		LDA		#01H
		STA		<minus_area
		JMP		Set_behind
No_point_reset
Dont_goback
		RTS
;-------------------------------------------------------------------------
Car_goback
		LDX		!car_ranking+00H
		LDA		!Area_status+00H,X
		BMI		Dont_goback
		LDA		!car_distance+01H,X
		CMP		#1BH
		BCC		Dont_goback
		LDA		!car_flag+00H,X
		BIT		#00001000B
		BNE		Dont_goback
		BIT		#01000000B
		BNE		Zako_goback
		DEC		!rival_flag
Zako_goback
		INC		!car_stack
		LDY		!car_stack
		STA		!stack_data1,Y
		LDA		!car_number,X
		STA		!stack_data2,Y
;Set_enemy_to_back
		LDY		!rival_late
		BEQ		No_rival_late
		DEC		!rival_late
		INC		!rival_flag
		STZ		!car_course,X
		LDA		#080H
		STA		!car_flag,X
		LDA		!rival_late_data,Y
		STA		!car_number,X
		ORA		#00000100B
		BRA		Set_param_goback
No_rival_late
		LDA		#0C0H		;Back set = Zako
		STA		!car_flag+0,X
		DEC		!zako_pointa
		LDA		!zako_pointa
		AND		#00000001B
		STA		!car_number,X
		PHA
		INC		A
		STA		!car_course,X
		PLA
Set_param_goback
		ASL		A
		STA		!car_char_num+1,X
;
		LDY		!late_number
		LDA		CGB_set_data,Y
		TAY
		LDA		!car_ranking,Y
		TAY
		LDA		#03H
		STA		<minus_area
		JMP		Set_behind
Dont_latecar
I_am_not_latecar
Cont_latecar
		RTS
;----------------------------------------------------------------------------
Inc_latecar
		TAX
		LDA		Late_num_pointa,X
		TAX
		LDA		!car_ranking,X
		BEQ		Car_goback
		TAX
		LDA		!car_flag+00H,X
		BIT		#00001000B
		BNE		Dont_latecar
		INC		!late_number
		BIT		#01000000B
		BNE		Set_latecar
		LDA		#0C0H
		STA		!car_flag+00H,X
		INC		!rival_late
		LDY		!rival_late
		LDA		!car_number,X
		STA		!rival_late_data,Y
		DEC		!rival_flag
		BRA		Set_latecar
;--------------------------------------------------------------------------
Late_car_control
		LDY		#08H
		LDA		<frame_counter
		BMI		Check_08H_car
		LDY		#06H
Check_08H_car
		LDA		!car_ranking,Y
		TAX
		LDA		!car_flag+01H,X
		BIT		#00000010B
		BEQ		I_am_not_latecar
		BIT		#00001000B
		BNE		Cont_latecar
		BIT		#00010000B
		BEQ		Close_latecar
		LDA		!car_distance+01H,X
		CMP		#0FFH
		BEQ		Set_latecar
		RTS
Close_latecar
		LDA		!car_distance+01H,X
		CMP		#04H
		BCC		Cont_latecar
Set_latecar
		JSR		Car_crash_control
;
		DEC		!late_pointa
		LDA		!late_pointa
		AND		#00000111B
		TAY
		AND		#00000001B
		STA		!car_number,X
		INC		A
		STA		!car_course,X
;
		LDA		!round_counter+00H
		DEC		A
		STA		!round_counter+00H,X
		STA		!round_counter2+00H,X
		LDA		!car_flag+01H,X
		ORA		#00010011B
		STA		!car_flag+01H,X
		STZ		!round_counter+01H,X
		LDA		!Set_late_data,Y
		STA		<plus_area
		LDY		#00H
		JMP		Set_foward_dash
Set_late_data
		BYTE		01H,02H,09H,03H,05H   ;Late num pointa
CGB_set_data					      ; mo riyousuru
CGA_set_data
Late_num_pointa
		BYTE		08H,06H,04H
;------------------------------------------------------------------------
Car_goahead
		LDY		!late_number
		LDA		CGA_set_data,Y
		TAX
		LDA		!car_ranking,X
		TAX
		LDA		!Area_status+00H,X
		BMI		Dont_goahead
		LDA		!car_flag+00H,X
		BIT		#00001000B
		BNE		Dont_goahead
		LDY		!car_stack
		BEQ		No_car_stack
		BIT		#01000000B
		BNE		Goahead_zako
		INC		!rival_late
		LDY		!rival_late
		LDA		!car_number,X
		STA		!rival_late_data,Y
		DEC		!rival_flag
Goahead_zako
		LDY		!car_stack
		DEC		!car_stack
		LDA		!stack_data1,Y
		STA		!car_flag+00H,X
		BIT		#01000000B
		BNE		Zako_from_stack
		STZ		!car_course,X
		INC		!rival_flag
		LDA		!stack_data2,Y
		STA		!car_number,X
		ORA		#00000100B
		BRA		Set_param_goahead
Zako_from_stack
		DEC		!zako_pointa
		LDA		!zako_pointa
		AND		#00000001B
		STA		!car_number,X
		PHA
		INC		A
		STA		!car_course,X
		PLA
Set_param_goahead
		ASL		A
		STA		!car_char_num+1,X
		LDY		!car_ranking+00H
		LDA		#03H
		STA		<plus_area
		JMP		Set_foward
Dont_goahead
		RTS
No_car_stack
		JMP		Car_point_reset
;-------------------------------------------------------------------------
Car_crash_control
		LDA		!crash_counter
		BEQ		Set_crash
		DEC		!crash_counter
		LDA		!car_flag+00H,X
		AND		#11011100B
		STA		!car_flag+00H,X
		LDA		#00000100B
		STA		!car_char_num+01H,X
		RTS
Set_crash
		LDA		#11000001B
		DEC		!stop_counter
		BNE		Not_stop_crash
		LDA		#04H
		STA		!stop_counter
		LDA		#11100001B
Not_stop_crash
		STA		!car_flag+00H,X
		LDA		#00000110B
		STA		!car_char_num+01H,X
		LDA		<frame_counter
		ASL		A
		LDA		<frame_counter
		AND		#00000001B
		ADC		!crash_pase
		STA		!crash_counter
		RTS
;---------------------------------------------------------------------------
Set_behind	;   Entry
;			X:set car index	  minus_area :area number
;----------------------------------------------------------------------------
		MEM8
		LDA		!car_flag+1,X
		ORA		#00100000B
		STA		!car_flag+1,X
		LDA		!round_counter,Y
		STA		!round_counter+00H,X
		STA		!round_counter2+00H,X
		STZ		!round_counter+01H,X
;
		LDA		!car_area,Y	;Mokuhyo address
		SEC
		SBC		<minus_area
		BCS		S_BH_001
		ADC		<area_number
		DEC		!round_counter+00H,X
		DEC		!round_counter2+00H,X
S_BH_001
		TAY
		STA		!car_area,X
		BRA		Set_car_param
;-------------------------------------------------------------------------
Set_foward	;Entry
;		      X:set car index	 plus_area= area number
;------------------------------------------------------------------------
		MEM8
		IDX8
;
		LDA		!round_counter,Y
		STA		!round_counter+00H,X
		STA		!round_counter2+00H,X
Set_foward_dash
		STZ		!round_counter+01H,X
;***Set_foward_dash
		LDA		!car_flag+1,X
		ORA		#01000000B
		AND		#11011111B
		STA		!car_flag+1,X
;
		LDA		!car_area,Y   ;mycar area  or top car
		CLC
		ADC		<plus_area
		CMP		!area_number
		BCC		S_FW_001
		INC		!round_counter+00H,X
		INC		!round_counter2+00H,X
		STZ		!round_counter+01H,X
		LDA		#00H
S_FW_001
		STA		!car_area,X
		TAY
		BRA		Set_car_param
;-----------------------------------------------------------------------------
Reserve_INC
		LDA		!car_area,X
		INC		A
		CMP		<area_number
		BCC		NP_FWD
		INC		!round_counter+00H,X
		INC		!round_counter2+00H,X
		STZ		!round_counter+01H,X
		LDA		#00H
NP_FWD
		TAY
		STA		!car_area,X
		BRA		Set_car_param
;---------------------------------------------------------------------------------
Reserve_DEC
		LDA		!car_area,X
		DEC		A
		CMP		#0FFH
		BNE		NP_BHD
		DEC		!round_counter+00H,X
		DEC		!round_counter2+00H,X
		STZ		!round_counter+01H,X
		LDA		<area_number
		DEC		A
NP_BHD
		TAY
		STA		!car_area,X
Set_car_param
		MEM8
		IDX8
;
		JSR		Change_point
;
		MEM16
		REP		#00100000B
		LDA		!Mark_point_x,X
		STA		!car_locatex_h,X
		LDA		!Mark_point_y,X
		STA		!car_locatey_h,X
		STZ		!car_speed+00H,X
		STX		!set_pointa
		MEM8
		SEP		#00100000B
		LDA		!car_flag+0,X
		ORA		#00000100B
		STA		!car_flag+0,X
		RTS
;------------------------------------------------------------------------
		MEM8
		IDX8
Go_to_move
		STZ		!control_status,X
		STZ		!car_control,X
;
		STZ		!set_pointa
		LDA		!car_flag+1,X
		AND		#00010011B
		STA		!car_flag+1,X
;
		LDA		!car_flag+0,X
		AND		#11100001B
		STA		!car_flag+0,X
		BIT		#00100000B
		BNE		GTM_RTS
		LDA		!Area_angle,X
		STA		!car_angle+01H,X
		STA		!car_scrl_angle+01H,X
		LDA		#008H
		STA		!car_speed+01H,X
GTM_RTS
		JSR		Check_rank
		RTS
;===============================================================================
		MEM8
		IDX8
Rival_speed_control
		LDX		#00H
;
		LDA		!rival_flag
		CMP		#02H
		BEQ		Rivalcar_2
		BCS		Rivalcar_3
Rivalcar_1
		LDA		!mycar_rank
		CMP		#03H
		BCC		Rivalcar_3
		LDA		#00H
		BRA		Rivalcar_4
Rivalcar_2
		LDA		!calc_rank
		CMP		#03H
		BCC		Rivalcar_3
		LDA		#04H
		BRA		Rivalcar_4
Rivalcar_3
		LDA		#08H
Rivalcar_4
		STA		<tempolary
RV_speed_cont_loop
		LDA		!car_ranking+00H,X
		BEQ		Next_RV_speed_cont
		TAY
		LDA		!car_flag+00H,Y
		BIT		#01000000B
		BNE		Next_RV_speed_cont
		LDA		<tempolary
		STA		!car_rank,Y
		SEC
		SBC		#04H
		STA		<tempolary
Next_RV_speed_cont
		INX
		INX
		CPX		#0AH
		BNE		RV_speed_cont_loop
		RTS
;-------------------------------------------------------------------------
Change_point
;		Entry		Y=Changing point number (8bits)
		PHP
		MEM8
		IDX8
		SEP		#00110000B
		CPX		#00H
		BEQ		Point_change
		LDA		!car_flag+1,X
		BIT		#00000100B
		BEQ		Point_change
		LDA		!Area_status,X
		BIT		#00010000B
		BEQ		Clear_kyousei_strate
;Cont_kyousei_strate
		LDA		!area_status,Y
		STA		!Area_status,X
		MEM16
		IDX16
		REP		#00110000B
		TYA
		AND		#00FFH
		ASL		A
		TAY
		LDA		!position_x,Y
		STA		!Mark_point_x,X
		PLP
		RTS
;------------------------------------------------------------------
Clear_kyousei_strate
		MEM8
		IDX8
		LDA		#13H
		STA		!accele_perform,X
		LDA		!car_flag+1,X
		AND		#11111011B
		STA		!car_flag+1,X
;-----------------------------------------------------------------------------
Point_change
;--------------------------------------------------------------------------
		MEM8
		IDX8
;
		LDA		!car_course,X
		STA		<course_selecter
;
		LDA		!branch_flag
		BEQ		Set_area_palameter
;
		LDA		<course_selecter
		BPL		Set_area_palameter
;
		LDA		!area_status,Y
		BIT		#00100000B
		BEQ		Set_area_palameter
Check_branch_course
		TYA				  ;X=car_area,X
		SEC
		SBC		!branch_area	  ;BRANCH no hajimaru
		CLC
		ADC		!branch_addr	  ;Plus area_number+1
		TAY
Set_area_palameter
		LDA		!area_status,Y
		STA		<status_data
		AND		#00000111B
		STA		<line_data
;
		LDA		!area_angle,Y
		STA		<angle_data
;
		LDA		<course_selecter
		BIT		#00000010B
		BNE		Select_12
		BIT		#00000001B
		BNE		Select_11
;Select_10
		LDA		drive_data_0,Y
		STA		<drive_status
		BRA		Set_position
Select_11
		LDA		drive_data_1,Y
		STA		<drive_status
		BRA		Set_position
Select_12
		LDA		drive_data_2,Y
		STA		<drive_status
Set_position
		MEM16
		IDX16
		REP		#00110000B
		TYA
		AND		#00FFH
		ASL		A
		TAY
		LDA		position_x,Y
		STA		<mark_point_x
		LDA		position_y,Y
		STA		<mark_point_y
;-------------------------------------------------------------------------
Lane_change
		MEM8
		IDX8
		SEP		#00110000B
		CPX		#00H
		BEQ		Set_mycararea
		LDA		<line_data
		ASL		A
		TAY
		LDA		<drive_status+00H
		BMI		Out_course_lane
		BIT		#01000000B
		BNE		In_course_lane
Center_lane
		MEM16
		REP		#00100000B
		STZ		<lane_x
		STZ		<lane_y
		BRA		Set_car_shift
;----------------------------------------------------------------------------
Set_mycararea
		MEM8
		LDA		<angle_data
		STA		<road_angle
		PLP
		RTS
;----------------------------------------------------------------------------
		MEM16
In_course_lane
		REP		#00100000B
		LDA		In_course_x,Y
		STA		<lane_x
		LDA		In_course_y,Y
		STA		<lane_y
		BRA		Set_course_shift
Out_course_lane
		REP		#00100000B
		LDA		Out_course_x,Y
		STA		<lane_x
		LDA		Out_course_y,Y
		STA		<lane_y
Set_course_shift
		LDA		<drive_status
		AND		#0000000000001100B
		BEQ		Shift_4_char
		CMP		#0000000000001000B
		BEQ		Shift_6_char
		BCC		Set_car_shift
;----------------------------------------------------------------------------
Shift_8_char
		ASL		<lane_x
		ASL		<lane_y
Shift_4_char
		ASL		<lane_x
		ASL		<lane_y
		BRA		Set_car_shift
Shift_6_char
		LDA		<lane_x
		JSR		Six_time_sub
		STA		<lane_x
		LDA		<lane_y
		JSR		Six_time_sub
		STA		<lane_y
;----------------------------------------------------------------------------
Set_car_shift
		MEM8
		SEP		#00110000B
		LDA		!car_flag+00H,X
		BIT		#00100000B
		BEQ		No_stop_shift
		LDA		<frame_counter
		BMI		Right_shift
		BRA		Left_shift
No_stop_shift
		LDA		<status_data
		BIT		#01000000B
		BNE		No_shift
		CPX		#04H
		BCC		No_shift
		BEQ		Left_shift
		CPX		#06H
		BEQ		Right_shift
No_shift
		MEM16
		REP		#00100000B
		STZ		<shift_x
		STZ		<shift_y
		BRA		Set_mark_point
Right_shift
		REP		#00100000B
		LDA		Out_course_x,Y
		STA		<shift_x
		LDA		Out_course_y,Y
		STA		<shift_y
		BRA		Set_mark_point
Left_shift
		REP		#00100000B
		LDA		In_course_x,Y
		STA		<shift_x
		LDA		In_course_y,Y
		STA		<shift_y
;---------------------------------------------------------------------------
Set_mark_point
		LDA		!car_flag+00H,X
		AND		#0000000000100000B
		BEQ		No_stop_the_car
		CLC
		LDA		<shift_x
		BPL		Shift_x_plus
		SEC
Shift_x_plus
		ROR		<shift_x
		CLC
		LDA		<shift_y
		BPL		Shift_y_plus
		SEC
Shift_y_plus
		ROR		<shift_y
No_stop_the_car
		LDA		<mark_point_x
		CLC
		ADC		<lane_x
		CLC
		ADC		<shift_x
		STA		<mark_point_x
		LDA		<mark_point_y
		CLC
		ADC		<lane_y
		CLC
		ADC		<shift_y
		STA		!Mark_point_y,X
		LDA		<mark_point_x
		STA		!Mark_point_x,X
		MEM8
		SEP		#00100000B
		LDA		<angle_data
		STA		!Area_angle,X
		LDA		<status_data
		STA		!Area_status,X
		LDA		<drive_status
		STA		!Area_drive,X
Set_enemy_speed
		PHA
		AND		#00000011B
		ASL		A
		TAY
		LDA		<exception_flag
		BIT		#00010000B
		BNE		Set_Rival_speed
		LDA		!car_flag+0,X
		BIT		#01000000B
		BNE		Set_Zako_speed
Set_Rival_speed
		CPY		#06H
		BEQ		Set_rival_max
Set_nomal_speed
		MEM16
		REP		#00100000B
		LDA		!RV_speed_table,Y
		BRA		Set_area_speed
Set_rival_max
		MEM8
		LDA		<exception_flag
		BIT		#00010000B
		BEQ		Not_demo_ending
		LDY		#08H
		BRA		Skip_set_speed
Not_demo_ending
		LDA		!master_level
		BEQ		Not_master_speed
Set_max_speed
		MEM16
		REP		#00100000B
		LDA		#00900H
		BRA		Set_area_speed
		MEM8
Not_master_speed
		STZ		<tempolary
		LDA		!round_counter,X
		BMI		No_race_round
		TAY
		LDA		!car_pase_data,Y
		STA		<tempolary
No_race_round
		LDA		!car_flag+01H,X
		BIT		#00001000B
		BNE		On_screen_speed
		BIT		#00000001B
		BNE		On_kattobi_speed
		LDA		#0FCH
		BRA		Set_pase_rank
On_kattobi_speed
		LDA		!car_distance+01H,X
		CMP		#0FFH
		BNE		Not_so_far_rival
		BRA		Set_max_speed
Not_so_far_rival
		LDA		#02H
		BRA		Set_pase_rank
On_screen_speed
		LDA		#00H
Set_pase_rank
		CLC
		ADC		<tempolary	 ;round pase
Set_master_speed
		CLC
		ADC		!car_rank,X	 ;each car speed dif
		BMI		Minus_pase_speed
		CMP		#0EH
		BCC		Full_speed_set
		LDA		#0EH
		BRA		Full_speed_set
Minus_pase_speed
		LDA		#00H
Full_speed_set
		TAY
Skip_set_speed
		MEM16
		REP		#00100000B
		LDA		!RV_max_speed+00H,Y
Set_area_speed
		STA		!area_speed,X
		BRA		Set_car_handle
;-----------------------------------------------------------------------------
Set_Zako_speed
		MEM8
		STZ		<zako_plus
		LDA		!car_flag+1,X
		BIT		#00000010B
		BNE		Set_late_speed
Not_late_speed
		LDA		!master_level
		BEQ		Not_zako_master
		CPY		#06H
		BNE		Set_nomal_speed
		LDA		!car_course+00H,X
		AND		#10000000B
		STA		!car_course+00H,X
		LDA		!mycar_rank
		CMP		#08H
		BCC		Set_max_speed
Master_zako_slow
		LDA		!car_course+00H,X
		AND		#11111100B
		ORA		#00000001B
		STA		!car_course+00H,X
		LDY		#0EH
		BRA		Skip_set_speed
Set_late_speed
		LDA		#10H
		STA		<zako_plus
		LDA		!car_number,X
		BEQ		Set_speed
		LDA		#18H
		STA		<zako_plus
		BRA		Set_speed
Not_zako_master
		LDA		!mycar_rank
		CMP		#07H
		BCC		Set_speed
		LDA		#08H
		STA		<zako_plus
Set_speed
		TYA
		CLC
		ADC		<zako_plus
		TAY
		MEM16
		REP		#00100000B
		LDA		ZK_speed_table+00H,Y
		BRA		Set_area_speed
Set_car_handle
		MEM8
		SEP		#00110000B
		PLA
		AND		#00110000B
		LSR		A
		LSR		A
		LSR		A
		STA		<tempolary
		LDA		<exception_flag
		BIT		#00010000B
		BNE		Set_rival_handle
		LDA		!car_flag+00H,X
		BIT		#01000000B
		BNE		Set_zako_handle
Set_rival_handle
		LDA		#00H
		BRA		Set_handle_data
Set_zako_handle
		LDA		!car_flag+01H,X
		BIT		#00000010B
		BNE		Set_late_handle
		LDA		#08H
		BRA		Set_handle_data
Set_late_handle
		LDA		#10H
Set_handle_data
		CLC
		ADC		<tempolary
		TAY
		MEM16
		REP		#00100000B
		LDA		Car_handle_table,Y
		STA		!car_handle,X
		PLP
		RTS
;-------------------------------------------------------------------------
Six_time_sub
		STA		<tempolary
		ASL		A
		CLC
		ADC		<tempolary
		RTS
;------------------------------------------------------------------------
In_course_x
		WORD		00000H,0FFF6H,0FFF0H,0FFF6H
		WORD		00000H,0000AH,00010H,0000AH
In_course_y
		WORD		00010H,0000AH,00000H,0FFF6H
		WORD		0FFF0H,0FFF6H,00000H,0000AH
Out_course_x
		WORD		00000H,0000AH,00010H,0000AH
		WORD		00000H,0FFF6H,0FFF0H,0FFF6H
Out_course_y
		WORD		0FFF0H,0FFF6H,00000H,0000AH
		WORD		00010H,0000AH,00000H,0FFF6H
;---------------------------------------------------------------------------
Area_Check
Foward_check
		MEM16
		IDX8
		REP		#00100000B
;
		LDA		Area_status,X
		AND		#00000111B
		STA		<line_data
		LDA		!car_locatex_h,X
		STA		<view_point_x
		LDA		!car_locatey_h,X
		STA		<view_point_y
;
		LDA		!Mark_point_x,X
		STA		<mark_point_x
		LDA		!Mark_point_y,X
		STA		<mark_point_y
;
		JSR		Phase_check
		BCS		Foward_000
;
		MEM8
		CPX		#00H
		BNE		Foward_FFF
		LDA		!car_flag+01H
		BIT		#00000001B
		BEQ		Foward_FFF
		JMP		Car_rebound
Foward_FFF
		JSR		Angle_CALC
		STA		!point_angle,X
		JMP		Car_rebound
Foward_000
		SEP		#00100000B
		DEC		<area_change_flag
		LDA		!car_area,X
		CMP		!area_number
		BNE		Foward_001
		LDA		#0FFH
Foward_001
		INC		A
		STA		!car_area,X
		TAY
		SEC
		SBC		!car_area+00H
		BEQ		Not_close_point
		BCS		Area_comp_plus
		EOR		#0FFH
		INC		A
		CMP		<area_number
		BNE		Not_close_point
Close_point
		LDA		!car_flag+01H,X
		AND		#11101111B
		STA		!car_flag+01H,X
		BRA		Goto_changepoint
Area_comp_plus
		CMP		#01H
		BEQ		Close_point
Goto_changepoint
Not_close_point
		JSR		Change_point
		CPX		#00H
		BEQ		Foward_002
		LDA		<angle_data
		STA		!point_angle,X
		JMP		Car_rebound
Foward_002
		MEM16
		REP		#00100000B
		LDA		!Foward_x
		STA		!Behind_x
		LDA		!Foward_y
		STA		!Behind_y
		LDA		<mark_point_x
		STA		!Foward_x
		LDA		<mark_point_y
		STA		!Foward_y
		MEM8
		IDX8
		SEP		#00110000B
		LDA		!Foward_angle
		STA		!Behind_angle
		LDA		!Foward_status
		STA		!Behind_status
		LDA		<angle_data
		STA		!Foward_angle
		LDA		<status_data
		STA		!Foward_status
		JMP		Car_rebound
;-------------------------------------------------------------------------
Behind_check
		MEM16
		REP		#00100000B
		LDA		!car_locatex_h
		STA		<view_point_x
		LDA		!car_locatey_h
		STA		<view_point_y
		LDA		!Behind_x
		STA		<mark_point_x
		LDA		!Behind_y
		STA		<mark_point_y
		LDA		Behind_status
		AND		#0000000000000111B
		STA		<line_data
		JSR		Phase_check
		BCC		Behind_002
		MEM8
		LDA		!car_flag+01H
		BIT		#00000001B
		BNE		Behind_000
		BRA		Car_rebound
Behind_000
		JSR		Angle_CALC
		STA		!point_angle
		BRA		Car_rebound
Behind_002
		MEM8
		DEC		<area_change_flag
		LDY		!car_area
		DEY
		CPY		#0FFH
		BNE		Behind_001
		LDY		!area_number
Behind_001
		STY		!car_area
		DEY
		CPY		#0FFH
		BNE		Behind_003
		LDY		!area_number
Behind_003
		JSR		Change_point
		MEM16
		REP		#00100000B
		LDA		!Behind_x
		STA		!Foward_x
		LDA		!Behind_y
		STA		!Foward_y
		LDA		<mark_point_x
		STA		!Behind_x
		LDA		<mark_point_y
		STA		!Behind_y
;
		MEM8
		SEP		#00100000B
		LDA		!Behind_angle
		STA		!Foward_angle
		LDA		!Behind_status
		STA		!Foward_status
		LDA		<angle_data
		STA		!Behind_angle
		LDA		<status_data
		STA		!Behind_status
;------------------------------------------------------------------------
Car_rebound
		MEM8
		IDX8
		SEP		#00110000B
;
		LDA		!car_scrl_angle+1,X
		STA		<scrl_angle
		LDA		!car_angle+1,X
		STA		<pose_angle
		LDA		!Area_angle,X
		STA		<stand_angle
;
		LDA		#00100000B
		BIT		!car_BG_flag,X
		BMI		Emergency_area
		BVS		Iregular_rebound
		BNE		Regular_rebound
		RTS
;------------------------------------------------------------------------------
Emergency_area
		LDA		!point_angle,X
		STA		!car_scrl_angle+1,X
		STZ		!car_scrl_angle+0,X
;
		RTS
;--------------------------------------------------------------------------
Regular_rebound
		;
		LDA		<stand_angle
		CMP		#96
		BCC		Over_96
		SBC		#96
Over_96
		ASL		A
		SEC
		SBC		<scrl_angle
		BCS		Plus_angle
		ADC		#192
		BRA		Set_angle
Plus_angle
		CMP		#192
		BCC		Set_angle
		SBC		#192
Set_angle
		STA		<bound_angle
;
		JSR		Check_bound
		STA		<tempolary+2
;
		LDA		<scrl_angle
		JSR		Check_bound
;
		CMP		<tempolary+2
		BCC		Go_on_scrl
		LDA		<bound_angle
		BRA		Bound_set
Go_on_scrl
		JSR		Iregular_rebound
		STA		<bound_angle
Bound_set
		STA		!car_scrl_angle+1,X
		STZ		!car_scrl_angle+0,X
		RTS
;--------------------------------------------------------------------
;				Entry  A=bound or scrl angle
Check_bound
		SEC
		SBC		!point_angle,X
		BCC		Minus_bound
		CMP		#96
		BCC		Set_bound
		STA		<tempolary+0
		LDA		#192
		SBC		<tempolary+0
		RTS
Minus_bound
		EOR		#0FFH
		INC		A
		CMP		#060H		     ;minus #96(60H)
		BCC		Not_minus_over_96
		STA		<tempolary+0
		LDA		#192
		SBC		<tempolary+0
Not_minus_over_96
Set_bound
		RTS
;------------------------------------------------------------------------
Iregular_rebound
		LDA		!point_angle,X
		SEC
		SBC		<scrl_angle
		BCS		IRG_001
		EOR		#0FFH
		INC		A
IRG_001
		CMP		#96
		STZ		<tempolary+0
		BCC		IRG_002
		LDA		#96
		STA		<tempolary+0
IRG_002
		LDA		!point_angle,X
		LSR		A
		STA		<tempolary+2
		LDA		<scrl_angle
		LSR		A
		CLC
		ADC		<tempolary+2
		ADC		<tempolary+0
		CMP		#192
		BCC		IRG_003
		SBC		#192
IRG_003
		STA		!car_scrl_angle+1,X
		STZ		!car_scrl_angle+0,X
;
		RTS
;==========================================================================
Phase_check
		MEM16
		IDX8
		SEP		#00010000B
		STZ		<phase_pointer
		LDA		<mark_point_y
		SEC
		SBC		<view_point_y
		BNE		Dif_Y_not_EQU
;Dif_y_EQU
		LDA		<mark_point_x
		SEC
		SBC		<view_point_x
		BEQ		X_Y_EQU
		BCC		Dif_x_minus
;Dif_x_plus
		LDA		#1000010100000000B	;X>0 Y=0
		BRA		Set_phase_x
Dif_x_minus
		LDA		#1000011100000001B	;X<0 Y=0
Set_phase_x
		STA		<phase_pointer+00H
		BRA		Check_area_data
X_Y_EQU
		MEM8
		SEP		#00100000B
		BRA		Point_over
;------------------------------------------------------------------
		MEM16
Dif_Y_not_EQU
		BCS		Dif_y_plus
Dif_y_minus
		PHA
		LDA		#02H
		TSB		<phase_pointer
		PLA
		EOR		#0FFFFH
		INC		A
Dif_y_plus
		STA		<abs_dif_y
;-------------------------------------------------------
		LDA		<mark_point_x
		SEC
		SBC		<view_point_x
		BNE		Dif_x_not_EQU
		MEM8
		SEP		#00100000B
		LDA		<phase_pointer+00H
		ORA		#10000100B
		STA		<phase_pointer+01H
		BRA		Check_area_data
;-------------------------------------------------------
		MEM16
Dif_x_not_EQU
		BCS		Dif_x_plus
;Dif_x_minus
		PHA
		LDA		#0001H
		TSB		<phase_pointer
		PLA
		EOR		#0FFFFH
		INC		A
Dif_x_plus
		STA		<abs_dif_x
;---------------------------------------------------------
		SEC
		SBC		<abs_dif_y
		BNE		Not_EQU_XY
		MEM8
		SEP		#00100000B
		LDA		<phase_pointer+00H
		ORA		#10000000B
		STA		<phase_pointer+01H
		BRA		Check_area_data
		MEM16
Not_EQU_XY
		BCC		X_smaller_than_Y
X_bigger_than_Y
		STA		<abs_dif_xy
		LDA		<abs_dif_y
		STA		<smaller
		LDA		#0004H
		TSB		<phase_pointer
		BRA		Check_area_data
X_smaller_than_Y
		EOR		#0FFFFH
		INC		A
		STA		<abs_dif_xy
		LDA		<abs_dif_x
		STA		<smaller
Check_area_data
		MEM8
		IDX8
		SEP		#00110000B
		LDA		<phase_pointer+01H
		BNE		Position_on_line
Not_online_check
		LDY		<phase_pointer+00H
		LDA		Phase_check_data,Y
		LDY		<line_data
		AND		Area_check_data,Y
		BNE		Point_over
Not_point_over
		CLC
		RTS
Position_on_line
		AND		#01111111B
		TAY
		LDA		Line_check_data,Y
		LDY		<line_data
		AND		Phase_check_data,Y
		BEQ		Not_point_over
Point_over
		SEC
		RTS
;------------------------------------------------------------------------------------
Line_check_data
		BYTE		11111000B
		BYTE		11100011B
		BYTE		00111110B
		BYTE		10001111B
		BYTE		11110001B
		BYTE		01111100B
		BYTE		00011111B
		BYTE		11000111B
Phase_check_data
BIT_CHECK_DATA
		BYTE		00000001B
		BYTE		00000010B
		BYTE		00000100B
		BYTE		00001000B
		BYTE		00010000B
		BYTE		00100000B
		BYTE		01000000B
		BYTE		10000000B
Area_check_data
		BYTE		10101010B
		BYTE		10101100B
		BYTE		11001100B
		BYTE		01011100B
		BYTE		01010101B
		BYTE		01010011B
		BYTE		00110011B
		BYTE		10100011B
;---------------------------------------------------------------------------
Angle_CALC
		MEM8
		IDX8
		LDA		<phase_pointer+01H
		BEQ		Angle_not_on_line
		BIT		#00000100B
		BNE		On_axis
		AND		#00000011B
		TAY
		LDA		XY_axis_data,Y
		BRA		Set_calc_angle
Angle_not_on_line
		MEM16
		REP		#00100000B
		STZ		<angle_pointer
		LDA		<abs_dif_xy
		SEC
		SBC		<smaller
		BCS		Over_2_times
		LDA		#02H
		STA		<angle_pointer
		BRA		Set_angle_data
Over_2_times
		SEC
		SBC		<smaller
		BCS		Over_3_times
		LDA		#01H
		STA		<angle_pointer
		BRA		Set_angle_data
Over_3_times
		SEC
		SBC		<smaller
		BCS		Over_4_times
		LDA		#01H
		STA		<angle_pointer
Over_4_times
Set_angle_data
		MEM8
		IDX8
		SEP		#00110000B
		LDA		<phase_pointer
		ASL		A
		CLC
		ADC		<phase_pointer	   ;phase * 3
		ADC		<angle_pointer
		STA		<phase_pointer
		TAY
		LDA		Angle_data,Y
		BRA		Set_calc_angle
;-----------------------------------------------------------------------
On_axis
		MEM8
		IDX8
		AND		#00000011B
		TAY
		LDA		Axis_data,Y
Set_calc_angle
		STA		<calc_angle
		RTS
;-------------------------------------------------------------------------
Axis_data
		BYTE		096,048,000,144
XY_axis_data
		BYTE		072,120,024,168
;-----------------------------------------------------------------------
Angle_data
		BYTE		093,086,077
		BYTE		100,107,115
		BYTE		004,011,019
		BYTE		189,182,173
		BYTE		052,059,067
		BYTE		141,134,125
		BYTE		045,038,029
		BYTE		148,155,163
;-----------------------------------------------------------------------------
		MEM8
		IDX8
Goal_in_rank
		PHP
		SEP		#00110000B
		LDA		!car_area+00H
		PHA
		STZ		!car_area+00H
		JSR		Check_rank
		PLA
		STA		!car_area+00H
		PLP
		RTS
;--------------------------------------------------------------------------
Check_rank
		MEM8
		IDX8
		SEP		#00110000B
		LDX		!car_ranking+0
		BNE		Not_mycar_001
		LDA		!round_counter+00H
		BRA		Not_mycar_002
Not_mycar_001
		LDA		!round_counter2,X
Not_mycar_002
		STA		!rank_priority+1
		LDA		!car_area,X
		STA		!rank_priority+0
;
		LDX		#02H
		STX		<rank_counter
Check_rank_loop
		MEM8
		SEP		#00110000B
		LDA		!car_ranking,X
		TAY
		BNE		Not_mycar_003
		LDA		!round_counter+00H
		BRA		Not_mycar_004
Not_mycar_003
		LDA		!round_counter2,Y
Not_mycar_004
		STA		!rank_priority+1,X
		LDA		!car_area,Y
		STA		!rank_priority+0,X
Check_rank_priority
		MEM16
		REP		#00100000B
		LDA		!rank_priority+0,X
		SEC
		SBC		!rank_priority-2,X
		BEQ		No_change_rank	       ;********KARI*******
		BCC		Ans_minus
		CMP		#8000H
		BCS		No_change_rank
		BRA		Change_rank
;------------------------------------------------------------------------------
		MEM16
Ans_minus
		CMP		#8000H
		BCS		No_change_rank
Change_rank
		LDA		!rank_priority+0,X
		PHA
		LDA		!rank_priority-2,X
		STA		!rank_priority+0,X
		PLA
		STA		!rank_priority-2,X
;
		LDA		!car_ranking+0,X
		PHA
		LDA		!car_ranking-2,X
		STA		!car_ranking+0,X
		PLA
		STA		!car_ranking-2,X
		DEX
		DEX
		BNE		Check_rank_priority
No_change_rank
		MEM16
		REP		#00100000B
		LDX		<rank_counter
		INX
		INX
		STX		<rank_counter
		CPX		#0AH
		BNE		Check_rank_loop
;
		MEM8
		SEP		#00100000B
		LDX		#00H
Outrun_loop
		LDA		!car_ranking,X
		BEQ		Mycar_ranking
		TAY
		LDA		!car_flag+1,Y
		AND		#11111110B
		STA		!car_flag+1,Y
		INX
		INX
		BRA		Outrun_loop
;----------------------------------------------------------------------------------
Mycar_ranking
		TXA
		STA		!rank_index
		LSR		A
		INC		A
		STA		!calc_rank
Next_outrun
		INX
		INX
		CPX		#0AH
		BEQ		Outrun_RTS
		LDA		!car_ranking,X
		TAY
		LDA		!car_flag+1,Y
		ORA		#00000001B
		STA		!car_flag+1,Y
		BRA		Next_outrun
Outrun_RTS
		LDA		!control_status+00H
		BPL		Mycar_rank_comp
		LDA		!car_speed+01H
		CMP		#03H
		BCS		Mycar_no_change
Mycar_rank_comp
		LDA		!car_stack
		CLC
		ADC		!calc_rank
		CMP		!mycar_rank
		BEQ		Mycar_no_change
		STA		!mycar_rank
		DEC		!mycar_rank_flag
Mycar_no_change
No_marker
		RTS
;----------------------------------------------------------------------
		MEM8
		IDX8
Set_marker
		SEP		#00110000B
		LDY		!repair_counter
		BNE		No_marker
		LDY		!exception_flag
		BNE		No_marker
;--------------------------------------------------------------------------
On_marker
		LDA		!mycar_rank
		STA		<myrank_check
		LDY		!rank_index
Next_check_front
		DEY
		DEY
		BMI		No_front_marker
;
		DEC		<myrank_check
		LDA		#03H
		CMP		<myrank_check
		BCC		Next_check_front
;
		LDA		!car_ranking,Y
		TAX
		LDA		!car_flag+00H,X
		BIT		#00001000B
		BEQ		Next_check_front
		BIT		#00000100B
		BNE		Next_check_front
		LDA		!car_size,X
		CMP		#08H
		BCS		Next_check_front
		CMP		#05H
		BCC		Next_check_front
		JMP		Set_rank_marker
No_front_marker
		LDA		!040DH
		AND		#11110000B
		ORA		#00000101B
		STA		!040DH
		LDA		#080H
		STA		!02D0H
		STA		!02D4H
;--------------------------------------------------------------------------
Check_back_marker
		MEM8
		LDX		!rank_index
		INX
		INX
		LDA		!car_ranking+00H,X
		TAX
		CPX		!set_pointa
		BEQ		No_back_marker
		LDA		!car_flag+00H,X
		BPL		No_back_marker
		BIT		#00001000B
		BNE		No_back_marker
		LDA		!car_flag+01H,X
		BIT		#00000010B
		BNE		No_back_marker
		MEM16
		REP		#00100000B
		LDA		!car_distance,X
		CMP		!distance_check+00H
		BCS		No_back_marker
;
		LDA		!rotate_x,X
		ASL		A
		ASL		A
		CLC
		ADC		#080H
		BMI		Over_right
		CMP		#100H
		BCC		Set_back_pos
Over_left
		LDA		#0F0H
		BRA		Set_back_pos
Over_right
		LDA		#010H
		BRA		Set_back_pos
;----------------------------------------------------------------------------
No_back_marker
		MEM8
		SEP		#00100000B
		LDA		!040DH
		AND		#00001111B
		ORA		#01010000B
		STA		!040DH
		LDA		#080H
		STA		!02D8H
		STA		!02DCH
		RTS
;---------------------------------------------------------------------------
Set_move_speed
		ASL		A
		ASL		A
		ASL		A
		STA		<tempolary+02H
		PHX
		LDA		!car_distance,X
		LDX		#04H
SMS_loop
		CMP		!Check_dist_data,X
		BCS		SMS_RTS
		ASL		<tempolary+02H
		DEX
		DEX
		BNE		SMS_loop
SMS_RTS
		TXA
		LSR		A
		TAY
		PLX
		RTS
Check_dist_data
		WORD		010H,040H,090H
;---------------------------------------------------------------------------
Set_back_pos
		MEM16
		XBA
		AND		#0FF00H
		STA		<tempolary
		SEC
		SBC		!back_pos_x+00H
		XBA
		BCS		Move_pos_plus
		ORA		#0FF00H
		BRA		Next_back_pos
Move_pos_plus
		AND		#000FFH
Next_back_pos
		JSR		Set_move_speed
		CPX		!back_index
		BEQ		Cont_back_car
		LDA		<tempolary+00H
		BRA		Set_new_index
Cont_back_car
		LDA		<tempolary+02H
		CLC
		ADC		!back_pos_x+00H
Set_new_index
		STA		!back_pos_x+00H
		STX		!back_index
;----------------------------------------------------------------------------
Set_back_marker
		MEM8
		SEP		#00110000B
		LDA		Frash_data,Y
		BIT		<frame_counter
		BNE		Orange_mark
White_mark
		LDA		#00110001B	;white No.0 palet
		BRA		Set_attr_mark
Orange_mark
		LDA		#00110111B	;orange No.3 palet
Set_attr_mark
		STA		!02DBH
		STA		!02DFH
On_back_marker
		LDA		!back_pos_x+01H
		CMP		#0F0H
		BCC		Not_over_backpos
		LDA		#0F0H
		BRA		Reset_backpos
Not_over_backpos
		CMP		#010H
		BCS		Nomal_backpos
		LDA		#010H
Reset_backpos
		STA		!back_pos_x+01H
Nomal_backpos
		STA		!02D8H
		SEC
		SBC		#010H
		STA		!02DCH
		LDA		#0C2H
		STA		!02D9H
		STA		!02DDH
		LDA		#0CCH
		STA		!02DAH
		LDA		#0CAH
		STA		!02DEH
;Sub
		LDA		!040DH
		AND		#00001111B
		ORA		#10100000B
		STA		!040DH
		RTS
;--------------------------------------------------------------------------
Set_rank_marker
		TAY
		MEM16
		REP		#00100000B
		STZ		<tempolary
		LDA		!car_position_h+0,X
		STA		!02D0H
		CMP		#100H
		BCC		SRM_001
		ROL		<tempolary
SRM_001
		CLC
		ADC		#0008H
		STA		!02D4H
		CMP		#100H
		BCC		SRM_002
		LDA		#0004H
		ORA		<tempolary
		STA		<tempolary
SRM_002
		MEM8
		SEP		#00100000B
		LDA		!Mark_v_data-05H,Y
		CLC
		ADC		!car_display_v,X
		STA		!02D1H
		STA		!02D5H
		LDA		#0CEH
		STA		!02D2H
		LDY		<myrank_check
		LDA		Mark_ch_data-01H,Y
		STA		!02D6H
		LDA		#00110001B
		STA		!02D3H
		STA		!02D7H
;Sub
		LDA		!040DH
		AND		#11110000B
		ORA		<tempolary
		STA		!040DH
		JMP		Check_back_marker
;-------------------------------------------------------------------------------
Frash_data
		BYTE		00000010B,00000100B,00001000B
;------------------------------------------------------------------------------
Mark_v_data
		BYTE		0EEH,0F0H,0F2H
Mark_ch_data
		BYTE		0CFH,0DEH,0DFH
		end
