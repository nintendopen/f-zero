
Super Famicom/SNES game: F-ZERO
========================

![](https://i.imgur.com/8F6ndW0.png)

## TODO list

- Acertain that the commit history reconstruction is ok
- Translate all japanese comments/text files
- Find a way to actually build the game, and write a guide on how to do so in this README
- Document the filetree structure in this README

---

## Filepaths

These are the original filepaths of the data:
- other.7z/SFC.7z/ソースデータ/FZERO/
- other.7z/NEWS.7z/テープリストア/NEWS_05/home/kimura/fzero/

---

## Build system

TODO

---

## FileTree structure

```
.
├── _kimura
│   ├── 3d_check
│   ├── BUFFER
│   ├── calculate.asm
│   ├── calculate_usa.asm
│   ├── car_control.asm
│   ├── control_data.asm
│   ├── dat-6.asm
│   ├── DATA_4
│   ├── DATA_5
│   ├── data-4.asm
│   ├── data-4.osf
│   ├── data-4.pre
│   ├── data-4.rel
│   ├── data-5.asm
│   ├── data-5.rel
│   ├── data-6.asm
│   ├── data-6.rel
│   ├── effect.asm
│   ├── EN_CHECK
│   ├── EN_DATA1
│   ├── EN_DATA2
│   ├── EN_DRIVE
│   ├── EN_INIT1
│   ├── EN_INIT3
│   ├── EN_INIT4
│   ├── en-check.asm
│   ├── en-check.rel
│   ├── en-data-1.asm
│   ├── en-data-1.rel
│   ├── en-data-2.asm
│   ├── en-data-2.rel
│   ├── en-drive.asm
│   ├── en-drive.rel
│   ├── en-init-1.asm
│   ├── en-init-1.rel
│   ├── en-init-3.asm
│   ├── en-init-3.rel
│   ├── en-init-4.asm
│   ├── en-init-4.rel
│   ├── fz
│   ├── fz.rom_0
│   ├── fz.rom_1
│   ├── fz.rom_10
│   ├── fz.rom_11
│   ├── fz.rom_12
│   ├── fz.rom_13
│   ├── fz.rom_14
│   ├── fz.rom_15
│   ├── fz.rom_2
│   ├── fz.rom_3
│   ├── fz.rom_4
│   ├── fz.rom_5
│   ├── fz.rom_6
│   ├── fz.rom_7
│   ├── fz.rom_8
│   ├── fz.rom_9
│   ├── fz.rom0
│   ├── fz.rom1
│   ├── fz_0
│   ├── fz_1
│   ├── fz_10
│   ├── fz_11
│   ├── fz_12
│   ├── fz_13
│   ├── fz_14
│   ├── fz_15
│   ├── fz_2
│   ├── fz_3
│   ├── fz_4
│   ├── fz_5
│   ├── fz_6
│   ├── fz_7
│   ├── fz_8
│   ├── fz_9
│   ├── f-zero.exe
│   ├── f-zero.map
│   ├── fzero.rom_10
│   ├── fzero.rom_11
│   ├── fzero.rom_12
│   ├── fzero.rom_13
│   ├── fzero.rom_14
│   ├── fzero.rom_15
│   ├── fzero.rom_2
│   ├── fzero.rom_3
│   ├── fzero.rom_4
│   ├── fzero.rom_5
│   ├── fzero.rom_6
│   ├── fzero.rom_7
│   ├── fzero.rom_8
│   ├── fzero.rom_9
│   ├── fzero_main.asm
│   ├── fzero_main_pal.asm
│   ├── fzero_main_usa2.asm
│   ├── f-zero-icon.cbm
│   ├── fz-man
│   ├── game_over.asm
│   ├── game_over_usa.asm
│   ├── hp2news
│   ├── nishida
│   ├── play_main.asm
│   ├── play_main_pal.asm
│   ├── player.asm
│   ├── RP5A22_
│   ├── RP5C77_
│   ├── set_back.asm
│   ├── set_bg1.asm
│   ├── set_bg2.asm
│   ├── set_bg2_usa.asm
│   ├── SET_OBJ
│   ├── set-obj.asm
│   ├── set-obj.rel
│   ├── sound.asm
│   ├── title_main.asm
│   ├── title_main_usa.asm
│   ├── VARIABLE
│   ├── WORK
│   └── work.asm
├── Game
│   ├── ALPHA
│   ├── ALPHA2
│   ├── ANK
│   ├── ANK8
│   ├── BUFFER
│   ├── calculate.asm
│   ├── calculate_usa.asm
│   ├── car_control.asm
│   ├── control_data.asm
│   ├── data-5.asm
│   ├── data-6.asm
│   ├── effect.asm
│   ├── en-check.asm
│   ├── en-data-1.asm
│   ├── en-data-2.asm
│   ├── en-drive.asm
│   ├── en-init-1.asm
│   ├── en-init-3.asm
│   ├── en-init-4.asm
│   ├── fzero_main.asm
│   ├── fzero_main_pal.asm
│   ├── fzero_main_usa2.asm
│   ├── fzero_pal.make
│   ├── game_over.asm
│   ├── game_over_usa.asm
│   ├── makefile
│   ├── play_main.asm
│   ├── play_main_pal.asm
│   ├── player.asm
│   ├── RP5A22_
│   ├── RP5C77_
│   ├── set_back
│   ├── set_back.asm
│   ├── set_bg1.asm
│   ├── set_bg2.asm
│   ├── set_bg2_usa.asm
│   ├── set-obj.asm
│   ├── sound.asm
│   ├── TITLE.DOC
│   ├── title_main.asm
│   ├── title_main_usa.asm
│   ├── VARIABLE
│   └── WORK
├── README.md
└── Tools
    ├── armap.c
    ├── carmap.c
    ├── chrar.c
    ├── cmpbuf.s
    ├── cmppnl.s
    ├── enemy.c
    ├── mkback.c
    ├── mkbgchr.c
    ├── mkenemy.c
    ├── mkmap.c
    ├── mkpanel.c
    ├── mkpers.c
    ├── mkrival.c
    ├── mkselect.c
    ├── mkspchr.c
    ├── schpnl.s
    ├── setbomb.c
    └── slitpress.c

4 directories, 178 files
```
